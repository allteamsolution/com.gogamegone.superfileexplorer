package org.vocrama.analytics;

public class AnalyticsEvent {

    static final String CCL_EVENT_NON_GP_INSTALLER_SHOW_BANNER = "banner_loading_non_gp_user";
    static final String CCL_EVENT_GP_INSTALLER_SHOW_BANNER = "banner_loading_gp_user";

    public static String TRIAL_OFFER_SCREEN_OPEN_0 = "trial_offer_screen_open_0";
    public static String TRIAL_OFFER_SCREEN_CLOSE_0 = "trial_offer_screen_close_0";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_0 = "trial_offer_screen_clickbuy_0";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_0 = "trial_offer_screen_success_buy_0";


    public static String TRIAL_OFFER_SCREEN_OPEN_4 = "trial_offer_screen_open_4";
    public static String TRIAL_OFFER_SCREEN_CLOSE_4 = "trial_offer_screen_close_4";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_4 = "trial_offer_screen_clickbuy_4";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_4 = "trial_offer_screen_success_buy_4";


    public static String TRIAL_OFFER_SCREEN_OPEN_5 = "trial_offer_screen_open_5";
    public static String TRIAL_OFFER_SCREEN_CLOSE_5 = "trial_offer_screen_close_5";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_5 = "trial_offer_screen_clickbuy_5";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_5 = "trial_offer_screen_success_buy_5";

    public static String TRIAL_OFFER_SCREEN_OPEN_6 = "trial_offer_screen_open_6";
    public static String TRIAL_OFFER_SCREEN_CLOSE_6 = "trial_offer_screen_close_6";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_6 = "trial_offer_screen_clickbuy_6";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_6 = "trial_offer_screen_success_buy_6";

    public static String TRIAL_OFFER_SCREEN_OPEN_7 = "trial_offer_screen_open_7";
    public static String TRIAL_OFFER_SCREEN_CLOSE_7 = "trial_offer_screen_close_7";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_7 = "trial_offer_screen_clickbuy_7";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_7 = "trial_offer_screen_success_buy_7";

    public static String TRIAL_OFFER_SCREEN_OPEN_1 = "trial_offer_screen_open_1";
    public static String TRIAL_OFFER_SCREEN_CLOSE_1 = "trial_offer_screen_close_1";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_1 = "trial_offer_screen_clickbuy_1";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_1 = "trial_offer_screen_success_buy_1";

    public static String OFFER_SCREEN_OPEN_0 = "offer_show_0";
    public static String OFFER_SCREEN_CLOSE_0 = "offer_close_0";
    public static String OFFER_SCREEN_CLICKBUY_0 = "offer_clickbuy_0";
    public static String OFFER_SCREEN_SUCCESS_BUY_0 = "offer_successbuy_0";

    public static String PURCHASE_SCREEN_OPEN_0 = "purchase_screen_open_0";
    public static String PURCHASE_SCREEN_CLOSE_0 = "purchase_screen_close_0";
    public static String PURCHASE_SCREEN_CLICKBUY_0 = "purchase_screen_clickbuy_0";
    public static String PURCHASE_SCREEN_SUCCESS_BUY_0_WEEK = "purchase_buy_week_0";
    public static String PURCHASE_SCREEN_SUCCESS_BUY_0_MONTH = "purchase_buy_month_0";
    public static String PURCHASE_SCREEN_SUCCESS_BUY_0_YEAR = "purchase_buy_year_0";


    public static String PURCHASE_SCREEN_OPEN_2 = "purchase_screen_open_2";
    public static String PURCHASE_SCREEN_CLOSE_2 = "purchase_screen_close_2";
    public static String PURCHASE_SCREEN_CLICKBUY_2 = "purchase_screen_clickbuy_2";
    public static String PURCHASE_SCREEN_SUCCESS_BUY_2_UNLIM = "purchase_buy_unlim_2";
    public static String PURCHASE_SCREEN_SUCCESS_BUY_2_MONTH = "purchase_buy_month_2";
    public static String PURCHASE_SCREEN_SUCCESS_BUY_2_YEAR = "purchase_buy_year_2";



    public static String TRIAL_OFFER_SCREEN_OPEN_2 = "trial_offer_screen_open_8";
    public static String TRIAL_OFFER_SCREEN_CLOSE_2 = "trial_offer_screen_close_8";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_2 = "trial_offer_screen_clickbuy_8";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_2 = "trial_offer_screen_success_buy_8";

    public static String TRIAL_OFFER_SCREEN_OPEN_8 = "trial_offer_screen_open_8";
    public static String TRIAL_OFFER_SCREEN_CLOSE_8 = "trial_offer_screen_close_8";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_8 = "trial_offer_screen_clickbuy_8";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_8 = "trial_offer_screen_success_buy_8";

    public static String TRIAL_OFFER_SCREEN_OPEN_9 = "trial_offer_screen_open_9";
    public static String TRIAL_OFFER_SCREEN_CLOSE_9 = "trial_offer_screen_close_9";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_9 = "trial_offer_screen_clickbuy_9";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_9 = "trial_offer_screen_success_buy_9";

    public static String TRIAL_OFFER_SCREEN_OPEN_10 = "trial_offer_screen_open_10";
    public static String TRIAL_OFFER_SCREEN_CLOSE_10 = "trial_offer_screen_close_10";
    public static String TRIAL_OFFER_SCREEN_CLICKBUY_10 = "trial_offer_screen_clickbuy_10";
    public static String TRIAL_OFFER_SCREEN_SUCCESS_BUY_10 = "trial_offer_screen_success_buy_10";

}