package org.vocrama;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.Purchase;
import com.gen.rxbilling.client.RxBillingImpl;
import com.gen.rxbilling.connection.BillingClientFactory;
import com.gen.rxbilling.connection.BillingServiceFactory;
import com.gen.rxbilling.connection.RepeatConnectionTransformer;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.billing.BillingHelper;
import org.vocrama.billing.BillingHistory;
import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.ui.StartActivity;
import org.vocrama.tutorial.TutorialActivity;

import java.util.List;

public class RootActivity extends AppCompatActivity implements BillingHistory.BillingHistoryView {

    private BillingHistory billingHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        billingHistory = new BillingHistory(
                this, new RxBillingImpl(
                new BillingClientFactory(
                        this.getApplicationContext(),
                        new RepeatConnectionTransformer<>()
                )
        ),
                new RxBillingFlow(
                        getApplicationContext(),
                        new BillingServiceFactory(
                                this,
                                new RepeatConnectionTransformer<>()
                        )
                )
        );
        billingHistory.onCreate();
        getBillingHistory();

        if (App.getCurrentUser().isTutorialFinished()) {
            startActivity(new Intent(this, StartActivity.class));
        } else {
            startActivity(new Intent(this, TutorialActivity.class));
        }
        finish();
    }

    @Override
    public void onGetHistorySubscribe(List<Purchase> purchases) {
        App.getCurrentUser().setStartBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_WEEK, purchases));
        App.getCurrentUser().setBasicBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_MONTH, purchases));
        App.getCurrentUser().setSuperBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_YEAR, purchases));
        App.getCurrentUser().setOfferBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_OFFER, purchases));
        App.getCurrentUser().setSuperTrialBuy(BillingHelper.isActive(BillingHelper.SUBSCRIBE_YEAR_TRIAL, purchases));
        App.getCurrentUser().save();
    }

    @Override
    public void onGetHistoryPurchase(List<Purchase> purchases) {

    }


    @Override
    public void onErrorBilling(Throwable throwable) {

    }

    private void getBillingHistory() {
        billingHistory.getHistoryPurchase();
        billingHistory.getHistorySubscribe();
    }
}