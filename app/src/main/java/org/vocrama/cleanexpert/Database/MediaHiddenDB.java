package org.vocrama.cleanexpert.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

public class MediaHiddenDB extends SQLiteOpenHelper {
    private static final String FILENAME = "filename";
    private static final String FILEPATH = "filepath";
    private static final String database_NAME = "MediaDB";
    private static final int database_VERSION = 1;
    private static final String table_audio = "Audio";
    private static final String table_document = "Document";
    private static final String table_images = "Images";
    private static final String table_videos = "Videos";
    private SQLiteDatabase db;
    private SQLiteDatabase sql;

    public MediaHiddenDB(Context context) {
        super(context, database_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE  IF NOT EXISTS Images ( id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT, filepath TEXT )");
        db.execSQL("CREATE TABLE  IF NOT EXISTS Audio ( id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT, filepath TEXT )");
        db.execSQL("CREATE TABLE  IF NOT EXISTS Document ( id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT, filepath TEXT )");
        db.execSQL("CREATE TABLE  IF NOT EXISTS Videos ( id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT, filepath TEXT )");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void open() {
        this.db = getWritableDatabase();
        this.sql = getReadableDatabase();
    }

    public void close() {
        this.db.close();
        this.sql.close();
    }

    public void insertImage(String name, String path) {
        ContentValues values = new ContentValues();
        values.put(FILENAME, name);
        values.put(FILEPATH, path);
        this.db.insert(table_images, null, values);
    }

    public String originalPath(String name) {
        String originalPath = "";
        Cursor c = this.sql.query(table_images, new String[]{FILEPATH}, "filename=?", new String[]{name}, null, null, null, null);
        if (c.moveToFirst()) {
            originalPath = c.getString(c.getColumnIndex(FILEPATH));
        }
        c.close();
        return originalPath;
    }

    public ArrayList<String> getToTleImages() {
        ArrayList<String> list = new ArrayList();
        String originalPath = "";
        Cursor c = this.sql.query(table_images, null, null, null, null, null, null);
        c.moveToFirst();
        do {
            try {
                list.add(c.getString(c.getColumnIndex(FILEPATH)));
            } catch (Exception e) {
            }
        } while (c.moveToNext());
        c.close();
        return list;
    }

    public void deletePath(String name) {
        this.db.delete(table_images, "filename = ?", new String[]{name});
    }

    public void insertVideo(String name, String path) {
        ContentValues values = new ContentValues();
        values.put(FILENAME, name);
        values.put(FILEPATH, path);
        this.db.insert(table_videos, null, values);
    }

    public String originalPathVideo(String name) {
        String originalPath = "";
        Cursor c = this.sql.query(table_videos, new String[]{FILEPATH}, "filename=?", new String[]{name}, null, null, null, null);
        if (c.moveToFirst()) {
            originalPath = c.getString(c.getColumnIndex(FILEPATH));
        }
        c.close();
        return originalPath;
    }

    public void deletePathVideo(String name) {
        this.db.delete(table_videos, "filename = ?", new String[]{name});
    }

    public void insertAudio(String name, String path) {
        ContentValues values = new ContentValues();
        values.put(FILENAME, name);
        values.put(FILEPATH, path);
        this.db.insert(table_audio, null, values);
    }

    public String originalPathAudio(String name) {
        String originalPath = "";
        Cursor c = this.sql.query(table_audio, new String[]{FILEPATH}, "filename=?", new String[]{name}, null, null, null, null);
        if (c.moveToFirst()) {
            originalPath = c.getString(c.getColumnIndex(FILEPATH));
        }
        c.close();
        return originalPath;
    }

    public void deletePathAudio(String name) {
        this.db.delete(table_audio, "filename = ?", new String[]{name});
    }

    public void insertDocument(String name, String path) {
        ContentValues values = new ContentValues();
        values.put(FILENAME, name);
        values.put(FILEPATH, path);
        this.db.insert(table_document, null, values);
    }

    public String originalPathDocument(String name) {
        String originalPath = "";
        Cursor c = this.sql.query(table_document, new String[]{FILEPATH}, "filename=?", new String[]{name}, null, null, null, null);
        if (c.moveToFirst()) {
            originalPath = c.getString(c.getColumnIndex(FILEPATH));
        }
        c.close();
        return originalPath;
    }

    public void deletePathDocument(String name) {
        this.db.delete(table_document, "filename = ?", new String[]{name});
    }
}
