package org.vocrama.cleanexpert.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MediaRecentDB extends SQLiteOpenHelper {
    private static final String DBNAME = "RecentDB";
    private static final String FILEID = "fileid";
    private static final String FILENAME = "filename";
    private static final String FILEPATH = "filepath";
    private static final String FILTYPE = "filetype";
    private static final String RecentTABEL = "Recent";
    private SQLiteDatabase db;
    private SQLiteDatabase sql;

    public MediaRecentDB(Context context) {
        super(context, DBNAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE  IF NOT EXISTS Recent ( id INTEGER PRIMARY KEY AUTOINCREMENT, fileid TEXT,filename TEXT, filepath TEXT, filetype TEXT )");
    }

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    public void open() {
        this.db = getWritableDatabase();
        this.sql = getReadableDatabase();
    }

    public void close() {
        this.db.close();
        this.sql.close();
    }

    public void insertRecent(String id, String name, String path, String type) {
        if (!checkRecentMedia(id).booleanValue()) {
            ContentValues values = new ContentValues();
            values.put(FILEID, id);
            values.put(FILENAME, name);
            values.put(FILEPATH, path);
            values.put(FILTYPE, type);
            this.db.insert(RecentTABEL, null, values);
        }
    }

    public Cursor getRecentMedia() {
        Cursor mCursor = null;
        try {
            return this.db.query(RecentTABEL, null, null, null, null, null, "id DESC");
        } catch (Exception e) {
            e.printStackTrace();
            return mCursor;
        }
    }

    public Boolean deleteRecentMedia(String id) {
        return Boolean.valueOf(this.db.delete(RecentTABEL, new StringBuilder().append(" fileid  = ").append(id).toString(), null) > 0);
    }

    private Boolean checkRecentMedia(String id) {
        try {
            return Boolean.valueOf(this.db.rawQuery("SELECT * FROM Recent WHERE fileid = " + id, null).moveToFirst());
        } catch (Exception e) {
            return Boolean.valueOf(false);
        }
    }
}
