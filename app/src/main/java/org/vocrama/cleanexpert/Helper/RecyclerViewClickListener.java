package org.vocrama.cleanexpert.Helper;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int i);

    void onLongClick(View view, int i);
}
