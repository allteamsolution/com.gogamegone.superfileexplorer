package org.vocrama.cleanexpert.Helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class ItemDividerDecoration extends RecyclerView.ItemDecoration {
    public static final int HORIZONTAL_LIST = 0;
    public static final int VERTICAL_LIST = 1;
    private static final int[] ATTRS = new int[]{16843284};
    private Drawable divider;
    private int orientation;

    public ItemDividerDecoration(Context context, int orientation) {
        TypedArray a = context.obtainStyledAttributes(ATTRS);
        this.divider = a.getDrawable(0);
        a.recycle();
        setOrientation(orientation);
    }

    public void drawVertical(Canvas c, RecyclerView parent) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            int top = child.getBottom() + ((RecyclerView.LayoutParams) child.getLayoutParams()).bottomMargin;
            this.divider.setBounds(left, top, right, top + this.divider.getIntrinsicHeight());
            this.divider.draw(c);
        }
    }

    public void drawHorizontal(Canvas c, RecyclerView parent) {
        int top = parent.getPaddingTop();
        int bottom = parent.getHeight() - parent.getPaddingBottom();
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            int left = child.getRight() + ((RecyclerView.LayoutParams) child.getLayoutParams()).rightMargin;
            this.divider.setBounds(left, top, left + this.divider.getIntrinsicHeight(), bottom);
            this.divider.draw(c);
        }
    }

    public void setOrientation(int orientation) {
        if (orientation == 0 || orientation == 1) {
            this.orientation = orientation;
            return;
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (this.orientation == 1) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (this.orientation == 1) {
            outRect.set(0, 0, 0, this.divider.getIntrinsicHeight());
        } else {
            outRect.set(0, 0, this.divider.getIntrinsicWidth(), 0);
        }
    }
}
