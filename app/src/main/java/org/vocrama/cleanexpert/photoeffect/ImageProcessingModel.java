package org.vocrama.cleanexpert.photoeffect;

import android.graphics.Bitmap;

public class ImageProcessingModel {
    public int process;
    public String processName;
    public Bitmap processedBitmap;

    public ImageProcessingModel(int process, String processName, Bitmap processedBitmap) {
        this.process = process;
        this.processName = processName;
        this.processedBitmap = processedBitmap;
    }

    public int getProcess() {
        return this.process;
    }

    public void setProcess(int process) {
        this.process = process;
    }

    public String getProcessName() {
        return this.processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Bitmap getProcessedBitmap() {
        return this.processedBitmap;
    }

    public void setProcessedBitmap(Bitmap processedBitmap) {
        this.processedBitmap = processedBitmap;
    }
}
