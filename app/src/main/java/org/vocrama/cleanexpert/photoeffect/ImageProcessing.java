package org.vocrama.cleanexpert.photoeffect;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import androidx.core.view.ViewCompat;
import java.util.ArrayList;
import java.util.Random;

public class ImageProcessing {
    public static final String BLACK = "Black";
    public static final int BOOST_BLUE = 3;
    public static final int BOOST_GREEN = 2;
    public static final int BOOST_RED = 1;
    public static final String BRIGHTNESS = "Brightness";
    public static final String COLOR_DEPTH = "Color Depth";
    private static final int COLOR_MAX = 255;
    private static final int COLOR_MIN = 0;
    public static final String CONTRAST = "Contrast";
    public static final String FLIP = "Flip";
    public static final int FLIP_HORIZONTAL = 2;
    public static final int FLIP_VERTICAL = 1;
    public static final String GAMMA = "Gamma";
    public static final String GREY = "Grey";
    public static final String INVERT = "Invert";
    public static final String NORMAL = "Normal";
    public static final int PROCESSING_BLACK = 24;
    public static final int PROCESSING_BRIGHTNESS = 10;
    public static final int PROCESSING_COLOR_DEPTH = 7;
    public static final int PROCESSING_CONTRAST = 8;
    public static final int PROCESSING_FLIP = 20;
    public static final int PROCESSING_GAMMA = 4;
    public static final int PROCESSING_GREY = 3;
    public static final int PROCESSING_INVERT = 2;
    public static final int PROCESSING_NORMAL = 0;
    public static final int PROCESSING_REFLECTION = 29;
    public static final int PROCESSING_ROUND = 18;
    public static final int PROCESSING_SATURATION = 27;
    public static final int PROCESSING_SHADDING = 26;
    public static final String REFLECTION = "Reflection";
    public static final String ROUND = "Round";
    public static final String SATURATION = "Saturation";
    public static final String SHADDING = "Shadding";
    public static ArrayList<Integer> effectList = new ArrayList();
    public static ArrayList<ImageProcessingModel> imageProcessedList = new ArrayList();

    static {
        effectList.add(Integer.valueOf(0));
        effectList.add(Integer.valueOf(2));
        effectList.add(Integer.valueOf(3));
        effectList.add(Integer.valueOf(4));
        effectList.add(Integer.valueOf(7));
        effectList.add(Integer.valueOf(8));
        effectList.add(Integer.valueOf(10));
        effectList.add(Integer.valueOf(18));
        effectList.add(Integer.valueOf(20));
        effectList.add(Integer.valueOf(24));
        effectList.add(Integer.valueOf(26));
        effectList.add(Integer.valueOf(27));
        effectList.add(Integer.valueOf(29));
        imageProcessedList.add(new ImageProcessingModel(0, NORMAL, null));
        imageProcessedList.add(new ImageProcessingModel(2, INVERT, null));
        imageProcessedList.add(new ImageProcessingModel(3, GREY, null));
        imageProcessedList.add(new ImageProcessingModel(4, GAMMA, null));
        imageProcessedList.add(new ImageProcessingModel(7, COLOR_DEPTH, null));
        imageProcessedList.add(new ImageProcessingModel(8, CONTRAST, null));
        imageProcessedList.add(new ImageProcessingModel(10, BRIGHTNESS, null));
        imageProcessedList.add(new ImageProcessingModel(18, ROUND, null));
        imageProcessedList.add(new ImageProcessingModel(20, FLIP, null));
        imageProcessedList.add(new ImageProcessingModel(24, BLACK, null));
        imageProcessedList.add(new ImageProcessingModel(26, SHADDING, null));
        imageProcessedList.add(new ImageProcessingModel(27, SATURATION, null));
        imageProcessedList.add(new ImageProcessingModel(29, REFLECTION, null));
    }

    public static Bitmap applyNormalImage(Bitmap bm) {
        return bm;
    }

    public static Bitmap applyInvert(Bitmap src) {
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        int height = src.getHeight();
        int width = src.getWidth();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int pixelColor = src.getPixel(x, y);
                bmOut.setPixel(x, y, Color.argb(Color.alpha(pixelColor), 255 - Color.red(pixelColor), 255 - Color.green(pixelColor), 255 - Color.blue(pixelColor)));
            }
        }
        return bmOut;
    }

    public static Bitmap applyGreyscale(Bitmap src) {
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        int width = src.getWidth();
        int height = src.getHeight();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = src.getPixel(x, y);
                int A = Color.alpha(pixel);
                int B = (int) (((0.299d * ((double) Color.red(pixel))) + (0.587d * ((double) Color.green(pixel)))) + (0.114d * ((double) Color.blue(pixel))));
                bmOut.setPixel(x, y, Color.argb(A, B, B, B));
            }
        }
        return bmOut;
    }

    public static Bitmap applyGamma(Bitmap src, double red, double green, double blue) {
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        int width = src.getWidth();
        int height = src.getHeight();
        int[] gammaR = new int[256];
        int[] gammaG = new int[256];
        int[] gammaB = new int[256];
        for (int i = 0; i < 256; i++) {
            gammaR[i] = Math.min(255, (int) ((255.0d * Math.pow(((double) i) / 255.0d, 1.0d / red)) + 0.5d));
            gammaG[i] = Math.min(255, (int) ((255.0d * Math.pow(((double) i) / 255.0d, 1.0d / green)) + 0.5d));
            gammaB[i] = Math.min(255, (int) ((255.0d * Math.pow(((double) i) / 255.0d, 1.0d / blue)) + 0.5d));
        }
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = src.getPixel(x, y);
                bmOut.setPixel(x, y, Color.argb(Color.alpha(pixel), gammaR[Color.red(pixel)], gammaG[Color.green(pixel)], gammaB[Color.blue(pixel)]));
            }
        }
        return bmOut;
    }

    public static Bitmap applyColorDepth(Bitmap src, int bitOffset) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = src.getPixel(x, y);
                int A = Color.alpha(pixel);
                int R = Color.red(pixel);
                int G = Color.green(pixel);
                int B = Color.blue(pixel);
                R = (((bitOffset / 2) + R) - (((bitOffset / 2) + R) % bitOffset)) - 1;
                if (R < 0) {
                    R = 0;
                }
                G = (((bitOffset / 2) + G) - (((bitOffset / 2) + G) % bitOffset)) - 1;
                if (G < 0) {
                    G = 0;
                }
                B = (((bitOffset / 2) + B) - (((bitOffset / 2) + B) % bitOffset)) - 1;
                if (B < 0) {
                    B = 0;
                }
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }
        return bmOut;
    }

    public static Bitmap applyContrast(Bitmap src, double value) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        double contrast = Math.pow((100.0d + value) / 100.0d, 2.0d);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = src.getPixel(x, y);
                int A = Color.alpha(pixel);
                int R = (int) (((((((double) Color.red(pixel)) / 255.0d) - 0.5d) * contrast) + 0.5d) * 255.0d);
                if (R < 0) {
                    R = 0;
                } else if (R > 255) {
                    R = 255;
                }
                int G = (int) (((((((double) Color.red(pixel)) / 255.0d) - 0.5d) * contrast) + 0.5d) * 255.0d);
                if (G < 0) {
                    G = 0;
                } else if (G > 255) {
                    G = 255;
                }
                int B = (int) (((((((double) Color.red(pixel)) / 255.0d) - 0.5d) * contrast) + 0.5d) * 255.0d);
                if (B < 0) {
                    B = 0;
                } else if (B > 255) {
                    B = 255;
                }
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }
        return bmOut;
    }

    public static Bitmap applyBrightness(Bitmap src, int value) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = src.getPixel(x, y);
                int A = Color.alpha(pixel);
                int R = Color.red(pixel);
                int G = Color.green(pixel);
                int B = Color.blue(pixel);
                R += value;
                if (R > 255) {
                    R = 255;
                } else if (R < 0) {
                    R = 0;
                }
                G += value;
                if (G > 255) {
                    G = 255;
                } else if (G < 0) {
                    G = 0;
                }
                B += value;
                if (B > 255) {
                    B = 255;
                } else if (B < 0) {
                    B = 0;
                }
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }
        return bmOut;
    }

    public static Bitmap applyGaussianBlur(Bitmap src) {
        double[][] GaussianBlurConfig = new double[][]{new double[]{1.0d, 2.0d, 1.0d}, new double[]{2.0d, 4.0d, 2.0d}, new double[]{1.0d, 2.0d, 1.0d}};
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.applyConfig(GaussianBlurConfig);
        convMatrix.Factor = 16.0d;
        convMatrix.Offset = 0.0d;
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    public static Bitmap applySharpen(Bitmap src, double weight) {
        double[][] SharpConfig = new double[3][];
        SharpConfig[1] = new double[]{-2.0d, weight, -2.0d};
        SharpConfig[2] = new double[]{0.0d, -2.0d, 0.0d};
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.applyConfig(SharpConfig);
        convMatrix.Factor = weight - 8.0d;
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    public static Bitmap applySmooth(Bitmap src, double value) {
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.setAll(1.0d);
        convMatrix.Matrix[1][1] = value;
        convMatrix.Factor = 8.0d + value;
        convMatrix.Offset = 1.0d;
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    public static Bitmap applyEmboss(Bitmap src) {
        double[][] EmbossConfig = new double[][]{new double[]{-1.0d, 0.0d, -1.0d}, new double[]{0.0d, 4.0d, 0.0d}, new double[]{-1.0d, 0.0d, -1.0d}};
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.applyConfig(EmbossConfig);
        convMatrix.Factor = 1.0d;
        convMatrix.Offset = 127.0d;
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    public static Bitmap applyEngrave(Bitmap src) {
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.setAll(0.0d);
        convMatrix.Matrix[0][0] = -2.0d;
        convMatrix.Matrix[1][1] = 2.0d;
        convMatrix.Factor = 1.0d;
        convMatrix.Offset = 95.0d;
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    public static Bitmap applyBoost(Bitmap src, int type, float percent) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pixel = src.getPixel(x, y);
                int A = Color.alpha(pixel);
                int R = Color.red(pixel);
                int G = Color.green(pixel);
                int B = Color.blue(pixel);
                if (type == 1) {
                    R = (int) (((float) R) * (1.0f + percent));
                    if (R > 255) {
                        R = 255;
                    }
                } else if (type == 2) {
                    G = (int) (((float) G) * (1.0f + percent));
                    if (G > 255) {
                        G = 255;
                    }
                } else if (type == 3) {
                    B = (int) (((float) B) * (1.0f + percent));
                    if (B > 255) {
                        B = 255;
                    }
                }
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }
        return bmOut;
    }

    public static Bitmap applyRoundCorner(Bitmap src, float round) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        canvas.drawARGB(0, 0, 0, 0);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        Rect rect = new Rect(0, 0, width, height);
        canvas.drawRoundRect(new RectF(rect), round, round, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(src, rect, rect, paint);
        return result;
    }

    public static Bitmap applyFlip(Bitmap src, int type) {
        Matrix matrix = new Matrix();
        if (type == 1) {
            matrix.preScale(1.0f, -1.0f);
        } else if (type != 2) {
            return null;
        } else {
            matrix.preScale(-1.0f, 1.0f);
        }
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    public static Bitmap applyBlackFilter(Bitmap source) {
        int width = source.getWidth();
        int height = source.getHeight();
        int[] pixels = new int[(width * height)];
        source.getPixels(pixels, 0, width, 0, 0, width, height);
        Random random = new Random();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int index = (y * width) + x;
                int R = Color.red(pixels[index]);
                int G = Color.green(pixels[index]);
                int B = Color.blue(pixels[index]);
                int thresHold = random.nextInt(255);
                if (R < thresHold && G < thresHold && B < thresHold) {
                    pixels[index] = Color.rgb(0, 0, 0);
                }
            }
        }
        Bitmap bmOut = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        bmOut.setPixels(pixels, 0, width, 0, 0, width, height);
        return bmOut;
    }

    public static Bitmap applyShadingFilter(Bitmap source, int shadingColor) {
        int width = source.getWidth();
        int height = source.getHeight();
        int[] pixels = new int[(width * height)];
        source.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int index = (y * width) + x;
                pixels[index] = pixels[index] & shadingColor;
            }
        }
        Bitmap bmOut = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        bmOut.setPixels(pixels, 0, width, 0, 0, width, height);
        return bmOut;
    }

    public static Bitmap applySaturationFilter(Bitmap source, int level) {
        int width = source.getWidth();
        int height = source.getHeight();
        int[] pixels = new int[(width * height)];
        float[] HSV = new float[3];
        source.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int index = (y * width) + x;
                Color.colorToHSV(pixels[index], HSV);
                HSV[1] = HSV[1] * ((float) level);
                HSV[1] = (float) Math.max(0.0d, Math.min((double) HSV[1], 1.0d));
                pixels[index] = pixels[index] | Color.HSVToColor(HSV);
            }
        }
        Bitmap bmOut = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        bmOut.setPixels(pixels, 0, width, 0, 0, width, height);
        return bmOut;
    }

    public static Bitmap applyHueFilter(Bitmap source, int level) {
        int width = source.getWidth();
        int height = source.getHeight();
        int[] pixels = new int[(width * height)];
        float[] HSV = new float[3];
        source.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int index = (y * width) + x;
                Color.colorToHSV(pixels[index], HSV);
                HSV[0] = HSV[0] * ((float) level);
                HSV[0] = (float) Math.max(0.0d, Math.min((double) HSV[0], 360.0d));
                pixels[index] = pixels[index] | Color.HSVToColor(HSV);
            }
        }
        Bitmap bmOut = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        bmOut.setPixels(pixels, 0, width, 0, 0, width, height);
        return bmOut;
    }

    public static Bitmap applyReflection(Bitmap originalImage) {
        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, height / 2, width, height / 2, matrix, false);
        Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height / 2) + height, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapWithReflection);
        canvas.drawBitmap(originalImage, 0.0f, 0.0f, null);
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (height + 4), new Paint());
        canvas.drawBitmap(reflectionImage, 0.0f, (float) (height + 4), null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) originalImage.getHeight(), 0.0f, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, ViewCompat.MEASURED_SIZE_MASK, TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
        return bitmapWithReflection;
    }

    public static Bitmap applyTransparency(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        result.eraseColor(-16777216);
        Canvas c = new Canvas(result);
        Bitmap alpha = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        int[] alphaPix = new int[(width * height)];
        bitmap.getPixels(alphaPix, 0, width, 0, 0, width, height);
        int count = width * height;
        for (int i = 0; i < count; i++) {
            alphaPix[i] = alphaPix[i] << 8;
        }
        alpha.setPixels(alphaPix, 0, width, 0, 0, width, height);
        Paint alphaP = new Paint();
        alphaP.setAntiAlias(true);
        alphaP.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
        c.drawBitmap(alpha, 0.0f, 0.0f, alphaP);
        bitmap.recycle();
        alpha.recycle();
        return result;
    }
}
