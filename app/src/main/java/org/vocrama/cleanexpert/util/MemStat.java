package org.vocrama.cleanexpert.util;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;

public class MemStat {
    private long mTotalMemory;
    private long mUsedMemory;

    @TargetApi(16)
    public MemStat(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        MemoryInfo memInfo = new MemoryInfo();
        am.getMemoryInfo(memInfo);
        this.mTotalMemory = memInfo.totalMem;
        this.mUsedMemory = memInfo.totalMem - memInfo.availMem;
    }

    public long getTotalMemory() {
        return this.mTotalMemory;
    }

    public long getUsedMemory() {
        return this.mUsedMemory;
    }
}
