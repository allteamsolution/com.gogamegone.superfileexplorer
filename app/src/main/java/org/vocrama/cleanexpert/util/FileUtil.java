package org.vocrama.cleanexpert.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileUtil {
    private String TAG = getClass().getSimpleName();
    private final String folderName = (Environment.getExternalStorageDirectory() + "/FileManager/" + "/BackUp");

    public FileUtil() {
        generateDefaultFile();
    }

    public File folderName() {
        File file = new File(this.folderName);
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public String getBaseFolderName() {
        return this.folderName;
    }

    public static File getFile(String path) {
        return new File(path);
    }

    private static String getPng(String path) {
        return path + ".png";
    }

    public boolean deleteFile(String path) {
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            if (file.exists()) {
                return file.delete();
            }
            file = new File(folderName(), path);
            if (file.exists()) {
                return file.delete();
            }
        }
        return false;
    }

    public void deleteFile(List<String> paths) {
        for (String path : paths) {
            if (path != null) {
                File file = new File(folderName(), path);
                if (file.exists()) {
                    file.delete();
                }
            }
        }
    }

    public static String generateFileName(String packageName) {
        return getPng(packageName);
    }

    public File generateFile(String path) {
        File file = new File(this.folderName, ".nomedia");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                file.mkdir();
            }
        }
        return new File(folderName(), path + ".apk");
    }

    private void generateDefaultFile() {
        File file = new File(this.folderName);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    public File generateZipFile(String name) {
        File file = new File(this.folderName, ".nomedia");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                file.mkdir();
            }
        }
        return new File(folderName(), name + ".zip");
    }

    public File generateFolder(String name) {
        File file = new File(this.folderName);
        if (!file.exists()) {
            file.mkdir();
        }
        File folderName = new File(file, name);
        if (!folderName.exists()) {
            folderName.mkdirs();
        }
        return folderName;
    }

    public static String getCacheFile(Context context, String packageName) {
        return context.getCacheDir().getPath() + "/" + generateFileName(packageName);
    }

    public static boolean exists(String path) {
        return getFile(path).exists();
    }

    public static Intent getAllIntent(String param) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(param)), "*/*");
        return intent;
    }
}
