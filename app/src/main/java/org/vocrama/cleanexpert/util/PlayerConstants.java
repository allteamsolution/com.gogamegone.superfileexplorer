package org.vocrama.cleanexpert.util;

import android.net.Uri;
import android.os.Handler;
import java.util.ArrayList;

public class PlayerConstants {
    public static Handler PLAY_PAUSE_HANDLER;
    public static Handler PROGRESSBAR_HANDLER;
    public static ArrayList<MediaItem> SONGS_LIST = new ArrayList();
    public static boolean SONG_CHANGED = false;
    public static Handler SONG_CHANGE_HANDLER;
    public static int SONG_NUMBER = 0;
    public static boolean SONG_PAUSED = true;
    public static final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
}
