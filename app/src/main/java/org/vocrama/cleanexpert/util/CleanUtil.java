package org.vocrama.cleanexpert.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageDataObserver.Stub;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;

import com.gogamegone.superfileexplorer.R;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;

import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.model.JunkInfo;
import org.vocrama.cleanexpert.ui.JunkCleanActivity;

public class CleanUtil {
    public static String formatShortFileSize(Context context, long number) {
        if (context == null) {
            return "";
        }
        String value;
        float result = (float) number;
        int suffix = R.string.byte_short;
        if (result > 900.0f) {
            suffix = R.string.kilo_byte_short;
            result /= 1024.0f;
        }
        if (result > 900.0f) {
            suffix = R.string.mega_byte_short;
            result /= 1024.0f;
        }
        if (result > 900.0f) {
            suffix = R.string.giga_byte_short;
            result /= 1024.0f;
        }
        if (result > 900.0f) {
            suffix = R.string.tera_byte_short;
            result /= 1024.0f;
        }
        if (result > 900.0f) {
            suffix = R.string.peta_byte_short;
            result /= 1024.0f;
        }
        if (result < 1.0f) {
            value = String.format("%.2f", new Object[]{Float.valueOf(result)});
        } else if (result < 10.0f) {
            value = String.format("%.2f", new Object[]{Float.valueOf(result)});
        } else if (result < 100.0f) {
            value = String.format("%.1f", new Object[]{Float.valueOf(result)});
        } else {
            value = String.format("%.0f", new Object[]{Float.valueOf(result)});
        }
        return context.getResources().getString(R.string.clean_file_size_suffix, new Object[]{value, context.getString(suffix)});
    }

    public static void freeAllAppsCache(Handler handler) {
        Context context = App.getInstance();
        File externalDir = context.getExternalCacheDir();
        if (externalDir != null) {
            PackageManager pm = context.getPackageManager();
            for (ApplicationInfo info : pm.getInstalledApplications(256)) {
                File externalCache = new File(externalDir.getAbsolutePath().replace(context.getPackageName(), info.packageName));
                if (externalCache.exists() && externalCache.isDirectory()) {
                    deleteFile(externalCache);
                }
            }
            boolean hanged = true;
            try {
                Method freeStorageAndNotify = pm.getClass().getMethod("freeStorageAndNotify", new Class[]{Long.TYPE, IPackageDataObserver.class});
                Object[] lArr = new Object[2];
                lArr[0] = Long.valueOf(Long.MAX_VALUE);
                final Handler handler2 = handler;
                lArr[1] = new Stub() {
                    @Override
                    public void onGetStatsCompleted(PackageStats pStats, boolean succeeded) throws RemoteException {

                        handler2.obtainMessage(JunkCleanActivity.MSG_SYS_CACHE_CLEAN_FINISH).sendToTarget();
                    }


                };
                freeStorageAndNotify.invoke(pm, lArr);
                hanged = false;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
            if (hanged) {
                Message msg = handler.obtainMessage(JunkCleanActivity.MSG_SYS_CACHE_CLEAN_FINISH);
                Bundle bundle = new Bundle();
                bundle.putBoolean(JunkCleanActivity.HANG_FLAG, true);
                msg.setData(bundle);
                msg.sendToTarget();
            }
        }
    }

    public static boolean deleteFile(File file) {
        if (file.isDirectory()) {
            for (String name : file.list()) {
                if (!deleteFile(new File(file, name))) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    public static void killAppProcesses(String packageName) {
        if (packageName != null && !packageName.isEmpty()) {
            ((ActivityManager) App.getInstance().getSystemService(Context.ACTIVITY_SERVICE)).killBackgroundProcesses(packageName);
        }
    }

    public static void freeJunkInfos(ArrayList<JunkInfo> junks, Handler handler) {
        Iterator it = junks.iterator();
        while (it.hasNext()) {
            File file = new File(((JunkInfo) it.next()).mPath);
            if (file != null && file.exists()) {
                file.delete();
            }
        }
        handler.obtainMessage(JunkCleanActivity.MSG_OVERALL_CLEAN_FINISH).sendToTarget();
    }
}
