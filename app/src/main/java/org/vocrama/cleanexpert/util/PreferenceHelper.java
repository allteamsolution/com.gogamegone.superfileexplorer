package org.vocrama.cleanexpert.util;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import org.vocrama.cleanexpert.App;

public class PreferenceHelper {
    public static SharedPreferences AppPreference;

    public static void putString(String key, String value) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        Editor editor = AppPreference.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getString(String key, String defaultValue) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        return AppPreference.getString(key, defaultValue);
    }

    public static void putInt(String key, int value) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        Editor editor = AppPreference.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getInt(String key, int defaultValue) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        return AppPreference.getInt(key, defaultValue);
    }

    public static void putBoolean(String key, boolean value) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        Editor editor = AppPreference.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        return AppPreference.getBoolean(key, defaultValue);
    }

    public static boolean contains(String key) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        if (AppPreference.contains(key)) {
            return true;
        }
        return false;
    }

    public static void remove(String key) {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        Editor editor = AppPreference.edit();
        editor.remove(key);
        editor.commit();
    }

    public static void clearPreference() {
        AppPreference = App.getContext().getSharedPreferences("UltimateFileHiderPreference", 0);
        AppPreference.edit().clear().commit();
    }
}
