package org.vocrama.cleanexpert.util;

public class MediaItem {
    public String album;
    public long albumId;
    public String albumart;
    public String artist;
    public String composer;
    public long duration;
    public boolean isSelected;
    public String path;
    public String title;

    public String toString() {
        return this.title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return this.album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getAlbumId() {
        return this.albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public String getComposer() {
        return this.composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public Boolean getSelected() {
        return Boolean.valueOf(this.isSelected);
    }

    public void setAlbumart(String albumart) {
        this.albumart = albumart;
    }
}
