package org.vocrama.cleanexpert.util;

import android.opengl.GLES20;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

public class GLUtils {
    public static int getMaxTextureSize() {
        int[] version = new int[2];
        int[] attribList = new int[]{12375, 100, 12374, 100, 12344};
        EGL10 mEGL = (EGL10) EGLContext.getEGL();
        EGLDisplay mEGLDisplay = mEGL.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        mEGL.eglInitialize(mEGLDisplay, version);
        int[] attribList1 = new int[]{12325, 0, 12326, 0, 12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, 4, 12344};
        int[] numConfig = new int[1];
        mEGL.eglChooseConfig(mEGLDisplay, attribList1, null, 0, numConfig);
        int configSize = numConfig[0];
        EGLConfig[] mEGLConfigs = new EGLConfig[configSize];
        mEGL.eglChooseConfig(mEGLDisplay, attribList1, mEGLConfigs, configSize, numConfig);
        EGLConfig mEGLConfig = mEGLConfigs[0];
        EGLContext mEGLContext = mEGL.eglCreateContext(mEGLDisplay, mEGLConfig, EGL10.EGL_NO_CONTEXT, new int[]{12440, 2, 12344});
        EGLSurface mEGLSurface = mEGL.eglCreatePbufferSurface(mEGLDisplay, mEGLConfig, attribList);
        mEGL.eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext);
        int[] maxTextureSize = new int[1];
        GLES20.glGetIntegerv(3379, maxTextureSize, 0);
        mEGL.eglDestroySurface(mEGLDisplay, mEGLSurface);
        mEGL.eglDestroyContext(mEGLDisplay, mEGLContext);
        mEGL.eglTerminate(mEGLDisplay);
        return maxTextureSize[0];
    }
}
