package org.vocrama.cleanexpert.util;

import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;

public class DiskStat {
    private long mExternalAvailableBlocks;
    private long mExternalBlockCount;
    private long mExternalBlockSize;
    private long mInternalAvailableBlocks;
    private long mInternalBlockCount;
    private long mInternalBlockSize;

    public DiskStat() {
        calculateInternalSpace();
        calculateExternalSpace();
    }

    public long getTotalSpace() {
        return (this.mInternalBlockSize * this.mInternalBlockCount) + (this.mExternalBlockSize * this.mExternalBlockCount);
    }

    public long getUsedSpace() {
        return (this.mInternalBlockSize * (this.mInternalBlockCount - this.mInternalAvailableBlocks)) + (this.mExternalBlockSize * (this.mExternalBlockCount - this.mExternalAvailableBlocks));
    }

    public long getUsableSpace() {
        return (this.mInternalBlockSize * this.mInternalAvailableBlocks) + (this.mExternalBlockSize * this.mExternalAvailableBlocks);
    }

    private void calculateExternalSpace() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if (VERSION.SDK_INT >= 18) {
                this.mExternalBlockSize = sf.getBlockSizeLong();
                this.mExternalBlockCount = sf.getBlockCountLong();
                this.mExternalAvailableBlocks = sf.getAvailableBlocksLong();
                return;
            }
            this.mExternalBlockSize = (long) sf.getBlockSize();
            this.mExternalBlockCount = (long) sf.getBlockCount();
            this.mExternalAvailableBlocks = (long) sf.getAvailableBlocks();
        }
    }

    private void calculateInternalSpace() {
        StatFs sf = new StatFs(Environment.getRootDirectory().getPath());
        if (VERSION.SDK_INT >= 18) {
            this.mInternalBlockSize = sf.getBlockSizeLong();
            this.mInternalBlockCount = sf.getBlockCountLong();
            this.mInternalAvailableBlocks = sf.getAvailableBlocksLong();
            return;
        }
        this.mInternalBlockSize = (long) sf.getBlockSize();
        this.mInternalBlockCount = (long) sf.getBlockCount();
        this.mInternalAvailableBlocks = (long) sf.getAvailableBlocks();
    }
}
