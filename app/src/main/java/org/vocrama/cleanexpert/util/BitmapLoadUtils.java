package org.vocrama.cleanexpert.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;
import java.io.IOException;

public class BitmapLoadUtils {
    private static final String TAG = "BitmapLoadUtils";

    public static Bitmap decode(String path, int reqWidth, int reqHeight) {
        return decode(path, reqWidth, reqHeight, false);
    }

    public static Bitmap decode(String path, int reqWidth, int reqHeight, boolean useImageView) {
        if (path == null) {
            return null;
        }
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight, useImageView);
        options.inJustDecodeBounds = false;
        Bitmap decodeSampledBitmap = null;
        boolean isSuccess = false;
        while (!isSuccess) {
            isSuccess = true;
            try {
                decodeSampledBitmap = BitmapFactory.decodeFile(path, options);
            } catch (OutOfMemoryError e) {
                Log.w(TAG, "BitmapLoadUtils decode OutOfMemoryError");
                options.inSampleSize *= 2;
                isSuccess = false;
            }
        }
        ExifInterface exif = getExif(path);
        return exif != null ? rotate(decodeSampledBitmap, exifToDegrees(exif.getAttributeInt("Orientation", 1))) : decodeSampledBitmap;
    }

    public static Bitmap rotate(Bitmap bitmap, int degrees) {
        if (degrees == 0 || bitmap == null) {
            return bitmap;
        }
        Matrix m = new Matrix();
        m.setRotate((float) degrees, ((float) bitmap.getWidth()) / 2.0f, ((float) bitmap.getHeight()) / 2.0f);
        try {
            Bitmap converted = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
            if (bitmap == converted) {
                return bitmap;
            }
            bitmap.recycle();
            return converted;
        } catch (OutOfMemoryError e) {
            return bitmap;
        }
    }

    private static int calculateInSampleSize(Options options, int reqWidth, int reqHeight, boolean useImageView) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
            if (useImageView) {
                int maxTextureSize = GLUtils.getMaxTextureSize();
              /*  while (true) {
                    if (height / inSampleSize <= maxTextureSize && width / inSampleSize <= maxTextureSize) {
                        break;
                    }*/
                    inSampleSize *= 2;

            }
        }
        return inSampleSize;
    }

    private static ExifInterface getExif(String path) {
        try {
            return new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == 6) {
            return 90;
        }
        if (exifOrientation == 3) {
            return 180;
        }
        if (exifOrientation == 8) {
            return 270;
        }
        return 0;
    }
}
