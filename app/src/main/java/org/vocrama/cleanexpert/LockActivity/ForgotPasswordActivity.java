package org.vocrama.cleanexpert.LockActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.gogamegone.superfileexplorer.R;


public class ForgotPasswordActivity extends AppCompatActivity {
    protected Button btnSubmit;
    protected EditText edtAnswer;
    protected EditText edtQuestion;
    private Context mContext;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_forgot_password);
        this.mContext = this;
        initView();
    }

    private void initView() {
        this.edtQuestion = (EditText) findViewById(R.id.edt_question);
        this.edtQuestion.setText((String) PreferenceHelper.getValueFromPreference(this.mContext, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_QUESTIONS, ""));
        this.edtAnswer = (EditText) findViewById(R.id.edt_answer);
        this.btnSubmit = (Button) findViewById(R.id.btn_submit);
        this.btnSubmit.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ForgotPasswordActivity.this.ButtonClickEvents();
            }
        });
    }

    private void ButtonClickEvents() {
        this.btnSubmit.setOnClickListener(new OnClickListener() {
            @SuppressLint({"WrongConstant"})
            public void onClick(View view) {
                if (ForgotPasswordActivity.this.edtAnswer.getText().toString().equalsIgnoreCase((String) PreferenceHelper.getValueFromPreference(ForgotPasswordActivity.this.mContext, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_ANSWER, ""))) {
                    ForgotPasswordActivity.this.showAnswerInputDialog();
                    return;
                }
                ((Vibrator) ForgotPasswordActivity.this.getSystemService("vibrator")).vibrate(200);
                Toast.makeText(ForgotPasswordActivity.this.getApplicationContext(), "Enter correct answer", 0).show();
            }
        });
    }

    private void showAnswerInputDialog() {
        View customView = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.get_answer_dialog_layout, null);
        final EditText answerEditText = (EditText) customView.findViewById(R.id.answer);
        new AlertDialog.Builder(getApplicationContext(), R.style.DialogTheme).setTitle((CharSequence) "Enter Answer").setView(customView).setMessage((String) PreferenceHelper.getValueFromPreference(this.mContext, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_QUESTIONS, "")).setPositiveButton((CharSequence) "ENTER", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (answerEditText.getText().toString().trim().isEmpty()) {
                    Toast.makeText(ForgotPasswordActivity.this.getApplicationContext(), "Enter correct answer", 0).show();
                } else if (answerEditText.getText().toString().trim().equals((String) PreferenceHelper.getValueFromPreference(ForgotPasswordActivity.this.mContext, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_ANSWER, ""))) {
                    ForgotPasswordActivity.this.showRightAnswerWithPasswordDialog();
                } else {
                    ForgotPasswordActivity.this.showWrongAnswerDialog();
                }
            }
        }).create().show();
    }

    private void showRightAnswerWithPasswordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Woohooo...");
        builder.setMessage("Your Password : " + ((String) PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.PASSWORD, ""))).setCancelable(false).setPositiveButton((CharSequence) "GOT IT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                ForgotPasswordActivity.this.finish();
            }
        });
        builder.create().show();
    }

    private void showWrongAnswerDialog() {
        new AlertDialog.Builder(getApplicationContext(), R.style.DialogTheme).setTitle((CharSequence) "Snap...").setMessage((CharSequence) "Your answer is wrong.").setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }
}
