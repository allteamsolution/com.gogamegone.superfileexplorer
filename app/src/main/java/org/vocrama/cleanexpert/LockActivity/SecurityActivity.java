package org.vocrama.cleanexpert.LockActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.BaseTheme;
import org.vocrama.cleanexpert.ui.FirstActivity;

public class SecurityActivity extends BaseTheme {
    String SelectedQuestion;
    Button btnOk;
    EditText edtAnswer;
    ImageView img_back_theme;
    ProgressDialog progress;
    Spinner spinner;
    TextView txt_security_title;
    private Context mContext;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_security);

        this.img_back_theme = (ImageView) findViewById(R.id.img_back_theme);
        this.img_back_theme.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                PreferenceHelper.clearPreference(SecurityActivity.this.getApplicationContext());
                SecurityActivity.this.startActivity(new Intent(SecurityActivity.this, FirstActivity.class));
                SecurityActivity.this.finish();
            }
        });

        this.mContext = this;

        if (((Boolean) PreferenceHelper.getValueFromPreference(this.mContext, Boolean.class, UtilsDemo.ISSECURITYQUESTTIONSET, Boolean.valueOf(false))).booleanValue()) {

            Intent intent = new Intent(getApplicationContext(), PinLockActivity.class);
            intent.putExtra("type", "newpassword");
            startActivity(intent);
            finish();

        }
        initview();
        SetAdapter();
        this.btnOk.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

                SecurityActivity.this.SetSecurity();

            }
        });

    }


    private void initview() {

        this.progress = new ProgressDialog(this);
        this.spinner = (Spinner) findViewById(R.id.spinner1);
        this.txt_security_title = (TextView) findViewById(R.id.txt_security_title);
        this.edtAnswer = (EditText) findViewById(R.id.edt_answer);
        this.edtAnswer.setHint("Enter your answer");
        this.btnOk = (Button) findViewById(R.id.btnOk);
        this.spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SecurityActivity.this.SelectedQuestion = parent.getItemAtPosition(position).toString().trim();
                PreferenceHelper.saveToPreference(SecurityActivity.this, UtilsDemo.LOGIN_PREFERENCE_KEY_QUESTIONS, SecurityActivity.this.SelectedQuestion);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }


    public void SetAdapter() {

        ArrayAdapter<String> adapter = new ArrayAdapter(this, R.layout.spinner_textview, getResources().getStringArray(R.array.array_question));
        adapter.setDropDownViewResource(R.layout.spinner_dropdowntextview);
        this.spinner.setAdapter(adapter);

    }


    public void SetSecurity() {

        if (PreferenceHelper.getValueFromPreference(this.mContext, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_QUESTIONS, null) == null) {

            Toast.makeText(getApplicationContext(), "Select security question", Toast.LENGTH_SHORT).show();

        } else if (this.edtAnswer.getText().length() > 0) {

            PreferenceHelper.saveToPreference(this.mContext, UtilsDemo.LOGIN_PREFERENCE_KEY_ANSWER, this.edtAnswer.getText().toString());
            PreferenceHelper.saveToPreference(getApplicationContext(), UtilsDemo.ISSECURITYQUESTTIONSET, Boolean.valueOf(true));
            Intent intent = new Intent(getApplicationContext(), PinLockActivity.class);
            intent.putExtra("type", "newpassword");
            startActivity(intent);
            finish();

        } else {

            Toast.makeText(getApplicationContext(), "Enter security answer", Toast.LENGTH_SHORT).show();

        }
    }


    public void onBackPressed() {
        super.onBackPressed();

        PreferenceHelper.clearPreference(getApplicationContext());
        startActivity(new Intent(this, FirstActivity.class));
        finish();
    }


}
