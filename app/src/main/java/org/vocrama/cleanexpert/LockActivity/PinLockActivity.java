package org.vocrama.cleanexpert.LockActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gogamegone.superfileexplorer.R;

import java.util.ArrayList;
import java.util.Iterator;
import org.vocrama.cleanexpert.ui.BaseTheme;
import org.vocrama.cleanexpert.ui.FirstActivity;

public class PinLockActivity extends BaseTheme implements OnClickListener {
    public String PASSWORD = null;
    public int PIN_STATUS = 1;
    ArrayList<Integer> Pinarray;
    protected Button btn0;
    protected Button btn1;
    protected Button btn2;
    protected Button btn3;
    protected Button btn4;
    protected Button btn5;
    protected Button btn6;
    protected Button btn7;
    protected Button btn8;
    protected Button btn9;
    protected Button btnBack;
    int default_color;
    protected View dotFour;
    protected View dotOne;
    protected View dotThree;
    protected View dotTwo;
    int fillup_color;
    private Context mContext;
    TextView txt_forgotpassword;
    private TextView txt_title;

    public static void DrawRoundView(View v, int backgroundColor, int borderColor, boolean isbackgroundset) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        shape.setCornerRadii(new float[]{4.0f, 4.0f, 4.0f, 4.0f, 0.0f, 0.0f, 0.0f, 0.0f});
        if (isbackgroundset) {
            shape.setColor(backgroundColor);
        }
        shape.setStroke(1, borderColor);
        v.setBackgroundDrawable(shape);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_pin_lock1);


        this.fillup_color = getResources().getColor(R.color.white);
        this.mContext = this;
        if (getIntent() != null) {
            String type = getIntent().getStringExtra("type");
            int i = -1;
            switch (type.hashCode()) {
                case -1885879237:
                    if (type.equals("newpassword")) {
                        i = 0;
                        break;
                    }
                    break;
                case 565299902:
                    if (type.equals("forgotpassword")) {
                        i = 1;
                        break;
                    }
                    break;
                case 866786891:
                    if (type.equals("changepassword")) {
                        i = 2;
                        break;
                    }
                    break;
            }
            switch (i) {
                case 0:
                case 1:
                    this.PIN_STATUS = 1;
                    break;
                case 2:
                    this.PIN_STATUS = 0;
                    break;
            }
        }
        initView();
        IntializePinLock();
    }




    private void initView() {
        this.Pinarray = new ArrayList();
        this.txt_title = (TextView) findViewById(R.id.txt_title);
        this.txt_forgotpassword = (TextView) findViewById(R.id.txt_forgotpassword);
        this.txt_forgotpassword.setOnClickListener(this);
        this.btn1 = (Button) findViewById(R.id.btn_1);
        this.btn1.setOnClickListener(this);
        this.btn2 = (Button) findViewById(R.id.btn_2);
        this.btn2.setOnClickListener(this);
        this.btn3 = (Button) findViewById(R.id.btn_3);
        this.btn3.setOnClickListener(this);
        this.btn4 = (Button) findViewById(R.id.btn_4);
        this.btn4.setOnClickListener(this);
        this.btn5 = (Button) findViewById(R.id.btn_5);
        this.btn5.setOnClickListener(this);
        this.btn6 = (Button) findViewById(R.id.btn_6);
        this.btn6.setOnClickListener(this);
        this.btn7 = (Button) findViewById(R.id.btn_7);
        this.btn7.setOnClickListener(this);
        this.btn8 = (Button) findViewById(R.id.btn_8);
        this.btn8.setOnClickListener(this);
        this.btn9 = (Button) findViewById(R.id.btn_9);
        this.btn9.setOnClickListener(this);
        this.btn0 = (Button) findViewById(R.id.btn_0);
        this.btn0.setOnClickListener(this);
        this.btnBack = (Button) findViewById(R.id.btn_back);
        this.btnBack.setOnClickListener(this);
        this.dotOne = findViewById(R.id.dot_one);
        this.dotTwo = findViewById(R.id.dot_two);
        this.dotThree = findViewById(R.id.dot_three);
        this.dotFour = findViewById(R.id.dot_four);
        FillUpCircle(this.Pinarray, this.default_color, this.fillup_color);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btn_1) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_2) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_3) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_4) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_5) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_6) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_7) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_8) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_9) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_0) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_back) {
            OnBackpress();
        } else {
            if (view.getId() != R.id.txt_forgotpassword) {
            }
        }
    }

    public void IntializePinLock() {
        String pwd = (String) PreferenceHelper.getValueFromPreference(this.mContext, String.class, UtilsDemo.PASSWORD, "");
        if (this.PIN_STATUS == 0) {
            this.txt_title.setText("Enter your old password");
        } else if (this.PIN_STATUS == 1) {
            this.txt_title.setText("Set a new password");
        } else {
            this.txt_title.setText("Confirm new password");
            Reset();
        }
    }

    @SuppressLint({"WrongConstant"})
    public void SetPin(int no) {
        if (this.Pinarray.size() > 0) {
            this.Pinarray.add(Integer.valueOf(no));
            FillUpCircle(this.Pinarray, this.default_color, this.fillup_color);
            if (this.Pinarray.size() > 3) {
                if (this.PIN_STATUS >= 2) {
                    if (this.PASSWORD.equalsIgnoreCase(ConvertArrayToString(this.Pinarray))) {
                        PreferenceHelper.saveToPreference(this.mContext, UtilsDemo.PASSWORD, this.PASSWORD);
                        Toast.makeText(getApplicationContext(), "Password set successfully", 0).show();
                        startActivity(new Intent(getApplicationContext(), FirstActivity.class));
                        finish();
                    } else {
                        ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(200);
                        Toast.makeText(this.mContext, "Enter correct password", 0).show();
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                PinLockActivity.this.Reset();
                            }
                        }, 200);
                        Toast.makeText(getApplicationContext(), "Enter correct password", 0).show();
                    }
                    this.PIN_STATUS++;
                    IntializePinLock();
                    return;
                } else if (this.PIN_STATUS == 1) {
                    this.PASSWORD = ConvertArrayToString(this.Pinarray);
                    this.PIN_STATUS++;
                    IntializePinLock();
                    return;
                } else if (this.PIN_STATUS == 0) {
                    String pwd = (String) PreferenceHelper.getValueFromPreference(this.mContext, String.class, UtilsDemo.PASSWORD, "");
                    this.PASSWORD = ConvertArrayToString(this.Pinarray);
                    if (this.PASSWORD.equalsIgnoreCase(pwd)) {
                        this.PIN_STATUS++;
                        IntializePinLock();
                    } else {
                        ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(200);
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                PinLockActivity.this.Reset();
                            }
                        }, 200);
                        Toast.makeText(this.mContext, "Please Enter Corrent Password", 0).show();
                    }
                    Reset();
                    return;
                } else {
                    return;
                }
            }
            return;
        }
        this.Pinarray.add(Integer.valueOf(no));
        FillUpCircle(this.Pinarray, this.default_color, this.fillup_color);
    }

    public void OnBackpress() {
        if (this.Pinarray.size() > 1) {
            this.Pinarray.remove(this.Pinarray.size() - 1);
            FillUpCircle(this.Pinarray, this.default_color, this.fillup_color);
            return;
        }
        Reset();
    }

    public String ConvertArrayToString(ArrayList<Integer> array) {
        String listString = "";
        Iterator it = array.iterator();
        while (it.hasNext()) {
            listString = listString + ((Integer) it.next());
        }
        return listString;
    }

    public void FillUpCircle(ArrayList<Integer> Pinarray, int defaultcolor, int fillupcolor) {
        if (Pinarray.size() <= 0) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 1) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 2) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 3) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 4) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, true);
        }
    }

    public void Reset() {
        this.Pinarray = new ArrayList();
        DrawRoundView(this.dotOne, this.fillup_color, this.default_color, false);
        DrawRoundView(this.dotTwo, this.fillup_color, this.default_color, false);
        DrawRoundView(this.dotThree, this.fillup_color, this.default_color, false);
        DrawRoundView(this.dotFour, this.fillup_color, this.default_color, false);
    }

    public void onBackPressed() {
    }
}
