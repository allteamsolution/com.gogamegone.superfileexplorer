package org.vocrama.cleanexpert.LockActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.gogamegone.superfileexplorer.R;


public class SettingActivity extends AppCompatActivity {
    ActionBar actionBar;
    SwitchCompat compat;
    RelativeLayout upDatePassWord;
    private boolean switch_value;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_setting);
        this.actionBar = getSupportActionBar();
        if (this.actionBar != null) {
            this.actionBar.setDisplayHomeAsUpEnabled(true);
            this.actionBar.setDisplayShowTitleEnabled(true);
            this.actionBar.setHomeAsUpIndicator((int) R.drawable.ic_back_arrow);
            this.actionBar.setTitle((CharSequence) "Settings");
        }
        this.compat = (SwitchCompat) findViewById(R.id.cswitch);
        this.compat.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingActivity.this.setButtonState(isChecked);
            }
        });
    }

    protected void onResume() {
        super.onResume();
        if (PreferenceHelper.contains(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED)) {
            this.switch_value = PreferenceHelper.getValueBoolean(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED, false);
            this.compat.setChecked(this.switch_value);
            return;
        }
        PreferenceHelper.setValueBoolean(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED, false);
    }

    private void setButtonState(boolean state) {
        if (state) {
            PreferenceHelper.setValueBoolean(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED, true);
            if (PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_ANSWER, null) == null) {
                startActivity(new Intent(getApplicationContext(), SecurityActivity.class));
                finish();
                return;
            } else if (PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.PASSWORD, null) == null) {
                startActivity(new Intent(getApplicationContext(), SecurityActivity.class));
                finish();
                return;
            } else {
                return;
            }
        }
        PreferenceHelper.setValueBoolean(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED, false);
    }
}
