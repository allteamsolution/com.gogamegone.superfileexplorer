package org.vocrama.cleanexpert.LockActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceHelper {
    public static SharedPreferences AppPreference = null;
    public static String PREF_KEY_ONOFF_ENABLED = "IS_ONOFF_ENABLED";
    public static String PREF_NAME = "LockDemo";
    public static final String RESPONSE_TITLE = "title";
    public static final String RESPONSE_TYPE = "type";
    public static final String SWITCH_TYPE = "sw_type";
    private static PreferenceHelper instance = null;

    public PreferenceHelper(Context applicationContext) {
    }

    public static boolean getValueBoolean(Context context, String key, boolean defaultValue) {
        AppPreference = context.getSharedPreferences(PREF_NAME, 0);
        return AppPreference.getBoolean(key, defaultValue);
    }

    public static void setValueBoolean(Context context, String key, boolean value) {
        AppPreference = context.getSharedPreferences(PREF_NAME, 0);
        Editor editor = AppPreference.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean contains(Context context, String key) {
        AppPreference = context.getSharedPreferences(PREF_NAME, 0);
        if (AppPreference.contains(key)) {
            return true;
        }
        return false;
    }

    public static void clearPreference(Context context) {
        AppPreference = context.getSharedPreferences(PREF_NAME, 0);
        AppPreference.edit().clear().commit();
    }

    public static PreferenceHelper getInstance(Context context) {
        if (instance == null) {
            if (context == null) {
                throw new IllegalStateException(PreferenceHelper.class.getSimpleName() + " is not initialized, call getInstance(Context) with a VALID Context first.");
            }
            instance = new PreferenceHelper(context.getApplicationContext());
        }
        return instance;
    }

    public static Object getValueFromPreference(Context context, Class<?> type, String key, Object defValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        if (String.class.isAssignableFrom(type)) {
            return sharedPreferences.getString(key, (String) defValue);
        }
        if (Integer.class.isAssignableFrom(type)) {
            return Integer.valueOf(sharedPreferences.getInt(key, ((Integer) defValue).intValue()));
        }
        if (Long.class.isAssignableFrom(type)) {
            return Long.valueOf(sharedPreferences.getLong(key, ((Long) defValue).longValue()));
        }
        if (Float.class.isAssignableFrom(type)) {
            return Float.valueOf(sharedPreferences.getFloat(key, ((Float) defValue).floatValue()));
        }
        if (Boolean.class.isAssignableFrom(type)) {
            return Boolean.valueOf(sharedPreferences.getBoolean(key, ((Boolean) defValue).booleanValue()));
        }
        return null;
    }

    public static boolean saveToPreference(Context context, String key, Object value) {
        Editor editor = context.getSharedPreferences(PREF_NAME, 0).edit();
        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, ((Integer) value).intValue());
        } else if (value instanceof Float) {
            editor.putFloat(key, ((Float) value).floatValue());
        } else if (value instanceof Long) {
            editor.putLong(key, ((Long) value).longValue());
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, ((Boolean) value).booleanValue());
        }
        return editor.commit();
    }
}
