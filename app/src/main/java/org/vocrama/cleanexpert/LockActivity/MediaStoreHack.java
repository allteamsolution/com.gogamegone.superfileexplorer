package org.vocrama.cleanexpert.LockActivity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Files;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MediaStoreHack {
    private static final String ALBUM_ART_URI = "content://media/external/audio/albumart";
    private static final String[] ALBUM_PROJECTION = new String[]{"_id", "album_id", "media_type"};

    public static boolean delete(Context context, File file) {
        String where = "_data=?";
        String[] selectionArgs = new String[]{file.getAbsolutePath()};
        ContentResolver contentResolver = context.getContentResolver();
        Uri filesUri = Files.getContentUri("external");
        contentResolver.delete(filesUri, "_data=?", selectionArgs);
        if (file.exists()) {
            ContentValues values = new ContentValues();
            values.put("_data", file.getAbsolutePath());
            contentResolver.insert(Media.EXTERNAL_CONTENT_URI, values);
            contentResolver.delete(filesUri, "_data=?", selectionArgs);
        }
        if (file.exists()) {
            return false;
        }
        return true;
    }

    private static File getExternalFilesDir(Context context) {
        return context.getExternalFilesDir(null);
    }

    public static InputStream getInputStream(Context context, File file, long size) {
        try {
            String where = "_data=?";
            String[] selectionArgs = new String[]{file.getAbsolutePath()};
            ContentResolver contentResolver = context.getContentResolver();
            Uri filesUri = Files.getContentUri("external");
            contentResolver.delete(filesUri, "_data=?", selectionArgs);
            ContentValues values = new ContentValues();
            values.put("_data", file.getAbsolutePath());
            values.put("_size", Long.valueOf(size));
            return contentResolver.openInputStream(contentResolver.insert(filesUri, values));
        } catch (Throwable th) {
            return null;
        }
    }

    public static OutputStream getOutputStream(Context context, String str) {
        OutputStream outputStream = null;
        Uri fileUri = getUriFromFile(str, context);
        if (fileUri == null) {
            return outputStream;
        }
        try {
            return context.getContentResolver().openOutputStream(fileUri);
        } catch (Throwable th) {
            Log.d("Error", th.getMessage());
            return outputStream;
        }
    }

    public static Uri getUriFromFile(String path, Context context) {
        ContentResolver resolver = context.getContentResolver();
        Cursor filecursor = resolver.query(Files.getContentUri("external"), new String[]{"_id"}, "_data = ?", new String[]{path}, "date_added desc");
        filecursor.moveToFirst();
        if (filecursor.isAfterLast()) {
            filecursor.close();
            ContentValues values = new ContentValues();
            values.put("_data", path);
            return resolver.insert(Files.getContentUri("external"), values);
        }
        Uri uri = Files.getContentUri("external").buildUpon().appendPath(Integer.toString(filecursor.getInt(filecursor.getColumnIndex("_id")))).build();
        filecursor.close();
        return uri;
    }

    public static boolean mkfile(Context context, File file) {
        OutputStream outputStream = getOutputStream(context, file.getPath());
        if (outputStream == null) {
            return false;
        }
        try {
            outputStream.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
