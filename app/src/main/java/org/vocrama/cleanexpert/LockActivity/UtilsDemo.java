package org.vocrama.cleanexpert.LockActivity;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;


public class UtilsDemo {

    public static String ISSECURITYQUESTTIONSET = "issecurityquestionset";
    public static final String LOGIN_PREFERENCE_KEY_ANSWER = "securityanswer";
    public static final String LOGIN_PREFERENCE_KEY_QUESTIONS = "securityquestion";
    public static final String PASSWORD = "PASSWORD";
    private final Context mContext;

    public UtilsDemo(Context mContext) {
        this.mContext = mContext;
    }

    @TargetApi(19)
    public static String[] getExtSdCardPathsForActivity(Context context) {
        List<String> paths = new ArrayList();
        for (File file : context.getExternalFilesDirs("external")) {
            if (file != null) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index < 0) {
                    Log.w("FileUtils", "Unexpected external file dir: " + file.getAbsolutePath());
                } else {
                    String path = file.getAbsolutePath().substring(0, index);
                    try {
                        path = new File(path).getCanonicalPath();
                    } catch (IOException e) {
                    }
                    paths.add(path);
                }
            }
        }
        if (paths.isEmpty()) {
            paths.add("/storage/sdcard1");
        }
        return (String[]) paths.toArray(new String[0]);
    }

    public static String convertTimeFromUnixTimeStamp(String date) {
        Date d = null;
        try {
            d = new SimpleDateFormat("EE MMM dd HH:mm:ss zz yyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("MM-dd-yyyy").format(d);
    }

    public static String convertTimeFromUnixTimeStamp(String format, String date) {
        Date d = null;
        try {
            d = new SimpleDateFormat("EE MMM dd HH:mm:ss zz yyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat(format).format(d);
    }

    public String getString(int a) {
        return this.mContext.getResources().getString(a);
    }

    public String StoragePath(String StorageType) {
        List<String> paths = getStorageDirectories();
        if (paths.size() > 0) {
            try {
                if (StorageType.equalsIgnoreCase("InternalStorage")) {
                    return (String) paths.get(0);
                }
                if (StorageType.equalsIgnoreCase("ExternalStorage")) {
                    if (paths.size() >= 1) {
                        return (String) paths.get(1);
                    }
                } else if (!StorageType.equalsIgnoreCase("UsbStorage")) {
                    return "";
                } else {
                    if (paths.size() >= 2) {
                        return (String) paths.get(2);
                    }
                }
            } catch (Exception e) {
            }
        }
        return "";
    }

    public List<String> getStorageDirectories() {
        Pattern DIR_SEPARATOR = Pattern.compile("/");
        ArrayList<String> rv = new ArrayList();
        String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
        String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
        String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
        if (!TextUtils.isEmpty(rawEmulatedStorageTarget)) {
            String rawUserId;
            if (VERSION.SDK_INT < 17) {
                rawUserId = "";
            } else {
                String[] folders = DIR_SEPARATOR.split(Environment.getExternalStorageDirectory().getAbsolutePath());
                String lastFolder = folders[folders.length - 1];
                boolean isDigit = false;
                try {
                    Integer.valueOf(lastFolder);
                    isDigit = true;
                } catch (NumberFormatException e) {
                }
                rawUserId = isDigit ? lastFolder : "";
            }
            if (TextUtils.isEmpty(rawUserId)) {
                rv.add(rawEmulatedStorageTarget);
            } else {
                rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
            }
        } else if (TextUtils.isEmpty(rawExternalStorage)) {
            rv.add("/storage/sdcard0");
        } else {
            rv.add(rawExternalStorage);
        }
        if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
            Collections.addAll(rv, rawSecondaryStoragesStr.split(File.pathSeparator));
        }
        if (VERSION.SDK_INT >= 23) {
            rv.clear();
        }
        if (VERSION.SDK_INT >= 19) {
            for (String s : getExtSdCardPathsForActivity(this.mContext)) {
                File f = new File(s);
                if (!rv.contains(s) && canListFiles(f)) {
                    rv.add(s);
                }
            }
        }
        return rv;
    }

    public boolean canListFiles(File f) {
        try {
            if (f.canRead() && f.isDirectory()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
