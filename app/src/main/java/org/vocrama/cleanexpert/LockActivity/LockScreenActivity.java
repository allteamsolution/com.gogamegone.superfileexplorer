package org.vocrama.cleanexpert.LockActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.StarImageView;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.BaseTheme;
import org.vocrama.cleanexpert.ui.FirstActivity;

import java.util.ArrayList;
import java.util.Iterator;

public class LockScreenActivity extends BaseTheme implements OnClickListener {
    protected Button btn0;
    protected Button btn1;
    protected Button btn2;
    protected Button btn3;
    protected Button btn4;
    protected Button btn5;
    protected Button btn6;
    protected Button btn7;
    protected Button btn8;
    protected Button btn9;
    protected Button btnBack;
    protected StarImageView dotFour;
    protected StarImageView dotOne;
    protected StarImageView dotThree;
    protected StarImageView dotTwo;
    ArrayList<Integer> Pinarray;
    int default_color;
    int fillup_color;
    TextView txt_forgotpassword;

    public static void DrawRoundView(View v, int backgroundColor, int borderColor, boolean isbackgroundset) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        shape.setCornerRadii(new float[]{4.0f, 4.0f, 4.0f, 4.0f, 0.0f, 0.0f, 0.0f, 0.0f});
        if (isbackgroundset) {
            shape.setColor(backgroundColor);
        }
        shape.setStroke(1, borderColor);
        v.setBackgroundDrawable(shape);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_lock1);


        this.fillup_color = getResources().getColor(R.color.card);
        initView();
    }


    private void initView() {
        this.Pinarray = new ArrayList<>();
        this.txt_forgotpassword = findViewById(R.id.txt_forgotpassword);
        this.txt_forgotpassword.setOnClickListener(this);
        this.txt_forgotpassword.setVisibility(0);
        this.btn1 = findViewById(R.id.btn_1);
        this.btn1.setOnClickListener(this);
        btn2 = findViewById(R.id.btn_2);
        this.btn2.setOnClickListener(this);
        this.btn3 = findViewById(R.id.btn_3);
        this.btn3.setOnClickListener(this);
        this.btn4 = findViewById(R.id.btn_4);
        this.btn4.setOnClickListener(this);
        this.btn5 = findViewById(R.id.btn_5);
        this.btn5.setOnClickListener(this);
        this.btn6 = findViewById(R.id.btn_6);
        this.btn6.setOnClickListener(this);
        this.btn7 = findViewById(R.id.btn_7);
        this.btn7.setOnClickListener(this);
        this.btn8 = findViewById(R.id.btn_8);
        this.btn8.setOnClickListener(this);
        this.btn9 = findViewById(R.id.btn_9);
        this.btn9.setOnClickListener(this);
        this.btn0 = findViewById(R.id.btn_0);
        this.btn0.setOnClickListener(this);
        this.btnBack = findViewById(R.id.btn_back);
        this.btnBack.setOnClickListener(this);
        this.dotOne = findViewById(R.id.dot_one);
        this.dotTwo = findViewById(R.id.dot_two);
        this.dotThree = findViewById(R.id.dot_three);
        this.dotFour = findViewById(R.id.dot_four);
        FillUpCircle(this.Pinarray, this.default_color, this.fillup_color);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btn_1) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_2) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_3) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_4) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_5) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_6) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_7) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_8) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_9) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_0) {
            SetPin(Integer.parseInt((String) view.getTag()));
        } else if (view.getId() == R.id.btn_back) {
            OnBackpress();
        } else if (view.getId() == R.id.txt_forgotpassword) {
            showAnswerInputDialog();
        }
    }

    @SuppressLint({"RestrictedApi"})
    private void showAnswerInputDialog() {
        View customView = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.get_answer_dialog_layout, null);
        final EditText answerEditText = (EditText) customView.findViewById(R.id.answer);
        new AlertDialog.Builder(this, R.style.DialogTheme).setTitle((CharSequence) "Enter Answer").setView(customView).setMessage((String) PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_QUESTIONS, "")).setPositiveButton((CharSequence) "ENTER", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (answerEditText.getText().toString().trim().isEmpty()) {
                    Toast.makeText(LockScreenActivity.this, "Enter correct answer", 0).show();
                } else if (answerEditText.getText().toString().equals((String) PreferenceHelper.getValueFromPreference(LockScreenActivity.this, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_ANSWER, ""))) {
                    LockScreenActivity.this.showRightAnswerWithPasswordDialog();
                } else {
                    LockScreenActivity.this.showWrongAnswerDialog();
                }
            }
        }).create().show();
    }

    private void showRightAnswerWithPasswordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Woohooo...");
        builder.setMessage("Your Password : " + ((String) PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.PASSWORD, ""))).setCancelable(false).setPositiveButton((CharSequence) "GOT IT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void showWrongAnswerDialog() {
        new AlertDialog.Builder(this, R.style.DialogTheme).setTitle((CharSequence) "Snap...").setMessage((CharSequence) "Your answer is wrong.").setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    @SuppressLint({"WrongConstant"})
    public void SetPin(int no) {
        this.Pinarray.add(Integer.valueOf(no));
        FillUpCircle(this.Pinarray, this.default_color, this.fillup_color);
        if (this.Pinarray.size() > 3) {
            if (((String) PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.PASSWORD, "")).equalsIgnoreCase(ConvertArrayToString(this.Pinarray))) {
                startActivity(new Intent(this, FirstActivity.class));
                finish();
                return;
            }
            ((Vibrator) getSystemService("vibrator")).vibrate(200);
            Toast.makeText(this, "Enter correct password", 0).show();
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    LockScreenActivity.this.Reset();
                }
            }, 200);
        }
    }

    public void OnBackpress() {
        if (this.Pinarray.size() > 1) {
            this.Pinarray.remove(this.Pinarray.size() - 1);
            FillUpCircle(this.Pinarray, this.default_color, this.fillup_color);
            return;
        }
        Reset();
    }

    public String ConvertArrayToString(ArrayList<Integer> array) {
        String listString = "";
        Iterator it = array.iterator();
        while (it.hasNext()) {
            listString = listString + ((Integer) it.next());
        }
        return listString;
    }

    public void FillUpCircle(ArrayList<Integer> Pinarray, int defaultcolor, int fillupcolor) {
        if (Pinarray.size() <= 0) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 1) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 2) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, false);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 3) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, false);
        } else if (Pinarray.size() == 4) {
            DrawRoundView(this.dotOne, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotTwo, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotThree, fillupcolor, defaultcolor, true);
            DrawRoundView(this.dotFour, fillupcolor, defaultcolor, true);
        }
    }

    public void Reset() {
        this.Pinarray = new ArrayList();
        DrawRoundView(this.dotOne, this.fillup_color, this.default_color, false);
        DrawRoundView(this.dotTwo, this.fillup_color, this.default_color, false);
        DrawRoundView(this.dotThree, this.fillup_color, this.default_color, false);
        DrawRoundView(this.dotFour, this.fillup_color, this.default_color, false);
    }

    public void onBackPressed() {
        super.onBackPressed();
    }
}
