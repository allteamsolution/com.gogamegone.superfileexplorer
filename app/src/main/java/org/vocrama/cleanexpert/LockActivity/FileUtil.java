package org.vocrama.cleanexpert.LockActivity;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.provider.MediaStore.Files;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public abstract class FileUtil {
    public static final int COMPRESS = 7;
    public static final int COPY = 1;
    public static final int DELETE = 0;
    public static final int EXTRACT = 6;
    public static final int MOVE = 2;
    public static final int NEW_FILE = 5;
    public static final int NEW_FOLDER = 3;
    public static final int RENAME = 4;
    public static boolean FileOperation = false;
    static int countFile = 0;
    static int countFolder = 0;
    static int[] totalFileFolder = new int[2];

    public static String getFileNameFromPath(String path) {
        return path.substring(path.lastIndexOf("/"), path.length());
    }

    public static final boolean isWritable(File file) {
        boolean z = false;
        if (file != null) {
            boolean isExisting = file.exists();
            try {
                new FileOutputStream(file, true).close();
            } catch (IOException e) {
            }
            try {
                z = file.canWrite();
                if (!isExisting) {
                    file.delete();
                }
            } catch (Exception e2) {
            }
        }
        return z;
    }

    public static final boolean isWritableNormalOrSaf(File folder, Context c) {
        boolean result = true;
        if (folder == null || !folder.exists() || !folder.isDirectory()) {
            return false;
        }
        File file;
        int i = 0;
        do {
            i++;
            file = new File(folder, "AugendiagnoseDummyFile" + i);
        } while (file.exists());
        if (isWritable(file)) {
            return true;
        }
        DocumentFile document = getDocumentFile(file, false, c);
        if (document == null) {
            return false;
        }
        if (!(document.canWrite() && file.exists())) {
            result = false;
        }
        deleteFile(file, c);
        return result;
    }

    public static final boolean deleteFile(@NonNull File file, Context context) {
        if (file == null) {
            return true;
        }
        boolean fileDelete = deleteFilesInFolder(file, context);
        if (file.delete() || fileDelete) {
            return true;
        }
        if (VERSION.SDK_INT >= 21 && isOnExtSdCard(file, context)) {
            return getDocumentFile(file, false, context).delete();
        }
        if (VERSION.SDK_INT == 19) {
            try {
                context.getContentResolver().delete(MediaStoreHack.getUriFromFile(file.getAbsolutePath(), context), null, null);
                if (file.exists()) {
                    return false;
                }
                return true;
            } catch (Exception e) {
                Log.e("FileUtils", "Error when deleting file " + file.getAbsolutePath(), e);
                return false;
            }
        } else if (file.exists()) {
            return false;
        } else {
            return true;
        }
    }

    public static final boolean deleteFilesInFolder(File folder, Context context) {
        boolean totalSuccess = true;
        if (folder == null) {
            return false;
        }
        if (folder.isDirectory()) {
            for (File deleteFilesInFolder : folder.listFiles()) {
                deleteFilesInFolder(deleteFilesInFolder, context);
            }
            if (!folder.delete()) {
                totalSuccess = false;
            }
        } else if (!folder.delete()) {
            totalSuccess = false;
        }
        return totalSuccess;
    }

    @TargetApi(19)
    public static String[] getExtSdCardPathsForActivity(Context context) {
        List<String> paths = new ArrayList();
        for (File file : context.getExternalFilesDirs("external")) {
            if (file != null) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index < 0) {
                    Log.w("FileUtils", "Unexpected external file dir: " + file.getAbsolutePath());
                } else {
                    String path = file.getAbsolutePath().substring(0, index);
                    try {
                        path = new File(path).getCanonicalPath();
                    } catch (IOException e) {
                    }
                    paths.add(path);
                }
            }
        }
        if (paths.isEmpty()) {
            paths.add("/storage/sdcard1");
        }
        return (String[]) paths.toArray(new String[0]);
    }

    @TargetApi(19)
    public static String[] getExtSdCardPaths(Context context) {
        List<String> paths = new ArrayList();
        for (File file : ContextCompat.getExternalFilesDirs(context, "external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index < 0) {
                    Log.w("FileUtils", "Unexpected external file dir: " + file.getAbsolutePath());
                } else {
                    String path = file.getAbsolutePath().substring(0, index);
                    try {
                        path = new File(path).getCanonicalPath();
                    } catch (IOException e) {
                    }
                    paths.add(path);
                }
            }
        }
        if (paths.isEmpty()) {
            paths.add("/storage/sdcard1");
        }
        return (String[]) paths.toArray(new String[0]);
    }

    @TargetApi(19)
    public static String getExtSdCardFolder(File file, Context context) {
        String[] extSdPaths = getExtSdCardPaths(context);
        int i = 0;
        while (i < extSdPaths.length) {
            try {
                if (file.getCanonicalPath().startsWith(extSdPaths[i])) {
                    return extSdPaths[i];
                }
                i++;
            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    @TargetApi(19)
    public static boolean isOnExtSdCard(File file, Context c) {
        return getExtSdCardFolder(file, c) != null;
    }

    public static DocumentFile getDocumentFile(File file, boolean isDirectory, Context context) {
        String baseFolder = getExtSdCardFolder(file, context);
        boolean originalDirectory = false;
        if (baseFolder == null) {
            return null;
        }
        String relativePath = null;
        try {
            String fullPath = file.getCanonicalPath();
            if (baseFolder.equals(fullPath)) {
                originalDirectory = true;
            } else {
                relativePath = fullPath.substring(baseFolder.length() + 1);
            }
        } catch (IOException e) {
            return null;
        } catch (Exception e2) {
            originalDirectory = true;
        }
        String as = (String) PreferenceHelper.getValueFromPreference(context, String.class, "URI", null);
        Uri treeUri = null;
        if (as != null) {
            treeUri = Uri.parse(as);
        }
        if (treeUri == null) {
            return null;
        }
        DocumentFile document = DocumentFile.fromTreeUri(context, treeUri);
        if (originalDirectory) {
            return document;
        }
        String[] parts = relativePath.split("\\/");
        for (int i = 0; i < parts.length; i++) {
            DocumentFile nextDocument = document.findFile(parts[i]);
            if (nextDocument == null) {
                if (i < parts.length - 1 || isDirectory) {
                    nextDocument = document.createDirectory(parts[i]);
                } else {
                    nextDocument = document.createFile("image", parts[i]);
                }
            }
            document = nextDocument;
        }
        return document;
    }

    public static OutputStream getOutputStream(@NonNull File target, Context context, long s) throws Exception {
        OutputStream outStream = null;
        try {
            if (isWritable(target)) {
                outStream = new FileOutputStream(target);
            } else if (VERSION.SDK_INT >= 21) {
                outStream = context.getContentResolver().openOutputStream(getDocumentFile(target, false, context).getUri());
            } else if (VERSION.SDK_INT == 19) {
                return MediaStoreHack.getOutputStream(context, target.getPath());
            }
        } catch (Exception e) {
            Log.e("AmazeFileUtils", "Error when copying file from " + target.getAbsolutePath(), e);
        }
        return outStream;
    }

    public static boolean mkfile(File file, Context context) throws IOException {
        boolean b = true;
        boolean z = true;
        boolean z2 = false;
        if (file == null) {
            return z2;
        }
        if (file.exists()) {
            if (file.isDirectory()) {
                z = z2;
            }
            return z;
        }
        try {
            if (file.createNewFile()) {
                try {
                    insertMediaQUERY(context, file);
                } catch (Exception e) {
                }
                return true;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (VERSION.SDK_INT >= 21 && isOnExtSdCard(file, context)) {
            try {
                if (getDocumentFile(file.getParentFile(), true, context).createFile(getMimeType(file.getAbsolutePath()), file.getName()) == null) {
                    b = z2;
                }
                try {
                    insertMediaQUERY(context, file);
                } catch (Exception e3) {
                }
                return b;
            } catch (Exception e4) {
                e4.printStackTrace();
                return z2;
            }
        } else if (VERSION.SDK_INT != 19) {
            return z2;
        } else {
            try {
                return MediaStoreHack.mkfile(context, file);
            } catch (Exception e5) {
                return z2;
            }
        }
    }

    private static void insertMediaQUERY(Context context, File file) {
        context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
        try {
            ContentValues values = new ContentValues();
            values.put("_data", file.getPath());
            values.put("mime_type", getMimeType(file.getPath()));
            context.getContentResolver().insert(Files.getContentUri("external"), values);
            context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
        } catch (Exception e) {
        }
    }

    public static String getMimeType(String filePath) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
        if (extension != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return null;
    }

    public static void deleteFileFromMediaStore(ContentResolver contentResolver, File file) {
        if (VERSION.SDK_INT >= 11) {
            String canonicalPath;
            try {
                canonicalPath = file.getCanonicalPath();
            } catch (IOException e) {
                canonicalPath = file.getAbsolutePath();
            }
            Uri uri = Files.getContentUri("external");
            if (contentResolver.delete(uri, "_data=?", new String[]{canonicalPath}) == 0) {
                if (!file.getAbsolutePath().equals(canonicalPath)) {
                    contentResolver.delete(uri, "_data=?", new String[]{file.getAbsolutePath()});
                }
            }
        }
    }

    public static boolean exists(String path) {
        return new File(path).exists();
    }
}
