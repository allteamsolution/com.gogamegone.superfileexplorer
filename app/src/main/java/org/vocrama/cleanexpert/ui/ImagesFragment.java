package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.callback.OnMediaFileClickListener;
import org.vocrama.cleanexpert.ui.Constants.database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ImagesFragment extends Fragment {

    public static ImagesFragment getInstance() {
        return new ImagesFragment();
    }

    public ImagesFragment() {

    }

    public static ImageAdapter adapter;
    public static String path;
    public static int pathdmeo;
    private final String[] projection = new String[]{"_id", "_display_name", "_data"};
    ActionMode actionMode;
    int countSelected;
    FileManager fileManager;
    RecyclerView grid_image;
    ProgressDialog progressDialog;
    ArrayList<MediaFile> selectedImage = new ArrayList<>();
    Uri uri = Media.EXTERNAL_CONTENT_URI;

    boolean isThreadStarted = false;
    private FileManager.OnFileLoadingCallback listener;
    public final ArrayList<MediaFile> mediaItemList = new ArrayList<>();

    private CompositeDisposable compositeDisposable;

    private final Callback callback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.image_context_menu, menu);
            MenuItem mi = menu.getItem(0);
            mi.setTitle(mi.getTitle().toString());
            ImagesFragment.this.actionMode = mode;
            ImagesFragment.this.countSelected = 0;
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.image_id:
                    ImagesFragment.this.deleteImage(mode);
                    break;
                case R.id.image_share:
                    ImagesFragment.this.shareImage(mode);
                    break;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            if (ImagesFragment.this.countSelected > 0) {
                ImagesFragment.this.deselectAll();
            }
            ImagesFragment.this.actionMode = null;
        }
    };

    private final OnMediaFileClickListener onItemClickListener = new OnMediaFileClickListener() {
        @Override
        public void onClick(int position) {
            if (ImagesFragment.this.countSelected > 0) {
                if (ImagesFragment.this.actionMode == null) {
                    ImagesFragment.this.actionMode = requireActivity().startActionMode(ImagesFragment.this.callback);
                }
                ImagesFragment.this.toggleSelection(position);
                ImagesFragment.this.actionMode.setTitle(ImagesFragment.this.countSelected + " " + "Selected");
                if (ImagesFragment.this.countSelected == 0) {
                    ImagesFragment.this.actionMode.finish();
                }
            } else if (ImagesFragment.this.countSelected == 0) {
                ImagesFragment.pathdmeo = mediaItemList.size();
                ImagesFragment.path = ((MediaFile) mediaItemList.get(position)).path;
                Intent i = new Intent(requireActivity(), ShowImage.class);
                i.putParcelableArrayListExtra("images", mediaItemList);
                i.putExtra("pos", position);
                ImagesFragment.this.startActivity(i);
            }
        }

        @Override
        public void onLongClick(int position) {
            if (ImagesFragment.this.actionMode == null) {
                ImagesFragment.this.actionMode = requireActivity().startActionMode(ImagesFragment.this.callback);
            }
            ImagesFragment.this.toggleSelection(position);
            ImagesFragment.this.actionMode.setTitle(ImagesFragment.this.countSelected + " " + "Selected");
            if (ImagesFragment.this.countSelected == 0) {
                ImagesFragment.this.actionMode.finish();
            }
        }
    };

    @RequiresApi(api = 19)
    public static String checkSDCARD(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_images, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listener = (FileManager.OnFileLoadingCallback) requireActivity();
        compositeDisposable = new CompositeDisposable();

        this.grid_image = view.findViewById(R.id.grid_image);
        this.fileManager = new FileManager();

        adapter = new ImageAdapter(mediaItemList, onItemClickListener);
        grid_image.setLayoutManager(new GridLayoutManager(requireContext(), 4, RecyclerView.VERTICAL, false));
        grid_image.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getImages();
    }

    private List<MediaFile> cursorLoadImages() {
        Cursor cursor = requireActivity().getContentResolver().query(ImagesFragment.this.uri, ImagesFragment.this.projection, null, null, "date_added");
        ArrayList<MediaFile> temp = new ArrayList<>(cursor.getCount());
        if (cursor.moveToLast()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(ImagesFragment.this.projection[0]));
                String name = cursor.getString(cursor.getColumnIndex(ImagesFragment.this.projection[1]));
                String path = cursor.getString(cursor.getColumnIndex(ImagesFragment.this.projection[2]));
                File file = new File(path);
                if (file.exists()) {
                    temp.add(new MediaFile(id, name, path, false, "image", file.length()));
                }
            } while (cursor.moveToPrevious());
        }
        cursor.close();
        return temp;
    }


    private void getImages() {
        if (isThreadStarted)
            return;

        if (mediaItemList.isEmpty()) {
            compositeDisposable.add(Observable.
                    fromIterable(cursorLoadImages())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> {
                        isThreadStarted = true;
                        listener.onFileLoading();
                    })
                    .doOnNext(mediaItemList::add)
                    .doOnError(throwable -> {
                        isThreadStarted = false;
                        listener.onFileLoaded();
                    })
                    .doOnComplete(() -> {
                        isThreadStarted = false;
                        listener.onFileLoaded();
                        adapter.notifyDataSetChanged();
                    })
                    .subscribe());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    private void toggleSelection(int position) {
        ((MediaFile) mediaItemList.get(position)).isSelected = !((MediaFile) mediaItemList.get(position)).isSelected;
        if (((MediaFile) mediaItemList.get(position)).isSelected) {
            this.selectedImage.add(mediaItemList.get(position));
            this.countSelected++;
        } else {
            this.selectedImage.remove(mediaItemList.get(position));
            this.countSelected--;
        }
        adapter.notifyDataSetChanged();
    }

    private void shareImage(ActionMode mode) {
        Intent ii = new Intent();
        ii.setAction("android.intent.action.SEND_MULTIPLE");
        ii.setType("image/*");
        ArrayList<MediaFile> checkedItems = this.selectedImage;
        ArrayList<Uri> share = new ArrayList(this.selectedImage.size());
        for (int i = 0; i < checkedItems.size(); i++) {
            String d_path = ((MediaFile) this.selectedImage.get(i)).path;
            File file = new File(d_path);
            if (VERSION.SDK_INT >= 24) {
                this.uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
            } else {
                this.uri = Uri.fromFile(file);
            }
            share.add(this.uri);
            Log.e(database.APPLIST_PATH, "" + d_path);
        }
        ii.putParcelableArrayListExtra("android.intent.extra.STREAM", share);
        startActivity(Intent.createChooser(ii, ""));
        this.selectedImage.clear();
        adapter.notifyDataSetChanged();
        mode.finish();
    }

    private void deleteImage(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage((CharSequence) "Are You Sure Want To Delete ?");
        builder.setTitle((CharSequence) "Delete Images");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(v -> {
            new DeleteTask().execute(new Void[0]);
            alertDialog.dismiss();
            mode.finish();
        });
        alertDialog.getButton(-2).setOnClickListener(v -> {
            alertDialog.dismiss();
            mode.finish();
        });
    }

    private void deselectAll() {
        int l = mediaItemList.size();
        for (int i = 0; i < l; i++) {
            ((MediaFile) mediaItemList.get(i)).isSelected = false;
        }
        this.countSelected = 0;
        adapter.notifyDataSetChanged();
    }

    private void scanFile(File file) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                ImagesFragment.this.getActivity().getContentResolver().delete(uri, null, null);
            }
        });
    }

    private class DeleteTask extends AsyncTask<Void, Void, Void> {
        private DeleteTask() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            ImagesFragment.this.progressDialog = new ProgressDialog(ImagesFragment.this.getActivity());
            ImagesFragment.this.progressDialog.setProgressStyle(0);
            ImagesFragment.this.progressDialog.setCancelable(false);
            ImagesFragment.this.progressDialog.setTitle("Delete");
            ImagesFragment.this.progressDialog.setMessage("Please Wait.....");
            ImagesFragment.this.progressDialog.show();
        }

        @RequiresApi(api = 19)
        protected Void doInBackground(Void... params) {
            ArrayList<MediaFile> checkedItems = ImagesFragment.this.selectedImage;
            for (int i = 0; i < checkedItems.size(); i++) {
                String S1 = ((MediaFile) ImagesFragment.this.selectedImage.get(i)).path;
                if (S1.startsWith("/storage/emulated/0/")) {
                    ImagesFragment.this.scanFile(new File(S1));
                } else if (S1.startsWith(ImagesFragment.checkSDCARD(ImagesFragment.this.getActivity()))) {
                    if (VERSION.SDK_INT < 24) {
                        ImagesFragment.this.scanFile(new File(S1));
                    } else {
                        try {
                            File f = new File(S1);
                            if (StorageHelper.deleteFile(ImagesFragment.this.getActivity(), f)) {
                                Toast.makeText(ImagesFragment.this.getActivity(), "Done...", Toast.LENGTH_SHORT).show();
                                ImagesFragment.this.scanFile(f);
                                if (f.delete()) {
                                    ImagesFragment.this.scanFile(f);
                                }
                            } else {
                                Toast.makeText(ImagesFragment.this.getActivity(), "Error...", 0).show();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                FileManager.delete(ImagesFragment.this.requireActivity(), new File(S1));
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ImagesFragment.this.selectedImage.clear();
            mediaItemList.clear();
            ImagesFragment.this.getImages();
            ImagesFragment.this.progressDialog.dismiss();
        }
    }
}
