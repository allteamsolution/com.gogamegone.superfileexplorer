package org.vocrama.cleanexpert.ui;

import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DecompressFast {
    String _location;
    String _zipFile;

    public DecompressFast(String zipFile, String location) {
        this._zipFile = zipFile;
        this._location = location;
        _dirChecker("");
    }

    public void unzip() {
        try {
            ZipInputStream zin = new ZipInputStream(new FileInputStream(this._zipFile));
            while (true) {
                ZipEntry ze = zin.getNextEntry();
                if (ze != null) {
                    Log.v("Decompress", "Unzipping " + ze.getName());
                    if (ze.isDirectory()) {
                        _dirChecker(ze.getName());
                    } else {
                        FileOutputStream fout = new FileOutputStream(this._location + ze.getName());
                        BufferedOutputStream bufout = new BufferedOutputStream(fout);
                        byte[] buffer = new byte[1024];
                        while (true) {
                            int read = zin.read(buffer);
                            if (read == -1) {
                                break;
                            }
                            bufout.write(buffer, 0, read);
                        }
                        bufout.close();
                        zin.closeEntry();
                        fout.close();
                    }
                } else {
                    zin.close();
                    Log.d("Unzip", "Unzipping complete. path :  " + this._location);
                    return;
                }
            }
        } catch (Exception e) {
            Log.e("Decompress", "unzip", e);
            Log.d("Unzip", "Unzipping failed");
        }
    }

    private void _dirChecker(String dir) {
        File f = new File(this._location + dir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }
}
