package org.vocrama.cleanexpert.ui;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.callback.OnMediaFileClickListener;

import java.io.File;
import java.util.ArrayList;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {

    private final ArrayList<MediaFile> images;
    private final OnMediaFileClickListener listener;

    public ImageAdapter(ArrayList<MediaFile> images, OnMediaFileClickListener listener) {
        this.images = images;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_image_adapter, parent, false);
        return new ImageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, int position) {
        MediaFile mediaFile = images.get(position);
        holder.bind(mediaFile, listener);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class ImageHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private FrameLayout fmImage;
        private View view;

        public ImageHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            fmImage = itemView.findViewById(R.id.fm_image);
            view = itemView.findViewById(R.id.view_alpha);
        }

        public void bind(MediaFile mediaFile, OnMediaFileClickListener listener) {
            Glide
                    .with(itemView.getContext())
                    .load(new File(mediaFile.path))
                    .placeholder((int) R.drawable.image_place)
                    .error((int) R.drawable.image_place)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(image);

            if (mediaFile.isSelected) {
                view.setAlpha(0.5f);
                fmImage.setForeground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_check_box_black_24dp));
            } else {
                view.setAlpha(0.0f);
                fmImage.setForeground(null);
            }

            itemView.setOnClickListener(v -> {
                listener.onClick(getAdapterPosition());
            });

            itemView.setOnLongClickListener(v -> {
                listener.onLongClick(getAdapterPosition());
                return true;
            });
        }
    }
}
