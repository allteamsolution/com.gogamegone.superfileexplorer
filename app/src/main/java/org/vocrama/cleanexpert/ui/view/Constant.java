package org.vocrama.cleanexpert.ui.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Constant {
    public static boolean all_flag = true;
    public static boolean copy_flag = true;
    public static boolean flagForVisibleButton = false;
    private static Constant instance = null;
    public static boolean isExternalPath = false;
    public static boolean isInternalPath = false;
    public static boolean move_flag = true;
    public static SharedPreferences preferences;
    public static boolean secret_flag = true;

    public Constant(Context context) {
    }

    public static Constant getInstance(Context context) {
        if (instance == null) {
            if (context == null) {
                throw new IllegalStateException(Constant.class.getSimpleName() + " is not initialized, call getInstance(Context) with a VALID Context first.");
            }
            instance = new Constant(context.getApplicationContext());
        }
        return instance;
    }

    public void setOrderType(Context context, OrderType orderType) {
        preferences = context.getSharedPreferences("File", 0);
        Editor editor = preferences.edit();
        editor.putString("Order", orderType.name());
        editor.commit();
    }

    public OrderType getOrderType(Context context) {
        preferences = context.getSharedPreferences("File", 0);
        return OrderType.valueOf(preferences.getString("Order", OrderType.NAME.name()));
    }
}
