package org.vocrama.cleanexpert.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.gogamegone.superfileexplorer.R;

import java.util.Timer;
import java.util.TimerTask;

public class PiechartView extends View {
    int angle;
    int backgroundColor;
    int color;

    public PiechartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PiechartView, 0, 0);
        int count = typedArray.getIndexCount();
        for (int i = 0; i < count; i++) {
            int indexName = typedArray.getIndex(i);
            switch (indexName) {
                case 0:
                    this.angle = typedArray.getInt(indexName, 0);
                    break;
                case 1:
                    this.backgroundColor = typedArray.getColor(indexName, -16711936);
                    break;
                case 2:
                    this.color = typedArray.getColor(indexName, -16776961);
                    break;
                default:
                    break;
            }
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (width > height) {
            width = height;
        } else {
            height = width;
        }
        setMeasuredDimension(width, height);
    }

    protected void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(this.backgroundColor);
        paint.setAntiAlias(true);
        RectF oval = new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        canvas.drawArc(oval, 0.0f, 360.0f, true, paint);
        paint.setColor(this.color);
        canvas.drawArc(oval, -90.0f, (float) this.angle, true, paint);
    }

    public void showPiechart(final int targetAngle) {
        this.angle = 0;
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                PiechartView piechartView = PiechartView.this;
                piechartView.angle += 4;
                if (PiechartView.this.angle > targetAngle) {
                    PiechartView.this.angle = targetAngle;
                    PiechartView.this.postInvalidate();
                    timer.cancel();
                }
                PiechartView.this.postInvalidate();
            }
        }, 40, 40);
    }
}
