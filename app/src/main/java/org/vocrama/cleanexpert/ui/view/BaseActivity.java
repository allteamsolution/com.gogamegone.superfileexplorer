package org.vocrama.cleanexpert.ui.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    public void startActivity(Class targetClass) {
        startActivity(new Intent(this, targetClass));
    }

    public void startActivity(Class targetClass, Bundle bundle) {
        Intent intent = new Intent(this, targetClass);
        intent.putExtra("bundle", bundle);
        startActivity(intent);
    }

    public void startActivity(String action, Uri data) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.setData(data);
        startActivity(intent);
    }

    public void startService(Class targetClass) {
        startService(new Intent(this, targetClass));
    }

    public void stopService(Class targetClass) {
        stopService(new Intent(this, targetClass));
    }
}
