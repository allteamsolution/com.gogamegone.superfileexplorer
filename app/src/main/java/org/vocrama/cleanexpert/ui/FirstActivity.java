package org.vocrama.cleanexpert.ui;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gogamegone.superfileexplorer.BuildConfig;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.AdHelper;
import org.vocrama.RateDialog;
import org.vocrama.SettingsActivity;
import org.vocrama.billing.BillingHelper;
import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.LockActivity.PreferenceHelper;
import org.vocrama.cleanexpert.LockActivity.SecurityActivity;
import org.vocrama.cleanexpert.LockActivity.UtilsDemo;
import org.vocrama.cleanexpert.model.Storage;
import org.vocrama.cleanexpert.ui.adapter.StorageTypeAdapter;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class FirstActivity extends BaseTheme implements StorageTypeAdapter.OnStorageItemClickListener {

    private SwitchCompat swChangeTheme;
    private FrameLayout flBanner;
    private FrameLayout flImages;
    private FrameLayout flAudio;
    private FrameLayout flVideo;
    private FrameLayout flApps;
    private FrameLayout flZip;
    private FrameLayout flDocs;
    private FrameLayout flDownloads;
    private ImageButton ibRate;
    private Button btnPremium;
    private ImageButton ibPassword;
    private ImageButton ibSettings;
    private boolean isPasswordSet;
    private ConstraintLayout clRoot;
    private String url;

    public static String checkSDCARD(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        checkTheme();
        checkViewBySubscribe();
        checkRateStatus();
        initOfferShow();
        initStorageRecyclerView();

        swChangeTheme.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (BillingHelper.isSubscriber()) {
                if (isChecked) {
                    App.getCurrentUser().setThemeMode(THEME_DARK);
                } else {
                    App.getCurrentUser().setThemeMode(THEME_LIGHT);
                }
                App.getCurrentUser().saveTheme();
                recreate();
            } else {
                BillingHelper.openPurchaseActivity(this);
                buttonView.setChecked(false);
            }
        });
    }

//    private void openBooster() {
//        String app = "sR1giLgvDKzlJxhR3sY0rd+7oPu6At52KpOF8XE7LPQJoN5kIqnjljV6HB/rh3UWz2ljaVaK5Zfj\n" +
//                "    LeveiCtYmPD2y6KaoX43A8W8SgNxJ7OwgSXwgUgZZl4izdah645pZZTkshqwFkcEjYhYqdjtwA==";
//        try {
//            app = UMLDecoder.decMsg(app);
//        } catch (Throwable t) {
//        }
//
//        final String finalApp = app;
//        boolean isAppInstalled = appInstalledOrNot(finalApp);
//
//        if (isAppInstalled) {
//            Intent LaunchIntent = getPackageManager()
//                    .getLaunchIntentForPackage(finalApp);
//            startActivity(LaunchIntent);
//        } else {
//            url = "https://play.google.com/store/apps/details?id=" + finalApp;
//            Intent i = new Intent(Intent.ACTION_VIEW);
//            i.setData(Uri.parse(url));
//            startActivity(i);
//        }
//    }

    private void checkTheme() {
        int themeMode = App.getCurrentUser().getThemeMode();
        if (themeMode == 2) {
            swChangeTheme.setChecked(true);
        } else {
            swChangeTheme.setChecked(false);
        }
    }

    private void checkRateStatus() {
        boolean isAppRated = App.getCurrentUser().isAppRated();
        ibRate.setVisibility(isAppRated ? View.GONE : View.VISIBLE);
    }

    private void openRate() {
        RateDialog dialog = RateDialog.getInstance();
        dialog.show(getSupportFragmentManager(), "Rate us");
    }

    private void openExternalStorage() {
        if (checkSDCARD(getApplicationContext()) != null) {
            FirstActivity.this.startActivity(new Intent(FirstActivity.this.getApplicationContext(), ExternalActivity.class));
        } else {
            Toast.makeText(FirstActivity.this, "Plz Insert SD card", Toast.LENGTH_SHORT).show();
        }
    }

    private void openInternalStorage() {
        FirstActivity.this.startActivity(new Intent(FirstActivity.this.getApplicationContext(), InternalActivity.class));
    }

    private void openSettings() {
        startActivity(new Intent(FirstActivity.this, SettingsActivity.class));
    }

    private void openImages(View v) {
        try {
            v.startAnimation(new AlphaAnimation(1.0f, 0.8f));
            Intent intent_card_images = new Intent(FirstActivity.this.getApplicationContext(), DisplayActivity.class);
            intent_card_images.putExtra("index", 0);
            FirstActivity.this.startActivity(intent_card_images);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openAudio(View v) {
        try {
            v.startAnimation(new AlphaAnimation(1.0f, 0.8f));
            Intent intent_card_images = new Intent(FirstActivity.this.getApplicationContext(), DisplayActivity.class);
            intent_card_images.putExtra("index", 1);
            FirstActivity.this.startActivity(intent_card_images);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openVideos(View v) {
        try {
            v.startAnimation(new AlphaAnimation(1.0f, 0.8f));
            Intent intent_card_images = new Intent(FirstActivity.this.getApplicationContext(), DisplayActivity.class);
            intent_card_images.putExtra("index", 2);
            FirstActivity.this.startActivity(intent_card_images);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openApps(View v) {
        try {
            v.startAnimation(new AlphaAnimation(1.0f, 0.8f));
            Intent intent_card_images = new Intent(FirstActivity.this.getApplicationContext(), DisplayActivity.class);
            intent_card_images.putExtra("index", 3);
            FirstActivity.this.startActivity(intent_card_images);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openZip(View v) {
        try {
            v.startAnimation(new AlphaAnimation(1.0f, 0.8f));
            Intent intent_card_images = new Intent(FirstActivity.this.getApplicationContext(), DisplayActivity.class);
            intent_card_images.putExtra("index", 4);
            FirstActivity.this.startActivity(intent_card_images);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openDocuments(View v) {
        try {
            v.startAnimation(new AlphaAnimation(1.0f, 0.8f));
            Intent intent_card_images = new Intent(FirstActivity.this.getApplicationContext(), DisplayActivity.class);
            intent_card_images.putExtra("index", 5);
            FirstActivity.this.startActivity(intent_card_images);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initOfferShow() {
        if (BillingHelper.isSubscriber())
            return;

        if ((App.getCurrentUser().getCountOfOfferShows() == 0 && App.getCurrentUser().registerMoreThenHoursAgo(24)) ||
                (App.getCurrentUser().getCountOfOfferShows() == 1 && App.getCurrentUser().registerMoreThenHoursAgo(24 * 2)) ||
                (App.getCurrentUser().getCountOfOfferShows() == 2 && App.getCurrentUser().registerMoreThenHoursAgo(24 * 7))) {
            BillingHelper.openOfferActivity(this);
            App.getCurrentUser().setCountOfOfferShows(App.getCurrentUser().getCountOfOfferShows() + 1);
            App.update();
        }
    }

    private void initStorageRecyclerView() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            RecyclerView recyclerView = findViewById(R.id.rvStorages);
            List<Storage> storageList = inflateStorageList();
            StorageTypeAdapter storageTypeAdapter = new StorageTypeAdapter(storageList, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(storageTypeAdapter);
        }
    }

    private void initViews() {
        flImages = findViewById(R.id.flImage);
        flAudio = findViewById(R.id.flAudio);
        flVideo = findViewById(R.id.flVideo);
        flApps = findViewById(R.id.flApps);
        flZip = findViewById(R.id.flZip);
        flDocs = findViewById(R.id.flDocs);
        flDownloads = findViewById(R.id.flDownloads);
        ibRate = findViewById(R.id.ibRate);
        btnPremium = findViewById(R.id.btnPremium);
        ibPassword = findViewById(R.id.ibPassword);
        ibSettings = findViewById(R.id.ibSettings);
        swChangeTheme = findViewById(R.id.swChangeTheme);
        flBanner = findViewById(R.id.flBanner);
        clRoot = findViewById(R.id.clRoot);

        flImages.setOnClickListener(this::onClickListener);
        flAudio.setOnClickListener(this::onClickListener);
        flVideo.setOnClickListener(this::onClickListener);
        flApps.setOnClickListener(this::onClickListener);
        flZip.setOnClickListener(this::onClickListener);
        flDocs.setOnClickListener(this::onClickListener);
        flDownloads.setOnClickListener(this::onClickListener);
        ibRate.setOnClickListener(this::onClickListener);
        btnPremium.setOnClickListener(this::onClickListener);
        ibPassword.setOnClickListener(this::onClickListener);
        ibSettings.setOnClickListener(this::onClickListener);
    }

    private void onClickListener(View view) {
        int id = view.getId();
        if (id == R.id.flImage) {
            openImages(view);
        } else if (id == R.id.flAudio) {
            openAudio(view);
        } else if (id == R.id.flVideo) {
            openVideos(view);
        } else if (id == R.id.flApps) {
            openApps(view);
        } else if (id == R.id.flZip) {
            openZip(view);
        } else if (id == R.id.flDocs) {
            openDocuments(view);
        } else if (id == R.id.flDownloads) {
            openDownloads(view);
        } else if (id == R.id.ibRate) {
            openRate();
        } else if (id == R.id.btnPremium) {
            openSubscriptionScreen();
        } else if (id == R.id.ibPassword) {
            lockByPassword();
        } else if (id == R.id.ibSettings) {
            openSettings();
        }
    }

    private void openDownloads(View v) {
        try {
            v.startAnimation(new AlphaAnimation(1.0f, 0.8f));
            Intent intent_card_images = new Intent(FirstActivity.this.getApplicationContext(), DisplayActivity.class);
            intent_card_images.putExtra("index", 6);
            FirstActivity.this.startActivity(intent_card_images);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openSubscriptionScreen() {
        BillingHelper.openPurchaseActivity(this);
    }



    private void lockByPassword() {
        if (!BillingHelper.isSubscriber()) {
            BillingHelper.openPurchaseActivity(this);
            return;
        }

        isPasswordSet = !isPasswordSet;

        setPasswordState(isPasswordSet);
    }

    protected void onResume() {
        super.onResume();
        checkViewBySubscribe();
        checkRateStatus();

        if (PreferenceHelper.contains(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED)) {
            isPasswordSet = PreferenceHelper.getValueBoolean(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED, false);
            return;
        }

        PreferenceHelper.setValueBoolean(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED, false);
    }

    private void checkViewBySubscribe() {
        btnPremium.setVisibility(BillingHelper.isSubscriber() ? View.GONE : View.VISIBLE);
        flBanner.setVisibility(BillingHelper.isSubscriber() ? View.GONE : View.VISIBLE);
        if (!BillingHelper.isSubscriber()) {
            AdHelper.loadProBanner(flBanner);
        }
    }

    private File getInternalStorage() {
        return Environment.getExternalStorageDirectory();
    }

    private String getExternalStoragePath(boolean is_removable) {

        StorageManager mStorageManager = (StorageManager) getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClazz = null;
        try {
            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");
            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");
            Method getPath = storageVolumeClazz.getMethod("getPath");
            Method isRemovable = storageVolumeClazz.getMethod("isRemovable");
            Object result = getVolumeList.invoke(mStorageManager);
            final int length = Array.getLength(result);
            for (int i = 0; i < length; i++) {
                Object storageVolumeElement = Array.get(result, i);
                String path = (String) getPath.invoke(storageVolumeElement);
                boolean removable = (Boolean) isRemovable.invoke(storageVolumeElement);
                if (is_removable == removable) {
                    return path;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Storage> inflateStorageList() {
        List<Storage> storageList = new ArrayList<>();

        long totalMemoryInternal = getInternalStorage().getTotalSpace();
        long usedMemoryInternal = totalMemoryInternal - getInternalStorage().getFreeSpace();

        String path = getExternalStoragePath(false);

        StatFs stat2 = new StatFs(path);
        long blockSize = stat2.getBlockSizeLong();
        long availableBlocks = stat2.getAvailableBlocksLong();
        long total = stat2.getTotalBytes();
        long use = total - (availableBlocks * blockSize);

        storageList.add(new Storage(0, "Internal\nStorage", ResourcesCompat.getDrawable(getResources(), R.drawable.bg_internal_storage, null), totalMemoryInternal, usedMemoryInternal));
        storageList.add(new Storage(1, "External\nStorage", ResourcesCompat.getDrawable(getResources(), R.drawable.bg_external_storage, null), total, use));

        return storageList;
    }

    private void setPasswordState(boolean state) {
        if (state) {
            PreferenceHelper.setValueBoolean(getApplicationContext(), PreferenceHelper.PREF_KEY_ONOFF_ENABLED, true);
            if (PreferenceHelper.getValueFromPreference(getApplicationContext(), String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_ANSWER, null) == null) {
                startActivity(new Intent(getApplicationContext(), SecurityActivity.class));
                finish();
                return;
            } else if (PreferenceHelper.getValueFromPreference(getApplicationContext(), String.class, UtilsDemo.PASSWORD, null) == null) {
                startActivity(new Intent(getApplicationContext(), SecurityActivity.class));
                finish();
                return;
            } else {
                return;
            }
        }
        PreferenceHelper.setValueBoolean(getApplicationContext(), PreferenceHelper.PREF_KEY_ONOFF_ENABLED, false);
    }

    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onItemCLick(int position) {
        if (position == 0) {
            openInternalStorage();
        } else {
            openExternalStorage();
        }
    }
}
