package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.os.Build.VERSION;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.CircleSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.sprite.SpriteContainer;

public class DoubleBounce extends SpriteContainer {

    private class Bounce extends CircleSprite {
        Bounce() {
            setAlpha(153);
            setScale(0.0f);
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.5f, 1.0f};
            return new SpriteAnimatorBuilder(this).scale(fractions, Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.0f)).duration(2000).easeInOut(fractions).build();
        }
    }

    public Sprite[] onCreateChild() {
        return new Sprite[]{new Bounce(), new Bounce()};
    }

    public void onChildCreated(Sprite... sprites) {
        super.onChildCreated(sprites);
        if (VERSION.SDK_INT >= 24) {
            sprites[1].setAnimationDelay(1000);
        } else {
            sprites[1].setAnimationDelay(-1000);
        }
    }
}
