package org.vocrama.cleanexpert.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.view.ImageCropView;

public class CropActivity extends BaseTheme {
    public static final String TAG = "CropActivity";
    ImageView img_back_crop;
    Toolbar toolbar;
    TextView txt_save_crop;
    ProgressDialog progressDialog;
    private ImageCropView imageCropView;
    private float[] positionInfo;

    @SuppressLint({"WrongViewCast"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_crop);
        this.imageCropView =  findViewById(R.id.image1);
        this.img_back_crop = (ImageView) findViewById(R.id.img_back_crop);
        this.txt_save_crop = (TextView) findViewById(R.id.txt_save_crop);
        this.toolbar = (Toolbar) findViewById(R.id.tool_bareCrop);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setTitle((CharSequence) "");
        Uri uri = getIntent().getData();
        this.img_back_crop.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                CropActivity.this.finish();
            }
        });
        this.txt_save_crop.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (!CropActivity.this.imageCropView.isChangingScale()) {
                    Bitmap b = CropActivity.this.imageCropView.getCroppedImage();
                    if (b != null) {
                        try {
                            save(b);
                        } catch (Throwable t) {
                        }
                    } else {
                        Toast.makeText(CropActivity.this, R.string.fail_to_crop, 0).show();
                    }
                }
            }
        });
        this.imageCropView.setImageFilePath(((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path);
        this.imageCropView.setAspectRatio(1, 1);
        findViewById(R.id.ratio11btn).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Log.d(CropActivity.TAG, "click 1 : 1");
                if (CropActivity.this.isPossibleCrop(1, 1)) {
                    CropActivity.this.imageCropView.setAspectRatio(1, 1);
                } else {
                    Toast.makeText(CropActivity.this, R.string.can_not_crop, 0).show();
                }
            }
        });
        findViewById(R.id.ratio34btn).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Log.d(CropActivity.TAG, "click 3 : 4");
                if (CropActivity.this.isPossibleCrop(3, 4)) {
                    CropActivity.this.imageCropView.setAspectRatio(3, 4);
                } else {
                    Toast.makeText(CropActivity.this, R.string.can_not_crop, 0).show();
                }
            }
        });
        findViewById(R.id.ratio43btn).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Log.d(CropActivity.TAG, "click 4 : 3");
                if (CropActivity.this.isPossibleCrop(4, 3)) {
                    CropActivity.this.imageCropView.setAspectRatio(4, 3);
                } else {
                    Toast.makeText(CropActivity.this, R.string.can_not_crop, 0).show();
                }
            }
        });
        findViewById(R.id.ratio169btn).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Log.d(CropActivity.TAG, "click 16 : 9");
                if (CropActivity.this.isPossibleCrop(16, 9)) {
                    CropActivity.this.imageCropView.setAspectRatio(16, 9);
                } else {
                    Toast.makeText(CropActivity.this, R.string.can_not_crop, 0).show();
                }
            }
        });
        findViewById(R.id.ratio916btn).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Log.d(CropActivity.TAG, "click 9 : 16");
                if (CropActivity.this.isPossibleCrop(9, 16)) {
                    CropActivity.this.imageCropView.setAspectRatio(9, 16);
                } else {
                    Toast.makeText(CropActivity.this, R.string.can_not_crop, 0).show();
                }
            }
        });
    }


    /* JADX WARNING: Removed duplicated region for block: B:40:? A:{SYNTHETIC, RETURN} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00af A:{SYNTHETIC, Splitter: B:20:0x00af} */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bb A:{SYNTHETIC, Splitter: B:25:0x00bb} */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bb A:{SYNTHETIC, Splitter: B:25:0x00bb} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00af A:{SYNTHETIC, Splitter: B:20:0x00af} */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A:{SYNTHETIC, RETURN} */
    public java.io.File bitmapConvertToFile(Bitmap r11) {
        /*
        r10 = this;
        r4 = 0;
        r0 = 0;
        r7 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x00a9 }
        r7.<init>();	 Catch:{ Exception -> 0x00a9 }
        r8 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x00a9 }
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r8 = "/FileManager/";
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r8 = "/Zip/";
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r6 = r7.toString();	 Catch:{ Exception -> 0x00a9 }
        r3 = new java.io.File;	 Catch:{ Exception -> 0x00a9 }
        r7 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x00a9 }
        r7.<init>();	 Catch:{ Exception -> 0x00a9 }
        r8 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x00a9 }
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r8 = "/FileManager/";
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r8 = "/CropImages/";
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r7 = r7.toString();	 Catch:{ Exception -> 0x00a9 }
        r3.<init>(r7);	 Catch:{ Exception -> 0x00a9 }
        r7 = r3.exists();	 Catch:{ Exception -> 0x00a9 }
        if (r7 != 0) goto L_0x004a;
    L_0x0047:
        r3.mkdir();	 Catch:{ Exception -> 0x00a9 }
    L_0x004a:
        r1 = new java.io.File;	 Catch:{ Exception -> 0x00a9 }
        r7 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x00a9 }
        r7.<init>();	 Catch:{ Exception -> 0x00a9 }
        r8 = "IMG_";
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r8 = new java.text.SimpleDateFormat;	 Catch:{ Exception -> 0x00a9 }
        r9 = "yyyyMMddHHmmss";
        r8.<init>(r9);	 Catch:{ Exception -> 0x00a9 }
        r9 = java.util.Calendar.getInstance();	 Catch:{ Exception -> 0x00a9 }
        r9 = r9.getTime();	 Catch:{ Exception -> 0x00a9 }
        r8 = r8.format(r9);	 Catch:{ Exception -> 0x00a9 }
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r8 = ".jpg";
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x00a9 }
        r7 = r7.toString();	 Catch:{ Exception -> 0x00a9 }
        r1.<init>(r3, r7);	 Catch:{ Exception -> 0x00a9 }
        r5 = new java.io.FileOutputStream;	 Catch:{ Exception -> 0x00cb, all -> 0x00c4 }
        r5.<init>(r1);	 Catch:{ Exception -> 0x00cb, all -> 0x00c4 }
        r7 = android.graphics.Bitmap.CompressFormat.JPEG;	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        r8 = 100;
        r11.compress(r7, r8, r5);	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        r7 = 1;
        r7 = new java.lang.String[r7];	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        r8 = 0;
        r9 = r1.getAbsolutePath();	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        r7[r8] = r9;	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        r8 = 0;
        r9 = new org.mazhuang.cleanexpert.ui.CropActivity$8;	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        r9.<init>();	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        android.media.MediaScannerConnection.scanFile(r10, r7, r8, r9);	 Catch:{ Exception -> 0x00ce, all -> 0x00c7 }
        if (r5 == 0) goto L_0x00d2;
    L_0x009c:
        r5.flush();	 Catch:{ Exception -> 0x00a5 }
        r5.close();	 Catch:{ Exception -> 0x00a5 }
        r0 = r1;
        r4 = r5;
    L_0x00a4:
        return r0;
    L_0x00a5:
        r7 = move-exception;
        r0 = r1;
        r4 = r5;
        goto L_0x00a4;
    L_0x00a9:
        r2 = move-exception;
    L_0x00aa:
        r2.printStackTrace();	 Catch:{ all -> 0x00b8 }
        if (r4 == 0) goto L_0x00a4;
    L_0x00af:
        r4.flush();	 Catch:{ Exception -> 0x00b6 }
        r4.close();	 Catch:{ Exception -> 0x00b6 }
        goto L_0x00a4;
    L_0x00b6:
        r7 = move-exception;
        goto L_0x00a4;
    L_0x00b8:
        r7 = move-exception;
    L_0x00b9:
        if (r4 == 0) goto L_0x00c1;
    L_0x00bb:
        r4.flush();	 Catch:{ Exception -> 0x00c2 }
        r4.close();	 Catch:{ Exception -> 0x00c2 }
    L_0x00c1:
        throw r7;
    L_0x00c2:
        r8 = move-exception;
        goto L_0x00c1;
    L_0x00c4:
        r7 = move-exception;
        r0 = r1;
        goto L_0x00b9;
    L_0x00c7:
        r7 = move-exception;
        r0 = r1;
        r4 = r5;
        goto L_0x00b9;
    L_0x00cb:
        r2 = move-exception;
        r0 = r1;
        goto L_0x00aa;
    L_0x00ce:
        r2 = move-exception;
        r0 = r1;
        r4 = r5;
        goto L_0x00aa;
    L_0x00d2:
        r0 = r1;
        r4 = r5;
        goto L_0x00a4;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mazhuang.cleanexpert.ui.CropActivity.bitmapConvertToFile(android.graphics.Bitmap):java.io.File");
    }

    private void save(Bitmap b) {
        new CropActivity.ImageSaveTask().execute(new String[0]);
    }

    public Bitmap takeScreenShot() {
        this.imageCropView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(this.imageCropView.getCroppedImage());
        this.imageCropView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    private boolean isPossibleCrop(int widthRatio, int heightRatio) {
        Bitmap bitmap = this.imageCropView.getViewBitmap();
        if (bitmap == null) {
            return false;
        }
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        if (bitmapWidth >= widthRatio || bitmapHeight >= heightRatio) {
            return true;
        }
        return false;
    }

    class ImageSaveTask extends AsyncTask<String, String, String> {
        ImageSaveTask() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            CropActivity.this.progressDialog = new ProgressDialog(CropActivity.this);
            CropActivity.this.progressDialog.setMessage("Saving Photo...");
            CropActivity.this.progressDialog.setIndeterminate(false);
            CropActivity.this.progressDialog.setCancelable(false);
            CropActivity.this.progressDialog.show();
        }

        protected String doInBackground(String... params) {
            new SaveImage().Save(CropActivity.this.getApplicationContext(), CropActivity.this.takeScreenShot());
            return null;
        }

        protected void onPostExecute(String result) {
            CropActivity.this.progressDialog.dismiss();
            Toast.makeText(CropActivity.this.getApplicationContext(), "Photo Saved To My FileManager", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
