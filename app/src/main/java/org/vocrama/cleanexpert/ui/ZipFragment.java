package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.FileUtils;
import com.gogamegone.superfileexplorer.R;
import com.google.gson.Gson;

import org.vocrama.cleanexpert.ui.presenter.IPresenter.IAsyncFragmentPresenter;
import org.vocrama.cleanexpert.ui.presenter.IPresenter.IView;
import org.vocrama.cleanexpert.ui.presenter.ZipPresenter;
import org.vocrama.cleanexpert.ui.view.FileItem;
import org.vocrama.cleanexpert.util.ACache;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ZipFragment extends Fragment implements IView<FileItem> {

    public static ZipFragment getInstance() {
        return new ZipFragment();
    }

    private ZipAdapter mAdapter;
    private GridView mRecyclerView;
    private ArrayList<FileItem> items;
    private FileManager.OnFileLoadingCallback listener;
    private IAsyncFragmentPresenter<FileItem> presenter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_zip, container, false);

        if (presenter == null) {
            presenter = new ZipPresenter();
            presenter.setView(ZipFragment.this);
            presenter.onCreate();
        }

        return inflate;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (FileManager.OnFileLoadingCallback) requireActivity();
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (GridView) view.findViewById(R.id.id_recyclerview);
        presenter.getDataAsync();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.onDestroy();
            presenter = null;
        }
    }

    //    private Thread getDataThread = new Thread(() -> {
//        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
//        getZipDocuments(file, items);
//        App.getUIHandler().post(() -> {
//            mAdapter.setList(items);
//            mAdapter.notifyDataSetChanged();
//            listener.onFileLoaded();
//        });
//    });
//
//    private void getZipDocuments(File dir, List<FileItem> fileItems) {
//        File[] listFile = dir.listFiles();
//        if (listFile != null && listFile.length > 0) {
//            for (File file : listFile) {
//                if (file.isDirectory()) {
//                    getZipDocuments(file, fileItems);
//                } else {
//                    if (file.getName().endsWith(".zip")
//                            || file.getName().endsWith(".rar")
//                            || file.getName().endsWith(".7z")
//                            || file.getName().endsWith(".tar")) {
//                        fileItems.add(FileManager.getFileItem(file));
//                    }
//                }
//            }
//        }
//    }

//    private void disposeDisposable() {
//        getDataThread.interrupt();
//    }

    @Override
    public void showProgress() {
        listener.onFileLoading();
    }

    @Override
    public void onDetData(List<FileItem> data) {
        items = new ArrayList<>();
        items.addAll(data);
        mAdapter = new ZipAdapter(requireActivity());
        mAdapter.setList(items);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void hideProgress() {
        listener.onFileLoaded();
    }

    public static class ZipAdapter extends BaseAdapter {
        LinearLayout cardview;
        Context mContext;
        ImageView more_document;
        ImageView show_image;
        TextView tv;

        private ACache mCache;
        private List<FileItem> mDatas;
        private Gson mGson;

        public ZipAdapter(Context context) {
            this.mContext = context;
            this.mGson = new Gson();
            try {
                this.mCache = ACache.get(context);
            } catch (Exception e) {
            }
        }

        public void setList(List<FileItem> list) {
            mDatas = list;
        }

        public int getCount() {
            return this.mDatas.size();
        }

        public Object getItem(int position) {
            return this.mDatas.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(this.mContext).inflate(R.layout.zip_item, parent, false);
            this.tv = (TextView) convertView.findViewById(R.id.item_zip_name);
            this.more_document = (ImageView) convertView.findViewById(R.id.more_zip);
            this.show_image = (ImageView) convertView.findViewById(R.id.show_image);
            this.cardview = (LinearLayout) convertView.findViewById(R.id.cardview);
            this.tv.setText((mDatas.get(position)).getName());
            this.more_document.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    try {
                        PopupMenu popup = new PopupMenu(ZipAdapter.this.mContext, v);
                        popup.getMenuInflater().inflate(R.menu.first_more, popup.getMenu());
                        popup.show();
                        popup.setOnMenuItemClickListener((OnMenuItemClickListener) item -> {
                            switch (item.getItemId()) {
                                case R.id.delete:
                                    ZipAdapter.this.removeData(position);
                                    break;
                                case R.id.details:
                                    ZipAdapter.this.ShowDetial(position);
                                    break;
                                case R.id.share:
                                    Uri uri;
                                    Intent intent = new Intent("android.intent.action.SEND");
                                    intent.setType("application/vnd.android.package-archive");
                                    FileItem file = ZipAdapter.this.mDatas.get(position);
                                    if (VERSION.SDK_INT >= 24) {
                                        File fl = new File(file.getPathFile());
                                        uri = FileProvider.getUriForFile(ZipAdapter.this.mContext, ZipAdapter.this.mContext.getPackageName() + ".provider", fl);
                                    } else {
                                        uri = Uri.parse(file.getPathFile());
                                    }
                                    intent.putExtra("android.intent.extra.STREAM", uri);
                                    intent.setFlags(268435456);
                                    ZipAdapter.this.mContext.startActivity(Intent.createChooser(intent, "Share"));
                                    break;
                            }
                            return true;
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            this.cardview.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    File file3 = new File((ZipAdapter.this.mDatas.get(position)).getPathFile());
                    String fullName = file3.getName();
                    int dotAt = fullName.lastIndexOf(".");
                    LayoutInflater inflater = LayoutInflater.from(ZipAdapter.this.mContext);
                    String name = fullName.substring(0, dotAt);
                    String extension = fullName.substring(dotAt + 1, fullName.length());
                    View renameFileView = inflater.inflate(R.layout.rename_file, null);
                    Button btnCancle = (Button) renameFileView.findViewById(R.id.btnCancle);
                    Button btnExtract = (Button) renameFileView.findViewById(R.id.btnExtract);
                    final EditText fileNameET = (EditText) renameFileView.findViewById(R.id.file_name);
                    fileNameET.setText(name);
                    ((TextView) renameFileView.findViewById(R.id.file_path)).setText(file3.getParent() + "/");
                    AlertDialog.Builder builder = new AlertDialog.Builder(ZipAdapter.this.mContext);
                    builder.setView(renameFileView);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    btnExtract.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            String fileName = fileNameET.getText().toString().trim();
                            new DecompressFast((ZipAdapter.this.mDatas.get(position)).getPathFile(), Environment.getExternalStorageDirectory() + "/FileManager/" + "/Zip/" + fileName + "/").unzip();
                            Toast.makeText(ZipAdapter.this.mContext, "FileManager/UnZip/" + fileName, Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }
                    });
                    btnCancle.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                }
            });
            this.cardview.setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View v) {
                    return true;
                }
            });
            return convertView;
        }

        private void ShowDetial(int position) {
            FileItem file = mDatas.get(position);
            String size = FileUtils.getFileSize(file.getPathFile());
            String name = file.getName();
            String path = file.getPathFile();
            new AlertDialog.Builder(this.mContext).setTitle((CharSequence) "Show Detail").setCancelable(false).setNegativeButton((CharSequence) "ok", null).setMessage("\nname：" + name + "\n\n" + "size：" + size + "\n\n" + "path：" + path + "\n\n" + "time：" + file.getDate()).show();
        }

        private void removeData(int position) {
            int i;
            FileUtils.deleteFile((this.mDatas.get(position)).getPathFile());
            for (i = 0; i < this.mDatas.size(); i++) {
                this.mCache.remove(String.valueOf(i) + "zip");
            }
            this.mDatas.remove(position);
            ArrayList<String> strings = new ArrayList();
            for (i = 0; i < this.mDatas.size(); i++) {
                strings.add(this.mGson.toJson(this.mDatas.get(i)));
            }
            for (i = 0; i < strings.size(); i++) {
                this.mCache.put(String.valueOf(i) + "zip", (String) strings.get(i), (int) ACache.TIME_DAY);
            }
            notifyDataSetChanged();
        }
    }
}
