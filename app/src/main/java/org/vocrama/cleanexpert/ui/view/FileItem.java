package org.vocrama.cleanexpert.ui.view;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class FileItem implements Parcelable {
    public static final Creator<FileItem> CREATOR = new Creator<FileItem>() {
        @Override
        public FileItem createFromParcel(Parcel in) {
            return new FileItem(in);
        }

        @Override
        public FileItem[] newArray(int size) {
            return new FileItem[size];
        }
    };
    public Date date1;
    public boolean isHidden;
    public boolean isSelected;
    public long length;
    private String date;
    private boolean isDirectory;
    private boolean isFile;
    private String name;
    private String pathFile;

    public FileItem(String name, String pathFile, String date, boolean isFile, boolean isDirectory, boolean isSelected, long length, Date date1, boolean isHidden) {
        this.name = name;
        this.pathFile = pathFile;
        this.date = date;
        this.isFile = isFile;
        this.isDirectory = isDirectory;
        this.isSelected = isSelected;
        this.length = length;
        this.date1 = date1;
        this.isHidden = isHidden;
    }

    protected FileItem(Parcel in) {
        date = in.readString();
        isDirectory = in.readByte() != 0;
        isFile = in.readByte() != 0;
        isHidden = in.readByte() != 0;
        isSelected = in.readByte() != 0;
        length = in.readLong();
        name = in.readString();
        pathFile = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeByte((byte) (isDirectory ? 1 : 0));
        dest.writeByte((byte) (isFile ? 1 : 0));
        dest.writeByte((byte) (isHidden ? 1 : 0));
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeLong(length);
        dest.writeString(name);
        dest.writeString(pathFile);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathFile() {
        return this.pathFile;
    }

    public String getDate() {
        return this.date;
    }

    public long getLength() {
        return this.length;
    }

    public Date getDate1() {
        return this.date1;
    }

    public boolean isFile() {
        return this.isFile;
    }

    public boolean isDirectory() {
        return this.isDirectory;
    }
}
