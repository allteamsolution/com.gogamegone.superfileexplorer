package org.vocrama.cleanexpert.ui;

public class Document {
    private Long dataId;
    private String dataName;
    private String type;

    public Document(Long dataId, String dataName, String type) {
        this.dataId = dataId;
        this.dataName = dataName;
        this.type = type;
    }

    public Long getDataId() {
        return this.dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getDataName() {
        return this.dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
