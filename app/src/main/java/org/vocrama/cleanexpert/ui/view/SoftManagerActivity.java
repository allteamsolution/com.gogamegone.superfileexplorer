package org.vocrama.cleanexpert.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.gogamegone.superfileexplorer.R;

import java.io.File;

import org.vocrama.cleanexpert.ui.SoftwareActivity;

public class SoftManagerActivity extends BaseActivity implements OnClickListener {
    ProgressBar pb_softmgr;
    PiechartView pv_softmgr;
    RelativeLayout rl_allSoftware;
    RelativeLayout rl_systemSoftware;
    RelativeLayout rl_usedSoftware;
    TextView tv_softmgr;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_soft_manager);


        initView();
        showMemory();
    }


    private void initView() {
        this.pv_softmgr = (PiechartView) findViewById(R.id.pv_softmgr);
        this.tv_softmgr = (TextView) findViewById(R.id.tv_softmgr);
        this.pb_softmgr = (ProgressBar) findViewById(R.id.pb_softmgr);
        this.rl_allSoftware = (RelativeLayout) findViewById(R.id.rl_allSoftware);
        this.rl_systemSoftware = (RelativeLayout) findViewById(R.id.rl_systemSoftware);
        this.rl_usedSoftware = (RelativeLayout) findViewById(R.id.rl_usedSoftware);
        this.rl_usedSoftware.setOnClickListener(this);
        this.rl_systemSoftware.setOnClickListener(this);
        this.rl_allSoftware.setOnClickListener(this);
    }

    private void showMemory() {
        File file = Environment.getExternalStorageDirectory();
        long total = file.getTotalSpace();
        long used = total - file.getFreeSpace();
        this.pv_softmgr.showPiechart((int) ((360.0d * ((double) used)) / ((double) total)));
        this.pb_softmgr.setProgress((int) ((100.0d * ((double) used)) / ((double) total)));
        this.tv_softmgr.setText("Storage：" + Formatter.formatFileSize(this, file.getFreeSpace()) + "/" + Formatter.formatFileSize(this, total));
    }

    public void onClick(View view) {
        int id = view.getId();
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        switch (id) {
            case R.id.rl_allSoftware:
                bundle.putString("appType", "all");
                bundle.putString("softwareType", "Type");
                startActivity(SoftwareActivity.class, bundle);
                return;
            case R.id.rl_systemSoftware:
                bundle.putString("appType","system");
                bundle.putString("softwareType", "Type");
                startActivity(SoftwareActivity.class, bundle);
                return;
            case R.id.rl_usedSoftware:
                bundle.putString("appType", "used");
                bundle.putString("softwareType", "Type");
                startActivity(SoftwareActivity.class, bundle);
                return;
            default:
                return;
        }
    }
}
