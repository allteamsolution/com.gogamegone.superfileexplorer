package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import org.vocrama.cleanexpert.photoeffect.ImageProcessing;

public class ProcessingAsyncTask extends AsyncTask<Integer, Void, Bitmap> {
    Bitmap _bitmap;
    Context _context;
    ProcessingListener _listener;

    public interface ProcessingListener {
        void onFinish(Bitmap bitmap);
    }

    public ProcessingAsyncTask(Context context, Bitmap b, ProcessingListener listener) {
        this._context = context;
        this._bitmap = b;
        this._listener = listener;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected Bitmap doInBackground(Integer... params) {
        switch (params[0].intValue()) {
            case 0:
                return ImageProcessing.applyNormalImage(this._bitmap);
            case 2:
                return ImageProcessing.applyInvert(this._bitmap);
            case 3:
                return ImageProcessing.applyGreyscale(this._bitmap);
            case 4:
                return ImageProcessing.applyGamma(this._bitmap, 1.8d, 1.8d, 0.6d);
            case 7:
                return ImageProcessing.applyColorDepth(this._bitmap, 64);
            case 8:
                return ImageProcessing.applyContrast(this._bitmap, 20.0d);
            case 10:
                return ImageProcessing.applyBrightness(this._bitmap, 60);
            case 18:
                return ImageProcessing.applyRoundCorner(this._bitmap, 100.0f);
            case 20:
                return ImageProcessing.applyFlip(this._bitmap, 2);
            case 24:
                return ImageProcessing.applyBlackFilter(this._bitmap);
            case 26:
                return ImageProcessing.applyShadingFilter(this._bitmap, -65281);
            case 27:
                return ImageProcessing.applySaturationFilter(this._bitmap, 40);
            case 29:
                return ImageProcessing.applyReflection(this._bitmap);
            default:
                return null;
        }
    }

    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        this._listener.onFinish(result);
    }
}
