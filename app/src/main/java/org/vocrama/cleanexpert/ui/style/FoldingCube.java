package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.animation.LinearInterpolator;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.RectSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.sprite.SpriteContainer;

public class FoldingCube extends SpriteContainer {
    private boolean wrapContent = false;

    private class Cube extends RectSprite {
        Cube() {
            setAlpha(0);
            setRotateX(-180);
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.1f, 0.25f, 0.75f, 0.9f, 1.0f};
            return new SpriteAnimatorBuilder(this).alpha(fractions, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(255), Integer.valueOf(255), Integer.valueOf(0), Integer.valueOf(0)).rotateX(fractions, Integer.valueOf(-180), Integer.valueOf(-180), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0)).rotateY(fractions, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(180), Integer.valueOf(180)).duration(2400).interpolator(new LinearInterpolator()).build();
        }
    }

    public Sprite[] onCreateChild() {
        Cube[] cubes = new Cube[4];
        for (int i = 0; i < cubes.length; i++) {
            cubes[i] = new Cube();
            if (VERSION.SDK_INT >= 24) {
                cubes[i].setAnimationDelay(i * 300);
            } else {
                cubes[i].setAnimationDelay((i * 300) - 1200);
            }
        }
        return cubes;
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        bounds = clipSquare(bounds);
        int size = Math.min(bounds.width(), bounds.height());
        if (this.wrapContent) {
            size = (int) Math.sqrt((double) ((size * size) / 2));
            int oW = (bounds.width() - size) / 2;
            int oH = (bounds.height() - size) / 2;
            bounds = new Rect(bounds.left + oW, bounds.top + oH, bounds.right - oW, bounds.bottom - oH);
        }
        int px = (bounds.left + (size / 2)) + 1;
        int py = (bounds.top + (size / 2)) + 1;
        for (int i = 0; i < getChildCount(); i++) {
            Sprite sprite = getChildAt(i);
            sprite.setDrawBounds(bounds.left, bounds.top, px, py);
            sprite.setPivotX((float) sprite.getDrawBounds().right);
            sprite.setPivotY((float) sprite.getDrawBounds().bottom);
        }
    }

    public void drawChild(Canvas canvas) {
        Rect bounds = clipSquare(getBounds());
        for (int i = 0; i < getChildCount(); i++) {
            int count = canvas.save();
            canvas.rotate((float) ((i * 90) + 45), (float) bounds.centerX(), (float) bounds.centerY());
            getChildAt(i).draw(canvas);
            canvas.restoreToCount(count);
        }
    }
}
