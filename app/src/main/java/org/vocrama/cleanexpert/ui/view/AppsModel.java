package org.vocrama.cleanexpert.ui.view;

import android.content.ComponentName;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import java.util.HashMap;
import java.util.List;

public class AppsModel implements Parcelable {
    public static final Creator<AppsModel> CREATOR = new Creator<AppsModel>() {
        public AppsModel createFromParcel(Parcel source) {
            return new AppsModel(source);
        }

        public AppsModel[] newArray(int size) {
            return new AppsModel[size];
        }
    };
    @Expose
    private String activityInfoName;
    @Expose
    private String appName;
    private Bitmap bitmap;
    private ComponentName componentName;
    @Expose
    private String filePath;
    @Expose
    private long firstInstallTime;
    private IconCache iconCache;
    private ResolveInfo info;
    private HashMap<Object, CharSequence> labelCache;
    @Expose
    private long lastUpdateTime;
    @Expose
    private String packageName;
    private List<String> permissions;
    private PackageManager pm;
    @Expose
    private String versionCode;
    @Expose
    private String versionName;

    public AppsModel(PackageManager pm, ResolveInfo info, IconCache iconCache, HashMap<Object, CharSequence> labelCache) {
        this.packageName = info.activityInfo.applicationInfo.packageName;
        this.componentName = new ComponentName(this.packageName, info.activityInfo.name);
        this.activityInfoName = info.activityInfo.name;
        try {
            PackageInfo pi = pm.getPackageInfo(this.packageName, 0);
            this.firstInstallTime = pi.firstInstallTime;
            this.lastUpdateTime = pi.lastUpdateTime;
            this.versionCode = Integer.toString(pi.versionCode);
            this.versionName = pi.versionName;
            this.filePath = info.activityInfo.applicationInfo.sourceDir;
            this.appName = info.loadLabel(pm).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        iconCache.getTitleAndIcon(this, info, labelCache);
    }

    public PackageManager getPm() {
        return this.pm;
    }

    public void setPm(PackageManager pm) {
        this.pm = pm;
    }

    public ResolveInfo getInfo() {
        return this.info;
    }

    public void setInfo(ResolveInfo info) {
        this.info = info;
    }

    public HashMap<Object, CharSequence> getLabelCache() {
        return this.labelCache;
    }

    public void setLabelCache(HashMap<Object, CharSequence> labelCache) {
        this.labelCache = labelCache;
    }

    public static Creator<AppsModel> getCREATOR() {
        return CREATOR;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getVersionCode() {
        return this.versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public long getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public long getFirstInstallTime() {
        return this.firstInstallTime;
    }

    public void setFirstInstallTime(long firstInstallTime) {
        this.firstInstallTime = firstInstallTime;
    }

    public List<String> getPermissions() {
        return this.permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public ComponentName getComponentName() {
        return this.componentName;
    }

    public void setComponentName(ComponentName componentName) {
        this.componentName = componentName;
    }

    public IconCache getIconCache() {
        return this.iconCache;
    }

    public void setIconCache(IconCache iconCache) {
        this.iconCache = iconCache;
    }

    public String getActivityInfoName() {
        return this.activityInfoName;
    }

    public void setActivityInfoName(String activityInfoName) {
        this.activityInfoName = activityInfoName;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.appName);
        dest.writeString(this.packageName);
        dest.writeString(this.filePath);
        dest.writeString(this.versionName);
        dest.writeString(this.versionCode);
        dest.writeLong(this.firstInstallTime);
        dest.writeLong(this.lastUpdateTime);
        dest.writeString(this.activityInfoName);
    }

    protected AppsModel(Parcel in) {
        this.appName = in.readString();
        this.packageName = in.readString();
        this.filePath = in.readString();
        this.versionName = in.readString();
        this.versionCode = in.readString();
        this.firstInstallTime = in.readLong();
        this.lastUpdateTime = in.readLong();
        this.activityInfoName = in.readString();
    }
}
