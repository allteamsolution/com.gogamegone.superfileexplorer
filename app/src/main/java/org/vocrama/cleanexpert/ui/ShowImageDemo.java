package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gogamegone.superfileexplorer.R;

import java.io.File;
import java.util.ArrayList;


public class ShowImageDemo extends AppCompatActivity {
    ShowImageAdapter adapter;
    ArrayList<MediaFile> images = new ArrayList();
    int int_position;
    ProgressDialog progressDialog;
    ViewPager viewPager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_show_imagedemo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator((int) R.drawable.ic_back_arrow);
            getSupportActionBar().setTitle((CharSequence) "");
        }
        this.int_position = getIntent().getIntExtra("pos", 0);
        this.viewPager = (ViewPager) findViewById(R.id.image_vp);
        this.adapter = new ShowImageAdapter(getApplicationContext(), this.images, this.int_position);
        this.viewPager.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
        this.viewPager.setCurrentItem(this.int_position);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.show_image_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_image_id:
                show_delete();
                break;
            case R.id.show_image_share:
                show_share();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void show_share() {
        File file = new File(((MediaFile) this.images.get(this.int_position)).path);
        Intent i = new Intent();
        i.setAction("android.intent.action.SEND");
        i.setType("image/jpeg");
        i.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
        i.addFlags(1);
        startActivity(Intent.createChooser(i, ""));
    }

    private void show_delete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
        builder.setTitle((CharSequence) "Delete Image");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new Delete().execute(new Void[0]);
                alertDialog.dismiss();
            }
        });
        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private class Delete extends AsyncTask<Void, Void, Void> {
        private Delete() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Void doInBackground(Void... params) {
            FileManager.delete(ShowImageDemo.this.getApplicationContext(), new File(((MediaFile) ShowImageDemo.this.images.get(ShowImageDemo.this.int_position)).path));
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowImageDemo.this.images.remove(ShowImageDemo.this.int_position);
            ShowImageDemo.this.adapter.notifyDataSetChanged();
            Toast.makeText(ShowImageDemo.this.getApplicationContext(), "Delete Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    public class ShowImageAdapter extends PagerAdapter {
        ArrayList<MediaFile> arrayList;
        Context context;
        TouchImageView imageView;
        LayoutInflater inflater;
        int position;

        public ShowImageAdapter(Context context, ArrayList<MediaFile> images, int int_position) {
            this.context = context;
            this.arrayList = images;
            this.position = int_position;
        }

        public int getItemPosition(Object object) {
            return -2;
        }

        public int getCount() {
            return this.arrayList.size();
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        public Object instantiateItem(ViewGroup container, int position) {
            this.inflater = (LayoutInflater) this.context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View viewLayout = this.inflater.inflate(R.layout.activity_show_image_adapter, container, false);
            this.imageView = (TouchImageView) viewLayout.findViewById(R.id.im_page);
            this.imageView.setMaxZoom(8.0f);
            Glide.with(this.context).load(((MediaFile) this.arrayList.get(position)).path).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(this.imageView);
            container.addView(viewLayout);
            return viewLayout;
        }

        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }
}
