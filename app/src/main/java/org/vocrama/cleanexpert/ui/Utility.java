package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.preference.PreferenceManager;

import com.gogamegone.superfileexplorer.R;


public class Utility {
    public static void setTheme(Context context, int theme) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(context.getString(R.string.prefs_theme_key), theme).apply();
    }

    public static int getTheme(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(context.getString(R.string.prefs_theme_key), 1);
    }
}
