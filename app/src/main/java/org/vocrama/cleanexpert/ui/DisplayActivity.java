package org.vocrama.cleanexpert.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.gogamegone.superfileexplorer.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import org.vocrama.AdHelper;
import org.vocrama.billing.BillingHelper;
import org.vocrama.cleanexpert.App;

import java.util.ArrayList;
import java.util.List;

public class DisplayActivity extends BaseTheme implements FileManager.OnFileLoadingCallback {

    private ViewPager2 viewPager = null;
    private TabLayout tabLayout;
    private FrameLayout flBanner;
    private ImageButton ibBack;
    private Button btnPremium;
    private FrameLayout flProgress;

    private ImagesFragment imagesFragment = ImagesFragment.getInstance();
    private AudioFragment audioFragment = AudioFragment.getInstance();
    private VideoFragment videoFragment = VideoFragment.getInstance();
    private AppBackupFragment appBackupFragment = AppBackupFragment.getInstance();
    private ZipFragment zipFragment = ZipFragment.getInstance();
    private DocumentFragment documentFragment = DocumentFragment.getInstance();
    private DownloadFragment downloadFragment = DownloadFragment.getInstance();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        initViews();
        checkViewBySubscribe();

        if (!BillingHelper.isSubscriber()) {
            AdHelper.loadProBanner(flBanner);
        }

        viewPager.setAdapter(new FragmentAdapter(this));

        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> {
                    switch (position) {
                        case 1:
                            tab.setText(getString(R.string.tab_audio_name));
                            break;
                        case 2:
                            tab.setText(getResources().getString(R.string.tab_video_name));
                            break;
                        case 3:
                            tab.setText(getResources().getString(R.string.tab_apps_name));
                            break;
                        case 4:
                            tab.setText(getResources().getString(R.string.tab_zip_name));
                            break;
                        case 5:
                            tab.setText(getResources().getString(R.string.tab_docs_name));
                            break;
                        case 6:
                            tab.setText(getResources().getString(R.string.tab_downloads_name));
                            break;
                        default:
                            tab.setText(getString(R.string.tab_image_name));
                            break;
                    }
                }
        ).attach();

//        adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.clear();
//        adapter.addFragment(imagesFragment, getString(R.string.tab_image_name));
//        adapter.addFragment(audioFragment, getString(R.string.tab_audio_name));
//        adapter.addFragment(videoFragment, getString(R.string.));
//        adapter.addFragment(appBackupFragment, getString(R.string.tab_apps_name));
//        adapter.addFragment(zipFragment, getString(R.string.tab_zip_name));
//        adapter.addFragment(documentFragment, getString(R.string.tab_docs_name));
//        adapter.addFragment(downloadFragment, getString(R.string.tab_downloads_name));

//        viewPager.setAdapter(this.adapter);

        try {
            viewPager.setCurrentItem(getIntent().getIntExtra("index", 0), true);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        tabLayout.setViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkViewBySubscribe();
    }

    private void initViews() {
        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tabs);
        flBanner = findViewById(R.id.flBanner);
        ibBack = findViewById(R.id.ibBack);
        btnPremium = findViewById(R.id.btnPremium);
        flProgress = findViewById(R.id.flLoading);

        ibBack.setOnClickListener(v -> {
            finish();
        });

        btnPremium.setOnClickListener(v -> {
            BillingHelper.openPurchaseActivity(this);
        });


    }

    private void checkViewBySubscribe() {
        btnPremium.setVisibility(BillingHelper.isSubscriber() ? View.GONE : View.VISIBLE);
        flBanner.setVisibility(BillingHelper.isSubscriber() ? View.GONE : View.VISIBLE);
        if (!BillingHelper.isSubscriber()) {
            AdHelper.loadProBanner(flBanner);
        }
    }

    @Override
    public void onFileLoading() {
        flProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFileLoaded() {
        flProgress.setVisibility(View.GONE);
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        private static final List<Fragment> mFragmentList = new ArrayList<>();
        private static final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void clear() {
            mFragmentList.clear();
            mFragmentTitleList.clear();
        }

        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private class FragmentAdapter extends FragmentStateAdapter {

        public FragmentAdapter(@NonNull FragmentActivity fragmentActivity) {
            super(fragmentActivity);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            switch (position) {
                case 1:
                    return audioFragment;
                case 2:
                    return videoFragment;
                case 3:
                    return appBackupFragment;
                case 4:
                    return zipFragment;
                case 5:
                    return documentFragment;
                case 6:
                    return downloadFragment;
                default:
                    return imagesFragment;
            }
        }

        @Override
        public int getItemCount() {
            return 7;
        }
    }
}
