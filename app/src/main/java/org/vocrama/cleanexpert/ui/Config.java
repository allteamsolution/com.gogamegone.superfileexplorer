package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.content.SharedPreferences;

public class Config {
    private SharedPreferences mPrefs;

    public static Config newInstance(Context context) {
        return new Config(context);
    }

    public Config(Context context) {
        this.mPrefs = context.getSharedPreferences(Constants.PREFS_KEY, 0);
    }

    public boolean getIsFirstRun() {
        return this.mPrefs.getBoolean(Constants.IS_FIRST_RUN, true);
    }

    public void setIsFirstRun(boolean firstRun) {
        this.mPrefs.edit().putBoolean(Constants.IS_FIRST_RUN, firstRun).apply();
    }

    public boolean getIsDarkTheme() {
        return this.mPrefs.getBoolean(Constants.IS_DARK_THEME, false);
    }

    public void setIsDarkTheme(boolean isDarkTheme) {
        this.mPrefs.edit().putBoolean(Constants.IS_DARK_THEME, isDarkTheme).apply();
    }

    public boolean getIsSameSorting() {
        return this.mPrefs.getBoolean(Constants.IS_SAME_SORTING, true);
    }

    public void setIsSameSorting(boolean isSameSorting) {
        this.mPrefs.edit().putBoolean(Constants.IS_SAME_SORTING, isSameSorting).apply();
    }

    public int getSorting() {
        if (getIsSameSorting()) {
            return getDirectorySorting();
        }
        return this.mPrefs.getInt(Constants.SORT_ORDER, 1026);
    }

    public void setSorting(int order) {
        if (getIsSameSorting()) {
            setDirectorySorting(order);
        } else {
            this.mPrefs.edit().putInt(Constants.SORT_ORDER, order).apply();
        }
    }

    public int getDirectorySorting() {
        return this.mPrefs.getInt(Constants.DIRECTORY_SORT_ORDER, 1);
    }

    public void setDirectorySorting(int order) {
        this.mPrefs.edit().putInt(Constants.DIRECTORY_SORT_ORDER, order).apply();
    }
}
