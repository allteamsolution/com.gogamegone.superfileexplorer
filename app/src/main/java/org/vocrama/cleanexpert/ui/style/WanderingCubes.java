package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.os.Build.VERSION;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.RectSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.sprite.SpriteContainer;

public class WanderingCubes extends SpriteContainer {

    private class Cube extends RectSprite {
        int startFrame;

        public Cube(int startFrame) {
            this.startFrame = startFrame;
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.25f, 0.5f, 0.51f, 0.75f, 1.0f};
            SpriteAnimatorBuilder builder = new SpriteAnimatorBuilder(this).rotate(fractions, Integer.valueOf(0), Integer.valueOf(-90), Integer.valueOf(-179), Integer.valueOf(-180), Integer.valueOf(-270), Integer.valueOf(-360)).translateXPercentage(fractions, Float.valueOf(0.0f), Float.valueOf(0.75f), Float.valueOf(0.75f), Float.valueOf(0.75f), Float.valueOf(0.0f), Float.valueOf(0.0f)).translateYPercentage(fractions, Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(0.75f), Float.valueOf(0.75f), Float.valueOf(0.75f), Float.valueOf(0.0f)).scale(fractions, Float.valueOf(1.0f), Float.valueOf(0.5f), Float.valueOf(1.0f), Float.valueOf(1.0f), Float.valueOf(0.5f), Float.valueOf(1.0f)).duration(1800).easeInOut(fractions);
            if (VERSION.SDK_INT >= 24) {
                builder.startFrame(this.startFrame);
            }
            return builder.build();
        }
    }

    public Sprite[] onCreateChild() {
        return new Sprite[]{new Cube(0), new Cube(3)};
    }

    public void onChildCreated(Sprite... sprites) {
        super.onChildCreated(sprites);
        if (VERSION.SDK_INT < 24) {
            sprites[1].setAnimationDelay(-900);
        }
    }

    protected void onBoundsChange(Rect bounds) {
        bounds = clipSquare(bounds);
        super.onBoundsChange(bounds);
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setDrawBounds(bounds.left, bounds.top, bounds.left + (bounds.width() / 4), bounds.top + (bounds.height() / 4));
        }
    }
}
