package org.vocrama.cleanexpert.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.LockActivity.LockScreenActivity;
import org.vocrama.cleanexpert.LockActivity.PreferenceHelper;
import org.vocrama.cleanexpert.LockActivity.SecurityActivity;
import org.vocrama.cleanexpert.LockActivity.UtilsDemo;

import java.util.ArrayList;
import java.util.List;

public class StartActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    boolean switch_status = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_start);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (VERSION.SDK_INT < 23) {
                    Switch();
                    return;
                }
                if (checkAndRequestPermissions()) {
                    Switch();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void Switch() {
        if (PreferenceHelper.getValueBoolean(this, PreferenceHelper.PREF_KEY_ONOFF_ENABLED, false)) {
            startActivity(new Intent(this, LockScreenActivity.class));
            return;
        }
        startActivity(new Intent(getApplicationContext(), FirstActivity.class));
        finish();
    }

    private void SwitchLock() {
        if (!this.switch_status) {
            startActivity(new Intent(getApplicationContext(), StartActivity.class));
            finish();
        } else if (PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.LOGIN_PREFERENCE_KEY_ANSWER, null) == null) {
            startActivity(new Intent(getApplicationContext(), SecurityActivity.class));
            finish();
        } else if (PreferenceHelper.getValueFromPreference(this, String.class, UtilsDemo.PASSWORD, null) == null) {
            startActivity(new Intent(getApplicationContext(), SecurityActivity.class));
            finish();
        }
    }

    private void gallery_intent() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
            }
        }, 2000);
    }

    private boolean checkAndRequestPermissions() {
        int storagePermission = ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE");
        int storageWrite = ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE");
        List<String> listPermissionsNeeded = new ArrayList();
        if (storagePermission != 0) {
            listPermissionsNeeded.add("android.permission.READ_EXTERNAL_STORAGE");
        }
        if (storageWrite != 0) {
            listPermissionsNeeded.add("android.permission.WRITE_EXTERNAL_STORAGE");
        }
        if (listPermissionsNeeded.isEmpty()) {
            return true;
        }
        ActivityCompat.requestPermissions(this, (String[]) listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!(requestCode == 1234 && resultCode == -1 && !getPackageManager().canRequestPackageInstalls())) {
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length <= 0 || grantResults[0] != 0) {
                    finish();
                    return;
                } else {
                    Switch();
                    return;
                }
            default:
                return;
        }
    }
}
