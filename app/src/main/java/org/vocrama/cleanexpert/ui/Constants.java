package org.vocrama.cleanexpert.ui;


import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.App;

public class Constants {
    public static final String ANSWER = App.getContext().getResources().getString(R.string.answer_preference_key);
    public static final int CHANGE_PASSWORD = 1;
    public static final String CHANGE_PASSWORD_REQUEST = "change_password_request";
    public static final String DIRECTORY = "directory";
    public static final String DIRECTORY_SORT_ORDER = "directory_sort_order";
    public static final String GET_ANY_INTENT = "get_any_intent";
    public static final String GET_IMAGE_INTENT = "get_image_intent";
    public static final String GET_VIDEO_INTENT = "get_video_intent";
    public static final String IS_DARK_THEME = "is_dark_theme";
    public static final String IS_FIRST_RUN = "is_first_run";
    public static final String IS_SAME_SORTING = "is_same_sorting";
    public static final String IS_SOUND_ON = App.getContext().getResources().getString(R.string.sound_on_off_key);
    public static final int MAXIMUM_NUMBER_PASSWORD_LENGTH = 4;
    public static final String MEDIUM = "medium";
    public static final String PASSWORD = "password";
    public static final String PREFERENCE_NAME = "UltimateFileHiderPreference";
    public static final String PREFS_KEY = "Gallery";
    public static final String SECURITY_QUESTION = App.getContext().getResources().getString(R.string.security_question_list_preference_key);
    public static final String SET_WALLPAPER_INTENT = "set_wallpaper_intent";
    public static final int SORT_BY_DATE = 2;
    public static final int SORT_BY_NAME = 1;
    public static final int SORT_BY_SIZE = 4;
    public static final int SORT_DESCENDING = 1024;
    public static final String SORT_ORDER = "sort_order";
    public static final String SOUND_NO = App.getContext().getResources().getString(R.string.sound_list_preference_key);

    public static final class app {
        public static final int APP_CLEANER = 4000;
        public static final int APP_CLEANER_FINISHED = 4001;
        public static final int APP_LIST_PREPARE = 4002;
        public static final int APP_LIST_PREPARE_FINISHED = 4003;
    }

    public static final class call {
        public static final int CALL_CLEANER_FINISHED = 3005;
        public static final int REMOVE_DELETE_ITEM = 3004;
        public static final int REMOVE_RESERVE_ITEM = 3003;
        public static final int RETRIVE_CONTACT_DELETE = 3002;
        public static final int RETRIVE_CONTACT_RESERVE = 3001;
    }

    public static final class database {
        public static final String APPLIST_ID = "_id";
        public static final String APPLIST_ISCACHE = "cache";
        public static final String APPLIST_ISDATA = "data";
        public static final String APPLIST_PATH = "path";
        public static final int CHANGE_PASSWORD = 1;
        public static final String CHANGE_PASSWORD_REQUEST = "change_password_request";
        public static final String CONFIG_ID = "_id";
        public static final String CONFIG_NAME = "name";
        public static final String CONFIG_VALUE = "value";
        public static final String DATABASE_NAME = "OneTouchCleaner.db";
        public static final String DATABASE_TABLE_APPLIST = "appList";
        public static final String DATABASE_TABLE_CONFIG = "config";
        public static final String DATABASE_TABLE_FILTER = "filter";
        public static final int DATABASE_VERSION = 1;
        public static final String FILTER_ID = "_id";
        public static final String FILTER_NAME = "name";
        public static final String FILTER_NUMBER = "number";
        public static final String FILTER_TYPE = "type";
        public static final int MAXIMUM_NUMBER_PASSWORD_LENGTH = 4;
        public static final String PASSWORD = "password";
        public static final String PREFERENCE_NAME = "UltimateFileHiderPreference";
    }

    public static final class main {
        public static final int MAIN_APPINFO_SAVE_FIN = 3002;
        public static final int MAIN_CONFIG_SAVE_FIN = 3001;
        public static final int MAIN_SAVE_FINISHED = 3000;
    }

    public static final class other {
        public static final int RAM_CLEAR_FINISHED = 5001;
    }

    public static final class sms {
        public static final int REMOVE_DELETE_ITEM = 6004;
        public static final int REMOVE_RESERVE_ITEM = 6003;
        public static final int RETRIVE_CONTACT_DELETE = 6002;
        public static final int RETRIVE_CONTACT_RESERVE = 6001;
        public static final int SMS_CLEANER_FINISHED = 6005;
    }

    public static final class splash {
        public static final int SPLASH_DISPLAY_LENGHT = 2500;
    }
}
