package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ResolveInfo;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.Constants.database;
import org.vocrama.cleanexpert.ui.CreateDialog.OnReceiveDataListener;
import org.vocrama.cleanexpert.ui.view.Constant;
import org.vocrama.cleanexpert.ui.view.FileItem;
import org.vocrama.cleanexpert.ui.view.OrderType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.vocrama.cleanexpert.ui.FileManager.SortBy.Sort.NAME;

public class InternalActivity extends BaseTheme implements OnClickListener, FileManager.OnFileLoadingCallback {
    public static String S;
    public static ActionMode actionMode;
    public static InternalAdapter adapter;
    public static List<FileItem> arrayList;
    public static ArrayList<FileItem> selectedFile = new ArrayList();
    public static ArrayList<FileItem> checked = selectedFile;
    public static FileManager fileManager;
    public static ImageView home;
    public static ImageView img_menu;
    public static ImageView img_past_internal;
    public static ImageView menu_item;
    public static String multiCopy = "";
    public static String multimove = "";
    public static ImageView past;
    public static String pathCopy = "";
    public static String pathItem;
    public static int selectCount;

    private FrameLayout flProgress;

    public static TextView title;
    public static Toolbar toolbar;
    AlertDialog alertDialog;
    FloatingActionMenu floatingActionMenu;
    FloatingActionButton floatingFile;
    FloatingActionButton floatingFolder;
    File folder;
    SwipeMenuListView list_in;
    ProgressDialog progressDialog;

    private Callback callback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.internal_context_menu, menu);
            MenuItem mi = menu.getItem(0);
            mi.setTitle(mi.getTitle().toString());
            InternalActivity.actionMode = mode;
            InternalActivity.selectCount = 0;
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.i_copy:
                    Constant.copy_flag = false;
                    InternalActivity.this.copyMultipleItem(mode);
                    Constant.flagForVisibleButton = true;
                    InternalActivity.past.setVisibility(View.VISIBLE);
                    Constant.isInternalPath = true;
                    break;
                case R.id.i_de:
                    InternalActivity.this.deleteMultipleItem(mode);
                    break;
                case R.id.i_move:
                    Constant.move_flag = false;
                    InternalActivity.this.moveMultipleItem(mode);
                    Constant.flagForVisibleButton = true;
                    InternalActivity.past.setVisibility(0);
                    Constant.isInternalPath = true;
                    break;
                case R.id.i_share:
                    InternalActivity.this.shareMultipleFolder(mode);
                    break;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            if (InternalActivity.selectCount > 0) {
                InternalActivity.this.deSelect();
            }
            InternalActivity.actionMode = null;
            InternalActivity.toolbar.setVisibility(View.VISIBLE);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_internal);

        toolbar = (Toolbar) findViewById(R.id.tool_bar1);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setTitle((CharSequence) "");

        title = (TextView) findViewById(R.id.txt_name_file);
        title.setText("Phone Memory");
        past = (ImageView) findViewById(R.id.img_paste);
        img_past_internal = (ImageView) findViewById(R.id.img_paste_internal);
        home = (ImageView) findViewById(R.id.img_home);
        img_menu = (ImageView) findViewById(R.id.img_menu);

        flProgress = findViewById(R.id.flLoading);
        menu_item = (ImageView) findViewById(R.id.img_more);

        if (Constant.flagForVisibleButton) {
            Toast.makeText(getApplicationContext(), "" + Constant.flagForVisibleButton, 0).show();
            past.setVisibility(View.VISIBLE);
        }

        img_menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (InternalActivity.pathItem != null) {
                    String url = InternalActivity.pathItem.substring(0, InternalActivity.pathItem.lastIndexOf("/"));
                    InternalActivity.title.setText(url.substring(url.lastIndexOf("/") + 1));
                    if (InternalActivity.pathItem.equals(FileManager.PATH)) {
                        InternalActivity.this.finish();
                        return;
                    }
                    InternalActivity.arrayList.clear();
                    InternalActivity.this.readFile(url, InternalActivity.this.getApplicationContext());
                    InternalActivity.pathItem = url;
                } else if (InternalActivity.pathItem == null) {
                    InternalActivity.this.finish();
                }
            }
        });

        this.list_in = (SwipeMenuListView) findViewById(R.id.list_in);

        this.list_in.setMenuCreator(new SwipeMenuCreator() {
            public void create(SwipeMenu menu) {
                SwipeMenuItem info = new SwipeMenuItem(InternalActivity.this.getApplicationContext());
                info.setBackground((int) R.color.menu1);
                info.setWidth(InternalActivity.this.dp2px(75));
                info.setIcon((int) R.drawable.ic_info_black_24dp);
                menu.addMenuItem(info);
                SwipeMenuItem copyItem = new SwipeMenuItem(InternalActivity.this.getApplicationContext());
                copyItem.setBackground((int) R.color.menu3);
                copyItem.setWidth(InternalActivity.this.dp2px(75));
                copyItem.setIcon((int) R.drawable.ic_content_copy_black_24dp);
                menu.addMenuItem(copyItem);
                SwipeMenuItem reName = new SwipeMenuItem(InternalActivity.this.getApplicationContext());
                reName.setBackground((int) R.color.menu4);
                reName.setWidth(InternalActivity.this.dp2px(75));
                reName.setIcon((int) R.drawable.ic_edit_black_24dp);
                menu.addMenuItem(reName);
                SwipeMenuItem openItem = new SwipeMenuItem(InternalActivity.this.getApplicationContext());
                openItem.setBackground((int) R.color.menu2);
                openItem.setWidth(InternalActivity.this.dp2px(75));
                openItem.setIcon((int) R.drawable.ic_delete_black_ic);
                menu.addMenuItem(openItem);
            }
        });

        this.list_in.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        InternalActivity.this.folderInfo(position);
                        break;
                    case 1:
                        InternalActivity.this.copyFolder(position);
                        Constant.flagForVisibleButton = true;
                        InternalActivity.past.setVisibility(0);
                        Constant.isInternalPath = true;
                        break;
                    case 2:
                        InternalActivity.this.renameFile(position);
                        break;
                    case 3:
                        InternalActivity.this.deleteFolder(position);
                        break;
                }
                return false;
            }
        });

        this.list_in.setSwipeDirection(1);

        arrayList = new ArrayList<>();

        fileManager = new FileManager();
        fileManager.onCreate();
        fileManager.setListener(this);


        readFile(FileManager.PATH, getApplicationContext());
        adapter = new InternalAdapter(getApplicationContext(), arrayList);
        this.list_in.setAdapter(adapter);

        loadFileItem();

        this.list_in.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                if (InternalActivity.selectCount > 0) {
                    if (InternalActivity.actionMode == null) {
                        InternalActivity.actionMode = InternalActivity.this.startActionMode(InternalActivity.this.callback);
                    }
                    InternalActivity.this.toggleselection(i);
                    InternalActivity.actionMode.setTitle(InternalActivity.selectCount + " " + "Selected");
                    if (InternalActivity.selectCount == 0) {
                        InternalActivity.actionMode.finish();
                    }
                } else if (InternalActivity.selectCount == 0) {
                    InternalActivity.S = ((FileItem) InternalActivity.arrayList.get(i)).getPathFile();
                    File file = new File(InternalActivity.S);
                    if (!((FileItem) InternalActivity.arrayList.get(i)).isFile()) {
                        InternalActivity.pathItem = ((FileItem) InternalActivity.arrayList.get(i)).getPathFile();
                        InternalActivity.title.setText(InternalActivity.pathItem.substring(InternalActivity.pathItem.lastIndexOf("/") + 1));
                        InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
                    } else if (file.canRead()) {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        String name = ((FileItem) InternalActivity.arrayList.get(i)).getName();

                        Uri uri = FileProvider.getUriForFile(InternalActivity.this, InternalActivity.this.getPackageName() + ".provider", file);

                        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(uri.toString());

                        if (extension.length() == 0) {

                          /*  Uri uris = FileProvider.getUriForFile(InternalActivity.this, InternalActivity.this.getPackageName() + ".provider", new File(file.getPath()+".txt"));
                            Log.e("extension","extension");
                            intent.setDataAndType(uris, ".txt");*/

                            Toast.makeText(InternalActivity.this, "This File Can't Open", Toast.LENGTH_SHORT).show();
                            return;

                        } else {

                            String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);


                            intent.setDataAndType(uri, type);

                        }


                        intent.addFlags(1);
                        intent.addFlags(268435456);
                        for (ResolveInfo resolveInfo : InternalActivity.this.getPackageManager().queryIntentActivities(intent, 65536)) {
                            InternalActivity.this.grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                        }

                        try {
                            InternalActivity.this.startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        this.list_in.setOnItemLongClickListener(new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                InternalActivity.toolbar.setVisibility(View.GONE);
                Constant.all_flag = false;
                if (InternalActivity.actionMode == null) {
                    InternalActivity.actionMode = InternalActivity.this.startActionMode(InternalActivity.this.callback);
                }
                InternalActivity.this.toggleselection(position);
                InternalActivity.actionMode.setTitle(InternalActivity.selectCount + " " + "Selected");
                if (InternalActivity.selectCount == 0) {
                    InternalActivity.actionMode.finish();
                }
                return true;
            }
        });
        this.folder = new File(FileManager.SECRET);
        if (!this.folder.exists()) {
            this.folder.mkdirs();
        }
    }

    private void toggleselection(int position) {
        ((FileItem) arrayList.get(position)).isSelected = !((FileItem) arrayList.get(position)).isSelected;
        if (((FileItem) arrayList.get(position)).isSelected) {
            selectedFile.add(arrayList.get(position));
            selectCount++;
        } else {
            selectedFile.add(arrayList.get(position));
            selectCount--;
        }
        adapter.notifyDataSetChanged();
    }

    private void deleteMultipleItem(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Delete Folder");
        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new DeleteTask().execute(new Void[0]);
                alertDialog.dismiss();
                mode.finish();
            }
        });
        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
                mode.finish();
            }
        });
    }

    private void shareMultipleFolder(ActionMode mode) {
        ArrayList<FileItem> checked = selectedFile;
        ArrayList<Uri> share = new ArrayList();
        for (int i = 0; i < checked.size(); i++) {
            if (((FileItem) selectedFile.get(i)).isSelected) {
                share.add(FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", new File(((FileItem) selectedFile.get(i)).getPathFile())));
            }
        }
        Intent i2 = new Intent();
        i2.setAction("android.intent.action.SEND_MULTIPLE");
        i2.setType("audio/*");
        i2.putParcelableArrayListExtra("android.intent.extra.STREAM", share);
        i2.addFlags(1);
        startActivity(Intent.createChooser(i2, ""));
        mode.finish();
    }

    private void moveMultipleItem(ActionMode mode) {
        ArrayList<FileItem> checked = selectedFile;
        for (int i = 0; i < checked.size(); i++) {
            if (((FileItem) selectedFile.get(i)).isSelected) {
                multimove = ((FileItem) selectedFile.get(i)).getPathFile();
            }
        }
        mode.finish();
    }

    private void copyMultipleItem(ActionMode mode) {
        ArrayList<FileItem> checked = selectedFile;
        for (int i = 0; i < checked.size(); i++) {
            if (((FileItem) selectedFile.get(i)).isSelected) {
                multiCopy = ((FileItem) selectedFile.get(i)).getPathFile();
            }
        }
        mode.finish();
    }

    private void deSelect() {
        for (int i = 0; i < arrayList.size(); i++) {
            ((FileItem) arrayList.get(i)).isSelected = false;
        }
        selectCount = 0;
        adapter.notifyDataSetChanged();
    }

    private void loadFileItem() {
        this.floatingActionMenu = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        this.floatingFolder = (FloatingActionButton) findViewById(R.id.floating_folder);
        this.floatingFile = (FloatingActionButton) findViewById(R.id.floating_file);
        home.setOnClickListener(this);
        past.setOnClickListener(this);
        img_past_internal.setOnClickListener(this);
        menu_item.setOnClickListener(this);
        this.floatingFolder.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                InternalActivity.this.createFolder();
            }
        });
        this.floatingFile.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                InternalActivity.this.createFile();
            }
        });
    }

    private void readFile(String path, Context applicationContext) {
        arrayList.clear();
        fileManager.readFile(arrayList, path, applicationContext);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_home:
                arrayList.clear();
                readFile(FileManager.PATH, getApplicationContext());
                if (actionMode != null) {
                    actionMode.finish();
                    return;
                }
                return;
            case R.id.img_more:
                PopupMenu menu = new PopupMenu(this, menu_item);
                menu.getMenuInflater().inflate(R.menu.more_menu_popup, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.exit:
                                InternalActivity.this.finish();
                                break;
                            case R.id.menu_sort:
                                InternalActivity.this.SortBy();
                                break;
                        }
                        return true;
                    }
                });
                menu.show();
                return;
            case R.id.img_paste:
                past.setVisibility(4);
                Constant.flagForVisibleButton = false;
                if (Constant.isExternalPath) {
                    Toast.makeText(getApplicationContext(), "external to internal...", 0).show();
                    int i;
                    if (!Constant.copy_flag) {
                        Constant.copy_flag = true;
                        i = 0;
                        while (i < ExternalActivity.checked.size()) {
                            try {
                                multiCopy = ((FileItem) ExternalActivity.selectedFile.get(i)).getPathFile();
                                if (pathItem == null) {
                                    pathItem = FileManager.PATH;
                                }
                                if (new File(multiCopy).isDirectory()) {
                                    fileManager.copyDirectory(getApplicationContext(), multiCopy, pathItem);
                                    Toast.makeText(this, "copy Successfully..", 0).show();
                                } else {
                                    fileManager.copyDirectory(getApplicationContext(), multiCopy, pathItem);
                                    Toast.makeText(this, "copy Successfully..", 0).show();
                                }
                                i++;
                            } catch (Exception e) {
                            }
                        }
                        ExternalActivity.selectedFile.clear();
                        readFile(pathItem, getApplicationContext());
                    } else if (Constant.move_flag) {
                        try {
                            if (pathItem == null) {
                                pathItem = FileManager.PATH;
                            }
                            fileManager.copyDirectory(getApplicationContext(), ExternalActivity.pathCopy, pathItem);
                            readFile(pathItem, getApplicationContext());
                            Toast.makeText(this, "copy Successfully..", Toast.LENGTH_SHORT).show();
                        } catch (Exception e2) {
                        }
                    } else {
                        Constant.move_flag = true;
                        i = 0;
                        while (i < ExternalActivity.checked.size()) {
                            try {
                                multimove = ((FileItem) ExternalActivity.selectedFile.get(i)).getPathFile();
                                if (pathItem == null) {
                                    pathItem = FileManager.PATH;
                                }
                                if (new File(multimove).isDirectory()) {
                                    fileManager.copyDirectory(getApplicationContext(), multimove, pathItem);
                                    StorageHelper.deleteDirectory(getApplicationContext(), multimove);
                                    Toast.makeText(this, "Move Successfully..", 0).show();
                                } else {
                                    fileManager.copyDirectory(getApplicationContext(), multimove, pathItem);
                                    StorageHelper.deleteDirectory(getApplicationContext(), multimove);
                                    Toast.makeText(this, "Move Successfully..", 0).show();
                                }
                                i++;
                            } catch (Exception e3) {
                            }
                        }
                        ExternalActivity.selectedFile.clear();
                        readFile(pathItem, getApplicationContext());
                    }
                    Constant.isExternalPath = false;
                    return;
                } else if (!Constant.copy_flag) {
                    Constant.copy_flag = true;
                    new CopyTask().execute(new Void[0]);
                    return;
                } else if (Constant.move_flag) {
                    new SingleCopy().execute(new Void[0]);
                    return;
                } else {
                    Constant.move_flag = true;
                    new MoveTask().execute(new Void[0]);
                    return;
                }
            default:
                return;
        }
    }

    private void SortBy() {
        FileManager.SortBy[] values = new FileManager.SortBy[] { FileManager.SortBy.NAME(), FileManager.SortBy.DATE(), FileManager.SortBy.SIZE()};
        CharSequence[] valueNames = new CharSequence[]{" Name ", " Date ", " Size "};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Sort By");
        builder.setItems(valueNames, (dialog, which) -> {
            fileManager.sortList(arrayList, values[which]);
            adapter.notifyDataSetChanged();
//                new SortTask(which).execute(new Void[0]);
        });
        this.alertDialog = builder.create();
        this.alertDialog.show();
    }

    private void createFile() {
        final CreateDialog createDialog = new CreateDialog(this);
        createDialog.getTitle().setText("New File");
        createDialog.setOnReceiveDataListener(new OnReceiveDataListener() {
            public void onClickOk(String nameFolder) {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    if (InternalActivity.pathItem != null) {
                        try {
                            File file = new File(InternalActivity.pathItem, nameFolder);
                            if (!file.exists()) {
                                file.createNewFile();
                                Toast.makeText(InternalActivity.this.getApplicationContext(), "New File Create Successfully", 0).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
                    } else {
                        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
                        File mediaStorageDir = new File(path, nameFolder);
                        if (!mediaStorageDir.exists()) {
                            try {
                                mediaStorageDir.createNewFile();
                                Toast.makeText(InternalActivity.this.getApplicationContext(), "New File Create Successfully", 0).show();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                            InternalActivity.this.readFile(path, InternalActivity.this.getApplicationContext());
                        }
                    }
                    InternalActivity.this.floatingActionMenu.close(false);
                    createDialog.dismiss();
                }
            }

            public void onClickCancel() {
                InternalActivity.this.floatingActionMenu.close(false);
                createDialog.dismiss();
            }
        });
        createDialog.show();
    }

    private void createFolder() {
        final CreateDialog createDialog = new CreateDialog(this);
        createDialog.getTitle().setText("New Folder");
        createDialog.setOnReceiveDataListener(new OnReceiveDataListener() {
            public void onClickOk(String nameFolder) {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    if (InternalActivity.pathItem != null) {
                        new File(InternalActivity.pathItem + File.separator + nameFolder).mkdirs();
                        InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
                        Toast.makeText(InternalActivity.this.getApplicationContext(), "New Folder Create Successfully", 0).show();
                    } else {
                        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
                        new File(path, nameFolder).mkdir();
                        InternalActivity.this.readFile(path, InternalActivity.this.getApplicationContext());
                        Toast.makeText(InternalActivity.this.getApplicationContext(), "New Folder Create Successfully", 0).show();
                    }
                    InternalActivity.this.floatingActionMenu.close(false);
                    createDialog.dismiss();
                }
            }

            public void onClickCancel() {
                InternalActivity.this.floatingActionMenu.close(false);
                createDialog.dismiss();
            }
        });
        createDialog.show();
    }

    private void deleteFolder(final int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Delete Folder");
        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String path = ((FileItem) InternalActivity.arrayList.get(i)).getPathFile();
                FileManager fileManager = InternalActivity.fileManager;
                FileManager.deleteDirectory(path);
                Toast.makeText(InternalActivity.this.getApplicationContext(), "Deleted Successfully", 0).show();
                String link = path.substring(0, path.lastIndexOf("/"));
                InternalActivity.pathItem = link;
                InternalActivity.this.readFile(link, InternalActivity.this.getApplicationContext());
                alertDialog.dismiss();
            }
        });
        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void folderInfo(int position) {
        FileItem fileItem = arrayList.get(position);
        InfoDialog.getInstance(fileItem).show(getSupportFragmentManager(), null);
    }

    private void renameFile(final int i) {
        final CreateDialog createDialog = new CreateDialog(this);
        createDialog.getTitle().setText("ReName");
        createDialog.editText_title.setText(((FileItem) arrayList.get(i)).getName());
        createDialog.setOnReceiveDataListener(new OnReceiveDataListener() {
            public void onClickOk(String nameFolder) {
                ArrayList<String> UpdateFile = new ArrayList();
                String path = ((FileItem) InternalActivity.arrayList.get(i)).getPathFile();
                File f = new File(path);
                UpdateFile.add(f.getAbsolutePath());
                if (InternalActivity.pathItem != null) {
                    try {
                        InternalActivity.fileManager.renameFile(path, nameFolder);
                        String s1 = f.getParent() + "/" + nameFolder;
                        InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
                        createDialog.dismiss();
                        UpdateFile.add(s1);
                        Toast.makeText(InternalActivity.this.getApplicationContext(), "ReName Successfully", 0).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        String s = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
                        InternalActivity.fileManager.renameFile(s, nameFolder);
                        InternalActivity.this.readFile(s, InternalActivity.this.getApplicationContext());
                        createDialog.dismiss();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                MediaScannerConnection.scanFile(InternalActivity.this.getApplicationContext(), (String[]) UpdateFile.toArray(new String[0]), null, new OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
            }

            public void onClickCancel() {
                InternalActivity.this.floatingActionMenu.close(false);
                createDialog.dismiss();
            }
        });
        createDialog.show();
    }

    private void copyFolder(int i) {
        pathCopy = ((FileItem) arrayList.get(i)).getPathFile();
    }

    public void onBackPressed() {
        if (pathItem != null && !pathItem.equals(FileManager.PATH)) {
            String url = pathItem.substring(0, pathItem.lastIndexOf("/"));
            title.setText(url.substring(url.lastIndexOf("/") + 1));
            readFile(url, getApplicationContext());
            pathItem = url;
        } else {
            finish();
        }
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(1, (float) dp, getResources().getDisplayMetrics());
    }

    @Override
    public void onFileLoading() {
        flProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFileLoaded() {
        adapter.notifyDataSetChanged();
        flProgress.setVisibility(View.GONE);
    }

    public class CopyTask extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            InternalActivity.this.progressDialog = new ProgressDialog(InternalActivity.this);
            InternalActivity.this.progressDialog.setProgressStyle(0);
            InternalActivity.this.progressDialog.setTitle("Copy");
            InternalActivity.this.progressDialog.setCancelable(false);
            InternalActivity.this.progressDialog.setMessage("Please Wait.....");
            InternalActivity.this.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            for (int i = 0; i < InternalActivity.checked.size(); i++) {
                InternalActivity.multiCopy = ((FileItem) InternalActivity.selectedFile.get(i)).getPathFile();
                if (new File(InternalActivity.multiCopy).isDirectory()) {
                    try {
                        if (InternalActivity.multiCopy == InternalActivity.pathItem) {
                            Toast.makeText(InternalActivity.this, "data..", 0).show();
                        } else {
                            InternalActivity.fileManager.copyDirectory(InternalActivity.this.getApplicationContext(), InternalActivity.multiCopy, InternalActivity.pathItem);
                        }
                    } catch (Exception e) {
                    }
                } else {
                    InternalActivity.fileManager.copyDirectory(InternalActivity.this.getApplicationContext(), InternalActivity.multiCopy, InternalActivity.pathItem);
                }
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            InternalActivity.this.progressDialog.dismiss();
            InternalActivity.selectedFile.clear();
            try {
                if (InternalActivity.multiCopy == InternalActivity.pathItem) {
                    Toast.makeText(InternalActivity.this, "same directory data are not paste..", 0).show();
                } else {
                    Toast.makeText(InternalActivity.this.getApplicationContext(), "Copy Successfully", 0).show();
                }
            } catch (Exception e) {
            }
            InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
        }
    }

    private class DeleteTask extends AsyncTask<Void, Void, Void> {
        private DeleteTask() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            InternalActivity.this.progressDialog = new ProgressDialog(InternalActivity.this);
            InternalActivity.this.progressDialog.setProgressStyle(0);
            InternalActivity.this.progressDialog.setCancelable(false);
            InternalActivity.this.progressDialog.setTitle("Delete");
            InternalActivity.this.progressDialog.setMessage("Please Wait.....");
            InternalActivity.this.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            ArrayList<FileItem> checkedItems = InternalActivity.selectedFile;
            for (int i = 0; i < checkedItems.size(); i++) {
                String d_path = ((FileItem) InternalActivity.selectedFile.get(i)).getPathFile();
                FileManager fileManager = InternalActivity.fileManager;
                FileManager.deleteDirectory(d_path);
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (InternalActivity.pathItem != null) {
                InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
            } else {
                InternalActivity.this.readFile(FileManager.PATH, InternalActivity.this.getApplicationContext());
            }
            InternalActivity.selectedFile.clear();
            Toast.makeText(InternalActivity.this.getApplicationContext(), "Delete Successfully", Toast.LENGTH_SHORT).show();
            InternalActivity.this.progressDialog.dismiss();
        }
    }

    public class InternalAdapter extends BaseAdapter {
        List<FileItem> arrayList;
        Context context;
        ViewHolder viewHolder;

        public InternalAdapter(Context context, List<FileItem> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        public int getCount() {
            return this.arrayList.size();
        }

        public FileItem getItem(int position) {
            return (FileItem) this.arrayList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View view, ViewGroup parent) {
            this.viewHolder = new ViewHolder();
            if (view == null) {
                view = LayoutInflater.from(this.context).inflate(R.layout.activity_internal_adapter, parent, false);
                this.viewHolder.imgFolder = (ImageView) view.findViewById(R.id.img_folder);
                this.viewHolder.txtNameFile = (TextView) view.findViewById(R.id.txt_name_folder);
                this.viewHolder.txtDate = (TextView) view.findViewById(R.id.txt_date);
                this.viewHolder.imgCreate = (ImageView) view.findViewById(R.id.img_create);
                this.viewHolder.frameLayout = (FrameLayout) view.findViewById(R.id.fl_view);
                view.setTag(this.viewHolder);
            } else {
                this.viewHolder = (ViewHolder) view.getTag();
            }
            FileItem fileItem = getItem(position);
            if (fileItem.isSelected) {
                view.setBackgroundResource(R.color.multiple_image_select_albumTextBackground);
            } else {
                view.setBackgroundResource(R.color.white);
            }
            try {
                if (fileItem.isDirectory()) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.ic_folder_black_24dp);
                } else if (fileItem.getName().endsWith(".txt")) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.im_txt);
                } else if (fileItem.getName().endsWith(".apk")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_apk).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".pdf")) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.im_pdf);
                } else if (fileItem.getName().endsWith(".doc")) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.im_doc);
                } else if (fileItem.getName().endsWith(".jpg")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_jpg).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".png")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_png).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".jpeg")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_png).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".mp3")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_mp3).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".mp4")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_mp4).centerCrop().into(this.viewHolder.imgFolder);
                } else {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.ic_insert_drive_file_black_24dp);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.viewHolder.txtNameFile.setText(fileItem.getName());
            this.viewHolder.txtDate.setText(fileItem.getDate());
            return view;
        }

        private class ViewHolder {
            FrameLayout frameLayout;
            ImageView imgCreate;
            ImageView imgFolder;
            TextView txtDate;
            TextView txtNameFile;

            private ViewHolder() {
            }
        }
    }

    private class MoveTask extends AsyncTask<Void, Void, Void> {
        private MoveTask() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            InternalActivity.this.progressDialog = new ProgressDialog(InternalActivity.this);
            InternalActivity.this.progressDialog.setProgressStyle(0);
            InternalActivity.this.progressDialog.setCancelable(false);
            InternalActivity.this.progressDialog.setTitle("Move");
            InternalActivity.this.progressDialog.setMessage("Please Wait.....");
            InternalActivity.this.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            ArrayList<FileItem> checked = InternalActivity.selectedFile;
            for (int i = 0; i < checked.size(); i++) {
                InternalActivity.multimove = ((FileItem) InternalActivity.selectedFile.get(i)).getPathFile();
                FileManager fileManager;
                if (new File(InternalActivity.multimove).isDirectory()) {
                    try {
                        if (InternalActivity.multimove == InternalActivity.pathItem) {
                            Toast.makeText(InternalActivity.this, "data..", 0).show();
                        } else {
                            InternalActivity.fileManager.copyDirectory(InternalActivity.this.getApplicationContext(), InternalActivity.multimove, InternalActivity.pathItem);
                            fileManager = InternalActivity.fileManager;
                            FileManager.deleteDirectory(InternalActivity.multimove);
                        }
                    } catch (Exception e) {
                    }
                } else {
                    InternalActivity.fileManager.copyDirectory(InternalActivity.this.getApplicationContext(), InternalActivity.multimove, InternalActivity.pathItem);
                    fileManager = InternalActivity.fileManager;
                    FileManager.deleteDirectory(InternalActivity.multimove);
                }
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (InternalActivity.multimove == InternalActivity.pathItem) {
                    Toast.makeText(InternalActivity.this, "same directory data are not paste..", 0).show();
                } else {
                    Toast.makeText(InternalActivity.this.getApplicationContext(), "Move Successfully", 0).show();
                }
            } catch (Exception e) {
            }
            InternalActivity.selectedFile.clear();
            InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
            InternalActivity.this.progressDialog.dismiss();
        }
    }

    private class SingleCopy extends AsyncTask<Void, Void, Void> {
        private SingleCopy() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            InternalActivity.this.progressDialog = new ProgressDialog(InternalActivity.this);
            InternalActivity.this.progressDialog.setProgressStyle(0);
            InternalActivity.this.progressDialog.setCancelable(false);
            InternalActivity.this.progressDialog.setTitle("Copy");
            InternalActivity.this.progressDialog.setMessage("Please Wait.....");
            InternalActivity.this.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            if (new File(InternalActivity.pathCopy).isDirectory()) {
                try {
                    if (InternalActivity.pathCopy == InternalActivity.pathItem) {
                        Toast.makeText(InternalActivity.this, "data..", 0).show();
                    } else {
                        InternalActivity.fileManager.copyDirectory(InternalActivity.this.getApplicationContext(), InternalActivity.pathCopy, InternalActivity.pathItem);
                    }
                } catch (Exception e) {
                }
            } else {
                InternalActivity.fileManager.copyDirectory(InternalActivity.this.getApplicationContext(), InternalActivity.pathCopy, InternalActivity.pathItem);
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (InternalActivity.pathCopy == InternalActivity.pathItem) {
                    Toast.makeText(InternalActivity.this, "same directory data are not paste..", 0).show();
                } else {
                    Toast.makeText(InternalActivity.this.getApplicationContext(), "Copy Successfully", 0).show();
                }
            } catch (Exception e) {
            }
            InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
            InternalActivity.this.progressDialog.dismiss();
        }
    }

    private class SortTask extends AsyncTask<Void, Void, Void> {
        int whi;

        public SortTask(int which) {
            this.whi = which;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            InternalActivity.this.progressDialog = new ProgressDialog(InternalActivity.this);
            InternalActivity.this.progressDialog.setProgressStyle(0);
            InternalActivity.this.progressDialog.setCancelable(false);
            InternalActivity.this.progressDialog.setTitle("Sorting");
            InternalActivity.this.progressDialog.setMessage("Please Wait.....");
            InternalActivity.this.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            OrderType orderType = OrderType.values()[this.whi];
            Constant.getInstance(InternalActivity.this.getApplicationContext()).setOrderType(InternalActivity.this.getApplicationContext(), orderType);
            if (InternalActivity.pathItem == null) {
                switch (orderType) {
                    case NAME:
                        Collections.sort(InternalActivity.arrayList, OrderType.NAME.getComparator());
                        break;
                    case DATE:
                        Collections.sort(InternalActivity.arrayList, OrderType.DATE.getComparator());
                        break;
                    case SIZE:
                        Collections.sort(InternalActivity.arrayList, OrderType.SIZE.getComparator());
                        break;
                }
            }
            switch (orderType) {
                case NAME:
                    Collections.sort(InternalActivity.arrayList, OrderType.NAME.getComparator());
                    break;
                case DATE:
                    Collections.sort(InternalActivity.arrayList, OrderType.DATE.getComparator());
                    break;
                case SIZE:
                    Collections.sort(InternalActivity.arrayList, OrderType.SIZE.getComparator());
                    break;
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (InternalActivity.pathItem != null) {
                InternalActivity.this.readFile(InternalActivity.pathItem, InternalActivity.this.getApplicationContext());
            } else {
                InternalActivity.this.readFile(FileManager.PATH, InternalActivity.this.getApplicationContext());
            }
            Toast.makeText(InternalActivity.this.getApplicationContext(), "Sorting Successfully", 0).show();
            InternalActivity.this.progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        arrayList.clear();
        fileManager.onDestroy();
    }
}
