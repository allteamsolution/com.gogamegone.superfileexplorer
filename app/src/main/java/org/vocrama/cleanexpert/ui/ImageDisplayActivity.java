package org.vocrama.cleanexpert.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.Constants.database;

public class ImageDisplayActivity extends AppCompatActivity {
    ImageView image_view;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_image_display);
        this.image_view = (ImageView) findViewById(R.id.image_view);
        String pathImage = getIntent().getStringExtra("pathImage");
        Toast.makeText(this, database.APPLIST_PATH + pathImage, 0).show();
        this.image_view.setImageBitmap(StringToBitMap(pathImage));
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, 0);
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }
}
