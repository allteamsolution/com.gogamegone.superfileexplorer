package org.vocrama.cleanexpert.ui;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.view.AppList;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.transform.Result;

import static android.app.Activity.RESULT_OK;

public class ApkFragment extends Fragment {
    private static final int UNINSTALL_REQUEST_CODE = 124;
    ActionBar actionBar;
    ActionMode actionMode;
    ApkAdapter apkAdapter;
    AppList appList;
    int countSelected;
    GridView grid_apk;
    ProgressDialog progressDialog;
    List<AppList> res = new ArrayList();
    SwipeRefreshLayout swipeRefreshLayout;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.actionBar = getActivity().getActionBar();
        if (this.actionBar != null) {
            this.actionBar.setDisplayShowTitleEnabled(true);
            this.actionBar.setTitle("Apps");
        }
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.activity_apk, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.grid_apk = (GridView) view.findViewById(R.id.grid_apk);
        GetApp();
        this.apkAdapter = new ApkAdapter(getActivity(), this.res);
        this.grid_apk.setAdapter(this.apkAdapter);
        registerForContextMenu(this.grid_apk);
        this.grid_apk.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ApkFragment.this.appList = (AppList) ApkFragment.this.res.get(position);
                ApkFragment.this.apk_open();
            }
        });
    }

    private void GetApp() {
        List<PackageInfo> packs = getActivity().getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < packs.size(); i++) {
            PackageInfo p = (PackageInfo) packs.get(i);
            File file = new File(p.applicationInfo.publicSourceDir);
            if (!isSystemPackage(p)) {
                this.res.add(new AppList(p.applicationInfo.loadLabel(getActivity().getPackageManager()).toString(), p.applicationInfo.loadIcon(getActivity().getPackageManager()), file.length(), p, false, file.getAbsolutePath()));
            }
        }
        Collections.sort(this.res, new Comparator<AppList>() {
            public int compare(AppList o1, AppList o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.play_store_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent homeIntent = new Intent(getActivity(), MainActivity.class);
                homeIntent.addFlags(67108864);
                startActivity(homeIntent);
                break;
            case R.id.play_store:
                startActivity(getActivity().getPackageManager().getLaunchIntentForPackage("com.android.vending"));
                break;
        }
        super.onOptionsItemSelected(item);
        return true;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.apk_context_menu, menu);
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.apk_info:
                apk_info();
                break;
            case R.id.apk_open:
                apk_open();
                break;
            case R.id.apk_unin:
                apk_unin();
                break;
        }
        return super.onContextItemSelected(item);
    }

    private boolean isSystemPackage(PackageInfo p) {
        return (p.applicationInfo.flags & 1) != 0;
    }

    public void apk_unin() {
        Intent uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
        uninstallIntent.setData(Uri.parse("package:" + appList.getPackageInfo().packageName));
        uninstallIntent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
        startActivityForResult(uninstallIntent, UNINSTALL_REQUEST_CODE);
    }

    public void apk_open() {
        getActivity().startActivity(getActivity().getPackageManager().getLaunchIntentForPackage(this.appList.getPackageInfo().packageName));
    }

    public void apk_info() {
        Intent i = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        i.setData(Uri.parse("package:" + this.appList.getPackageInfo().packageName));
        startActivity(i);
    }

    public class ApkAdapter extends BaseAdapter {
        public ViewHoder viewHoder;
        Context context;
        List<AppList> images;

        public ApkAdapter(Context context, List<AppList> images) {
            this.context = context;
            this.images = images;
        }

        public int getCount() {
            return this.images.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            this.viewHoder = new ViewHoder();
            if (convertView == null) {
                convertView = LayoutInflater.from(this.context).inflate(R.layout.adapter_app, parent, false);
                this.viewHoder.image_apk = (ImageView) convertView.findViewById(R.id.app_icon);
                this.viewHoder.textView = (TextView) convertView.findViewById(R.id.list_app_name);
                this.viewHoder.img_more_main_audio = (ImageView) convertView.findViewById(R.id.img_more_main_apk);
                convertView.setTag(this.viewHoder);
            } else {
                this.viewHoder = (ViewHoder) convertView.getTag();
            }
            this.viewHoder.image_apk.setImageDrawable(((AppList) this.images.get(position)).getIcon());
            this.viewHoder.textView.setText(((AppList) this.images.get(position)).getName());
            this.viewHoder.img_more_main_audio.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    try {
                        PopupMenu popup = new PopupMenu(ApkAdapter.this.context, v);
                        popup.getMenuInflater().inflate(R.menu.apk_context_menu, popup.getMenu());
                        popup.show();
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.apk_info:
                                        ApkFragment.this.appList = (AppList) ApkFragment.this.res.get(position);
                                        Intent info = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                                        info.setData(Uri.parse("package:" + ApkFragment.this.appList.getPackageInfo().packageName));
                                        ApkFragment.this.startActivity(info);
                                        break;
                                    case R.id.apk_open:
                                        ApkFragment.this.appList = (AppList) ApkFragment.this.res.get(position);
                                        ApkAdapter.this.context.startActivity(ApkAdapter.this.context.getPackageManager().getLaunchIntentForPackage(ApkFragment.this.appList.getPackageInfo().packageName));
                                        break;
                                    case R.id.apk_unin:
                                        ApkFragment.this.appList = (AppList) ApkFragment.this.res.get(position);
                                        Intent intent = new Intent("android.intent.action.UNINSTALL_PACKAGE");
                                        intent.setData(Uri.parse("package:" + ApkFragment.this.appList.getPackageInfo().packageName));
                                        ApkAdapter.this.context.startActivity(intent);
                                        break;
                                }
                                return true;
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return convertView;
        }

        public class ViewHoder {
            public ImageView image_apk;
            public ImageView img_more_main_audio;
            public TextView textView;
            public TextView textView_size;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            if(requestCode == UNINSTALL_REQUEST_CODE) {
                Toast.makeText(requireContext(), "Uninstall success", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
