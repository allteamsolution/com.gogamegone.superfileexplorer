package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;

import java.util.List;

import org.vocrama.cleanexpert.ui.view.AppList;

public class ApkAdapter extends BaseAdapter {
    Context context;
    List<AppList> images;
    ViewHoder viewHoder;

    private class ViewHoder {
        ImageView image_apk;
        TextView textView;
        TextView textView_size;

        private ViewHoder() {
        }
    }

    public ApkAdapter(Context context, List<AppList> images) {
        this.context = context;
        this.images = images;
    }

    public int getCount() {
        return this.images.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        this.viewHoder = new ViewHoder();
        convertView = LayoutInflater.from(this.context).inflate(R.layout.activity_apk_adapter, parent, false);
        this.viewHoder.image_apk = (ImageView) convertView.findViewById(R.id.app_icon);
        this.viewHoder.textView = (TextView) convertView.findViewById(R.id.list_app_name);
        this.viewHoder.image_apk.setImageDrawable(((AppList) this.images.get(position)).getIcon());
        this.viewHoder.textView.setText(((AppList) this.images.get(position)).getName());
        return convertView;
    }
}
