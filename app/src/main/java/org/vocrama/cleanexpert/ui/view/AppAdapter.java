package org.vocrama.cleanexpert.ui.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;

import java.util.List;


public class AppAdapter extends MyBaseAdapter<AppInfo> {

    private static class ViewHolder {
        ImageView appicon;
        TextView appname;
        TextView appversion;
        CheckBox cb_delete;
        TextView packagename;

        private ViewHolder() {
        }


    }

    public AppAdapter(List dateList, Context context) {
        super(dateList, context);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            view = this.inflater.inflate(R.layout.item_software, null);
            holder = new ViewHolder();
            holder.appversion = (TextView) view.findViewById(R.id.appversion);
            holder.packagename = (TextView) view.findViewById(R.id.packagename);
            holder.appicon = (ImageView) view.findViewById(R.id.iv_appicon);
            holder.appname = (TextView) view.findViewById(R.id.appname);
            holder.cb_delete = (CheckBox) view.findViewById(R.id.cb_delete);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.cb_delete.setTag(Integer.valueOf(i));
        AppInfo appInfo = (AppInfo) getItem(i);
        holder.appname.setText(appInfo.appname);
        holder.packagename.setText(appInfo.packagename);
        holder.appversion.setText(appInfo.appversion);
        holder.appicon.setImageDrawable(appInfo.appicon);
        if (((AppInfo) getItem(i)).isSystem) {
            holder.cb_delete.setClickable(false);
        } else {
            holder.cb_delete.setClickable(true);
        }
        holder.cb_delete.setChecked(appInfo.isDelete);
        holder.cb_delete.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ((AppInfo) AppAdapter.this.getItem(((Integer) holder.cb_delete.getTag()).intValue())).isDelete = b;
            }
        });
        return view;
    }
}
