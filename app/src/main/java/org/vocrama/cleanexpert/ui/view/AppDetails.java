package org.vocrama.cleanexpert.ui.view;

import android.graphics.drawable.Drawable;
import java.util.ArrayList;

class AppDetails {
    public Drawable icon;
    public String name;
    public String packageName;
    public ArrayList<String> permissionList;

    AppDetails() {
    }
}
