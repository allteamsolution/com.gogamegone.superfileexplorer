package org.vocrama.cleanexpert.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;


public class ListHeaderView extends RelativeLayout {
    private Context mContext;
    public TextView mProgress = ((TextView) findViewById(R.id.progress_msg));
    public TextView mSize = ((TextView) findViewById(R.id.total_size));

    public ListHeaderView(Context context, ViewGroup listView) {
        super(context);
        this.mContext = context;
        addView(LayoutInflater.from(this.mContext).inflate(R.layout.list_header_view, listView, false));
    }
}
