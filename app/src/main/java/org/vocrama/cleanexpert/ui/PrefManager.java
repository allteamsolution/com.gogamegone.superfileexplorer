package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

import androidx.annotation.Nullable;

import com.gogamegone.superfileexplorer.R;


public class PrefManager {
    private static final String IS_PERMISSON_GET = "IsPermissionGet";
    private static final String PREF_NAME = "welcome";
    int PRIVATE_MODE = 0;
    private Context _context;
    private Editor editor;
    private SharedPreferences pref;

    PrefManager(Context context) {
        this._context = context;
        this.pref = this._context.getSharedPreferences(PREF_NAME, this.PRIVATE_MODE);
        this.editor = this.pref.edit();
        this.editor.apply();
    }

    void GetSdCardPermission(boolean isFirstTime) {
        this.editor.putBoolean(IS_PERMISSON_GET, isFirstTime);
        this.editor.commit();
    }

    void SaveTreeUri(Context context, @Nullable Uri uri) {
        this.editor.putString(context.getString(R.string.preference_internal_uri_extsdcard_photos), uri == null ? null : uri.toString());
        this.editor.putString("sd_card_path", StorageHelper.getSdcardPath(context));
    }

    public String getSavedSdcardPath() {
        return this.pref.getString("sd_card_path", null);
    }

    public String getTreeUri(Context context) {
        return this.pref.getString(context.getString(R.string.preference_internal_uri_extsdcard_photos), null);
    }

    public boolean isFirstTimeLaunch() {
        return this.pref.getBoolean(IS_PERMISSON_GET, true);
    }
}
