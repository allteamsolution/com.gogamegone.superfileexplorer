package org.vocrama.cleanexpert.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.view.SoftManagerActivity;

public class MainActivity extends BaseActivity {
    public static final String PARAM_TOTAL_MEMORY = "total_memory";
    public static final String PARAM_TOTAL_SPACE = "total_space";
    public static final String PARAM_USED_MEMORY = "used_memory";
    public static final String PARAM_USED_SPACE = "used_space";
    private Button bettery;
    private Button mJunkCleanButton;
    private Button permission;
    private Button storage;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.content_main);



        this.mJunkCleanButton = (Button) findViewById(R.id.junk_clean);
        this.mJunkCleanButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, JunkCleanActivity.class));
            }
        });
        this.storage = (Button) findViewById(R.id.storage);
        this.storage.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, SoftManagerActivity.class));
            }
        });
    }


}
