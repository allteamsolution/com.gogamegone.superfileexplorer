package org.vocrama.cleanexpert.ui;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Compress {
    private static final int BUFFER = 2048;
    private String[] _files;
    private String _zipFile;

    public Compress(String[] files, String zipFile) {
        this._files = files;
        this._zipFile = zipFile;
    }

    public void zip() {
        Exception e;
        BufferedInputStream origin = null;
        try {
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(this._zipFile)));
            byte[] data = new byte[2048];
            int i = 0;
            while (true) {
                BufferedInputStream origin2 = null;
                try {
                    origin2 = origin;
                    if (i < this._files.length) {
                        Log.v("Compress", "Adding: " + this._files[i]);
                        String name = this._files[i];
                        File f = new File(this._files[i]);
                        int count;
                        if (f.isDirectory()) {
                            if (!name.endsWith("/")) {
                                name = name + "/";
                            }
                            String[] list = f.list();
                            int length = list.length;
                            int i2 = 0;
                            while (i2 < length) {
                                String file = list[i2];
                                System.out.println(" checking " + file);
                                System.out.println("The folder name is: " + f.getName());
                                out.putNextEntry(new ZipEntry(f.getName() + "/" + file));
                                origin = new BufferedInputStream(new FileInputStream(this._files[i] + "/" + file), 2048);
                                while (true) {
                                    count = origin.read(data, 0, 2048);
                                    if (count == -1) {
                                        break;
                                    }
                                    out.write(data, 0, count);
                                }
                                origin.close();
                                i2++;
                                origin2 = origin;
                            }
                            System.out.println(" checking folder" + name);
                            origin = origin2;
                        } else {
                            origin = new BufferedInputStream(new FileInputStream(this._files[i]), 2048);
                            out.putNextEntry(new ZipEntry(this._files[i].substring(this._files[i].lastIndexOf("/") + 1)));
                            while (true) {
                                count = origin.read(data, 0, 2048);
                                if (count == -1) {
                                    break;
                                }
                                out.write(data, 0, count);
                            }
                            origin.close();
                        }
                        i++;
                    } else {
                        out.close();
                        return;
                    }
                } catch (Exception e2) {
                    e = e2;
                    origin = origin2;
                }
            }
        } catch (Exception e3) {
            e = e3;
        }
        e.printStackTrace();
    }
}
