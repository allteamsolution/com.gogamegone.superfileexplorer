package org.vocrama.cleanexpert.ui.view;

import android.graphics.drawable.Drawable;

public class AppInfo {
    public Drawable appicon;
    public String appname;
    public String appversion;
    public boolean isDelete;
    public boolean isSystem;
    public String packagename;

    public AppInfo(Drawable appicon, String appname, String packagename, String appversion, boolean isSystem, boolean isDelete) {
        this.appicon = appicon;
        this.appname = appname;
        this.isSystem = isSystem;
        this.packagename = packagename;
        this.appversion = appversion;
        if (isSystem) {
            this.isDelete = false;
        } else {
            this.isDelete = isDelete;
        }
    }
}
