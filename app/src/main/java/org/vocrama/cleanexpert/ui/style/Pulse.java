package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.CircleSprite;

public class Pulse extends CircleSprite {
    public Pulse() {
        setScale(0.0f);
    }

    public ValueAnimator onCreateAnimation() {
        float[] fractions = new float[]{0.0f, 1.0f};
        return new SpriteAnimatorBuilder(this).scale(fractions, Float.valueOf(0.0f), Float.valueOf(1.0f)).alpha(fractions, Integer.valueOf(255), Integer.valueOf(0)).duration(1000).easeInOut(fractions).build();
    }
}
