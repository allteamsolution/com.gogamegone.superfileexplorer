package org.vocrama.cleanexpert.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gogamegone.superfileexplorer.R;


public class CreateDialog extends Dialog implements OnClickListener {
    TextView cancel;
    EditText editText_title ;
    TextView ok;
    private OnReceiveDataListener onReceiveDataListener;
    TextView title ;

    public interface OnReceiveDataListener {
        void onClickCancel();

        void onClickOk(String str);
    }

    public CreateDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.createdialog);

        cancel = ((TextView) findViewById(R.id.d_cancel));
        title = ((TextView) findViewById(R.id.d_title));
        editText_title = ((EditText) findViewById(R.id.d_title_edit));
        ok = ((TextView) findViewById(R.id.d_ok));

        this.ok.setOnClickListener(this);
        this.cancel.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.d_cancel:
                this.onReceiveDataListener.onClickCancel();
                return;
            case R.id.d_ok:
                String folder = this.editText_title.getText().toString();
                if (!folder.equals("")) {
                    this.onReceiveDataListener.onClickOk(folder);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public TextView getTitle() {
        return this.title;
    }

    public void setOnReceiveDataListener(OnReceiveDataListener onReceiveDataListener) {
        this.onReceiveDataListener = onReceiveDataListener;
    }
}
