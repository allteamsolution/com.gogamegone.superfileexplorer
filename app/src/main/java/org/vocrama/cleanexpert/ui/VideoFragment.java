package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.MediaStore.Video.Media;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.callback.OnMediaFileClickListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class VideoFragment extends Fragment {

    private boolean isThreadStarted = false;
    private CompositeDisposable compositeDisposable;

    public static VideoFragment getInstance() {
        return new VideoFragment();
    }

    public VideoFragment() {

    }

    public static Cursor cursor;
    public static ArrayList<MediaFile> videosList = new ArrayList<>();
    public static String mediaFile;
    public static String share;
    private final String[] projection = new String[]{"_id", "_display_name", "_data", "duration"};
    public TextView txtNameDetail_video;
    public TextView txtPathDetail_video;
    public TextView txtSizeDetail_video;
    ActionMode actionMode;
    VideoAdapter adapter;
    LinearLayout firstLinearLaout;
    RecyclerView rvVideo;
    LinearLayout linearLayout;
    ProgressDialog progressDialog;
    int selectCount;
    ArrayList<MediaFile> selectedVideo = new ArrayList<>();
    Uri uri = Media.EXTERNAL_CONTENT_URI;

    private FileManager.OnFileLoadingCallback onFileLoadingCallback;

    private Callback callback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.video_context_menu, menu);
            MenuItem mi = menu.getItem(0);
            mi.setTitle(mi.getTitle().toString());
            VideoFragment.this.actionMode = mode;
            VideoFragment.this.selectCount = 0;
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.video_d:
                    VideoFragment.this.video_delete(mode);
                    break;
                case R.id.video_s:
                    VideoFragment.this.video_share(mode);
                    break;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            if (VideoFragment.this.selectCount > 0) {
                VideoFragment.this.deselect();
            }
            VideoFragment.this.actionMode = null;
        }
    };

    private final OnMediaFileClickListener onMediaFileClickListener = new OnMediaFileClickListener() {
        @Override
        public void onClick(int position) {
            VideoFragment.mediaFile = (videosList.get(position)).path;
            if (VideoFragment.this.selectCount > 0) {
                if (VideoFragment.this.actionMode == null) {
                    VideoFragment.this.actionMode = requireActivity().startActionMode(VideoFragment.this.callback);
                }
                VideoFragment.this.toggleSelection(position);
                VideoFragment.this.actionMode.setTitle(VideoFragment.this.selectCount + " " + "Selected");
                if (VideoFragment.this.selectCount == 0) {
                    VideoFragment.this.actionMode.finish();
                    VideoFragment.this.adapter.notifyDataSetChanged();
                }
            } else if (VideoFragment.this.selectCount == 0) {
                Intent i = new Intent("android.intent.action.VIEW");
                String name = (videosList.get(position)).name;
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1));
                Uri uri = FileProvider.getUriForFile(requireActivity(), requireActivity().getPackageName() + ".provider", new File(((MediaFile) videosList.get(position)).path));
                i.setDataAndType(uri, type);
                i.addFlags(1);
                i.addFlags(268435456);
                for (ResolveInfo resolveInfo : VideoFragment.this.getActivity().getPackageManager().queryIntentActivities(i, 65536)) {
                    VideoFragment.this.getActivity().grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                }
                VideoFragment.this.startActivity(i);
            }
        }

        @Override
        public void onLongClick(int position) {
            if (VideoFragment.this.actionMode == null) {
                VideoFragment.this.actionMode = requireActivity().startActionMode(VideoFragment.this.callback);
            }
            VideoFragment.this.toggleSelection(position);
            VideoFragment.this.actionMode.setTitle(VideoFragment.this.selectCount + " " + "Selected");
            if (VideoFragment.this.selectCount == 0) {
                VideoFragment.this.actionMode.finish();
            }
        }
    };

    @RequiresApi(api = 19)
    public static String checkSDCARD(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_video, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            LayoutParams p = new LayoutParams(-1, -1);
            p.weight = 6.0f;
            this.firstLinearLaout.setLayoutParams(p);
        } catch (Exception e) {
            e.printStackTrace();
        }

        compositeDisposable = new CompositeDisposable();
        onFileLoadingCallback = (FileManager.OnFileLoadingCallback) requireActivity();

        this.linearLayout = (LinearLayout) view.findViewById(R.id.linear1);
        rvVideo = view.findViewById(R.id.grid_video);

        this.adapter = new VideoAdapter(videosList, onMediaFileClickListener);
        rvVideo.setLayoutManager(new GridLayoutManager(requireActivity(), 4, RecyclerView.VERTICAL, false));
        rvVideo.setAdapter(adapter);

        registerForContextMenu(rvVideo);
    }

    @Override
    public void onResume() {
        super.onResume();
        getVideo();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.video_camera_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.vi) {
            startActivity(new Intent("android.media.action.VIDEO_CAPTURE"));
        }
        super.onOptionsItemSelected(item);
        return true;
    }

    private void toggleSelection(int position) {
        ((MediaFile) videosList.get(position)).isSelected = !((MediaFile) videosList.get(position)).isSelected;
        if (((MediaFile) videosList.get(position)).isSelected) {
            this.selectedVideo.add(videosList.get(position));
            this.selectCount++;
        } else {
            this.selectedVideo.add(videosList.get(position));
            this.selectCount--;
        }
        this.adapter.notifyDataSetChanged();
    }

    private void video_delete(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
        builder.setTitle((CharSequence) "Delete Videos");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(v -> {
            new MultipleDeleteTask().execute(new Void[0]);
            alertDialog.dismiss();
            mode.finish();
        });
        alertDialog.getButton(-2).setOnClickListener(v -> {
            alertDialog.dismiss();
            mode.finish();
        });
    }

    private void video_share(ActionMode mode) {
        ArrayList<MediaFile> checked = this.selectedVideo;
        ArrayList<Uri> share = new ArrayList();
        for (int i = 0; i < checked.size(); i++) {
            if (((MediaFile) this.selectedVideo.get(i)).isSelected) {
                Uri uri;
                File file = new File(((MediaFile) this.selectedVideo.get(i)).path);
                if (VERSION.SDK_INT >= 24) {
                    uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
                } else {
                    uri = Uri.fromFile(file);
                }
                share.add(uri);
            }
        }
        Intent i2 = new Intent();
        i2.setAction("android.intent.action.SEND_MULTIPLE");
        i2.setType("video/*");
        i2.addFlags(1);
        i2.putParcelableArrayListExtra("android.intent.extra.STREAM", share);
        startActivity(Intent.createChooser(i2, ""));
        this.adapter.notifyDataSetChanged();
        mode.finish();
    }

    private void deselect() {
        for (int i = 0; i < videosList.size(); i++) {
            ((MediaFile) videosList.get(i)).isSelected = false;
        }
        this.selectCount = 0;
        this.adapter.notifyDataSetChanged();
    }

    private List<MediaFile> cursorLoadVideo() {
        VideoFragment.cursor = requireActivity().getContentResolver().query(VideoFragment.this.uri, VideoFragment.this.projection, null, null, "date_added");
        ArrayList<MediaFile> temp = new ArrayList<>(VideoFragment.cursor.getCount());
        if (VideoFragment.cursor.moveToLast()) {
            do {
                long id = VideoFragment.cursor.getLong(VideoFragment.cursor.getColumnIndex(VideoFragment.this.projection[0]));
                String name = VideoFragment.cursor.getString(VideoFragment.cursor.getColumnIndex(VideoFragment.this.projection[1]));
                String path = VideoFragment.cursor.getString(VideoFragment.cursor.getColumnIndex(VideoFragment.this.projection[2]));
                int duration = VideoFragment.cursor.getInt(VideoFragment.cursor.getColumnIndex(VideoFragment.this.projection[3]));
                File file = new File(path);
                if (file.exists()) {
                    temp.add(new MediaFile(id, name, path, false, "Video", duration, file.length()));
                }
            } while (VideoFragment.cursor.moveToPrevious());
        }
        VideoFragment.cursor.close();
        return temp;
    }

    private void getVideo() {
        if (isThreadStarted)
            return;

        if (videosList.isEmpty()) {
            compositeDisposable.add(Observable.
                    fromIterable(cursorLoadVideo())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> {
                        isThreadStarted = true;
                        onFileLoadingCallback.onFileLoading();
                    })
                    .doOnNext(mediaFile -> {
                        videosList.add(mediaFile);
                    })
                    .doOnError(throwable -> {
                        isThreadStarted = false;
                        onFileLoadingCallback.onFileLoaded();
                    })
                    .doOnComplete(() -> {
                        isThreadStarted = false;
                        onFileLoadingCallback.onFileLoaded();
                        adapter.notifyDataSetChanged();
                    })
                    .subscribe());
        }
    }

    private void scanFile(File file) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                requireActivity().getContentResolver().delete(uri, null, null);
            }
        });
    }


    private class MultipleDeleteTask extends AsyncTask<Void, Void, Void> {
        private MultipleDeleteTask() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            VideoFragment.this.progressDialog = new ProgressDialog(VideoFragment.this.getActivity());
            VideoFragment.this.progressDialog.setProgressStyle(0);
            VideoFragment.this.progressDialog.setCancelable(false);
            VideoFragment.this.progressDialog.setTitle("Delete");
            VideoFragment.this.progressDialog.setMessage("Please Wait.....");
            VideoFragment.this.progressDialog.show();
        }

        @RequiresApi(api = 19)
        protected Void doInBackground(Void... params) {
            ArrayList<MediaFile> checkedItems = VideoFragment.this.selectedVideo;
            for (int i = 0; i < checkedItems.size(); i++) {
                String S1 = ((MediaFile) VideoFragment.this.selectedVideo.get(i)).path;
                if (S1.startsWith("/storage/emulated/0/")) {
                    VideoFragment.this.scanFile(new File(S1));
                } else if (S1.startsWith(VideoFragment.checkSDCARD(VideoFragment.this.getActivity()))) {
                    if (VERSION.SDK_INT < 24) {
                        VideoFragment.this.scanFile(new File(S1));
                    } else {
                        try {
                            File f = new File(S1);
                            if (StorageHelper.deleteFile(VideoFragment.this.getActivity(), f)) {
                                Toast.makeText(VideoFragment.this.getActivity(), "Done...", 0).show();
                                VideoFragment.this.scanFile(f);
                                if (f.delete()) {
                                    VideoFragment.this.scanFile(f);
                                }
                            } else {
                                Toast.makeText(VideoFragment.this.getActivity(), "Error...", 0).show();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                FileManager.delete(requireActivity(), new File(S1));
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            VideoFragment.this.selectedVideo.clear();
            videosList.clear();
            getVideo();
            Toast.makeText(VideoFragment.this.getActivity(), "Delete Successfully", 0).show();
            VideoFragment.this.progressDialog.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoHolder> {

        private final ArrayList<MediaFile> videoList;
        private final OnMediaFileClickListener listener;

        public VideoAdapter(ArrayList<MediaFile> videoList, OnMediaFileClickListener listener) {
            this.videoList = videoList;
            this.listener = listener;
        }

        @NonNull
        @Override
        public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_video_adapter, parent, false);
            return new VideoHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
            MediaFile mediaFile = videoList.get(position);
            holder.bind(mediaFile, listener);
        }

        @Override
        public int getItemCount() {
            return videoList.size();
        }

        public class VideoHolder extends RecyclerView.ViewHolder {
            private FrameLayout frameLayout;
            private ImageView imageView_video;
            private ImageView img_more_main;
            private TextView tvDuration;
            private View view;

            public VideoHolder(@NonNull View itemView) {
                super(itemView);
                imageView_video = (ImageView) itemView.findViewById(R.id.image_view_video);
                tvDuration = (TextView) itemView.findViewById(R.id.tvDuration);
                frameLayout = (FrameLayout) itemView.findViewById(R.id.frameLayout_video);
                view = itemView.findViewById(R.id.view_video);
                img_more_main = (ImageView) itemView.findViewById(R.id.img_more_main);
            }

            public void bind(MediaFile mediaFile, OnMediaFileClickListener listener) {
                Glide.with(itemView.getContext())
                        .load(mediaFile.path)
                        .placeholder((int) R.drawable.video_place)
                        .error((int) R.drawable.video_place)
                        .centerCrop()
                        .into(imageView_video);

                tvDuration.setText(mediaFile.name);

                if (mediaFile.isSelected) {
                    view.setAlpha(0.5f);
                    frameLayout.setForeground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_check_box_black_24dp));
                } else {
                    view.setAlpha(0.0f);
                    frameLayout.setForeground(null);
                }

                itemView.setOnClickListener(v -> {
                    listener.onClick(getAdapterPosition());
                });

                itemView.setOnLongClickListener(v -> {
                    listener.onLongClick(getAdapterPosition());
                    return true;
                });

                img_more_main.setOnClickListener(v -> {
                    try {
                        PopupMenu popup = new PopupMenu(itemView.getContext(), v);
                        popup.getMenuInflater().inflate(R.menu.first_more, popup.getMenu());
                        popup.show();
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.delete:
                                        Delete_video();
                                        break;
                                    case R.id.details:
                                        Detail_Video();
                                        break;
                                    case R.id.share:
                                        Share_Video();
                                        break;
                                }
                                return true;
                            }

                            private void Share_Video() {
                                Uri uri;
                                VideoFragment.share = ((MediaFile) videoList.get(getAdapterPosition())).path;
                                File file = new File(VideoFragment.share);
                                if (VERSION.SDK_INT >= 24) {
                                    uri = FileProvider.getUriForFile(requireActivity(), requireActivity().getPackageName() + ".provider", file);
                                } else {
                                    uri = Uri.fromFile(file);
                                }
                                Intent i = new Intent();
                                i.setAction("android.intent.action.SEND");
                                i.setType("video/*");
                                i.putExtra("android.intent.extra.STREAM", uri);
                                i.addFlags(1);
                                VideoFragment.this.startActivity(Intent.createChooser(i, ""));
                            }

                            private void Delete_video() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(VideoFragment.this.getActivity());
                                builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
                                builder.setTitle((CharSequence) "Delete Videos");
                                builder.setIcon((int) R.drawable.ic_delete_black_24dp);
                                builder.setPositiveButton((CharSequence) "OK", null);
                                builder.setNegativeButton((CharSequence) "Cancel", null);
                                final AlertDialog alertDialog = builder.create();
                                alertDialog.show();
                                alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
                                    @RequiresApi(api = 19)
                                    public void onClick(View v) {
                                        VideoFragment.share = ((MediaFile) videoList.get(getAdapterPosition())).path;
                                        if (VideoFragment.share.startsWith("/storage/emulated/0/")) {
                                            VideoFragment.this.scanFile(new File(VideoFragment.share));
                                        } else if (VideoFragment.share.startsWith(VideoFragment.checkSDCARD(requireActivity()))) {
                                            if (VERSION.SDK_INT < 24) {
                                                VideoFragment.this.scanFile(new File(VideoFragment.share));
                                            } else {
                                                try {
                                                    File f = new File(VideoFragment.share);
                                                    if (StorageHelper.deleteFile(VideoFragment.this.getActivity(), f)) {
                                                        Toast.makeText(VideoFragment.this.getActivity(), "Done...", 0).show();
                                                        VideoFragment.this.scanFile(f);
                                                        if (f.delete()) {
                                                            VideoFragment.this.scanFile(f);
                                                        }
                                                    } else {
                                                        Toast.makeText(VideoFragment.this.getActivity(), "Error...", 0).show();
                                                    }
                                                } catch (Exception e) {
                                                }
                                            }
                                        }
                                        FileManager.delete(requireActivity(), new File(VideoFragment.share));
                                        alertDialog.dismiss();
                                        videoList.clear();
                                        getVideo();
                                        Toast.makeText(itemView.getContext(), "Delete Successfully", 0).show();
                                    }
                                });
                                alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });
                            }

                            private void Detail_Video() {
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VideoFragment.this.getActivity());
                                View dialogView = VideoFragment.this.getLayoutInflater().inflate(R.layout.detail_video, null);
                                dialogBuilder.setView(dialogView);
                                VideoFragment.this.txtNameDetail_video = (TextView) dialogView.findViewById(R.id.txtNameDetail_video);
                                VideoFragment.this.txtPathDetail_video = (TextView) dialogView.findViewById(R.id.txtPathDetail_video);
                                VideoFragment.this.txtSizeDetail_video = (TextView) dialogView.findViewById(R.id.txtSizeDetail_video);
                                VideoFragment.this.txtPathDetail_video.setText("" + (videoList.get(getAdapterPosition())).path);
                                VideoFragment.this.txtNameDetail_video.setText("" + (videoList.get(getAdapterPosition())).name);
                                VideoFragment.this.txtSizeDetail_video.setText("" + Formatter.formatFileSize(VideoFragment.this.getActivity(), (videoList.get(getAdapterPosition())).imageSize));
                                dialogBuilder.setPositiveButton((CharSequence) "Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                });
                                dialogBuilder.create().show();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

            }
        }
    }
}
