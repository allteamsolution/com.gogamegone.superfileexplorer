package org.vocrama.cleanexpert.ui;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Rect;
import android.os.Environment;
import android.util.DisplayMetrics;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CommonUtils {
    public static String generateTime(long time) {
        int totalSeconds = (int) (time / 1000);
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        if (totalSeconds / 3600 > 0) {
            return String.format("%02d:%02d:%02d", new Object[]{Integer.valueOf(totalSeconds / 3600), Integer.valueOf(minutes), Integer.valueOf(seconds)});
        }
        return String.format("%02d:%02d", new Object[]{Integer.valueOf(minutes), Integer.valueOf(seconds)});
    }

    public static int getStatusHeight(Activity activity) {
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            return activity.getResources().getDimensionPixelSize(((Integer) c.getField("status_bar_height").get(c.newInstance())).intValue());
        } catch (Exception e) {
            e.printStackTrace();
            Rect frame = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
            return frame.top;
        }
    }

    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 60, baos);
        int options = 60;
        while (baos.toByteArray().length / 1024 > 100) {
            baos.reset();
            image.compress(CompressFormat.JPEG, options, baos);
            options -= 10;
        }
        return BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()), null, null);
    }

    public static Bitmap getimage(String srcPath) {
        Options newOpts = new Options();
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        int be = 1;
        if (w > h && ((float) w) > 480.0f) {
            be = (int) (((float) newOpts.outWidth) / 480.0f);
        } else if (w < h && ((float) h) > 800.0f) {
            be = (int) (((float) newOpts.outHeight) / 800.0f);
        }
        if (be <= 0) {
            be = 1;
        }
        newOpts.inSampleSize = be;
        return compressImage(BitmapFactory.decodeFile(srcPath, newOpts));
    }

    public static Bitmap comp(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 100, baos);
        if (baos.toByteArray().length / 1024 > 300) {
            baos.reset();
            image.compress(CompressFormat.JPEG, 50, baos);
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        Options newOpts = new Options();
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        int be = 1;
        if (w > h && ((float) w) > 480.0f) {
            be = (int) (((float) newOpts.outWidth) / 480.0f);
        } else if (w < h && ((float) h) > 800.0f) {
            be = (int) (((float) newOpts.outHeight) / 800.0f);
        }
        if (be <= 0) {
            be = 1;
        }
        newOpts.inSampleSize = be;
        return compressImage(BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()), null, newOpts));
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath;
        if ("mounted".equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return new File(cachePath + File.separator + uniqueName);
    }

    public static int getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static String getAppVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String hashKeyForDisk(String key) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            return bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            return String.valueOf(key.hashCode());
        }
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            String hex = Integer.toHexString(b & 255);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038 A:{SYNTHETIC, Splitter: B:15:0x0038} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003d A:{Catch:{ IOException -> 0x0055 }} */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005d A:{SYNTHETIC, Splitter: B:33:0x005d} */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0062 A:{Catch:{ IOException -> 0x0066 }} */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005d A:{SYNTHETIC, Splitter: B:33:0x005d} */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0062 A:{Catch:{ IOException -> 0x0066 }} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038 A:{SYNTHETIC, Splitter: B:15:0x0038} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003d A:{Catch:{ IOException -> 0x0055 }} */
    public static boolean bitmapToOutPutStream(Bitmap r10, java.io.OutputStream r11) {
        /*
        r6 = 0;
        r3 = 0;
        r1 = new java.io.ByteArrayOutputStream;	 Catch:{ IOException -> 0x0072 }
        r1.<init>();	 Catch:{ IOException -> 0x0072 }
        r8 = android.graphics.Bitmap.CompressFormat.PNG;	 Catch:{ IOException -> 0x0072 }
        r9 = 100;
        r10.compress(r8, r9, r1);	 Catch:{ IOException -> 0x0072 }
        r5 = new java.io.ByteArrayInputStream;	 Catch:{ IOException -> 0x0072 }
        r8 = r1.toByteArray();	 Catch:{ IOException -> 0x0072 }
        r5.<init>(r8);	 Catch:{ IOException -> 0x0072 }
        r4 = new java.io.BufferedInputStream;	 Catch:{ IOException -> 0x0072 }
        r8 = 8192; // 0x2000 float:1.14794E-41 double:4.0474E-320;
        r4.<init>(r5, r8);	 Catch:{ IOException -> 0x0072 }
        r7 = new java.io.BufferedOutputStream;	 Catch:{ IOException -> 0x0074, all -> 0x006b }
        r8 = 8192; // 0x2000 float:1.14794E-41 double:4.0474E-320;
        r7.<init>(r11, r8);	 Catch:{ IOException -> 0x0074, all -> 0x006b }
    L_0x0025:
        r0 = r4.read();	 Catch:{ IOException -> 0x0030, all -> 0x006e }
        r8 = -1;
        if (r0 == r8) goto L_0x0042;
    L_0x002c:
        r7.write(r0);	 Catch:{ IOException -> 0x0030, all -> 0x006e }
        goto L_0x0025;
    L_0x0030:
        r2 = move-exception;
        r3 = r4;
        r6 = r7;
    L_0x0033:
        r2.printStackTrace();	 Catch:{ all -> 0x005a }
        if (r6 == 0) goto L_0x003b;
    L_0x0038:
        r6.close();	 Catch:{ IOException -> 0x0055 }
    L_0x003b:
        if (r3 == 0) goto L_0x0040;
    L_0x003d:
        r3.close();	 Catch:{ IOException -> 0x0055 }
    L_0x0040:
        r8 = 0;
    L_0x0041:
        return r8;
    L_0x0042:
        r8 = 1;
        if (r7 == 0) goto L_0x0048;
    L_0x0045:
        r7.close();	 Catch:{ IOException -> 0x0050 }
    L_0x0048:
        if (r4 == 0) goto L_0x004d;
    L_0x004a:
        r4.close();	 Catch:{ IOException -> 0x0050 }
    L_0x004d:
        r3 = r4;
        r6 = r7;
        goto L_0x0041;
    L_0x0050:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x004d;
    L_0x0055:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x0040;
    L_0x005a:
        r8 = move-exception;
    L_0x005b:
        if (r6 == 0) goto L_0x0060;
    L_0x005d:
        r6.close();	 Catch:{ IOException -> 0x0066 }
    L_0x0060:
        if (r3 == 0) goto L_0x0065;
    L_0x0062:
        r3.close();	 Catch:{ IOException -> 0x0066 }
    L_0x0065:
        throw r8;
    L_0x0066:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x0065;
    L_0x006b:
        r8 = move-exception;
        r3 = r4;
        goto L_0x005b;
    L_0x006e:
        r8 = move-exception;
        r3 = r4;
        r6 = r7;
        goto L_0x005b;
    L_0x0072:
        r2 = move-exception;
        goto L_0x0033;
    L_0x0074:
        r2 = move-exception;
        r3 = r4;
        goto L_0x0033;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mazhuang.cleanexpert.ui.CommonUtils.bitmapToOutPutStream(android.graphics.Bitmap, java.io.OutputStream):boolean");
    }

    public static int getScreenWidth(Activity context) {
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getScreenHeight(Activity context) {
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static void saveLoadTime(Context context, long time) {
        Editor edit = context.getSharedPreferences("loaddataTime", 0).edit();
        edit.putLong("time", time);
        edit.commit();
    }

    public static long getLastLoadTime(Context context) {
        return context.getSharedPreferences("loaddataTime", 0).getLong("time", 0);
    }

    public static boolean checkTimeOut(Context context) {
        long time = System.currentTimeMillis();
        if (time - getLastLoadTime(context) <= 7200000) {
            return false;
        }
        saveLoadTime(context, time);
        return true;
    }

    public static int dip2px(Context context, float dipValue) {
        return (int) ((dipValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }
}
