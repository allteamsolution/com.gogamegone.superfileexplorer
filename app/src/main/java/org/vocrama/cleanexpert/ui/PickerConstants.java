package org.vocrama.cleanexpert.ui;

class PickerConstants {
    PickerConstants() {
    }

    public static String getDurationMark(int time) {
        if (time == 0) {
            time = 0;
        }
        int duration = time / 1000;
        int hours = duration / 3600;
        int minutes = (duration % 3600) / 60;
        int seconds = duration % 60;
        StringBuilder sb = new StringBuilder();
        if (hours > 0) {
            sb.append(hours).append(":");
        }
        if (minutes < 10) {
            sb.append("0").append(minutes);
        } else {
            sb.append(minutes);
        }
        sb.append(":");
        if (seconds < 10) {
            sb.append("0").append(seconds);
        } else {
            sb.append(seconds);
        }
        return sb.toString();
    }

    public static String getBytesString(long bytes) {
        String[] quantifiers = new String[]{"KB", "MB", "GB", "TB"};
        double speedNum = (double) bytes;
        for (int i = 0; i < quantifiers.length; i++) {
            speedNum /= 1024.0d;
            if (speedNum < 512.0d) {
                return String.format("%.2f", new Object[]{Double.valueOf(speedNum)}) + " " + quantifiers[i];
            }
        }
        return "";
    }
}
