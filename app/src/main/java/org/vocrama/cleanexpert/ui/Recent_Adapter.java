package org.vocrama.cleanexpert.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap.Config;
import android.provider.MediaStore.Audio.Media;
import android.provider.MediaStore.Files;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gogamegone.superfileexplorer.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;


public class Recent_Adapter extends RecyclerView.Adapter<Recent_Adapter.MainViewHolder> {
    public static ArrayList<Document> mFragmentRecent_recent = new ArrayList();
    private final Listener callback;
    private final List<Integer> selectedIndices = new ArrayList();
    private ArrayList<Document> recent;

    public Recent_Adapter(Listener callback, ArrayList<Document> recent) {
        this.callback = callback;
        this.recent = recent;
    }

    public List<Integer> getSelectedIndices() {
        return this.selectedIndices;
    }

    public void toggleSelected(int index) {
        if (this.selectedIndices.contains(Integer.valueOf(index))) {
            this.selectedIndices.remove(Integer.valueOf(index));
        } else {
            this.selectedIndices.add(Integer.valueOf(index));
        }
        notifyItemChanged(index);
        if (this.callback != null) {
            this.callback.onSelectionChanged(this.selectedIndices.size());
        }
    }

    public void clearSelected() {
        if (!this.selectedIndices.isEmpty()) {
            this.selectedIndices.clear();
            notifyDataSetChanged();
            if (this.callback != null) {
                this.callback.onSelectionChanged(0);
            }
        }
    }

    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_video, parent, false), this.callback);
    }

    @SuppressLint({"ResourceType"})
    public void onBindViewHolder(MainViewHolder holder, int position) {
        String s1 = Files.getContentUri("external") + "/" + ((Document) this.recent.get(position)).getDataId();
        String type = ((Document) this.recent.get(position)).getType();
        Context c = holder.itemView.getContext();
        holder.videoIcon.setVisibility(8);
        String toLowerCase = type.toLowerCase();
        int i = -1;
        switch (toLowerCase.hashCode()) {
            case 1422702:
                if (toLowerCase.equals(".3gp")) {
                    i = 16;
                    break;
                }
                break;
            case 1467366:
                if (toLowerCase.equals(".avi")) {
                    i = 12;
                    break;
                }
                break;
            case 1470026:
                if (toLowerCase.equals(".doc")) {
                    i = 5;
                    break;
                }
                break;
            case 1471874:
                if (toLowerCase.equals(".flv")) {
                    i = 13;
                    break;
                }
                break;
            case 1478570:
                if (toLowerCase.equals(".mkv")) {
                    i = 11;
                    break;
                }
                break;
            case 1478658:
                if (toLowerCase.equals(".mp3")) {
                    i = 0;
                    break;
                }
                break;
            case 1478659:
                if (toLowerCase.equals(".mp4")) {
                    i = 10;
                    break;
                }
                break;
            case 1478694:
                if (toLowerCase.equals(".mov")) {
                    i = 15;
                    break;
                }
                break;
            case 1481220:
                if (toLowerCase.equals(".pdf")) {
                    i = 3;
                    break;
                }
                break;
            case 1481606:
                if (toLowerCase.equals(".ppt")) {
                    i = 7;
                    break;
                }
                break;
            case 1485698:
                if (toLowerCase.equals(".txt")) {
                    i = 2;
                    break;
                }
                break;
            case 1487870:
                if (toLowerCase.equals(".wav")) {
                    i = 1;
                    break;
                }
                break;
            case 1488242:
                if (toLowerCase.equals(".wmv")) {
                    i = 14;
                    break;
                }
                break;
            case 1489169:
                if (toLowerCase.equals(".xls")) {
                    i = 6;
                    break;
                }
                break;
            case 45565749:
                if (toLowerCase.equals(".divx")) {
                    i = 17;
                    break;
                }
                break;
            case 45570926:
                if (toLowerCase.equals(".docx")) {
                    i = 4;
                    break;
                }
                break;
            case 45695193:
                if (toLowerCase.equals(".html")) {
                    i = 9;
                    break;
                }
                break;
            case 45929906:
                if (toLowerCase.equals(".pptx")) {
                    i = 8;
                    break;
                }
                break;
        }
        switch (i) {
            case 0:
            case 1:
                holder.imageLoader.displayImage(Media.EXTERNAL_CONTENT_URI.toString() + "/" + ((Document) this.recent.get(position)).getDataId() + "/albumart", holder.imageView, holder.options);
                break;
            case 2:
                holder.imageView.setPadding(50, 50, 50, 50);
                try {
                    Glide.with(c).load(Integer.valueOf(R.drawable.im_txt)).into(holder.imageView);
                    break;
                } catch (Exception e) {
                    break;
                }
            case 3:
                holder.imageView.setPadding(50, 50, 50, 50);
                try {
                    Glide.with(c).load(Integer.valueOf(R.drawable.im_pdf)).into(holder.imageView);
                    break;
                } catch (Exception e2) {
                    break;
                }
            case 4:
            case 5:
                holder.imageView.setPadding(50, 50, 50, 50);
                try {
                    Glide.with(c).load(Integer.valueOf(R.drawable.im_doc)).into(holder.imageView);
                    break;
                } catch (Exception e3) {
                    break;
                }
            case 6:
            case 7:
            case 8:
                holder.imageView.setPadding(50, 50, 50, 50);
                try {
                    Glide.with(c).load(Integer.valueOf(R.drawable.im_txt)).into(holder.imageView);
                    break;
                } catch (Exception e4) {
                    break;
                }
            case 9:
                holder.imageView.setPadding(50, 50, 50, 50);
                try {
                    Glide.with(c).load(Integer.valueOf(R.drawable.im_txt)).into(holder.imageView);
                    break;
                } catch (Exception e5) {
                    break;
                }
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
                Glide.with(c).load(s1).into(holder.imageView);
                holder.videoIcon.setVisibility(0);
                break;
            default:
                ((Document) mFragmentRecent_recent.get(position)).getDataId();
                Glide.with(c).load(s1).into(holder.imageView);
                break;
        }
        holder.thumb_textview.setText(((Document) this.recent.get(position)).getDataName());
        if (this.selectedIndices.contains(Integer.valueOf(position))) {
            holder.viewAl.setAlpha(0.5f);
            holder.frameLayout.setForeground(ContextCompat.getDrawable(c, R.drawable.ic_check_box_black_24dp));
            return;
        }
        holder.viewAl.setAlpha(0.0f);
        holder.frameLayout.setForeground(null);
    }

    public int getItemCount() {
        return this.recent.size();
    }

    public interface Listener {
        void onClick(int i);

        void onLongClick(int i);

        void onSelectionChanged(int i);
    }

    static class MainViewHolder extends RecyclerView.ViewHolder implements OnClickListener, OnLongClickListener {
        private final Listener callback;
        FrameLayout frameLayout;
        private ImageLoader imageLoader = ImageLoader.getInstance();
        private ImageView imageView;
        private DisplayImageOptions options;
        private TextView thumb_textview;
        private ImageView videoIcon;
        private View viewAl;

        MainViewHolder(View itemView, Listener callback) {
            super(itemView);
            this.callback = callback;
            this.imageView = (ImageView) itemView.findViewById(R.id.thumb_image);
            this.frameLayout = (FrameLayout) itemView.findViewById(R.id.frameLayout);
            this.viewAl = itemView.findViewById(R.id.view_alpha);
            this.thumb_textview = (TextView) itemView.findViewById(R.id.thumb_textview);
            this.thumb_textview.setTextColor(R.color.black);
            this.itemView.setOnClickListener(this);
            this.itemView.setOnLongClickListener(this);
            this.videoIcon = (ImageView) itemView.findViewById(R.id.iv_icon);
            this.options = new Builder().showImageOnLoading((int) R.drawable.audioicon).showImageForEmptyUri((int) R.drawable.audioicon).showImageOnFail((int) R.drawable.audioicon).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).bitmapConfig(Config.RGB_565).build();
            ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(itemView.getContext()));
            this.videoIcon.setVisibility(8);
        }

        public void onClick(View v) {
            if (this.callback != null) {
                this.callback.onClick(getAdapterPosition());
            }
        }

        public boolean onLongClick(View v) {
            if (this.callback != null) {
                this.callback.onLongClick(getAdapterPosition());
            }
            return true;
        }
    }
}
