package org.vocrama.cleanexpert.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.gogamegone.superfileexplorer.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import org.vocrama.cleanexpert.callback.IScanCallback;
import org.vocrama.cleanexpert.model.JunkGroup;
import org.vocrama.cleanexpert.model.JunkInfo;
import org.vocrama.cleanexpert.task.OverallScanTask;
import org.vocrama.cleanexpert.task.ProcessScanTask;
import org.vocrama.cleanexpert.task.SysCacheScanTask;
import org.vocrama.cleanexpert.ui.view.ListHeaderView;
import org.vocrama.cleanexpert.util.CleanUtil;

public class JunkCleanActivity extends BaseActivity {
    public static final String HANG_FLAG = "hanged";
    public static final int MSG_OVERALL_BEGIN = 4129;
    public static final int MSG_OVERALL_CLEAN_FINISH = 4354;
    public static final int MSG_OVERALL_FINISH = 4131;
    public static final int MSG_OVERALL_POS = 4130;
    public static final int MSG_PROCESS_BEGIN = 4113;
    public static final int MSG_PROCESS_CLEAN_FINISH = 4353;
    public static final int MSG_PROCESS_FINISH = 4115;
    public static final int MSG_PROCESS_POS = 4114;
    public static final int MSG_SYS_CACHE_BEGIN = 4097;
    public static final int MSG_SYS_CACHE_CLEAN_FINISH = 4352;
    public static final int MSG_SYS_CACHE_FINISH = 4099;
    public static final int MSG_SYS_CACHE_POS = 4098;
    private Handler handler;
    private BaseExpandableListAdapter mAdapter;
    private Button mCleanButton;
    private ListHeaderView mHeaderView;
    private boolean mIsOverallCleanFinish = false;
    private boolean mIsOverallScanFinish = false;
    private boolean mIsProcessCleanFinish = false;
    private boolean mIsProcessScanFinish = false;
    private boolean mIsScanning = false;
    private boolean mIsSysCacheCleanFinish = false;
    private boolean mIsSysCacheScanFinish = false;
    private HashMap<Integer, JunkGroup> mJunkGroups = null;

    public static class ChildViewHolder {
        public TextView mJunkSizeTv;
        public TextView mJunkTypeTv;
    }

    public static class GroupViewHolder {
        public TextView mPackageNameTv;
        public TextView mPackageSizeTv;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_junk_clean);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 4098:
                        JunkCleanActivity.this.mHeaderView.mProgress.setText("System Cache pos:" + ((JunkInfo) msg.obj).mPackageName);
                        JunkCleanActivity.this.mHeaderView.mSize.setText(CleanUtil.formatShortFileSize(JunkCleanActivity.this, JunkCleanActivity.this.getTotalSize()));
                        return;
                    case 4099:
                        JunkCleanActivity.this.mIsSysCacheScanFinish = true;
                        JunkCleanActivity.this.checkScanFinish();
                        return;
                    case JunkCleanActivity.MSG_PROCESS_POS /*4114*/:
                        JunkCleanActivity.this.mHeaderView.mProgress.setText("process pos..:" + ((JunkInfo) msg.obj).mPackageName);
                        JunkCleanActivity.this.mHeaderView.mSize.setText(CleanUtil.formatShortFileSize(JunkCleanActivity.this, JunkCleanActivity.this.getTotalSize()));
                        return;
                    case JunkCleanActivity.MSG_PROCESS_FINISH /*4115*/:
                        JunkCleanActivity.this.mIsProcessScanFinish = true;
                        JunkCleanActivity.this.checkScanFinish();
                        return;
                    case JunkCleanActivity.MSG_OVERALL_POS /*4130*/:
                        JunkCleanActivity.this.mHeaderView.mProgress.setText("Overall pos.." + ((JunkInfo) msg.obj).mPath);
                        JunkCleanActivity.this.mHeaderView.mSize.setText(CleanUtil.formatShortFileSize(JunkCleanActivity.this, JunkCleanActivity.this.getTotalSize()));
                        return;
                    case JunkCleanActivity.MSG_OVERALL_FINISH /*4131*/:
                        JunkCleanActivity.this.mIsOverallScanFinish = true;
                        JunkCleanActivity.this.checkScanFinish();
                        return;
                    case JunkCleanActivity.MSG_SYS_CACHE_CLEAN_FINISH /*4352*/:
                        JunkCleanActivity.this.mIsSysCacheCleanFinish = true;
                        JunkCleanActivity.this.checkCleanFinish();
                        Bundle bundle = msg.getData();
                        if (bundle != null && bundle.getBoolean(JunkCleanActivity.HANG_FLAG, false)) {
                            Toast.makeText(JunkCleanActivity.this, "system clean finish..", 0).show();
                            return;
                        }
                        return;
                    case JunkCleanActivity.MSG_PROCESS_CLEAN_FINISH /*4353*/:
                        JunkCleanActivity.this.mIsProcessCleanFinish = true;
                        JunkCleanActivity.this.checkCleanFinish();
                        return;
                    case JunkCleanActivity.MSG_OVERALL_CLEAN_FINISH /*4354*/:
                        JunkCleanActivity.this.mIsOverallCleanFinish = true;
                        JunkCleanActivity.this.checkCleanFinish();
                        return;
                    default:
                        return;
                }
            }
        };
        this.mCleanButton = (Button) findViewById(R.id.do_junk_clean);
        this.mCleanButton.setEnabled(false);
        this.mCleanButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                JunkCleanActivity.this.mCleanButton.setEnabled(false);
                JunkCleanActivity.this.clearAll();
            }
        });
        resetState();
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.junk_list);
        this.mHeaderView = new ListHeaderView(this, listView);
        this.mHeaderView.mProgress.setGravity(19);
        listView.addHeaderView(this.mHeaderView);
        listView.setGroupIndicator(null);
        listView.setChildIndicator(null);
        listView.setDividerHeight(0);
        listView.setOnChildClickListener(new OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                JunkInfo info = (JunkInfo) JunkCleanActivity.this.mAdapter.getChild(groupPosition, childPosition);
                if (groupPosition != 2 && !info.mIsChild && (groupPosition != 5 || info.mIsChild || info.mPath == null)) {
                    int childrenInThisGroup = JunkCleanActivity.this.mAdapter.getChildrenCount(groupPosition);
                    for (int i = childPosition + 1; i < childrenInThisGroup; i++) {
                        JunkInfo child = (JunkInfo) JunkCleanActivity.this.mAdapter.getChild(groupPosition, i);
                        if (!child.mIsChild) {
                            break;
                        }
                        child.mIsVisible = !child.mIsVisible;
                    }
                    JunkCleanActivity.this.mAdapter.notifyDataSetChanged();
                } else if (info.mPath != null) {
                    Toast.makeText(JunkCleanActivity.this, info.mPath, 0).show();
                }
                return false;
            }
        });
        this.mAdapter = new BaseExpandableListAdapter() {
            public int getGroupCount() {
                return JunkCleanActivity.this.mJunkGroups.size();
            }

            public int getChildrenCount(int groupPosition) {
                if (((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(groupPosition))).mChildren != null) {
                    return ((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(groupPosition))).mChildren.size();
                }
                return 0;
            }

            public Object getGroup(int groupPosition) {
                return JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(groupPosition));
            }

            public Object getChild(int groupPosition, int childPosition) {
                return ((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(groupPosition))).mChildren.get(childPosition);
            }

            public long getGroupId(int groupPosition) {
                return 0;
            }

            public long getChildId(int groupPosition, int childPosition) {
                return 0;
            }

            public boolean hasStableIds() {
                return false;
            }

            public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
                GroupViewHolder holder;
                if (convertView == null) {
                    convertView = LayoutInflater.from(JunkCleanActivity.this).inflate(R.layout.group_list, null);
                    holder = new GroupViewHolder();
                    holder.mPackageNameTv = (TextView) convertView.findViewById(R.id.package_name);
                    holder.mPackageSizeTv = (TextView) convertView.findViewById(R.id.package_size);
                    convertView.setTag(holder);
                } else {
                    holder = (GroupViewHolder) convertView.getTag();
                }
                JunkGroup group = (JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(groupPosition));
                holder.mPackageNameTv.setText(group.mName);
                holder.mPackageSizeTv.setText(CleanUtil.formatShortFileSize(JunkCleanActivity.this, group.mSize));
                return convertView;
            }

            public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
                JunkInfo info = (JunkInfo) ((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(groupPosition))).mChildren.get(childPosition);
                if (!info.mIsVisible) {
                    return LayoutInflater.from(JunkCleanActivity.this).inflate(R.layout.item_null, null);
                }
                if (info.mIsChild) {
                    convertView = LayoutInflater.from(JunkCleanActivity.this).inflate(R.layout.level2_item_list, null);
                } else {
                    convertView = LayoutInflater.from(JunkCleanActivity.this).inflate(R.layout.level1_item_list, null);
                }
                ChildViewHolder holder = new ChildViewHolder();
                holder.mJunkTypeTv = (TextView) convertView.findViewById(R.id.junk_type);
                holder.mJunkSizeTv = (TextView) convertView.findViewById(R.id.junk_size);
                holder.mJunkTypeTv.setText(info.name);
                holder.mJunkSizeTv.setText(CleanUtil.formatShortFileSize(JunkCleanActivity.this, info.mSize));
                return convertView;
            }

            public boolean isChildSelectable(int groupPosition, int childPosition) {
                return true;
            }
        };
        listView.setAdapter(this.mAdapter);
        if (!this.mIsScanning) {
            this.mIsScanning = true;
            startScan();
        }
    }

    private void clearAll() {
        new Thread(new Runnable() {
            public void run() {
                Iterator it = ((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(0))).mChildren.iterator();
                while (it.hasNext()) {
                    CleanUtil.killAppProcesses(((JunkInfo) it.next()).mPackageName);
                }
                JunkCleanActivity.this.handler.obtainMessage(JunkCleanActivity.MSG_PROCESS_CLEAN_FINISH).sendToTarget();
                CleanUtil.freeAllAppsCache(JunkCleanActivity.this.handler);
                ArrayList<JunkInfo> junks = new ArrayList();
                junks.addAll(((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(2))).mChildren);
                junks.addAll(((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(4))).mChildren);
                junks.addAll(((JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(3))).mChildren);
                CleanUtil.freeJunkInfos(junks, JunkCleanActivity.this.handler);
            }
        }).start();
    }

    private void resetState() {
        this.mIsScanning = false;
        this.mIsSysCacheScanFinish = false;
        this.mIsSysCacheCleanFinish = false;
        this.mIsProcessScanFinish = false;
        this.mIsProcessCleanFinish = false;
        this.mJunkGroups = new HashMap();
        this.mCleanButton.setEnabled(false);
        JunkGroup cacheGroup = new JunkGroup();
        cacheGroup.mName = getString(R.string.cache_clean);
        cacheGroup.mChildren = new ArrayList();
        this.mJunkGroups.put(Integer.valueOf(1), cacheGroup);
        JunkGroup processGroup = new JunkGroup();
        processGroup.mName = getString(R.string.process_clean);
        processGroup.mChildren = new ArrayList();
        this.mJunkGroups.put(Integer.valueOf(0), processGroup);
        JunkGroup apkGroup = new JunkGroup();
        apkGroup.mName = getString(R.string.apk_clean);
        apkGroup.mChildren = new ArrayList();
        this.mJunkGroups.put(Integer.valueOf(2), apkGroup);
        JunkGroup tmpGroup = new JunkGroup();
        tmpGroup.mName = getString(R.string.tmp_clean);
        tmpGroup.mChildren = new ArrayList();
        this.mJunkGroups.put(Integer.valueOf(3), tmpGroup);
        JunkGroup logGroup = new JunkGroup();
        logGroup.mName = getString(R.string.log_clean);
        logGroup.mChildren = new ArrayList();
        this.mJunkGroups.put(Integer.valueOf(4), logGroup);
    }

    private void checkScanFinish() {
        this.mAdapter.notifyDataSetChanged();
        if (this.mIsProcessScanFinish && this.mIsSysCacheScanFinish && this.mIsOverallScanFinish) {
            this.mIsScanning = false;
            JunkGroup cacheGroup = (JunkGroup) this.mJunkGroups.get(Integer.valueOf(1));
            ArrayList<JunkInfo> children = cacheGroup.mChildren;
            cacheGroup.mChildren = new ArrayList();
            Iterator it = children.iterator();
            while (it.hasNext()) {
                JunkInfo info = (JunkInfo) it.next();
                cacheGroup.mChildren.add(info);
                if (info.mChildren != null) {
                    cacheGroup.mChildren.addAll(info.mChildren);
                }
            }
            String totalSize = CleanUtil.formatShortFileSize(this, getTotalSize());
            this.mHeaderView.mSize.setText(totalSize);
            this.mHeaderView.mProgress.setText("Total size:" + totalSize);
            this.mHeaderView.mProgress.setGravity(17);
            this.mCleanButton.setEnabled(true);
        }
    }

    private void checkCleanFinish() {
        if (this.mIsProcessCleanFinish && this.mIsSysCacheCleanFinish && this.mIsOverallCleanFinish) {
            this.mHeaderView.mProgress.setText("Clean finished..");
            this.mHeaderView.mSize.setText(CleanUtil.formatShortFileSize(this, 0));
            for (JunkGroup group : this.mJunkGroups.values()) {
                group.mSize = 0;
                group.mChildren = null;
            }
            this.mAdapter.notifyDataSetChanged();
        }
    }

    private void startScan() {
        new ProcessScanTask(new IScanCallback() {
            public void onBegin() {
                JunkCleanActivity.this.handler.obtainMessage(JunkCleanActivity.MSG_PROCESS_BEGIN).sendToTarget();
            }

            public void onProgress(JunkInfo info) {
                Message msg = JunkCleanActivity.this.handler.obtainMessage(JunkCleanActivity.MSG_PROCESS_POS);
                msg.obj = info;
                msg.sendToTarget();
            }

            public void onFinish(ArrayList<JunkInfo> children) {
                JunkGroup cacheGroup = (JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(0));
                cacheGroup.mChildren.addAll(children);
                Iterator it = children.iterator();
                while (it.hasNext()) {
                    cacheGroup.mSize += ((JunkInfo) it.next()).mSize;
                }
                JunkCleanActivity.this.handler.obtainMessage(JunkCleanActivity.MSG_PROCESS_FINISH).sendToTarget();
            }
        }).execute(new Void[0]);
        new SysCacheScanTask(new IScanCallback() {
            public void onBegin() {
                JunkCleanActivity.this.handler.obtainMessage(4097).sendToTarget();
            }

            public void onProgress(JunkInfo info) {
                Message msg = JunkCleanActivity.this.handler.obtainMessage(4098);
                msg.obj = info;
                msg.sendToTarget();
            }

            public void onFinish(ArrayList<JunkInfo> children) {
                JunkGroup cacheGroup = (JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(1));
                cacheGroup.mChildren.addAll(children);
                Collections.sort(cacheGroup.mChildren);
                Collections.reverse(cacheGroup.mChildren);
                Iterator it = children.iterator();
                while (it.hasNext()) {
                    cacheGroup.mSize += ((JunkInfo) it.next()).mSize;
                }
                JunkCleanActivity.this.handler.obtainMessage(4099).sendToTarget();
            }
        }).execute(new Void[0]);
        new OverallScanTask(new IScanCallback() {
            public void onBegin() {
                JunkCleanActivity.this.handler.obtainMessage(JunkCleanActivity.MSG_OVERALL_BEGIN).sendToTarget();
            }

            public void onProgress(JunkInfo info) {
                Message msg = JunkCleanActivity.this.handler.obtainMessage(JunkCleanActivity.MSG_OVERALL_POS);
                msg.obj = info;
                msg.sendToTarget();
            }

            public void onFinish(ArrayList<JunkInfo> children) {
                Iterator it = children.iterator();
                while (it.hasNext()) {
                    JunkInfo info = (JunkInfo) it.next();
                    String path = ((JunkInfo) info.mChildren.get(0)).mPath;
                    int groupFlag = 0;
                    if (path.endsWith(".apk")) {
                        groupFlag = 2;
                    } else if (path.endsWith(".log")) {
                        groupFlag = 4;
                    } else if (path.endsWith(".tmp") || path.endsWith(".temp")) {
                        groupFlag = 3;
                    }
                    JunkGroup cacheGroup = (JunkGroup) JunkCleanActivity.this.mJunkGroups.get(Integer.valueOf(groupFlag));
                    cacheGroup.mChildren.addAll(info.mChildren);
                    cacheGroup.mSize = info.mSize;
                }
                JunkCleanActivity.this.handler.obtainMessage(JunkCleanActivity.MSG_OVERALL_FINISH).sendToTarget();
            }
        }).execute(new Void[0]);
    }

    private long getTotalSize() {
        long size = 0;
        for (JunkGroup group : this.mJunkGroups.values()) {
            size += group.mSize;
        }
        return size;
    }
}
