package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ListAdapterExpandible extends BaseExpandableListAdapter {
    public static LinearLayout linearLayout;
    private HashMap<String, List<temp>> childArray;
    private Context context;
    private List<String> headerArray;
    private LayoutInflater infalInflater = ((LayoutInflater) this.context.getSystemService("layout_inflater"));

    public ListAdapterExpandible(Context context, ArrayList<String> headerArray, HashMap<String, List<temp>> listChildData) {
        this.context = context;
        this.headerArray = headerArray;
        this.childArray = listChildData;
    }

    public int getGroupCount() {
        return this.headerArray.size();
    }

    public int getChildrenCount(int groupPosition) {
        return ((List) this.childArray.get(this.headerArray.get(groupPosition))).size();
    }

    public Object getGroup(int groupPosition) {
        return this.headerArray.get(groupPosition);
    }

    public Object getChild(int groupPosition, int childPosition) {
        return ((List) this.childArray.get(this.headerArray.get(groupPosition))).get(childPosition);
    }

    public long getGroupId(int groupPosition) {
        return (long) groupPosition;
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) childPosition;
    }

    public boolean hasStableIds() {
        return false;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = this.infalInflater.inflate(R.layout.list_group_header, null);
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        ((TextView) convertView.findViewById(R.id.textViewHeader)).setText(headerTitle);
        if (isExpanded) {
            imageView.setImageResource(R.drawable.indicator);
        } else {
            imageView.setImageResource(R.drawable.indicatorrev);
        }
        return convertView;
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        temp temp = (temp) getChild(groupPosition, childPosition);
        Integer images = Integer.valueOf(temp.getImages());
        String expandedListText = temp.getName();
        if (convertView == null) {
            convertView = this.infalInflater.inflate(R.layout.list_sub_child, null);
        }
        TextView textViewChild = (TextView) convertView.findViewById(R.id.textViewChild);
        ((ImageView) convertView.findViewById(R.id.image)).setImageDrawable(this.context.getResources().getDrawable(images.intValue()));
        textViewChild.setText(expandedListText);
        return convertView;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
