package org.vocrama.cleanexpert.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.view.AppAdapter;
import org.vocrama.cleanexpert.ui.view.AppInfo;

import java.util.ArrayList;
import java.util.List;

public class SoftwareActivity extends AppCompatActivity {
    AppAdapter adapter;
    List<AppInfo> appInfoList = new ArrayList();
    String appType;
    Button btn_delete;
    CheckBox cb_deleteall;
    ListView ll_software;
    ProgressBar pb_softmgr_loading;
    Handler handler = new Handler(new Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    SoftwareActivity.this.ll_software.setVisibility(View.VISIBLE);
                    SoftwareActivity.this.pb_softmgr_loading.setVisibility(4);
                    SoftwareActivity.this.adapter.notifyDataSetChanged();
                    break;
            }
            return false;
        }
    });
    BroadcastReceiver receiver;
    String softwareType;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_software);


        initView();
        this.receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                SoftwareActivity.this.saveAppInfo();
                SoftwareActivity.this.adapter.notifyDataSetChanged();
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PACKAGE_REMOVED");
        filter.addDataScheme("package");
        registerReceiver(this.receiver, filter);
    }

    protected void onDestroy() {
        unregisterReceiver(this.receiver);
        super.onDestroy();
    }

    private void initView() {
        this.appType = getIntent().getBundleExtra("bundle").getString("appType", "all");
        this.softwareType = getIntent().getBundleExtra("bundle").getString("softwareType", "所有软件");
        this.ll_software = (ListView) findViewById(R.id.ll_software);
        this.pb_softmgr_loading = (ProgressBar) findViewById(R.id.pb_softmgr_loading);
        this.cb_deleteall = (CheckBox) findViewById(R.id.cb_deleteall);
        this.btn_delete = (Button) findViewById(R.id.btn_delete);
        this.adapter = new AppAdapter(this.appInfoList, this);
        this.ll_software.setAdapter(this.adapter);
        saveAppInfo();
        this.cb_deleteall.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for (int i = 0; i < SoftwareActivity.this.appInfoList.size(); i++) {
                    if (SoftwareActivity.this.appType.equals("all")) {
                        if (!((AppInfo) SoftwareActivity.this.appInfoList.get(i)).isSystem) {
                            ((AppInfo) SoftwareActivity.this.appInfoList.get(i)).isDelete = b;
                        }
                    } else if (SoftwareActivity.this.appType.equals("system")) {
                        ((AppInfo) SoftwareActivity.this.appInfoList.get(i)).isDelete = false;
                    } else {
                        ((AppInfo) SoftwareActivity.this.appInfoList.get(i)).isDelete = b;
                    }
                }
                SoftwareActivity.this.adapter.notifyDataSetChanged();
            }
        });
        this.btn_delete.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                for (AppInfo info : SoftwareActivity.this.appInfoList) {
                    if (info.isDelete && !info.packagename.equals(SoftwareActivity.this.getPackageName())) {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.DELETE");
                        intent.setData(Uri.parse("package:" + info.packagename));
                        SoftwareActivity.this.startActivity(intent);
                    }
                }
            }
        });
    }

    private void saveAppInfo() {
        this.pb_softmgr_loading.setVisibility(View.VISIBLE);
        this.ll_software.setVisibility(View.INVISIBLE);
        new Thread(new Runnable() {
            public void run() {
                SoftwareActivity.this.appInfoList.clear();
                for (PackageInfo info : SoftwareActivity.this.getPackageManager().getInstalledPackages(1)) {
                    boolean isSystem;
                    ApplicationInfo applicationInfo = info.applicationInfo;
                    if ((applicationInfo.flags & 128) == 0 && (applicationInfo.flags & 1) == 0) {
                        isSystem = false;
                    } else {
                        isSystem = true;
                    }
                    String appname = (String) SoftwareActivity.this.getPackageManager().getApplicationLabel(applicationInfo);
                    Drawable appicon = SoftwareActivity.this.getPackageManager().getApplicationIcon(applicationInfo);
                    String appversion = info.versionName;
                    String packagename = info.packageName;
                    if (SoftwareActivity.this.appType.equals("all")) {
                        SoftwareActivity.this.appInfoList.add(new AppInfo(appicon, appname, packagename, appversion, isSystem, false));
                    } else if (SoftwareActivity.this.appType.equals("system")) {
                        if (isSystem) {
                            SoftwareActivity.this.appInfoList.add(new AppInfo(appicon, appname, packagename, appversion, isSystem, false));
                        }
                    } else if (!isSystem) {
                        SoftwareActivity.this.appInfoList.add(new AppInfo(appicon, appname, packagename, appversion, isSystem, false));
                    }
                }
                Message message = SoftwareActivity.this.handler.obtainMessage();
                message.what = 1;
                SoftwareActivity.this.handler.sendMessage(message);
            }
        }).start();
    }


}
