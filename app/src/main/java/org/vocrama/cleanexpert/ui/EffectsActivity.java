package org.vocrama.cleanexpert.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.photoeffect.ImageProcessing;
import org.vocrama.cleanexpert.ui.ProcessingAsyncTask.ProcessingListener;

import java.util.ArrayList;
import java.util.Iterator;

public class EffectsActivity extends BaseTheme {
    public static DrawingPanel drawView;
    public HorizontalListView horlist;
    float angle = 0.0f;
    ImageView finalIMG;
    ImageView img_back_effect;
    ImageView img_rotate;
    int lastColor = -16777216;
    ProgressDialog progressDialog;
    RelativeLayout rl_main;
    Bitmap source;
    TextView txt_save;
    private EffectAdapter ea = null;

    public static Bitmap rotateImage(Bitmap sourceImage, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(sourceImage, 0, 0, sourceImage.getWidth(), sourceImage.getHeight(), matrix, true);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_effects);
        this.img_back_effect = (ImageView) findViewById(R.id.img_back_effect);
        this.img_rotate = (ImageView) findViewById(R.id.img_rotate);
        this.txt_save = (TextView) findViewById(R.id.txt_save);
        this.rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        this.finalIMG = (ImageView) findViewById(R.id.finalIMG);
        this.source = BitmapFactory.decodeFile(((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path);
        this.finalIMG.setImageBitmap(getRotation(((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path));
        this.horlist = (HorizontalListView) findViewById(R.id.horlist);
        this.ea = new EffectAdapter(this, getRotation(((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path));
        this.horlist.setAdapter(this.ea);
        this.horlist.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                final ProgressDialog pd = new ProgressDialog(EffectsActivity.this);
                pd.setCancelable(true);
                pd.setMessage("Applying Effect...");
                pd.show();
                new ProcessingAsyncTask(EffectsActivity.this, EffectsActivity.this.getRotation(((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path), new ProcessingListener() {
                    public void onFinish(Bitmap bm) {
                        pd.dismiss();
                        EffectsActivity.this.finalIMG.setImageBitmap(bm);
                    }
                }).execute(new Integer[]{(Integer) ImageProcessing.effectList.get(position)});
            }
        });
        this.img_back_effect.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EffectsActivity.this.finish();
            }
        });
        this.img_rotate.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EffectsActivity effectsActivity = EffectsActivity.this;
                effectsActivity.angle += 90.0f;
                EffectsActivity.this.finalIMG.setImageBitmap(EffectsActivity.rotateImage(EffectsActivity.this.source, EffectsActivity.this.angle));
            }
        });
        this.txt_save.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new ImageSaveTask().execute(new String[0]);
            }
        });
    }

    private Bitmap getRotation(String picturePath) {
        Exception e;
        Bitmap thumbnail = BitmapFactory.decodeFile(picturePath);
        try {
            ExifInterface exif = new ExifInterface(picturePath);
            ExifInterface exifInterface;
            try {
                int orientation = exif.getAttributeInt("Orientation", 1);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90.0f);
                } else if (orientation == 3) {
                    matrix.postRotate(180.0f);
                } else if (orientation == 8) {
                    matrix.postRotate(270.0f);
                }
                exifInterface = exif;
                return Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (Exception e2) {
                e = e2;
                exifInterface = exif;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
        return thumbnail;
    }

    public Bitmap takeScreenShot() {
        this.rl_main.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(this.rl_main.getDrawingCache());
        this.rl_main.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    public void colorChanged(int color) {
        this.lastColor = color;
        drawView.colorChanged(this.lastColor);
    }

    public class DrawingPanel extends View implements OnTouchListener {
        private static final float TOUCH_TOLERANCE = 0.0f;
        private final String TAG = getClass().getSimpleName();
        public Paint mBitmapPaint;
        public Paint mPaint;
        public int x;
        public int y;
        private int color;
        private boolean isTextModeOn = false;
        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        private float mX;
        private float mY;
        private ArrayList<PathPoints> paths = new ArrayList();
        private String textToDraw = null;
        private ArrayList<PathPoints> undonePaths = new ArrayList();

        @SuppressLint({"ClickableViewAccessibility"})
        public DrawingPanel(Context context, int color) {
            super(context);
            setFocusable(true);
            setFocusableInTouchMode(true);
            setOnTouchListener(this);
            this.mBitmapPaint = new Paint(4);
            this.mPaint = new Paint();
            this.mPaint.setAntiAlias(true);
            this.mPaint.setDither(true);
            this.mPaint.setColor(color);
            this.mPaint.setStyle(Style.STROKE);
            this.mPaint.setStrokeJoin(Join.ROUND);
            this.mPaint.setStrokeCap(Cap.ROUND);
            this.mPaint.setStrokeWidth(13.0f);
            this.mPath = new Path();
            this.paths.add(new PathPoints(this.mPath, color, false));
            this.mCanvas = new Canvas();
        }

        public void colorChanged(int color) {
            this.color = color;
            this.mPaint.setColor(color);
        }

        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            this.mBitmap = Bitmap.createBitmap(w, h, Config.ARGB_8888);
        }

        protected void onDraw(Canvas canvas) {
            canvas.drawBitmap(this.mBitmap, 0.0f, 0.0f, this.mBitmapPaint);
            Iterator it = this.paths.iterator();
            while (it.hasNext()) {
                PathPoints p = (PathPoints) it.next();
                this.mPaint.setColor(p.getColor());
                Log.v("", "Color code : " + p.getColor());
                if (p.isTextToDraw()) {
                    canvas.drawText(p.textToDraw, (float) p.x, (float) p.y, this.mPaint);
                } else {
                    canvas.drawPath(p.getPath(), this.mPaint);
                }
            }
        }

        private void touch_start(float x, float y) {
            this.mPath.reset();
            this.mPath.moveTo(x, y);
            this.mX = x;
            this.mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - this.mX);
            float dy = Math.abs(y - this.mY);
            if (dx >= 0.0f || dy >= 0.0f) {
                this.mPath.quadTo(this.mX, this.mY, (this.mX + x) / 2.0f, (this.mY + y) / 2.0f);
                this.mX = x;
                this.mY = y;
            }
        }

        private void touch_up() {
            this.mPath.lineTo(this.mX, this.mY);
            this.mCanvas.drawPath(this.mPath, this.mPaint);
            this.mPath = new Path();
            this.paths.add(new PathPoints(this.mPath, this.color, false));
        }

        private void drawText(int x, int y) {
            Log.v(this.TAG, "Here");
            Log.v(this.TAG, "X " + x + " Y " + y);
            this.x = x;
            this.y = y;
            this.paths.add(new PathPoints(this.color, this.textToDraw, true, x, y));
        }

        public boolean onTouch(View v, MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case 0:
                    if (!this.isTextModeOn) {
                        touch_start(x, y);
                        invalidate();
                        break;
                    }
                    break;
                case 1:
                    if (!this.isTextModeOn) {
                        touch_up();
                        invalidate();
                        break;
                    }
                    drawText((int) x, (int) y);
                    invalidate();
                    break;
                case 2:
                    if (!this.isTextModeOn) {
                        touch_move(x, y);
                        invalidate();
                        break;
                    }
                    break;
            }
            return true;
        }

        public void onClickUndo() {
            if (this.paths.size() > 0) {
                this.undonePaths.add(this.paths.remove(this.paths.size() - 1));
                invalidate();
            }
        }

        public void onClickRedo() {
            if (this.undonePaths.size() > 0) {
                this.paths.add(this.undonePaths.remove(this.undonePaths.size() - 1));
                invalidate();
            }
        }
    }

    class ImageSaveTask extends AsyncTask<String, String, String> {
        ImageSaveTask() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            EffectsActivity.this.progressDialog = new ProgressDialog(EffectsActivity.this);
            EffectsActivity.this.progressDialog.setMessage("Saving Photo...");
            EffectsActivity.this.progressDialog.setIndeterminate(false);
            EffectsActivity.this.progressDialog.setCancelable(false);
            EffectsActivity.this.progressDialog.show();
        }

        protected String doInBackground(String... params) {
            new SaveImage().Save(EffectsActivity.this.getApplicationContext(), EffectsActivity.this.takeScreenShot());
            return null;
        }

        protected void onPostExecute(String result) {
            EffectsActivity.this.progressDialog.dismiss();
            Toast.makeText(EffectsActivity.this.getApplicationContext(), "Photo Saved To My FileManager", Toast.LENGTH_SHORT).show();
        }
    }

    class ImageShareTask extends AsyncTask<String, String, String> {
        ImageShareTask() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            EffectsActivity.this.progressDialog = new ProgressDialog(EffectsActivity.this);
            EffectsActivity.this.progressDialog.setMessage("Please wait...");
            EffectsActivity.this.progressDialog.setIndeterminate(false);
            EffectsActivity.this.progressDialog.setCancelable(false);
            EffectsActivity.this.progressDialog.show();
        }

        protected String doInBackground(String... params) {
            new SaveImage().Save(EffectsActivity.this.getApplicationContext(), EffectsActivity.this.takeScreenShot());
            return null;
        }

        protected void onPostExecute(String result) {
            EffectsActivity.this.progressDialog.dismiss();
            EffectsActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Intent sharingIntent = new Intent();
                    sharingIntent.setAction("android.intent.action.SEND");
                    sharingIntent.setType("image/png");
                    sharingIntent.putExtra("android.intent.extra.STREAM", Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/FileManager/" + "/PhotoEffect/" + SaveImage.NameOfFile));
                    EffectsActivity.this.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });
        }
    }

    class PathPoints {
        String textToDraw;
        int x;
        int y;
        private int color;
        private boolean isTextToDraw;
        private Path path;

        public PathPoints(Path path, int color, boolean isTextToDraw) {
            this.path = path;
            this.color = color;
            this.isTextToDraw = isTextToDraw;
        }

        public PathPoints(int color, String textToDraw, boolean isTextToDraw, int x, int y) {
            this.color = color;
            this.textToDraw = textToDraw;
            this.isTextToDraw = isTextToDraw;
            this.x = x;
            this.y = y;
        }

        public Path getPath() {
            return this.path;
        }

        public void setPath(Path path) {
            this.path = path;
        }

        public int getColor() {
            return this.color;
        }

        public void setColor(int color) {
            this.color = color;
        }

        public String getTextToDraw() {
            return this.textToDraw;
        }

        public boolean isTextToDraw() {
            return this.isTextToDraw;
        }

        public void setTextToDraw(String textToDraw) {
            this.textToDraw = textToDraw;
        }

        public void setTextToDraw(boolean isTextToDraw) {
            this.isTextToDraw = isTextToDraw;
        }

        public int getX() {
            return this.x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return this.y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }
}
