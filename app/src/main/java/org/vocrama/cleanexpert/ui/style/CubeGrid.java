package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.graphics.Rect;

import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.RectSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.sprite.SpriteContainer;

public class CubeGrid extends SpriteContainer {

    private class GridItem extends RectSprite {
        private GridItem() {
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.35f, 0.7f, 1.0f};
            return new SpriteAnimatorBuilder(this).scale(fractions, Float.valueOf(1.0f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(1.0f)).duration(1300).easeInOut(fractions).build();
        }
    }

    public Sprite[] onCreateChild() {
        int[] delays = new int[]{200, 300, 400, 100, 200, 300, 0, 100, 200};
        GridItem[] gridItems = new GridItem[9];
        for (int i = 0; i < gridItems.length; i++) {
            gridItems[i] = new GridItem();
            gridItems[i].setAnimationDelay(delays[i]);
        }
        return gridItems;
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        bounds = clipSquare(bounds);
        int width = (int) (((float) bounds.width()) * 0.33f);
        int height = (int) (((float) bounds.height()) * 0.33f);
        for (int i = 0; i < getChildCount(); i++) {
            int l = bounds.left + ((i % 3) * width);
            int t = bounds.top + ((i / 3) * height);
            getChildAt(i).setDrawBounds(l, t, l + width, t + height);
        }
    }
}
