package org.vocrama.cleanexpert.ui.sprite;

import android.graphics.Canvas;
import android.graphics.Rect;


public abstract class CircleLayoutContainer extends SpriteContainer {
    public void drawChild(Canvas canvas) {
        for (int i = 0; i < getChildCount(); i++) {
            Sprite sprite = getChildAt(i);
            int count = canvas.save();
            canvas.rotate((float) ((i * 360) / getChildCount()), (float) getBounds().centerX(), (float) getBounds().centerY());
            sprite.draw(canvas);
            canvas.restoreToCount(count);
        }
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        bounds = clipSquare(bounds);
        int radius = (int) (((((double) bounds.width()) * 3.141592653589793d) / 3.5999999046325684d) / ((double) getChildCount()));
        int left = bounds.centerX() - radius;
        int right = bounds.centerX() + radius;
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setDrawBounds(left, bounds.top, right, bounds.top + (radius * 2));
        }
    }
}
