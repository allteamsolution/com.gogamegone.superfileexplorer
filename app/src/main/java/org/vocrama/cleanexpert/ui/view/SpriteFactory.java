package org.vocrama.cleanexpert.ui.view;

import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.style.FoldingCube;

public class SpriteFactory {
    public static Sprite create(Style style) {
        switch (style) {
            case FOLDING_CUBE:
                return new FoldingCube();
            default:
                return null;
        }
    }
}
