package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.os.Build.VERSION;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.CircleLayoutContainer;
import org.vocrama.cleanexpert.ui.sprite.CircleSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;

public class Circle extends CircleLayoutContainer {

    private class Dot extends CircleSprite {
        Dot() {
            setScale(0.0f);
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.5f, 1.0f};
            return new SpriteAnimatorBuilder(this).scale(fractions, Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.0f)).duration(1200).easeInOut(fractions).build();
        }
    }

    public Sprite[] onCreateChild() {
        Dot[] dots = new Dot[12];
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new Dot();
            if (VERSION.SDK_INT >= 24) {
                dots[i].setAnimationDelay(i * 100);
            } else {
                dots[i].setAnimationDelay((i * 100) - 1200);
            }
        }
        return dots;
    }
}
