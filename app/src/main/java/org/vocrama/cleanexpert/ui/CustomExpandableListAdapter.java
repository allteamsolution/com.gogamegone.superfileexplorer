package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;

import java.util.HashMap;
import java.util.List;


public class CustomExpandableListAdapter extends BaseExpandableListAdapter {
    public Context context;
    private HashMap<Integer, List<temp>> expandableListDetail;
    private List<Integer> expandableListTitle;

    CustomExpandableListAdapter(Context context, List<Integer> expandableListTitle, HashMap<Integer, List<temp>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    public Object getChild(int listPosition, int expandedListPosition) {
        return ((List) this.expandableListDetail.get(this.expandableListTitle.get(listPosition))).get(expandedListPosition);
    }

    public long getChildId(int listPosition, int expandedListPosition) {
        return (long) expandedListPosition;
    }

    public View getChildView(int listPosition, int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        temp temp = (temp) getChild(listPosition, expandedListPosition);
        Integer images = Integer.valueOf(temp.getImages());
        String expandedListText = temp.getName();
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.expandedListItem);
        ((ImageView) convertView.findViewById(R.id.image)).setImageDrawable(this.context.getResources().getDrawable(images.intValue()));
        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    public int getChildrenCount(int listPosition) {
        return ((List) this.expandableListDetail.get(this.expandableListTitle.get(listPosition))).size();
    }

    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    public long getGroupId(int listPosition) {
        return (long) listPosition;
    }

    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) ExpandableListDataPump.Gropename.get(listPosition);
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, 1);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
