package org.vocrama.cleanexpert.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Files;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.Database.MediaRecentDB;
import org.vocrama.cleanexpert.ui.Recent_Adapter.Listener;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class FragmentRecent extends Fragment implements Listener {
    static final /* synthetic */ boolean $assertionsDisabled = (!FragmentRecent.class.desiredAssertionStatus());
    public static Recent_Adapter recent_adapter;
    @SuppressLint({"StaticFieldLeak"})
    static Context Recent_context;
    RecyclerView listView;
    TextView tv_Empty_Data;
    Uri uri = Files.getContentUri("external");

    public static void mGetRecent() {
        Recent_Adapter.mFragmentRecent_recent.clear();
        MediaRecentDB mediaRecentDB = new MediaRecentDB(Recent_context);
        mediaRecentDB.open();
        Cursor cursor = mediaRecentDB.getRecentMedia();
        try {
            cursor.moveToFirst();
            int count = 0;
            do {
                if (count < 50) {
                    try {
                        if (new File(cursor.getString(cursor.getColumnIndex("filepath"))).exists()) {
                            Recent_Adapter.mFragmentRecent_recent.add(new Document(Long.valueOf(cursor.getString(cursor.getColumnIndex("fileid"))), cursor.getString(cursor.getColumnIndex("filename")), cursor.getString(cursor.getColumnIndex("filetype"))));
                            count++;
                        } else {
                            mediaRecentDB.deleteRecentMedia(cursor.getString(cursor.getColumnIndex("fileid")));
                        }
                    } catch (Exception e) {
                    }
                } else {
                    mediaRecentDB.deleteRecentMedia(cursor.getString(cursor.getColumnIndex("fileid")));
                }
            } while (cursor.moveToNext());
        } catch (Exception e2) {
        }
    }

    /* JADX WARNING: Failed to extract finally block: empty outs */
    /* JADX WARNING: Missing block: B:25:?, code:
            return r0;
     */
    public static String getRealPathFromUri(Context r10, Uri r11) {
        /*
        r9 = 0;
        r7 = 0;
        r0 = 1;
        r2 = new java.lang.String[r0];	 Catch:{ all -> 0x0040 }
        r0 = 0;
        r1 = "_data";
        r2[r0] = r1;	 Catch:{ all -> 0x0040 }
        r0 = r10.getContentResolver();	 Catch:{ all -> 0x0040 }
        r3 = 0;
        r4 = 0;
        r5 = 0;
        r1 = r11;
        r7 = r0.query(r1, r2, r3, r4, r5);	 Catch:{ all -> 0x0040 }
        r6 = 0;
        if (r7 == 0) goto L_0x001f;
    L_0x0019:
        r0 = "_data";
        r6 = r7.getColumnIndexOrThrow(r0);	 Catch:{ all -> 0x0040 }
    L_0x001f:
        r0 = $assertionsDisabled;	 Catch:{ CursorIndexOutOfBoundsException -> 0x002b }
        if (r0 != 0) goto L_0x0033;
    L_0x0023:
        if (r7 != 0) goto L_0x0033;
    L_0x0025:
        r0 = new java.lang.AssertionError;	 Catch:{ CursorIndexOutOfBoundsException -> 0x002b }
        r0.<init>();	 Catch:{ CursorIndexOutOfBoundsException -> 0x002b }
        throw r0;	 Catch:{ CursorIndexOutOfBoundsException -> 0x002b }
    L_0x002b:
        r8 = move-exception;
        if (r7 == 0) goto L_0x0031;
    L_0x002e:
        r7.close();
    L_0x0031:
        r0 = r9;
    L_0x0032:
        return r0;
    L_0x0033:
        r7.moveToFirst();	 Catch:{ CursorIndexOutOfBoundsException -> 0x002b }
        r0 = r7.getString(r6);	 Catch:{ CursorIndexOutOfBoundsException -> 0x002b }
        if (r7 == 0) goto L_0x0032;
    L_0x003c:
        r7.close();
        goto L_0x0032;
    L_0x0040:
        r0 = move-exception;
        if (r7 == 0) goto L_0x0046;
    L_0x0043:
        r7.close();
    L_0x0046:
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mazhuang.cleanexpert.ui.FragmentRecent.getRealPathFromUri(android.content.Context, android.net.Uri):java.lang.String");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_container, container, false);
        this.listView = (RecyclerView) view.findViewById(R.id.list);
        this.listView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        this.tv_Empty_Data = (TextView) view.findViewById(R.id.tv_empty_data);
        mGetRecent();
        recent_adapter = new Recent_Adapter(this, Recent_Adapter.mFragmentRecent_recent);
        this.listView.setAdapter(recent_adapter);
        this.tv_Empty_Data.setVisibility(8);
        return view;
    }

    public void onResume() {
        super.onResume();
    }

    public void onClick(int index) {
        if (recent_adapter.getSelectedIndices().size() > 0) {
            recent_adapter.toggleSelected(index);
            return;
        }
        String toLowerCase = ((Document) Recent_Adapter.mFragmentRecent_recent.get(index)).getType().toLowerCase();
        int obj = -1;
        switch (toLowerCase.hashCode()) {
            case 1422702:
                if (toLowerCase.equals(".3gp")) {
                    obj = 23;
                    break;
                }
                break;
            case 1467366:
                if (toLowerCase.equals(".avi")) {
                    obj = 19;
                    break;
                }
                break;
            case 1468055:
                if (toLowerCase.equals(".bmp")) {
                    obj = 30;
                    break;
                }
                break;
            case 1469205:
                if (toLowerCase.equals(".css")) {
                    obj = 12;
                    break;
                }
                break;
            case 1470026:
                if (toLowerCase.equals(".doc")) {
                    obj = 5;
                    break;
                }
                break;
            case 1471874:
                if (toLowerCase.equals(".flv")) {
                    obj = 20;
                    break;
                }
                break;
            case 1472726:
                if (toLowerCase.equals(".gif")) {
                    obj = 27;
                    break;
                }
                break;
            case 1474773:
                if (toLowerCase.equals(".img")) {
                    obj = 28;
                    break;
                }
                break;
            case 1475827:
                if (toLowerCase.equals(".jpg")) {
                    obj = 31;
                    break;
                }
                break;
            case 1478570:
                if (toLowerCase.equals(".mkv")) {
                    obj = 18;
                    break;
                }
                break;
            case 1478658:
                if (toLowerCase.equals(".mp3")) {
                    obj = 0;
                    break;
                }
                break;
            case 1478659:
                if (toLowerCase.equals(".mp4")) {
                    obj = 17;
                    break;
                }
                break;
            case 1478694:
                if (toLowerCase.equals(".mov")) {
                    obj = 22;
                    break;
                }
                break;
            case 1481220:
                if (toLowerCase.equals(".pdf")) {
                    obj = 3;
                    break;
                }
                break;
            case 1481531:
                if (toLowerCase.equals(".png")) {
                    obj = 26;
                    break;
                }
                break;
            case 1481606:
                if (toLowerCase.equals(".ppt")) {
                    obj = 8;
                    break;
                }
                break;
            case 1481683:
                if (toLowerCase.equals(".psd")) {
                    obj = 13;
                    break;
                }
                break;
            case 1483061:
                if (toLowerCase.equals(".rar")) {
                    obj = 16;
                    break;
                }
                break;
            case 1484662:
                if (toLowerCase.equals(".svg")) {
                    obj = 14;
                    break;
                }
                break;
            case 1485698:
                if (toLowerCase.equals(".txt")) {
                    obj = 2;
                    break;
                }
                break;
            case 1487870:
                if (toLowerCase.equals(".wav")) {
                    obj = 1;
                    break;
                }
                break;
            case 1488242:
                if (toLowerCase.equals(".wmv")) {
                    obj = 21;
                    break;
                }
                break;
            case 1489169:
                if (toLowerCase.equals(".xls")) {
                    obj = 6;
                    break;
                }
                break;
            case 1489193:
                if (toLowerCase.equals(".xml")) {
                    obj = 11;
                    break;
                }
                break;
            case 1490995:
                if (toLowerCase.equals(".zip")) {
                    obj = 15;
                    break;
                }
                break;
            case 45565749:
                if (toLowerCase.equals(".divx")) {
                    obj = 24;
                    break;
                }
                break;
            case 45570926:
                if (toLowerCase.equals(".docx")) {
                    obj = 4;
                    break;
                }
                break;
            case 45695193:
                if (toLowerCase.equals(".html")) {
                    obj = 10;
                    break;
                }
                break;
            case 45750678:
                if (toLowerCase.equals(".jpeg")) {
                    obj = 25;
                    break;
                }
                break;
            case 45929906:
                if (toLowerCase.equals(".pptx")) {
                    obj = 9;
                    break;
                }
                break;
            case 46041891:
                if (toLowerCase.equals(".tiff")) {
                    obj = 29;
                    break;
                }
                break;
            case 46164359:
                if (toLowerCase.equals(".xlsx")) {
                    obj = 7;
                    break;
                }
                break;
        }
        File file;
        switch (obj) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                String mimeType = null;
                String _data = null;
                String s = this.uri + "/" + ((Document) Recent_Adapter.mFragmentRecent_recent.get(index)).getDataId();
                Cursor cursor = getActivity().getContentResolver().query(Uri.parse(s), null, "mime_type!=0", null, "title ASC");
                cursor.moveToFirst();
                do {
                    try {
                        mimeType = cursor.getString(cursor.getColumnIndex("mime_type"));
                        _data = cursor.getString(cursor.getColumnIndex("_data"));
                    } catch (Exception e) {
                    }
                } while (cursor.moveToNext());
                if (_data != null && mimeType != null) {
                    file = new File(_data);
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.parse(s), mimeType);
                    startActivity(intent);
                    return;
                }
                return;
            case 15:
            case 16:
                String _data1 = null;
                Cursor cursor1 = getActivity().getContentResolver().query(Uri.parse(this.uri + "/" + ((Document) Recent_Adapter.mFragmentRecent_recent.get(index)).getDataId()), null, null, null, "title ASC");
                cursor1.moveToFirst();
                do {
                    try {
                        _data1 = cursor1.getString(cursor1.getColumnIndex("_data"));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } while (cursor1.moveToNext());
                if (_data1 != null) {
                    try {
                        file = new File(_data1);
                        if (_data1.endsWith(".zip")) {
                        }
                        return;
                    } catch (Exception e22) {
                        e22.printStackTrace();
                        return;
                    }
                }
                return;
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
                String videoUri = this.uri + "/" + ((Document) Recent_Adapter.mFragmentRecent_recent.get(index)).getDataId();
                Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(videoUri));
                intent2.setDataAndType(Uri.parse(videoUri), "video/*");
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    public void onLongClick(int index) {
    }

    public void onSelectionChanged(int count) {
    }

    @SuppressLint({"StaticFieldLeak"})
    public class RecentDeleteAsyncTask extends AsyncTask<String, Integer, String> {
        Context context;
        Dialog dialog = null;
        TextView showPr = null;

        RecentDeleteAsyncTask(Context context, TextView textView, Dialog dialog) {
            this.context = context;
            this.showPr = textView;
            this.dialog = dialog;
        }

        protected void onPreExecute() {
        }

        protected void onProgressUpdate(Integer... values) {
            String s1 = values[values.length - 1].toString();
            if (this.showPr != null) {
                this.showPr.setText(s1 + "%");
            }
        }

        protected String doInBackground(String... strings) {
            List<Integer> integers = FragmentRecent.recent_adapter.getSelectedIndices();
            Collections.sort(integers, Collections.reverseOrder());
            for (int i = 0; i < integers.size(); i++) {
                MediaRecentDB mediaRecentDB = new MediaRecentDB(FragmentRecent.this.getContext());
                mediaRecentDB.open();
                mediaRecentDB.deleteRecentMedia(String.valueOf(((Document) Recent_Adapter.mFragmentRecent_recent.get(((Integer) integers.get(i)).intValue())).getDataId()));
                FragmentRecent.getRealPathFromUri(FragmentRecent.this.getContext(), Uri.parse(Files.getContentUri("external").toString() + "/" + ((Document) Recent_Adapter.mFragmentRecent_recent.get(((Integer) integers.get(i)).intValue())).getDataId()));
            }
            return null;
        }

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                this.dialog.dismiss();
            } catch (Exception e) {
            }
            FragmentRecent.recent_adapter.clearSelected();
            FragmentRecent.mGetRecent();
            if (Recent_Adapter.mFragmentRecent_recent.size() != 0) {
                if (FragmentRecent.this.listView.getVisibility() == View.GONE) {
                    FragmentRecent.this.listView.setVisibility(0);
                    FragmentRecent.this.tv_Empty_Data.setVisibility(8);
                }
                FragmentRecent.recent_adapter = new Recent_Adapter(FragmentRecent.this, Recent_Adapter.mFragmentRecent_recent);
                FragmentRecent.this.listView.setAdapter(FragmentRecent.recent_adapter);
                return;
            }
            FragmentRecent.this.listView.setVisibility(8);
            FragmentRecent.this.tv_Empty_Data.setText("Empty Recents");
            FragmentRecent.this.tv_Empty_Data.setVisibility(0);
        }
    }
}
