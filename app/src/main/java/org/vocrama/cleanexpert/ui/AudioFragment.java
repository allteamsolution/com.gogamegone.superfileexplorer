package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.callback.OnMediaFileClickListener;
import org.vocrama.cleanexpert.util.MediaItem;
import org.vocrama.cleanexpert.util.PlayerConstants;
import org.vocrama.cleanexpert.util.UtilFunctions;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class AudioFragment extends Fragment {


    private boolean isThreadStarted = false;
    private final ArrayList<MediaItem> listOfSongs = new ArrayList<>();

    private FileManager.OnFileLoadingCallback fileLoadingCallback;

    public static AudioFragment getInstance() {
        return new AudioFragment();
    }

    public AudioFragment() {

    }

    public static String pathAudio;
    ActionMode actionMode;
    private AudioAdapter audioAdapter;
    String mediaFile;
    RecyclerView mediaListView;
    ProgressDialog progressDialog;
    int selectCount;
    ArrayList<MediaItem> selectedAudio = new ArrayList<>();

    private CompositeDisposable compositeDisposable;

    private final OnMediaFileClickListener listener = new OnMediaFileClickListener() {
        @Override
        public void onClick(int position) {
            AudioFragment.this.mediaFile = listOfSongs.get(position).path;
            if (AudioFragment.this.selectCount > 0) {
                if (AudioFragment.this.actionMode == null) {
                    AudioFragment.this.actionMode = AudioFragment.this.getActivity().startActionMode(AudioFragment.this.callback);
                }
                AudioFragment.this.toggleSelection(position);
                AudioFragment.this.actionMode.setTitle(AudioFragment.this.selectCount + " " + "Selected");
                if (AudioFragment.this.selectCount == 0) {
                    AudioFragment.this.actionMode.finish();
                    AudioFragment.this.audioAdapter.notifyDataSetChanged();
                }
            } else if (AudioFragment.this.selectCount == 0) {
                Intent i = new Intent("android.intent.action.VIEW");
                String name = listOfSongs.get(position).getAlbum();
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1));
                Uri uri = FileProvider.getUriForFile(AudioFragment.this.getActivity(), AudioFragment.this.getActivity().getPackageName() + ".provider", new File(listOfSongs.get(position).getPath()));
                i.setDataAndType(uri, type);
                i.addFlags(1);
                i.addFlags(268435456);
                for (ResolveInfo resolveInfo : AudioFragment.this.getActivity().getPackageManager().queryIntentActivities(i, 65536)) {
                    AudioFragment.this.getActivity().grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                }
                AudioFragment.this.startActivity(i);
            }
        }

        @Override
        public void onLongClick(int position) {
            if (AudioFragment.this.actionMode == null) {
                AudioFragment.this.actionMode = AudioFragment.this.getActivity().startActionMode(AudioFragment.this.callback);
            }
            AudioFragment.this.toggleSelection(position);
            AudioFragment.this.actionMode.setTitle(AudioFragment.this.selectCount + " " + "Selected");
            if (AudioFragment.this.selectCount == 0) {
                AudioFragment.this.actionMode.finish();
            }
        }
    };

    private Callback callback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.video_context_menu, menu);
            MenuItem mi = menu.getItem(0);
            mi.setTitle(mi.getTitle().toString());
            AudioFragment.this.actionMode = mode;
            AudioFragment.this.selectCount = 0;
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.video_d:
                    AudioFragment.this.audioDelete(mode);
                    break;
                case R.id.video_s:
                    AudioFragment.this.audioShare(mode);
                    break;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            if (AudioFragment.this.selectCount > 0) {
                AudioFragment.this.deselect();
            }
            AudioFragment.this.actionMode = null;
        }
    };

    @RequiresApi(api = 19)
    public static String checkSDCARD(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_audio, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutParams p = new LayoutParams(-1, -1);
        p.weight = 6.0f;

        compositeDisposable = new CompositeDisposable();
        fileLoadingCallback = (FileManager.OnFileLoadingCallback) requireActivity();
        mediaListView = view.findViewById(R.id.listViewMusic);

        audioAdapter = new AudioAdapter(listOfSongs, listener);
        mediaListView.setLayoutManager(new GridLayoutManager(requireActivity(), 4, RecyclerView.VERTICAL, false));
        mediaListView.setAdapter(audioAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getSongs();
    }

    private void getSongs() {
        if (isThreadStarted)
            return;

        if (listOfSongs.isEmpty()) {
            Observable.
                    fromIterable(listOfSongs())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> {
                        isThreadStarted = true;
                        fileLoadingCallback.onFileLoading();
                    })
                    .doOnNext(mediaItem -> {
                        listOfSongs.add(mediaItem);
                    })
                    .doOnError(throwable -> {
                        isThreadStarted = false;
                        fileLoadingCallback.onFileLoaded();
                    })
                    .doOnComplete(() -> {
                        isThreadStarted = false;
                        fileLoadingCallback.onFileLoaded();
                        audioAdapter.notifyDataSetChanged();
                    })
                    .subscribe();
        }
    }


    private ArrayList<MediaItem> listOfSongs() {
        ArrayList<MediaItem> songs  = new ArrayList<>();
        Cursor c = requireActivity().getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "is_music != 0", null, null);
        c.moveToFirst();
        while (c.moveToNext()) {
            MediaItem songData = new MediaItem();
            String title = c.getString(c.getColumnIndex("title"));
            String artist = c.getString(c.getColumnIndex("artist"));
            String album = c.getString(c.getColumnIndex("album"));
            long duration = c.getLong(c.getColumnIndex("duration"));
            String data = c.getString(c.getColumnIndex("_data"));
            long albumId = c.getLong(c.getColumnIndex("album_id"));
            String composer = c.getString(c.getColumnIndex("composer"));
            songData.setTitle(title);
            songData.setAlbum(album);
            songData.setArtist(artist);
            songData.setDuration(duration);
            songData.setPath(data);
            songData.setAlbumId(albumId);
            songData.setComposer(composer);
            songs.add(songData);
        }
        c.close();
        return songs;
    }

    private void deselect() {
        for (int i = 0; i < listOfSongs.size(); i++) {
            ((MediaItem) listOfSongs.get(i)).isSelected = false;
        }
        this.selectCount = 0;
        audioAdapter.notifyDataSetChanged();
    }

    private void toggleSelection(int position) {
        ((MediaItem) listOfSongs.get(position)).isSelected = !((MediaItem) listOfSongs.get(position)).isSelected;
        if (((MediaItem) listOfSongs.get(position)).isSelected) {
            this.selectedAudio.add(listOfSongs.get(position));
            this.selectCount++;
        } else {
            this.selectedAudio.add(listOfSongs.get(position));
            this.selectCount--;
        }
        this.audioAdapter.notifyDataSetChanged();
    }

    private void audioDelete(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
        builder.setTitle((CharSequence) "Delete Videos");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(v -> {
            new MultipleDeleteTask().execute(new Void[0]);
            alertDialog.dismiss();
            mode.finish();
        });
        alertDialog.getButton(-2).setOnClickListener(v -> {
            alertDialog.dismiss();
            mode.finish();
        });
    }

    private void audioShare(ActionMode mode) {
        ArrayList<MediaItem> checked = this.selectedAudio;
        ArrayList<Uri> share = new ArrayList<>();
        for (int i = 0; i < checked.size(); i++) {
            if (((MediaItem) this.selectedAudio.get(i)).isSelected) {
                Uri uri;
                File file = new File(((MediaItem) this.selectedAudio.get(i)).path);
                if (VERSION.SDK_INT >= 24) {
                    uri = FileProvider.getUriForFile(requireActivity(), requireActivity().getPackageName() + ".provider", file);
                } else {
                    uri = Uri.fromFile(file);
                }
                share.add(uri);
            }
        }
        Intent i2 = new Intent();
        i2.setAction("android.intent.action.SEND_MULTIPLE");
        i2.setType("video/*");
        i2.addFlags(1);
        i2.putParcelableArrayListExtra("android.intent.extra.STREAM", share);
        startActivity(Intent.createChooser(i2, ""));
        this.audioAdapter.notifyDataSetChanged();
        mode.finish();
    }

    private void scanFile(File file) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                AudioFragment.this.requireActivity().getContentResolver().delete(uri, null, null);
            }
        });
    }

    public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.AudioHolder> {

        private final ArrayList<MediaItem> audioList;
        private final OnMediaFileClickListener listener;

        public AudioAdapter(ArrayList<MediaItem> audioList, OnMediaFileClickListener listener) {
            this.audioList = audioList;
            this.listener = listener;
        }

        @NonNull
        @Override
        public AudioHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.audio_adapter, parent, false);
            return new AudioHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AudioHolder holder, int position) {
            MediaItem mediaFile = audioList.get(position);
            holder.bind(mediaFile, listener);
        }

        @Override
        public int getItemCount() {
            return audioList.size();
        }

        public class AudioHolder extends RecyclerView.ViewHolder {
            private FrameLayout frameLayout_audio;
            private ImageView img_more_main_audio;
            private ImageView profile_image;
            private TextView textViewSongName;
            private View view_audio;

            public String share;
            public TextView txtNameDetail_video;
            public TextView txtPathDetail_video;
            public TextView txtSizeDetail_video;
            public TextView txtartistDetail_artist;

            public AudioHolder(@NonNull View itemView) {
                super(itemView);
                textViewSongName = (TextView) itemView.findViewById(R.id.textViewSongName);
                profile_image = (ImageView) itemView.findViewById(R.id.profile_image);
                img_more_main_audio = (ImageView) itemView.findViewById(R.id.img_more_main_audio);
                view_audio = itemView.findViewById(R.id.view_audio);
                frameLayout_audio = (FrameLayout) itemView.findViewById(R.id.frameLayout_audio);
            }

            public void bind(MediaItem mediaItem, OnMediaFileClickListener listener) {
                if (mediaItem.isSelected) {
                    view_audio.setAlpha(0.5f);
                    frameLayout_audio.setForeground(ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_check_box_black_24dp));
                } else {
                    view_audio.setAlpha(0.0f);
                    frameLayout_audio.setForeground(null);
                }

                AudioFragment.pathAudio = mediaItem.getPath();
                MediaItem detail = (MediaItem) audioList.get(getAdapterPosition());
                textViewSongName.setText(detail.toString());

                Glide
                        .with(itemView.getContext())
                        .load(ContentUris.withAppendedId(PlayerConstants.sArtworkUri, mediaItem.getAlbumId()))
                        .placeholder((int) R.drawable.music_placeholder)
                        .error((int) R.drawable.music_placeholder)
                        .into(profile_image);

                itemView.setOnClickListener(v -> {
                    listener.onClick(getAdapterPosition());
                });

                itemView.setOnLongClickListener(v -> {
                    listener.onLongClick(getAdapterPosition());
                    return true;
                });


                img_more_main_audio.setOnClickListener(v -> {
                    try {
                        PopupMenu popup = new PopupMenu(requireContext(), requireView());
                        popup.getMenuInflater().inflate(R.menu.first_more, popup.getMenu());
                        popup.show();
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.delete:
                                        Delete_Audio();
                                        break;
                                    case R.id.details:
                                        Detail_Audio();
                                        break;
                                    case R.id.share:
                                        Share_Audio();
                                        break;
                                }
                                return true;
                            }

                            private void Share_Audio() {
                                Uri uri;
                                share = ((MediaItem) PlayerConstants.SONGS_LIST.get(getAdapterPosition())).path;
                                File file = new File(share);
                                if (VERSION.SDK_INT >= 24) {
                                    uri = FileProvider.getUriForFile(requireActivity(), requireActivity().getPackageName() + ".provider", file);
                                } else {
                                    uri = Uri.fromFile(file);
                                }
                                Intent i = new Intent();
                                i.setAction("android.intent.action.SEND");
                                i.setType("audio/*");
                                i.putExtra("android.intent.extra.STREAM", uri);
                                i.addFlags(1);
                                startActivity(Intent.createChooser(i, ""));
                            }

                            private void Delete_Audio() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AudioFragment.this.getActivity());
                                builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
                                builder.setTitle((CharSequence) "Delete Audio");
                                builder.setIcon((int) R.drawable.ic_delete_black_24dp);
                                builder.setPositiveButton((CharSequence) "OK", null);
                                builder.setNegativeButton((CharSequence) "Cancel", null);
                                final AlertDialog alertDialog = builder.create();
                                alertDialog.show();
                                alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
                                    public void onClick(View v) {
                                        share = mediaItem.getPath();
                                        FileManager.delete(requireContext(), new File(share));
                                        listOfSongs.clear();
                                        getSongs();
                                        alertDialog.dismiss();
                                        Toast.makeText(AudioFragment.this.getActivity(), "Delete Successfully", 0).show();
                                    }
                                });
                                alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });
                            }

                            private void Detail_Audio() {
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(requireActivity());
                                View dialogView = AudioFragment.this.getLayoutInflater().inflate(R.layout.detail_audio, null);
                                dialogBuilder.setView(dialogView);
                                txtNameDetail_video = (TextView) dialogView.findViewById(R.id.txtNameDetail_video);
                                txtPathDetail_video = (TextView) dialogView.findViewById(R.id.txtPathDetail_video);
                                txtSizeDetail_video = (TextView) dialogView.findViewById(R.id.txtSizeDetail_video);
                                txtartistDetail_artist = (TextView) dialogView.findViewById(R.id.txtartistDetail_artist);
                                txtPathDetail_video.setText("" + ((MediaItem) PlayerConstants.SONGS_LIST.get(getAdapterPosition())).getPath());
                                txtNameDetail_video.setText("" + ((MediaItem) PlayerConstants.SONGS_LIST.get(getAdapterPosition())).getTitle());
                                txtSizeDetail_video.setText("" + detail.getAlbum());
                                txtartistDetail_artist.setText("" + detail.getArtist());
                                dialogBuilder.setPositiveButton((CharSequence) "Done", (dialog, whichButton) -> {
                                });
                                dialogBuilder.create().show();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    private class MultipleDeleteTask extends AsyncTask<Void, Void, Void> {
        private MultipleDeleteTask() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            AudioFragment.this.progressDialog = new ProgressDialog(AudioFragment.this.getActivity());
            AudioFragment.this.progressDialog.setProgressStyle(0);
            AudioFragment.this.progressDialog.setCancelable(false);
            AudioFragment.this.progressDialog.setTitle("Delete");
            AudioFragment.this.progressDialog.setMessage("Please Wait.....");
            AudioFragment.this.progressDialog.show();
        }

        @RequiresApi(api = 19)
        protected Void doInBackground(Void... params) {
            ArrayList<MediaItem> checkedItems = AudioFragment.this.selectedAudio;
            for (int i = 0; i < checkedItems.size(); i++) {
                String S1 = ((MediaItem) AudioFragment.this.selectedAudio.get(i)).path;
                if (S1.startsWith("/storage/emulated/0/")) {
                    AudioFragment.this.scanFile(new File(S1));
                } else if (S1.startsWith(AudioFragment.checkSDCARD(requireActivity()))) {
                    if (VERSION.SDK_INT < 24) {
                        AudioFragment.this.scanFile(new File(S1));
                    } else {
                        try {
                            File f = new File(S1);
                            if (StorageHelper.deleteFile(AudioFragment.this.getActivity(), f)) {
                                Toast.makeText(AudioFragment.this.getActivity(), "Done...", 0).show();
                                AudioFragment.this.scanFile(f);
                                if (f.delete()) {
                                    AudioFragment.this.scanFile(f);
                                }
                            } else {
                                Toast.makeText(AudioFragment.this.getActivity(), "Error...", 0).show();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                FileManager.delete(requireActivity(), new File(S1));
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listOfSongs.clear();
            getSongs();
            AudioFragment.this.selectedAudio.clear();
            Toast.makeText(AudioFragment.this.getActivity(), "Delete Successfully", Toast.LENGTH_SHORT).show();
            AudioFragment.this.progressDialog.dismiss();
        }
    }


}
