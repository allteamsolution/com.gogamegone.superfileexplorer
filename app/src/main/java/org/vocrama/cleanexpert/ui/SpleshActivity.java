package org.vocrama.cleanexpert.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.gogamegone.superfileexplorer.R;

import java.util.ArrayList;
import java.util.List;


public class SpleshActivity extends AppCompatActivity {
    public static final int MULTIPLE_PERMISSIONS = 10;
    String[] permissions = new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.REQUEST_INSTALL_PACKAGES"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_start);
    }

    @RequiresApi(api = 26)
    protected void onResume() {
        super.onResume();
        if (checkPermissions()) {
            nextActivity();
        }
    }

    private boolean checkPermissions() {
        List<String> listPermissionsNeeded = new ArrayList();
        for (String p : this.permissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), p) != 0) {
                listPermissionsNeeded.add(p);
            } else if (VERSION.SDK_INT >= 26) {
                if (getPackageManager().canRequestPackageInstalls()) {
                    callInstallProcess();
                } else {
                    startActivityForResult(new Intent("android.settings.MANAGE_UNKNOWN_APP_SOURCES").setData(Uri.parse(String.format("package:%s", new Object[]{getPackageName()}))), 1234);
                }
            }
        }
        if (listPermissionsNeeded.isEmpty()) {
            return true;
        }
        ActivityCompat.requestPermissions(this, (String[]) listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 10);
        return false;
    }

    private void callInstallProcess() {
        try {
            nextActivity();
        } catch (Exception e) {
        }
    }

    @RequiresApi(api = 26)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!(requestCode == 1234 && resultCode == -1 && !getPackageManager().canRequestPackageInstalls())) {
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length <= 0 || grantResults[0] != 0) {
                    finish();
                    return;
                } else {
                    nextActivity();
                    return;
                }
            default:
                return;
        }
    }

    private void nextActivity() {
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(5894);
        }
    }
}
