package org.vocrama.cleanexpert.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import org.vocrama.cleanexpert.util.DiskStat;
import org.vocrama.cleanexpert.util.MemStat;

public class SplashActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                final DiskStat diskStat = new DiskStat();
                final MemStat memStat = new MemStat(SplashActivity.this);
                handler.post(new Runnable() {
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        intent.putExtra(MainActivity.PARAM_TOTAL_SPACE, diskStat.getTotalSpace());
                        intent.putExtra(MainActivity.PARAM_USED_SPACE, diskStat.getUsedSpace());
                        intent.putExtra(MainActivity.PARAM_TOTAL_MEMORY, memStat.getTotalMemory());
                        intent.putExtra(MainActivity.PARAM_USED_MEMORY, memStat.getUsedMemory());
                        SplashActivity.this.startActivity(intent);
                        SplashActivity.this.finish();
                    }
                });
            }
        }).start();
    }
}
