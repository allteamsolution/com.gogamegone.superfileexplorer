package org.vocrama.cleanexpert.ui;

import com.gogamegone.superfileexplorer.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class ExpandableListDataPump {
    public static ArrayList<String> Gropename = new ArrayList(3);
    public static ArrayList<Integer> detail = new ArrayList(3);

    public static HashMap<Integer, List<temp>> getData() {
        HashMap<Integer, List<temp>> expandableListDetail = new HashMap();
        Gropename.add("storage");
        Gropename.add("appmanager");
        Gropename.add("library");
        Gropename.add("tools");
        List<temp> storage = new ArrayList();
        storage.add(new temp(R.drawable.nav_home, "home"));
        storage.add(new temp(R.drawable.nav_phonememory, "Phone Memory"));
        storage.add(new temp(R.drawable.nav_sdcard, "SD card"));
        storage.add(new temp(R.drawable.nav_cloud, "Cloud"));
        List<temp> appmanager = new ArrayList();
        appmanager.add(new temp(R.drawable.nav_userapp, "User app"));
        appmanager.add(new temp(R.drawable.nav_systmapps, "System app"));
        List<temp> library = new ArrayList();
        library.add(new temp(R.drawable.nav_home, "Images"));
        library.add(new temp(R.drawable.nav_audio, "Audio"));
        library.add(new temp(R.drawable.nav_video, "Video"));
        List<temp> tools = new ArrayList();
        tools.add(new temp(R.drawable.nav_ftp, "FTP"));
        tools.add(new temp(R.drawable.nav_themes, "Themes"));
        tools.add(new temp(R.drawable.nav_safeguard, "Safe Gaurd"));
        expandableListDetail.put(Integer.valueOf(2), library);
        expandableListDetail.put(Integer.valueOf(0), makeJazzArtists());
        expandableListDetail.put(Integer.valueOf(1), appmanager);
        expandableListDetail.put(Integer.valueOf(3), tools);
        return expandableListDetail;
    }

    public static List<temp> makeJazzArtists() {
        temp milesDavis = new temp(R.drawable.nav_home, "home");
        temp ellaFitzgerald = new temp(R.drawable.nav_phonememory, "Phone Memory");
        temp billieHoliday = new temp(R.drawable.nav_sdcard, "SD card");
        return Arrays.asList(new temp[]{milesDavis, ellaFitzgerald, billieHoliday});
    }
}
