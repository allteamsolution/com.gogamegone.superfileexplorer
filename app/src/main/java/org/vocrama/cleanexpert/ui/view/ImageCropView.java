package org.vocrama.cleanexpert.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.ViewConfiguration;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.model.CropInfo;
import org.vocrama.cleanexpert.ui.view.graphics.FastBitmapDrawable;
import org.vocrama.cleanexpert.util.BitmapLoadUtils;

import java.io.File;

import it.sephiroth.android.library.easing.Cubic;
import it.sephiroth.android.library.easing.Easing;

public class ImageCropView extends AppCompatImageView {
    public static final int DEFAULT_ASPECT_RATIO_HEIGHT = 1;
    public static final int DEFAULT_ASPECT_RATIO_WIDTH = 1;
    public static final int GRID_OFF = 0;
    public static final int GRID_ON = 1;
    public static final String LOG_TAG = "ImageCropView";
    public static final float ZOOM_INVALID = -1.0f;
    protected static final boolean LOG_ENABLED = false;
    private static final String DEFAULT_BACKGROUND_COLOR_ID = "#99000000";
    protected final int DEFAULT_ANIMATION_DURATION;
    protected final Matrix mDisplayMatrix;
    protected final float[] mMatrixValues;
    private final int GRID_COLUMN_COUNT;
    private final int GRID_ROW_COUNT;
    protected Matrix mBaseMatrix;
    protected RectF mBitmapRect;
    protected RectF mCenterRect;
    protected RectF mCropRect;
    protected int mDoubleTapDirection;
    protected boolean mDoubleTapEnabled;
    protected Easing mEasing;
    protected GestureDetector mGestureDetector;
    protected OnGestureListener mGestureListener;
    protected Handler mHandler;
    protected Runnable mLayoutRunnable;
    protected ScaleGestureDetector mScaleDetector;
    protected boolean mScaleEnabled;
    protected float mScaleFactor;
    protected OnScaleGestureListener mScaleListener;
    protected boolean mScrollEnabled;
    protected RectF mScrollRect;
    protected Matrix mSuppMatrix;
    protected int mTouchSlop;
    protected boolean mUserScaled;
    private float baseScale;
    private int gridInnerMode;
    private float gridLeftRightMargin;
    private int gridOuterMode;
    private float gridTopBottomMargin;
    private String imageFilePath;
    private boolean isChangingScale;
    private int mAspectRatioHeight;
    private int mAspectRatioWidth;
    private boolean mBitmapChanged;
    private PointF mCenter;
    private OnImageViewTouchDoubleTapListener mDoubleTapListener;
    private Paint mGridInnerLinePaint;
    private Paint mGridOuterLinePaint;
    private float mMaxZoom;
    private boolean mMaxZoomDefined;
    private float mMinZoom;
    private boolean mMinZoomDefined;
    private float[] mPts;
    private boolean mRestoreRequest;
    private OnImageViewTouchSingleTapListener mSingleTapListener;
    private float mTargetAspectRatio;
    private int mThisHeight;
    private int mThisWidth;
    private Paint mTransparentLayerPaint;
    private int savedAspectRatioHeight;
    private int savedAspectRatioWidth;
    private float[] suppMatrixValues;

    public ImageCropView(Context context) {
        this(context, null);
    }

    public ImageCropView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageCropView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mEasing = new Cubic();
        this.mBaseMatrix = new Matrix();
        this.mSuppMatrix = new Matrix();
        this.mDisplayMatrix = new Matrix();
        this.mHandler = new Handler();
        this.mLayoutRunnable = null;
        this.mUserScaled = false;
        this.mMaxZoom = -1.0f;
        this.mMinZoom = -1.0f;
        this.mMatrixValues = new float[9];
        this.mThisWidth = -1;
        this.mThisHeight = -1;
        this.mCenter = new PointF();
        this.DEFAULT_ANIMATION_DURATION = ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        this.mBitmapRect = new RectF();
        this.mCenterRect = new RectF();
        this.mScrollRect = new RectF();
        this.mCropRect = new RectF();
        this.mAspectRatioWidth = 1;
        this.mAspectRatioHeight = 1;
        this.mTargetAspectRatio = (float) (this.mAspectRatioHeight / this.mAspectRatioWidth);
        this.GRID_ROW_COUNT = 3;
        this.GRID_COLUMN_COUNT = 3;
        this.mDoubleTapEnabled = false;
        this.mScaleEnabled = true;
        this.mScrollEnabled = true;
        this.isChangingScale = false;
        this.suppMatrixValues = new float[9];
        this.baseScale = 1.0f;
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ImageCropView);
        this.mTransparentLayerPaint = new Paint();
        this.mTransparentLayerPaint.setColor(Color.parseColor(DEFAULT_BACKGROUND_COLOR_ID));
        setScaleType(ScaleType.MATRIX);
        this.mGridInnerLinePaint = new Paint();
        this.mGridInnerLinePaint.setStrokeWidth(a.getDimension(1, 1.0f));
        this.mGridInnerLinePaint.setColor(a.getColor(0, -1));
        this.mGridOuterLinePaint = new Paint();
        this.mGridOuterLinePaint.setStrokeWidth(a.getDimension(4, 1.0f));
        this.mGridOuterLinePaint.setColor(a.getColor(3, -1));
        this.mGridOuterLinePaint.setStyle(Style.STROKE);
        this.gridInnerMode = a.getInt(6, 0);
        this.gridOuterMode = a.getInt(7, 0);
        this.gridLeftRightMargin = a.getDimension(2, 0.0f);
        this.gridTopBottomMargin = a.getDimension(5, 0.0f);
        this.mPts = new float[16];
        a.recycle();
        this.mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        this.mGestureListener = new GestureListener();
        this.mScaleListener = new ScaleListener();
        this.mScaleDetector = new ScaleGestureDetector(getContext(), this.mScaleListener);
        this.mGestureDetector = new GestureDetector(getContext(), this.mGestureListener, null, true);
        this.mDoubleTapDirection = 1;
        this.mBitmapChanged = false;
        this.mRestoreRequest = false;
    }

    public void setScaleType(ScaleType scaleType) {
        if (scaleType == ScaleType.MATRIX) {
            super.setScaleType(scaleType);
            return;
        }
        throw new IllegalArgumentException("Unsupported scaleType. Only ScaleType.MATRIX can be used");
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawTransparentLayer(canvas);
        drawGrid(canvas);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int deltaX = 0;
        int deltaY = 0;
        if (changed) {
            int oldW = this.mThisWidth;
            int oldH = this.mThisHeight;
            this.mThisWidth = right - left;
            this.mThisHeight = bottom - top;
            deltaX = this.mThisWidth - oldW;
            deltaY = this.mThisHeight - oldH;
            this.mCenter.x = ((float) this.mThisWidth) / 2.0f;
            this.mCenter.y = ((float) this.mThisHeight) / 2.0f;
        }
        int halfDiff;
        if (((int) (((float) this.mThisWidth) * this.mTargetAspectRatio)) > this.mThisHeight) {
            halfDiff = (this.mThisWidth - ((int) ((((float) this.mThisHeight) - (this.gridTopBottomMargin * 2.0f)) / this.mTargetAspectRatio))) / 2;
            this.mCropRect.set((float) (left + halfDiff), ((float) top) + this.gridTopBottomMargin, (float) (right - halfDiff), ((float) bottom) - this.gridTopBottomMargin);
        } else {
            int height = (int) ((((float) this.mThisWidth) - (this.gridLeftRightMargin * 2.0f)) * this.mTargetAspectRatio);
            halfDiff = (this.mThisHeight - height) / 2;
            this.mCropRect.set(((float) left) + this.gridLeftRightMargin, (float) (halfDiff - top), ((float) right) - this.gridLeftRightMargin, (float) (height + halfDiff));
        }
        Runnable r = this.mLayoutRunnable;
        if (r != null) {
            this.mLayoutRunnable = null;
            r.run();
        }
        Drawable drawable = getDrawable();
        if (drawable == null) {
            if (this.mBitmapChanged) {
                this.mBitmapChanged = false;
            }
            if (this.mRestoreRequest) {
                this.mRestoreRequest = false;
            }
        } else if (changed || this.mBitmapChanged) {
            if (this.mBitmapChanged) {
                this.mBaseMatrix.reset();
                if (!this.mMinZoomDefined) {
                    this.mMinZoom = -1.0f;
                }
                if (!this.mMaxZoomDefined) {
                    this.mMaxZoom = -1.0f;
                }
            }
            float scale = 1.0f;
            float oldMatrixScale = getScale(this.mBaseMatrix);
            float oldScale = getScale();
            float oldMinScale = Math.min(1.0f, 1.0f / oldMatrixScale);
            getProperBaseMatrix(drawable, this.mBaseMatrix);
            float new_matrix_scale = getScale(this.mBaseMatrix);
            if (this.mBitmapChanged) {
                setImageMatrix(getImageViewMatrix());
            } else if (changed) {
                if (!this.mMinZoomDefined) {
                    this.mMinZoom = -1.0f;
                }
                if (!this.mMaxZoomDefined) {
                    this.mMaxZoom = -1.0f;
                }
                setImageMatrix(getImageViewMatrix());
                postTranslate((float) (-deltaX), (float) (-deltaY));
                if (this.mUserScaled) {
                    if (((double) Math.abs(oldScale - oldMinScale)) > 0.001d) {
                        scale = (oldMatrixScale / new_matrix_scale) * oldScale;
                    }
                    zoomTo(scale);
                } else {
                    zoomTo(1.0f);
                }
            }
            this.mUserScaled = false;
            if (scale > getMaxScale() || scale < getMinScale()) {
                zoomTo(scale);
            }
            if (!this.mRestoreRequest) {
                center(true, true);
            }
            if (this.mBitmapChanged) {
                this.mBitmapChanged = false;
            }
            if (this.mRestoreRequest) {
                this.mRestoreRequest = false;
            }
        }
    }

    public void resetDisplay() {
        this.mBitmapChanged = true;
        resetMatrix();
        requestLayout();
    }

    public void resetMatrix() {
        this.mSuppMatrix = new Matrix();
        setImageMatrix(getImageViewMatrix());
        zoomTo(1.0f);
        postInvalidate();
    }

    private void drawTransparentLayer(Canvas canvas) {
        Rect r = new Rect();
        getLocalVisibleRect(r);
        canvas.drawRect((float) r.left, (float) r.top, (float) r.right, this.mCropRect.top, this.mTransparentLayerPaint);
        canvas.drawRect((float) r.left, this.mCropRect.bottom, (float) r.right, (float) r.bottom, this.mTransparentLayerPaint);
        canvas.drawRect((float) r.left, this.mCropRect.top, this.mCropRect.left, this.mCropRect.bottom, this.mTransparentLayerPaint);
        canvas.drawRect(this.mCropRect.right, this.mCropRect.top, (float) r.right, this.mCropRect.bottom, this.mTransparentLayerPaint);
    }

    private void drawGrid(Canvas canvas) {
        int i;
        int i2;
        int index = 0;
        for (i = 0; i < 2; i++) {
            i2 = index + 1;
            this.mPts[index] = this.mCropRect.left;
            index = i2 + 1;
            this.mPts[i2] = (this.mCropRect.height() * ((((float) i) + 1.0f) / 3.0f)) + this.mCropRect.top;
            i2 = index + 1;
            this.mPts[index] = this.mCropRect.right;
            index = i2 + 1;
            this.mPts[i2] = (this.mCropRect.height() * ((((float) i) + 1.0f) / 3.0f)) + this.mCropRect.top;
        }
        for (i = 0; i < 2; i++) {
            i2 = index + 1;
            this.mPts[index] = (this.mCropRect.width() * ((((float) i) + 1.0f) / 3.0f)) + this.mCropRect.left;
            index = i2 + 1;
            this.mPts[i2] = this.mCropRect.top;
            i2 = index + 1;
            this.mPts[index] = (this.mCropRect.width() * ((((float) i) + 1.0f) / 3.0f)) + this.mCropRect.left;
            index = i2 + 1;
            this.mPts[i2] = this.mCropRect.bottom;
        }
        if (this.gridInnerMode == 1) {
            canvas.drawLines(this.mPts, this.mGridInnerLinePaint);
        }
        if (this.gridOuterMode == 1) {
            float halfLineWidth = this.mGridOuterLinePaint.getStrokeWidth() * 0.5f;
            canvas.drawRect(this.mCropRect.left + halfLineWidth, this.mCropRect.top + halfLineWidth, this.mCropRect.right - halfLineWidth, this.mCropRect.bottom - halfLineWidth, this.mGridOuterLinePaint);
        }
    }

    public void setImageResource(int resId) {
        setImageDrawable(getContext().getResources().getDrawable(resId));
    }

    public void setAspectRatio(int aspectRatioWidth, int aspectRatioHeight) {
        if (aspectRatioWidth <= 0 || aspectRatioHeight <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        }
        this.mAspectRatioWidth = aspectRatioWidth;
        this.mAspectRatioHeight = aspectRatioHeight;
        this.mTargetAspectRatio = ((float) this.mAspectRatioHeight) / ((float) this.mAspectRatioWidth);
        resetDisplay();
    }

    public void setImageFilePath(String imageFilePath) {
        if (new File(imageFilePath).exists()) {
            this.imageFilePath = imageFilePath;
            setImageBitmap(BitmapLoadUtils.decode(imageFilePath, 1000, 1000, true));
            return;
        }
        throw new IllegalArgumentException("Image file does not exist");
    }

    public void setImageBitmap(Bitmap bitmap) {
        setImageBitmap(bitmap, 1.0f, 8.0f);
    }

    public void setImageBitmap(final Bitmap bitmap, final float min_zoom, final float max_zoom) {
        if (getWidth() <= 0) {
            this.mLayoutRunnable = new Runnable() {
                public void run() {
                    ImageCropView.this.setImageBitmap(bitmap, min_zoom, max_zoom);
                }
            };
        } else if (bitmap != null) {
            setImageDrawable(new FastBitmapDrawable(bitmap), min_zoom, max_zoom);
        } else {
            setImageDrawable(null, min_zoom, max_zoom);
        }
    }

    public void setImageDrawable(Drawable drawable) {
        setImageDrawable(drawable, 1.0f, 8.0f);
    }

    public void setImageDrawable(final Drawable drawable, final float min_zoom, final float max_zoom) {
        if (getWidth() <= 0) {
            this.mLayoutRunnable = new Runnable() {
                public void run() {
                    ImageCropView.this.setImageDrawable(drawable, min_zoom, max_zoom);
                }
            };
        } else {
            _setImageDrawable(drawable, min_zoom, max_zoom);
        }
    }

    protected void _setImageDrawable(Drawable drawable, float min_zoom, float max_zoom) {
        this.mBaseMatrix.reset();
        if (drawable != null) {
            super.setImageDrawable(drawable);
        } else {
            super.setImageDrawable(null);
        }
        if (min_zoom == -1.0f || max_zoom == -1.0f) {
            this.mMinZoom = -1.0f;
            this.mMaxZoom = -1.0f;
            this.mMinZoomDefined = false;
            this.mMaxZoomDefined = false;
        } else {
            min_zoom = Math.min(min_zoom, max_zoom);
            max_zoom = Math.max(min_zoom, max_zoom);
            this.mMinZoom = min_zoom;
            this.mMaxZoom = max_zoom;
            this.mMinZoomDefined = true;
            this.mMaxZoomDefined = true;
        }
        this.mBitmapChanged = true;
        this.mScaleFactor = getMaxScale() / 3.0f;
        requestLayout();
    }

    protected float computeMaxZoom() {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return 1.0f;
        }
        return Math.max(((float) drawable.getIntrinsicWidth()) / ((float) this.mThisWidth), ((float) drawable.getIntrinsicHeight()) / ((float) this.mThisHeight)) * 8.0f;
    }

    protected float computeMinZoom() {
        if (getDrawable() == null) {
            return 1.0f;
        }
        return Math.min(1.0f, 1.0f / getScale(this.mBaseMatrix));
    }

    public float getMaxScale() {
        if (this.mMaxZoom == -1.0f) {
            this.mMaxZoom = computeMaxZoom();
        }
        return this.mMaxZoom;
    }

    public float getMinScale() {
        if (this.mMinZoom == -1.0f) {
            this.mMinZoom = computeMinZoom();
        }
        return this.mMinZoom;
    }

    public Matrix getImageViewMatrix() {
        return getImageViewMatrix(this.mSuppMatrix);
    }

    public Matrix getImageViewMatrix(Matrix supportMatrix) {
        this.mDisplayMatrix.set(this.mBaseMatrix);
        this.mDisplayMatrix.postConcat(supportMatrix);
        return this.mDisplayMatrix;
    }

    protected void getProperBaseMatrix(Drawable drawable, Matrix matrix) {
        float viewWidth = this.mCropRect.width();
        float viewHeight = this.mCropRect.height();
        float w = (float) drawable.getIntrinsicWidth();
        float h = (float) drawable.getIntrinsicHeight();
        matrix.reset();
        if (w > viewWidth || h > viewHeight) {
            this.baseScale = Math.max(viewWidth / w, viewHeight / h);
            matrix.postScale(this.baseScale, this.baseScale);
            matrix.postTranslate((viewWidth - (this.baseScale * w)) / 2.0f, (viewHeight - (this.baseScale * h)) / 2.0f);
            return;
        }
        this.baseScale = Math.max(viewWidth / w, viewHeight / h);
        matrix.postScale(this.baseScale, this.baseScale);
        matrix.postTranslate((viewWidth - (this.baseScale * w)) / 2.0f, (viewHeight - (this.baseScale * h)) / 2.0f);
    }

    protected float getValue(Matrix matrix, int whichValue) {
        matrix.getValues(this.mMatrixValues);
        return this.mMatrixValues[whichValue];
    }

    public void printMatrix(Matrix matrix) {
        float scalex = getValue(matrix, 0);
        float scaley = getValue(matrix, 4);
        float tx = getValue(matrix, 2);
        Log.d(LOG_TAG, "matrix: { x: " + tx + ", y: " + getValue(matrix, 5) + ", scalex: " + scalex + ", scaley: " + scaley + " }");
    }

    public RectF getBitmapRect() {
        return getBitmapRect(this.mSuppMatrix);
    }

    protected RectF getBitmapRect(Matrix supportMatrix) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return null;
        }
        Matrix m = getImageViewMatrix(supportMatrix);
        this.mBitmapRect.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
        m.mapRect(this.mBitmapRect);
        return this.mBitmapRect;
    }

    protected float getScale(Matrix matrix) {
        return getValue(matrix, 0);
    }

    @SuppressLint({"Override"})
    public float getRotation() {
        return 0.0f;
    }

    public float getScale() {
        return getScale(this.mSuppMatrix);
    }

    public float getBaseScale() {
        return getScale(this.mBaseMatrix);
    }

    protected void center(boolean horizontal, boolean vertical) {
        if (getDrawable() != null) {
            RectF rect = getCenter(this.mSuppMatrix, horizontal, vertical);
            if (rect.left != 0.0f || rect.top != 0.0f) {
                postTranslate(rect.left, rect.top);
            }
        }
    }

    protected RectF getCenter(Matrix supportMatrix, boolean horizontal, boolean vertical) {
        if (getDrawable() == null) {
            return new RectF(0.0f, 0.0f, 0.0f, 0.0f);
        }
        this.mCenterRect.set(0.0f, 0.0f, 0.0f, 0.0f);
        RectF rect = getBitmapRect(supportMatrix);
        float height = rect.height();
        float width = rect.width();
        float deltaX = 0.0f;
        float deltaY = 0.0f;
        if (vertical) {
            int viewHeight = this.mThisHeight;
            if (height < ((float) viewHeight)) {
                deltaY = ((((float) viewHeight) - height) / 2.0f) - rect.top;
            } else if (rect.top > 0.0f) {
                deltaY = -rect.top;
            } else if (rect.bottom < ((float) viewHeight)) {
                deltaY = ((float) this.mThisHeight) - rect.bottom;
            }
        }
        if (horizontal) {
            int viewWidth = this.mThisWidth;
            if (width < ((float) viewWidth)) {
                deltaX = ((((float) viewWidth) - width) / 2.0f) - rect.left;
            } else if (rect.left > 0.0f) {
                deltaX = -rect.left;
            } else if (rect.right < ((float) viewWidth)) {
                deltaX = ((float) viewWidth) - rect.right;
            }
        }
        this.mCenterRect.set(deltaX, deltaY, 0.0f, 0.0f);
        return this.mCenterRect;
    }

    protected void postTranslate(float deltaX, float deltaY) {
        if (deltaX != 0.0f || deltaY != 0.0f) {
            this.mSuppMatrix.postTranslate(deltaX, deltaY);
            setImageMatrix(getImageViewMatrix());
        }
    }

    protected void postScale(float scale, float centerX, float centerY) {
        this.mSuppMatrix.postScale(scale, scale, centerX, centerY);
        setImageMatrix(getImageViewMatrix());
    }

    protected PointF getCenter() {
        return this.mCenter;
    }

    protected void zoomTo(float scale) {
        if (scale > getMaxScale()) {
            scale = getMaxScale();
        }
        if (scale < getMinScale()) {
            scale = getMinScale();
        }
        PointF center = getCenter();
        zoomTo(scale, center.x, center.y);
    }

    public void zoomTo(float scale, float durationMs) {
        PointF center = getCenter();
        zoomTo(scale, center.x, center.y, durationMs);
    }

    protected void zoomTo(float scale, float centerX, float centerY) {
        if (scale > getMaxScale()) {
            scale = getMaxScale();
        }
        postScale(scale / getScale(), centerX, centerY);
        center(true, true);
    }

    protected void onZoomAnimationCompleted(float scale) {
        if (scale < getMinScale()) {
            zoomTo(getMinScale(), 50.0f);
        }
    }

    public void scrollBy(float x, float y) {
        panBy((double) x, (double) y);
    }

    protected void panBy(double dx, double dy) {
        this.mScrollRect.set((float) dx, (float) dy, 0.0f, 0.0f);
        postTranslate(this.mScrollRect.left, this.mScrollRect.top);
        adjustCropAreaImage();
    }

    private void adjustCropAreaImage() {
        if (getDrawable() != null) {
            RectF rect = getAdjust(this.mSuppMatrix);
            if (rect.left != 0.0f || rect.top != 0.0f) {
                postTranslate(rect.left, rect.top);
            }
        }
    }

    private RectF getAdjust(Matrix supportMatrix) {
        if (getDrawable() == null) {
            return new RectF(0.0f, 0.0f, 0.0f, 0.0f);
        }
        this.mCenterRect.set(0.0f, 0.0f, 0.0f, 0.0f);
        RectF rect = getBitmapRect(supportMatrix);
        float deltaX = 0.0f;
        float deltaY = 0.0f;
        if (rect.top > this.mCropRect.top) {
            deltaY = this.mCropRect.top - rect.top;
        } else if (rect.bottom < this.mCropRect.bottom) {
            deltaY = this.mCropRect.bottom - rect.bottom;
        }
        if (rect.left > this.mCropRect.left) {
            deltaX = this.mCropRect.left - rect.left;
        } else if (rect.right < this.mCropRect.right) {
            deltaX = this.mCropRect.right - rect.right;
        }
        this.mCenterRect.set(deltaX, deltaY, 0.0f, 0.0f);
        return this.mCenterRect;
    }

    protected void scrollBy(float distanceX, float distanceY, double durationMs) {
        final double dx = (double) distanceX;
        final double dy = (double) distanceY;
        final long startTime = System.currentTimeMillis();
        final double d = durationMs;
        this.mHandler.post(new Runnable() {
            double old_x = 0.0d;
            double old_y = 0.0d;

            public void run() {
                double currentMs = Math.min(d, (double) (System.currentTimeMillis() - startTime));
                double x = ImageCropView.this.mEasing.easeOut(currentMs, 0.0d, dx, d);
                double y = ImageCropView.this.mEasing.easeOut(currentMs, 0.0d, dy, d);
                ImageCropView.this.panBy(x - this.old_x, y - this.old_y);
                this.old_x = x;
                this.old_y = y;
                if (currentMs < d) {
                    ImageCropView.this.mHandler.post(this);
                }
            }
        });
    }

    protected void zoomTo(float scale, float centerX, float centerY, float durationMs) {
        if (scale > getMaxScale()) {
            scale = getMaxScale();
        }
        final long startTime = System.currentTimeMillis();
        final float oldScale = getScale();
        final float deltaScale = scale - oldScale;
        Matrix m = new Matrix(this.mSuppMatrix);
        m.postScale(scale, scale, centerX, centerY);
        RectF rect = getCenter(m, true, true);
        final float destX = centerX + (rect.left * scale);
        final float destY = centerY + (rect.top * scale);
        final float f = durationMs;
        this.mHandler.post(new Runnable() {
            public void run() {
                float currentMs = Math.min(f, (float) (System.currentTimeMillis() - startTime));
                ImageCropView.this.zoomTo(oldScale + ((float) ImageCropView.this.mEasing.easeInOut((double) currentMs, 0.0d, (double) deltaScale, (double) f)), destX, destY);
                if (currentMs < f) {
                    ImageCropView.this.mHandler.post(this);
                    return;
                }
                ImageCropView.this.onZoomAnimationCompleted(ImageCropView.this.getScale());
                ImageCropView.this.center(true, true);
            }
        });
    }

    public Bitmap getCroppedImage() {
        CropInfo cropInfo = getCropInfo();
        if (cropInfo == null) {
            return null;
        }
        if (this.imageFilePath != null) {
            return cropInfo.getCroppedImage(this.imageFilePath);
        }
        Bitmap bitmap = getViewBitmap();
        if (bitmap != null) {
            return cropInfo.getCroppedImage(bitmap);
        }
        return bitmap;
    }

    public CropInfo getCropInfo() {
        Bitmap viewBitmap = getViewBitmap();
        if (viewBitmap == null) {
            return null;
        }
        float scale = this.baseScale * getScale();
        RectF viewImageRect = getBitmapRect();
        return new CropInfo(scale, (float) viewBitmap.getWidth(), viewImageRect.top, viewImageRect.left, this.mCropRect.top, this.mCropRect.left, this.mCropRect.width(), this.mCropRect.height());
    }

    public Bitmap getViewBitmap() {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            return ((FastBitmapDrawable) drawable).getBitmap();
        }
        Log.e(LOG_TAG, "drawable is null");
        return null;
    }

    public void setGridInnerMode(int gridInnerMode) {
        this.gridInnerMode = gridInnerMode;
        invalidate();
    }

    public void setGridOuterMode(int gridOuterMode) {
        this.gridOuterMode = gridOuterMode;
        invalidate();
    }

    public void setGridLeftRightMargin(int marginDP) {
        this.gridLeftRightMargin = (float) dpToPixel(marginDP);
        requestLayout();
    }

    public void setGridTopBottomMargin(int marginDP) {
        this.gridTopBottomMargin = (float) dpToPixel(marginDP);
        requestLayout();
    }

    private int dpToPixel(int dp) {
        return (int) TypedValue.applyDimension(1, (float) dp, getResources().getDisplayMetrics());
    }

    public void saveState() {
        this.savedAspectRatioWidth = this.mAspectRatioWidth;
        this.savedAspectRatioHeight = this.mAspectRatioHeight;
        this.mSuppMatrix.getValues(this.suppMatrixValues);
    }

    public void restoreState() {
        this.mBitmapChanged = true;
        this.mRestoreRequest = true;
        this.mAspectRatioWidth = this.savedAspectRatioWidth;
        this.mAspectRatioHeight = this.savedAspectRatioHeight;
        this.mTargetAspectRatio = ((float) this.mAspectRatioHeight) / ((float) this.mAspectRatioWidth);
        this.mSuppMatrix = new Matrix();
        this.mSuppMatrix.setValues(this.suppMatrixValues);
        setImageMatrix(getImageViewMatrix());
        postInvalidate();
        requestLayout();
    }

    public void setDoubleTapListener(OnImageViewTouchDoubleTapListener listener) {
        this.mDoubleTapListener = listener;
    }

    public void setSingleTapListener(OnImageViewTouchSingleTapListener listener) {
        this.mSingleTapListener = listener;
    }

    public void setScaleEnabled(boolean value) {
        this.mScaleEnabled = value;
    }

    public void setScrollEnabled(boolean value) {
        this.mScrollEnabled = value;
    }

    public boolean getDoubleTapEnabled() {
        return this.mDoubleTapEnabled;
    }

    public void setDoubleTapEnabled(boolean value) {
        this.mDoubleTapEnabled = value;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mBitmapChanged) {
            return false;
        }
        this.mScaleDetector.onTouchEvent(event);
        if (!this.mScaleDetector.isInProgress()) {
            this.mGestureDetector.onTouchEvent(event);
        }
        switch (event.getAction() & 255) {
            case 1:
                return onUp(event);
            default:
                return true;
        }
    }

    protected float onDoubleTapPost(float scale, float maxZoom) {
        if (this.mDoubleTapDirection != 1) {
            this.mDoubleTapDirection = 1;
            return 1.0f;
        } else if ((this.mScaleFactor * 2.0f) + scale <= maxZoom) {
            return scale + this.mScaleFactor;
        } else {
            this.mDoubleTapDirection = -1;
            return maxZoom;
        }
    }

    public boolean onSingleTapConfirmed(MotionEvent e) {
        return true;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        this.mUserScaled = true;
        scrollBy(-distanceX, -distanceY);
        invalidate();
        return true;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        float diffX = e2.getX() - e1.getX();
        float diffY = e2.getY() - e1.getY();
        if (Math.abs(velocityX) <= 800.0f && Math.abs(velocityY) <= 800.0f) {
            return false;
        }
        this.mUserScaled = true;
        scrollBy(diffX / 2.0f, diffY / 2.0f, 300.0d);
        invalidate();
        return true;
    }

    public boolean onDown(MotionEvent e) {
        if (this.mBitmapChanged) {
            return false;
        }
        return true;
    }

    public boolean onUp(MotionEvent e) {
        if (this.mBitmapChanged) {
            return false;
        }
        if (getScale() < getMinScale()) {
            zoomTo(getMinScale(), 50.0f);
        }
        return true;
    }

    public boolean onSingleTapUp(MotionEvent e) {
        if (this.mBitmapChanged) {
            return false;
        }
        return true;
    }

    public boolean isChangingScale() {
        return this.isChangingScale;
    }

    public interface OnImageViewTouchDoubleTapListener {
        void onDoubleTap();
    }

    public interface OnImageViewTouchSingleTapListener {
        void onSingleTapConfirmed();
    }

    public class GestureListener extends SimpleOnGestureListener {
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (ImageCropView.this.mSingleTapListener != null) {
                ImageCropView.this.mSingleTapListener.onSingleTapConfirmed();
            }
            return ImageCropView.this.onSingleTapConfirmed(e);
        }

        public boolean onDoubleTap(MotionEvent e) {
            if (ImageCropView.this.mDoubleTapEnabled) {
                ImageCropView.this.mUserScaled = true;
                ImageCropView.this.zoomTo(Math.min(ImageCropView.this.getMaxScale(), Math.max(ImageCropView.this.onDoubleTapPost(ImageCropView.this.getScale(), ImageCropView.this.getMaxScale()), ImageCropView.this.getMinScale())), e.getX(), e.getY(), 200.0f);
                ImageCropView.this.invalidate();
            }
            if (ImageCropView.this.mDoubleTapListener != null) {
                ImageCropView.this.mDoubleTapListener.onDoubleTap();
            }
            return super.onDoubleTap(e);
        }

        public void onLongPress(MotionEvent e) {
            if (ImageCropView.this.isLongClickable() && !ImageCropView.this.mScaleDetector.isInProgress()) {
                ImageCropView.this.setPressed(true);
                ImageCropView.this.performLongClick();
            }
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (ImageCropView.this.mScrollEnabled && e1 != null && e2 != null && e1.getPointerCount() <= 1 && e2.getPointerCount() <= 1 && !ImageCropView.this.mScaleDetector.isInProgress()) {
                return ImageCropView.this.onScroll(e1, e2, distanceX, distanceY);
            }
            return false;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (ImageCropView.this.mScrollEnabled && e1.getPointerCount() <= 1 && e2.getPointerCount() <= 1 && !ImageCropView.this.mScaleDetector.isInProgress()) {
                return ImageCropView.this.onFling(e1, e2, velocityX, velocityY);
            }
            return false;
        }

        public boolean onSingleTapUp(MotionEvent e) {
            return ImageCropView.this.onSingleTapUp(e);
        }

        public boolean onDown(MotionEvent e) {
            return ImageCropView.this.onDown(e);
        }
    }

    public class ScaleListener extends SimpleOnScaleGestureListener {
        protected boolean mScaled = false;

        public boolean onScaleBegin(ScaleGestureDetector detector) {
            ImageCropView.this.isChangingScale = true;
            return super.onScaleBegin(detector);
        }

        public boolean onScale(ScaleGestureDetector detector) {
            float span = detector.getCurrentSpan() - detector.getPreviousSpan();
            float targetScale = ImageCropView.this.getScale() * detector.getScaleFactor();
            if (ImageCropView.this.mScaleEnabled) {
                if (this.mScaled && span != 0.0f) {
                    ImageCropView.this.mUserScaled = true;
                    ImageCropView.this.zoomTo(Math.min(ImageCropView.this.getMaxScale(), Math.max(targetScale, ImageCropView.this.getMinScale() - 0.1f)), detector.getFocusX(), detector.getFocusY());
                    ImageCropView.this.mDoubleTapDirection = 1;
                    ImageCropView.this.invalidate();
                } else if (!this.mScaled) {
                    this.mScaled = true;
                }
            }
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
            ImageCropView.this.isChangingScale = false;
            super.onScaleEnd(detector);
        }
    }
}
