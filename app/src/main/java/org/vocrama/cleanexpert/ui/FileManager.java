package org.vocrama.cleanexpert.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.FileUtils;
import android.provider.MediaStore.Files;

import androidx.core.content.FileProvider;

import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.vocrama.cleanexpert.ui.view.Constant;
import org.vocrama.cleanexpert.ui.view.FileItem;
import org.vocrama.cleanexpert.ui.view.OrderType;
import org.vocrama.cleanexpert.util.FileUtil;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static android.icu.lang.UProperty.NAME;

public class FileManager {
    public static String PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static String SECRET = (Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MySecret007");

    private OnFileLoadingCallback listener;
    private CompositeDisposable compositeDisposable;
    private boolean isThreadStarted = false;

    public void onCreate() {
        compositeDisposable = new CompositeDisposable();
    }

    public void setListener(OnFileLoadingCallback listener) {
        this.listener = listener;
    }

    public void readFile(List<FileItem> arrFileItem, String path, Context context) {

        if (isThreadStarted)
            return;

        File directory = new File(path);

        compositeDisposable.add(
                Observable
                        .just(directory)
                        .flatMap(file -> Observable.fromArray(file.listFiles()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> {
                            isThreadStarted = true;
                            listener.onFileLoading();
                        })
                        .doOnNext(file -> {
                            if (file.isFile()) {
                                arrFileItem.add(getFileItem(file));
                            } else {
                                if (!file.isHidden()) {
                                    arrFileItem.add(getFileItem(file));
                                }
                            }
                        })
                        .doOnError(throwable -> {
                            isThreadStarted = false;
                            listener.onFileLoaded();
                        })
                        .doFinally(() -> {
                            isThreadStarted = false;
                            listener.onFileLoaded();
                        })
                        .subscribe());
    }

    public void sortList(List<FileItem> arrFileItem, SortBy sort) {
        switch (sort.sortBy) {
            case NAME:
                Collections.sort(arrFileItem, OrderType.NAME.getComparator());
                break;
            case DATE:
                Collections.sort(arrFileItem, OrderType.DATE.getComparator());
                break;
            case SIZE:
                Collections.sort(arrFileItem, OrderType.SIZE.getComparator());
                break;
            case TYPE:
                Collections.sort(arrFileItem, OrderType.TYPE.getComparator());
        }
    }

    public static class SortBy {

        private SortBy.Sort sortBy;
        private CharSequence name;

        public SortBy(SortBy.Sort sortBy, CharSequence name) {
            this.sortBy = sortBy;
            this.name = name;
        }

        public static SortBy NAME() {
            return new SortBy(Sort.NAME, " Name ");
        }

        public static SortBy DATE() {
            return new SortBy(Sort.DATE, " Date ");
        }

        public static SortBy SIZE() {
            return new SortBy(Sort.SIZE, " SIZE ");
        }

        public static SortBy TYPE() {
            return new SortBy(Sort.SIZE, " TYPE ");
        }

        public CharSequence getSortByCharSequence() {
            return name;
        }

        public void setSortBy(Sort sortBy) {
            this.sortBy = sortBy;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SortBy sortBy1 = (SortBy) o;
            return sortBy == sortBy1.sortBy;
        }

        @Override
        public int hashCode() {
            return Objects.hash(sortBy);
        }

        public enum Sort {
            NAME,
            DATE,
            SIZE,
            TYPE
        }
    }


    public static FileItem getFileItem(File file) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
        String name = file.getName();
        String pathFile = file.getPath();
        String date = simpleDateFormat.format(Long.valueOf(file.lastModified()));
        Date date1 = null;
        try {
            date1 = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        boolean isFile = file.isFile();
        boolean isDirectory = file.isDirectory();
        boolean isHidden = file.isHidden();
        long length = 0;
        try {
            if (file.isDirectory()) {
                for (File filee : file.listFiles()) {
                    long length2;
                    if (filee.isFile()) {
                        length2 = filee.length();
                    } else {
                        length2 = getFileFolderSize(filee);
                    }
                    length += length2;
                }
            } else if (file.isFile()) {
                length = 0 + file.length();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return new FileItem(name, pathFile, date, isFile, isDirectory, false, length, date1, isHidden);
    }

    public static long getFileFolderSize(File dir) {
        long size = 0;
        if (dir.exists()) {
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();
                if (files != null)
                    for (File file :files) {
                        long length;
                        if (file.isFile()) {
                            length = file.length();
                        } else {
                            length = getFileFolderSize(file);
                        }
                        size += length;
                    }
                return size;
            } else if (dir.isFile()) {
                return 0 + dir.length();
            }
        }
        return 0;
    }

    public static Intent createIntent(File file, Context context) {
        if (!file.isFile()) {
            return null;
        }
        Intent i = new Intent("android.intent.action.VIEW");
        String name = file.getName();
        i.setDataAndType(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file), MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1)));
        i.addFlags(1);
        i.addFlags(268435456);
        return i;
    }

    public static boolean delete(Context context, File file) {
        String where = "_data=?";
        String[] selectionArgs = new String[]{file.getAbsolutePath()};
        ContentResolver contentResolver = context.getContentResolver();
        Uri filesUri = Files.getContentUri("external");
        contentResolver.delete(filesUri, "_data=?", selectionArgs);
        if (file.exists()) {
            contentResolver.delete(filesUri, "_data=?", selectionArgs);
        }
        if (file.exists()) {
            return false;
        }
        return true;
    }

    public static void deleteDirectory(String folder) {
        File file = new File(folder);
        ArrayList<File> arrFiles = new ArrayList();
        if (file.isDirectory()) {
            if (file.exists()) {
                File[] files = file.listFiles();
                if (files != null) {
                    for (int i = 0; i < files.length; i++) {
                        deleteDirectory(files[i].getPath());
                        arrFiles.add(files[i]);
                    }
                } else {
                    return;
                }
            }
            arrFiles.add(file);
            Iterator it = arrFiles.iterator();
            while (it.hasNext()) {
                ((File) it.next()).delete();
            }
            return;
        }
        file.delete();
    }

    public void renameFile(String path, String nameChange) {
        new File(path).renameTo(new File(path.substring(0, path.lastIndexOf("/") + 1) + nameChange));
    }

    public void copyDirectory(Context context, String srcDir, String dstDir) {
        File src = new File(srcDir);
        File dst = new File(new File(dstDir, src.getName()).toString());
        try {
            if (src.isDirectory()) {
                if (!dst.exists()) {
                    dst.mkdir();
                }
                for (String file : src.list()) {
                    copyDirectory(context, new File(src, file).toString(), dst.toString());
                }
            } else {
                fileWriter(src, dst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        scanFile(context, new String[]{dst.getPath()});
    }

    public static void scanFile(Context context, String[] paths) {
        MediaScannerConnection.scanFile(context, paths, null, null);
    }

    public static void fileWriter(File srcDir, File dstDir) throws IOException {
        try {
            if (srcDir.exists()) {
                InputStream in = new FileInputStream(srcDir);
                OutputStream out = new FileOutputStream(dstDir);
                byte[] buf = new byte[1024];
                while (true) {
                    int len = in.read(buf);
                    if (len > 0) {
                        out.write(buf, 0, len);
                    } else {
                        in.close();
                        out.close();
                        return;
                    }
                }
            }
            System.out.println(srcDir + " doesnot exist");
            throw new IOException(srcDir + " doesnot exist");
        } catch (Exception e) {
        }
    }

    public void moveDirectory(Context context, String sourceLocation, String targetLocation) {
        File src = new File(sourceLocation);
        File dst = new File(new File(targetLocation, src.getName()).toString());
        try {
            if (src.isDirectory()) {
                if (!dst.exists()) {
                    dst.mkdirs();
                }
                String[] children = src.list();
                for (String file : children) {
                    copyDirectory(context, new File(src, file).toString(), dst.toString());
                }
            } else {
                fileWriter(src, dst);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        scanFile(context, new String[]{dst.getPath()});
    }

    public interface OnFileLoadingCallback {
        void onFileLoading();

        void onFileLoaded();
    }


    public void onDestroy() {
        compositeDisposable.dispose();
    }

}
