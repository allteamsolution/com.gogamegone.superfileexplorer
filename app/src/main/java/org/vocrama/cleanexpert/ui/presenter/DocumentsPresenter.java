package org.vocrama.cleanexpert.ui.presenter;

import android.os.Environment;

import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.ui.FileManager;
import org.vocrama.cleanexpert.ui.presenter.IPresenter.IAsyncFragmentPresenter;
import org.vocrama.cleanexpert.ui.presenter.IPresenter.IView;
import org.vocrama.cleanexpert.ui.view.FileItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DocumentsPresenter implements IAsyncFragmentPresenter<FileItem> {

    private IView<FileItem> view;
    private List<FileItem> items;

    @Override
    public void onCreate() {
        items = new ArrayList<>();
        view.showProgress();
    }

    @Override
    public void getDataAsync() {
        thread.start();
    }

    @Override
    public void setView(IView<FileItem> view) {
        this.view = view;
    }

    @Override
    public void onDestroy() {
        thread.interrupt();
    }

    private Thread thread = new Thread(() -> {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        getDocuments(file, items);
        App.getUIHandler().post(() -> {
            view.onDetData(items);
            view.hideProgress();
        });
    });

    private void getDocuments(File dir, List<FileItem> fileItems) {
        File[] listFile = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (File file : listFile) {
                if (file.isDirectory()) {
                    getDocuments(file, fileItems);
                } else {
                    if (file.getName().endsWith(".txt")
                            || file.getName().endsWith(".pdf")
                            || file.getName().endsWith(".doc")
                            || file.getName().endsWith(".docx")) {
                        fileItems.add(FileManager.getFileItem(file));
                    }
                }
            }
        }
    }
}
