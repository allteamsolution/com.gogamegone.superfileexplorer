package org.vocrama.cleanexpert.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.view.FileItem;

import java.io.File;


public class InfoDialog extends DialogFragment {

    public static final String ARGS = "INFO_DIALOG_ARGS";
    private FileItem params;

    public InfoDialog() {

    }

    public static InfoDialog getInstance(FileItem params) {
        InfoDialog infoDialog = new InfoDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARGS, params);
        infoDialog.setArguments(bundle);
        return infoDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            params = getArguments().getParcelable(ARGS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.infodialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView date = view.findViewById(R.id.date);
        TextView name = view.findViewById(R.id.name);
        TextView ok = view.findViewById(R.id.info_ok);
        TextView path = view.findViewById(R.id.path);
        TextView size = view.findViewById(R.id.size);

        if (params != null) {
            date.setText(params.getDate());
            name.setText(params.getName());
            path.setText(params.getPathFile());
            size.setText(PickerConstants.getBytesString(FileManager.getFileFolderSize(new File(String.valueOf(params.getPathFile())))));
        }

        ok.setOnClickListener(v -> {
            dismiss();
        });
    }
}
