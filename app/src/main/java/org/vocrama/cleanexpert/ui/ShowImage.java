package org.vocrama.cleanexpert.ui;

import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gogamegone.superfileexplorer.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class ShowImage extends BaseTheme implements OnTouchListener {
    public static String d_path;
    public static ArrayList<MediaFile> images = new ArrayList();
    public static LinearLayout linear_bottom;
    public static LinearLayout linear_crop;
    public static LinearLayout linear_detail;
    public static RelativeLayout linear_main;
    public static ViewPager viewPager;
    static int fp;
    static Uri myUri = null;
    private static ActionBar mActionbar;
    private static boolean mIsFullScreen;
    final int PIC_CROP = 1;
    ShowImageAdapter adapter;
    Bitmap bmpW;
    DisplayMetrics displayMetrics;
    Bitmap fbmpW;
    int height;
    ImageView img_back;
    ImageView img_delete;
    ImageView img_edit;
    ImageView img_info;
    ImageView img_moreIcon;
    ImageView img_setus;
    ImageView img_share;
    int int_position;
    boolean isVideoFromGallery = false;
    AlertDialog levelDialog;
    String pathImage;
    int per = 500;
    int pos;
    ProgressDialog progressDialog;
    AnimatorSet set;
    ImageView ss_cancel;
    ImageView ss_effects;
    TextView txtName;
    TextView txtNameDetail;
    TextView txtPath;
    TextView txtPathDetail;
    TextView txtSize;
    TextView txtSizeDetail;
    TextView txt_count;
    ImageView undo_delete;
    int width;
    private boolean isToolbarShow = true;

    @SuppressLint({"ClickableViewAccessibility"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.show_java_list);
        this.pathImage = getIntent().getStringExtra("File");
        images = getIntent().getParcelableArrayListExtra("images");
        this.int_position = getIntent().getIntExtra("pos", 0);
        viewPager = (ViewPager) findViewById(R.id.image_vp);
        this.txt_count = (TextView) findViewById(R.id.txt_count);
        this.img_share = (ImageView) findViewById(R.id.img_share);
        this.img_setus = (ImageView) findViewById(R.id.img_setus);
        this.img_moreIcon = (ImageView) findViewById(R.id.img_moreIcon);
        this.img_edit = (ImageView) findViewById(R.id.img_edit);
        this.img_delete = (ImageView) findViewById(R.id.img_delete);
        this.img_info = (ImageView) findViewById(R.id.img_info);
        linear_main = (RelativeLayout) findViewById(R.id.linear_main);
        linear_bottom = (LinearLayout) findViewById(R.id.linear_bottom);
        this.img_back = (ImageView) findViewById(R.id.img_back);
        this.pos = this.int_position + 1;
        this.txt_count.setText("" + this.pos + "/" + images.size());
        this.adapter = new ShowImageAdapter(getApplicationContext(), images, this.int_position);
        viewPager.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(this.int_position);
        this.img_back.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ShowImage.this.finish();
            }
        });
        this.img_share.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ShowImage.this.show_share();
            }
        });
        this.img_setus.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                View renameFileView = LayoutInflater.from(ShowImage.this).inflate(R.layout.dialog_wallpaper, null);
                Button btnCancle = (Button) renameFileView.findViewById(R.id.btnCancle);
                Button btnExtract = (Button) renameFileView.findViewById(R.id.btnwall);
                AlertDialog.Builder builder = new AlertDialog.Builder(ShowImage.this);
                builder.setView(renameFileView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                btnExtract.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        ShowImage.this.bmpW = ShowImage.this.getRotation(((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path);
                        ShowImage.this.setAsWallpaper();
                        alertDialog.dismiss();
                    }
                });
                btnCancle.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
            }
        });
        this.txt_count.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.img_moreIcon.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                View view = ShowImage.this.getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog, null);
                final BottomSheetDialog dialog = new BottomSheetDialog(ShowImage.this);
                ImageView deatail_img = (ImageView) dialog.findViewById(R.id.deatail_img);
                ShowImage.linear_crop = (LinearLayout) view.findViewById(R.id.linear_crop);
                ShowImage.linear_detail = (LinearLayout) view.findViewById(R.id.linear_detail);
                dialog.setContentView(view);
                dialog.show();
                ShowImage.linear_detail.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        ShowImage.this.information_detail();
                        dialog.dismiss();
                    }
                });
                ShowImage.linear_crop.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                        ShowImage.this.startActivity(new Intent(ShowImage.this, CropActivity.class));
                    }
                });
            }
        });
        this.img_edit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ShowImage.this.startActivity(new Intent(ShowImage.this, EffectsActivity.class));
            }
        });
        this.img_info.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ShowImage.this.information_detail();
            }
        });
        this.img_delete.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ShowImage.this.show_delete();
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                ShowImage.this.txt_count.setText("" + (position + 1) + "/" + ShowImage.images.size());
            }

            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    private void information_detail() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);
        this.txtNameDetail = (TextView) dialogView.findViewById(R.id.txtNameDetail);
        this.txtPathDetail = (TextView) dialogView.findViewById(R.id.txtPathDetail);
        this.txtSizeDetail = (TextView) dialogView.findViewById(R.id.txtSizeDetail);
        this.txtPathDetail.setText("" + ((MediaFile) images.get(viewPager.getCurrentItem())).path);
        this.txtNameDetail.setText("" + ((MediaFile) images.get(viewPager.getCurrentItem())).name);
        this.txtSizeDetail.setText("" + Formatter.formatFileSize(this, ((MediaFile) images.get(viewPager.getCurrentItem())).imageSize));
        dialogBuilder.setPositiveButton((CharSequence) "Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        dialogBuilder.setNegativeButton((CharSequence) "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        dialogBuilder.create().show();
    }

    public void GetScreenWidthHeight() {
        this.displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.displayMetrics);
        this.width = this.displayMetrics.widthPixels;
        this.height = this.displayMetrics.heightPixels;
    }

    public void SetBitmapSize() {
        this.fbmpW = Bitmap.createScaledBitmap(this.bmpW, this.width, this.height, false);
    }

    private void setAsWallpaper() {
        GetScreenWidthHeight();
        SetBitmapSize();
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
        try {
            if (this.fbmpW != null) {
                wallpaperManager.setBitmap(this.fbmpW);
                wallpaperManager.suggestDesiredDimensions(this.width, this.height);
                Toast.makeText(this, "set wallpaper successfully..", 0).show();
                return;
            }
            Toast.makeText(this, "No image was chosen", 1).show();
        } catch (IOException e) {
            Toast.makeText(this, "not set wallpaper", 0).show();
            e.printStackTrace();
        }
    }

    private Bitmap getRotation(String path) {
        Exception e;
        Bitmap thumbnail = BitmapFactory.decodeFile(path);
        try {
            ExifInterface exif = new ExifInterface(path);
            ExifInterface exifInterface;
            try {
                int orientation = exif.getAttributeInt("Orientation", 1);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90.0f);
                } else if (orientation == 3) {
                    matrix.postRotate(180.0f);
                } else if (orientation == 8) {
                    matrix.postRotate(270.0f);
                }
                exifInterface = exif;
                return Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (Exception e2) {
                e = e2;
                exifInterface = exif;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
        return thumbnail;
    }

    private void show_share() {
        Uri uri;
        d_path = ((MediaFile) images.get(viewPager.getCurrentItem())).path;
        File file = new File(d_path);
        Intent i = new Intent();
        i.setAction("android.intent.action.SEND");
        i.setType("image/jpeg");
        if (VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", file);
        } else {
            uri = Uri.fromFile(file);
        }
        i.putExtra("android.intent.extra.STREAM", uri);
        i.addFlags(1);
        startActivity(i);
    }

    private void show_delete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((CharSequence) "Are You Sure Want To Delete ?");
        builder.setTitle((CharSequence) "Delete Image");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String d_path = ((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path;
                new Delete().execute(new Void[0]);
                alertDialog.dismiss();
            }
        });
        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public static class ShowImageAdapter extends PagerAdapter {
        public static TouchImageView imageView;
        public static String path;
        ArrayList<MediaFile> arrayList;
        Context context;
        File image;
        LayoutInflater inflater;
        int position;

        public ShowImageAdapter(Context context, ArrayList<MediaFile> images, int int_position) {
            this.context = context;
            this.arrayList = images;
            this.position = int_position;
        }

        public ShowImageAdapter(Context context, File imge) {
            this.context = context;
            this.image = imge;
            this.position = this.position;
        }

        public int getItemPosition(Object object) {
            return -2;
        }

        public int getCount() {
            return this.arrayList.size();
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        public Object instantiateItem(ViewGroup container, int position) {
            this.inflater = (LayoutInflater) this.context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View viewLayout = this.inflater.inflate(R.layout.showimge, container, false);
            imageView = (TouchImageView) viewLayout.findViewById(R.id.im_page);
            imageView.setMaxZoom(8.0f);
            path = ((MediaFile) this.arrayList.get(position)).path;
            Glide.with(this.context).load(((MediaFile) this.arrayList.get(position)).path).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(imageView);
            imageView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (!ShowImage.linear_bottom.isShown()) {
                        ShowImage.linear_bottom.setVisibility(0);
                        ShowImage.linear_main.setVisibility(0);
                    } else if (ShowImage.linear_main.isShown()) {
                        ShowImage.linear_bottom.setVisibility(View.GONE);
                        ShowImage.linear_main.setVisibility(8);
                    }
                }
            });
            container.addView(viewLayout);
            return viewLayout;
        }

        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    private class Delete extends AsyncTask<Void, Void, Void> {
        private Delete() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            ShowImage.this.progressDialog = new ProgressDialog(ShowImage.this);
            ShowImage.this.progressDialog.setProgressStyle(0);
            ShowImage.this.progressDialog.setCancelable(false);
            ShowImage.this.progressDialog.setTitle("Delete");
            ShowImage.this.progressDialog.setMessage("Please Wait.....");
            ShowImage.this.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            FileManager.delete(ShowImage.this.getApplicationContext(), new File(((MediaFile) ShowImage.images.get(ShowImage.viewPager.getCurrentItem())).path));
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowImage.images.remove(ShowImage.this.int_position);
            ShowImage.this.adapter.notifyDataSetChanged();
            ShowImage.this.progressDialog.dismiss();
            Toast.makeText(ShowImage.this.getApplicationContext(), "Delete Successfully", Toast.LENGTH_SHORT).show();
        }
    }
}
