package org.vocrama.cleanexpert.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;

import java.util.ArrayList;

import org.vocrama.cleanexpert.photoeffect.ImageProcessing;
import org.vocrama.cleanexpert.photoeffect.ImageProcessingModel;
import org.vocrama.cleanexpert.ui.ProcessingAsyncTask.ProcessingListener;

public class EffectAdapter extends BaseAdapter {
    private Bitmap bitmap;
    private Context context;
    private ArrayList<ImageProcessingModel> listaEfectos = ImageProcessing.imageProcessedList;
    public View mConvertView;
    public ViewHolder mHolder;
    public int p;
    private ProcessingAsyncTask processingAsyncTask;

    static class ViewHolder {
        ImageView imageViewEffect;
        ProgressBar pb;
        TextView textViewEffect;

        ViewHolder() {
        }
    }

    public EffectAdapter(Context context, Bitmap bitmap) {
        this.context = context;
        this.bitmap = bitmap;
    }

    public int getCount() {
        return this.listaEfectos.size();
    }

    public Object getItem(int position) {
        return this.listaEfectos.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public void cancel() {
        this.processingAsyncTask.cancel(true);
    }

    public int getItemViewType(int arg0) {
        return 0;
    }

    @SuppressLint({"InflateParams"})
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        this.p = position;
        final ImageProcessingModel imagenProcesada = (ImageProcessingModel) this.listaEfectos.get(position);
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.effect_view, null);
            convertView.setDrawingCacheEnabled(true);
            holder = new ViewHolder();
            holder.pb = (ProgressBar) convertView.findViewById(R.id.progBar);
            holder.imageViewEffect = (ImageView) convertView.findViewById(R.id.image_effect);
            holder.textViewEffect = (TextView) convertView.findViewById(R.id.name_effect);
            convertView.setTag(holder);
            this.mConvertView = convertView;
        } else {
            convertView.setDrawingCacheEnabled(true);
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imageViewEffect.setImageBitmap(null);
        holder.textViewEffect.setText(imagenProcesada.getProcessName());
        this.processingAsyncTask = new ProcessingAsyncTask(this.context, Bitmap.createScaledBitmap(this.bitmap, 75, 70, true), new ProcessingListener() {
            public void onFinish(Bitmap bm) {
                imagenProcesada.setProcessedBitmap(bm);
                holder.pb.setVisibility(View.GONE);
                holder.imageViewEffect.setImageBitmap(bm);
            }
        });
        this.processingAsyncTask.execute(new Integer[]{Integer.valueOf(imagenProcesada.getProcess())});
        this.mHolder = holder;
        return convertView;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isEmpty() {
        return false;
    }

    public void registerDataSetObserver(DataSetObserver arg0) {
    }

    public void unregisterDataSetObserver(DataSetObserver arg0) {
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int arg0) {
        return true;
    }

    public void updateBitmap(Bitmap bm) {
        this.bitmap = bm;
    }
}
