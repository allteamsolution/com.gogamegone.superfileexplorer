package org.vocrama.cleanexpert.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.os.EnvironmentCompat;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.gogamegone.superfileexplorer.R;
import com.orhanobut.hawk.Hawk;

import org.vocrama.cleanexpert.ui.Constants.database;
import org.vocrama.cleanexpert.ui.CreateDialog.OnReceiveDataListener;
import org.vocrama.cleanexpert.ui.view.Constant;
import org.vocrama.cleanexpert.ui.view.FileItem;
import org.vocrama.cleanexpert.ui.view.OrderType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExternalActivity extends BaseTheme implements OnClickListener {
    private static final int OPEN_DOCUMENT_TREE = 2;
    public static String S;
    public static ActionMode actionMode;
    public static ExternalAdapter adapter;
    public static AlertDialog alertDialog;
    public static List<FileItem> arrayList;

    public static ArrayList<FileItem> selectedFile = new ArrayList();

    public static ArrayList<FileItem> checked = selectedFile;
    public static File copy;
    public static FileManager fileManager;
    public static File folder;
    public static String hide_path = "";
    public static ImageView home;
    public static ImageView img_menuu;
    public static ImageView img_paste_external;
    public static File itempath;
    public static ImageView menu_item;
    public static File moveMulti;
    public static String multiCopy = "";
    public static String multimove = "";
    public static ImageView past;
    public static String path;
    public static String pathCopy = "";
    public static String pathItem;
    public static ProgressDialog progressDialog;
    public static String sdCardPath;
    public static String sdCardPath1;
    public static int selectCount;

    public static TextView title;
    public static Uri treeUri;
    Boolean check_SDCARD = Boolean.valueOf(false);
    File[] externalDirs;
    FloatingActionMenu floatingActionMenu;
    FloatingActionButton floatingFile;
    FloatingActionButton floatingFolder;
    SwipeMenuListView list_in;
    private Callback callback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.external_context_menu, menu);
            MenuItem mi = menu.getItem(0);
            mi.setTitle(mi.getTitle().toString());
            ExternalActivity.actionMode = mode;
            ExternalActivity.selectCount = 0;
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.ii_copy:
                    Constant.copy_flag = false;
                    ExternalActivity.this.copyMultipleItem(mode);
                    Constant.flagForVisibleButton = true;
                    ExternalActivity.past.setVisibility(0);
                    Constant.isExternalPath = true;
                    break;
                case R.id.ii_de:
                    ExternalActivity.this.deleteMultipleItem(mode);
                    break;
                case R.id.ii_move:
                    Constant.move_flag = false;
                    ExternalActivity.this.moveMultipleItem(mode);
                    Constant.flagForVisibleButton = true;
                    ExternalActivity.past.setVisibility(View.VISIBLE);
                    Constant.isExternalPath = true;
                    break;
                case R.id.ii_share:
                    ExternalActivity.this.shareMultipleFolder(mode);
                    break;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            if (ExternalActivity.selectCount > 0) {
                ExternalActivity.this.deSelect();
            }
            ExternalActivity.actionMode = null;
        }
    };

    @RequiresApi(api = 19)
    public static String checkSDCARD(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public static void takeCardUriPermission(final Activity activity) {
        new AlertDialog.Builder(activity).setView(LayoutInflater.from(activity).inflate(R.layout.createdialogsdcard, null)).setPositiveButton((CharSequence) "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterfaceParam, int iParam) {
                ExternalActivity.GetSdCardPermission(activity);
            }
        }).create().show();
    }

    public static void GetSdCardPermission(Activity activity) {
        Intent intent = null;
        if (VERSION.SDK_INT >= 21) {
            intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
        }
        activity.startActivityForResult(intent, 2);
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    @RequiresApi(api = 21)
    private static boolean isRootUri(Uri uri) {
        return DocumentsContract.getTreeDocumentId(uri).endsWith(":");
    }

    @RequiresApi(api = 21)
    private static boolean checkIfSDCardRoot(Uri uri) {
        return isExternalStorageDocument(uri) && isRootUri(uri) && !isInternalStorage(uri);
    }

    @RequiresApi(api = 21)
    public static boolean isInternalStorage(Uri uri) {
        return isExternalStorageDocument(uri) && DocumentsContract.getTreeDocumentId(uri).contains("primary");
    }

    @RequiresApi(api = 19)
    public void onCreate(Bundle savedInstanceState) {
        boolean z;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_external);
        Hawk.init(this).build();


        sdCardPath = StorageHelper.getSdcardPath(this);
        if (sdCardPath != null) {
            z = true;
        } else {
            z = false;
        }
        this.check_SDCARD = Boolean.valueOf(z);
        sdCardPath = checkSDCARD(this);
        if (VERSION.SDK_INT >= 21 && sdCardPath != null) {
            Uri uri = StorageHelper.getTreeUri(this);
            File file = new File(sdCardPath);
            if (uri == null) {
                startActivityForResult(new Intent("android.intent.action.OPEN_DOCUMENT_TREE"), 2);
            } else if (uri.toString().contains(file.getName())) {
                try {
                    getContentResolver().takePersistableUriPermission(uri, 3);
                } catch (Exception e) {
                    startActivityForResult(new Intent("android.intent.action.OPEN_DOCUMENT_TREE"), 2);
                }
            } else {
                Hawk.deleteAll();
                Hawk.destroy();
                try {
                    getContentResolver().takePersistableUriPermission(uri, 3);
                } catch (Exception e2) {
                    startActivityForResult(new Intent("android.intent.action.OPEN_DOCUMENT_TREE"), 2);
                }
            }
        }
        path = Arrays.toString(getExternalStorageDirectories()).replace("[", "").replaceAll("]", "");
        title = (TextView) findViewById(R.id.txt_name_file);
        past = (ImageView) findViewById(R.id.img_paste);
        title.setText("SDcard");
        home = (ImageView) findViewById(R.id.img_home);
        menu_item = (ImageView) findViewById(R.id.img_more);
        if (Constant.flagForVisibleButton) {
            past.setVisibility(0);
        }
        this.list_in = (SwipeMenuListView) findViewById(R.id.list_inn);
        img_menuu = (ImageView) findViewById(R.id.img_menu);
        menu_item = (ImageView) findViewById(R.id.img_more);
        if (Constant.flagForVisibleButton) {
            Toast.makeText(getApplicationContext(), "" + Constant.flagForVisibleButton, 0).show();
            past.setVisibility(0);
        }
        img_menuu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (ExternalActivity.pathItem != null) {
                    String url = ExternalActivity.pathItem.substring(0, ExternalActivity.pathItem.lastIndexOf("/"));
                    ExternalActivity.title.setText(url.substring(url.lastIndexOf("/") + 1));
                    if (ExternalActivity.pathItem.equals(ExternalActivity.path)) {
                        ExternalActivity.this.finish();
                        return;
                    }
                    ExternalActivity.arrayList.clear();
                    ExternalActivity.this.readFile(url, ExternalActivity.this.getApplicationContext());
                    ExternalActivity.pathItem = url;
                } else if (ExternalActivity.pathItem == null) {
                    ExternalActivity.this.finish();
                }
            }
        });
        this.list_in.setMenuCreator(new SwipeMenuCreator() {
            public void create(SwipeMenu menu) {
                SwipeMenuItem info = new SwipeMenuItem(ExternalActivity.this.getApplicationContext());
                info.setBackground((int) R.color.menu1);
                info.setWidth(ExternalActivity.this.dp2px(75));
                info.setIcon((int) R.drawable.ic_info_black_24dp);
                menu.addMenuItem(info);
                SwipeMenuItem copyItem = new SwipeMenuItem(ExternalActivity.this.getApplicationContext());
                copyItem.setBackground((int) R.color.menu3);
                copyItem.setWidth(ExternalActivity.this.dp2px(75));
                copyItem.setIcon((int) R.drawable.ic_content_copy_black_24dp);
                menu.addMenuItem(copyItem);
                SwipeMenuItem reName = new SwipeMenuItem(ExternalActivity.this.getApplicationContext());
                reName.setBackground((int) R.color.menu4);
                reName.setWidth(ExternalActivity.this.dp2px(75));
                reName.setIcon((int) R.drawable.ic_edit_black_24dp);
                menu.addMenuItem(reName);
                SwipeMenuItem openItem = new SwipeMenuItem(ExternalActivity.this.getApplicationContext());
                openItem.setBackground((int) R.color.menu2);
                openItem.setWidth(ExternalActivity.this.dp2px(75));
                openItem.setIcon((int) R.drawable.ic_delete_black_ic);
                menu.addMenuItem(openItem);
            }
        });
        this.list_in.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        ExternalActivity.this.folderInfo(position);
                        break;
                    case 1:
                        ExternalActivity.this.copyFolder(position);
                        Constant.flagForVisibleButton = true;
                        ExternalActivity.past.setVisibility(0);
                        Constant.isExternalPath = true;
                        break;
                    case 2:
                        ExternalActivity.this.renameFile(position);
                        break;
                    case 3:
                        ExternalActivity.this.deleteFolder(position);
                        break;
                }
                return false;
            }
        });
        this.list_in.setSwipeDirection(1);
        fileManager = new FileManager();
        arrayList = new ArrayList();
        adapter = new ExternalAdapter(getApplicationContext(), arrayList);
        this.list_in.setAdapter(adapter);
        this.list_in.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                if (ExternalActivity.selectCount > 0) {
                    if (ExternalActivity.actionMode == null) {
                        ExternalActivity.actionMode = ExternalActivity.this.startActionMode(ExternalActivity.this.callback);
                    }
                    ExternalActivity.this.toggleselection(i);
                    ExternalActivity.actionMode.setTitle(ExternalActivity.selectCount + " " + "Selected");
                    if (ExternalActivity.selectCount == 0) {
                        ExternalActivity.actionMode.finish();
                    }
                } else if (ExternalActivity.selectCount == 0) {
                    ExternalActivity.S = ((FileItem) ExternalActivity.arrayList.get(i)).getPathFile();
                    File file = new File(ExternalActivity.S);
                    if (!((FileItem) ExternalActivity.arrayList.get(i)).isFile()) {
                        ExternalActivity.pathItem = ((FileItem) ExternalActivity.arrayList.get(i)).getPathFile();
                        ExternalActivity.itempath = new File(ExternalActivity.pathItem);
                        ExternalActivity.title.setText(ExternalActivity.pathItem.substring(ExternalActivity.pathItem.lastIndexOf("/") + 1));
                        ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
                    } else if (file.canRead()) {
                        ExternalActivity.pathItem = ((FileItem) ExternalActivity.arrayList.get(i)).getPathFile();
                        ExternalActivity.itempath = new File(ExternalActivity.pathItem);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        String name = ((FileItem) ExternalActivity.arrayList.get(i)).getName();
                        String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1));
                        Uri uri = StorageHelper.getUriFromFile(ExternalActivity.this, ExternalActivity.S);
                        intent.setDataAndType(uri, type);
                        intent.addFlags(1);
                        intent.addFlags(268435456);
                        for (ResolveInfo resolveInfo : ExternalActivity.this.getPackageManager().queryIntentActivities(intent, 65536)) {
                            ExternalActivity.this.grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                        }
                        ExternalActivity.this.startActivity(intent);
                    }
                }
            }
        });
        this.list_in.setOnItemLongClickListener(new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                Constant.all_flag = false;
                if (ExternalActivity.actionMode == null) {
                    ExternalActivity.actionMode = ExternalActivity.this.startActionMode(ExternalActivity.this.callback);
                }
                ExternalActivity.this.toggleselection(position);
                ExternalActivity.actionMode.setTitle(ExternalActivity.selectCount + " " + "Selected");
                if (ExternalActivity.selectCount == 0) {
                    ExternalActivity.actionMode.finish();
                }
                return true;
            }
        });
        folder = new File(path + File.separator + "MySecret007");
        if (!folder.exists()) {
            folder.mkdirs();
        }
        readFile(path, getApplicationContext());
        loadFileItem();
        Log.e(database.APPLIST_PATH, "" + path);
    }

    private void scanFile(File file) {
        MediaScannerConnection.scanFile(getApplicationContext(), new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                ExternalActivity.this.getApplicationContext().getContentResolver().delete(uri, null, null);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == 2 && data != null) {
            Uri treeUri = data.getData();
            if (checkIfSDCardRoot(data.getData())) {
                StorageHelper.saveSdCardInfo(getApplicationContext(), treeUri);
                getApplicationContext().getContentResolver().takePersistableUriPermission(treeUri, 3);
                Toast.makeText(getApplicationContext(), R.string.got_permission_wr_sdcard, Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(getApplicationContext(), "Wrong Folder Selected, Please select root of SD Card", 0).show();
            GetSdCardPermission(this);
        }
    }

    private void readFile(String path, Context applicationContext) {
        arrayList.clear();
        fileManager.readFile(arrayList, path, applicationContext);
        adapter.notifyDataSetChanged();
    }

    private void loadFileItem() {
        this.floatingActionMenu = (FloatingActionMenu) findViewById(R.id.floating_action_menuu);
        this.floatingFolder = (FloatingActionButton) findViewById(R.id.floating_folderr);
        home.setOnClickListener(this);
        past.setOnClickListener(this);
        menu_item.setOnClickListener(this);
        this.floatingFolder.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ExternalActivity.this.createFolder();
            }
        });
    }

    private void createFolder() {
        final CreateDialog createDialog = new CreateDialog(this);
        createDialog.getTitle().setText("New Folder");
        createDialog.setOnReceiveDataListener(new OnReceiveDataListener() {
            public void onClickOk(String nameFolder) {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    if (ExternalActivity.pathItem != null) {
                        StorageHelper.mkdir(ExternalActivity.this, new File(ExternalActivity.pathItem + File.separator + nameFolder));
                        ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
                        Toast.makeText(ExternalActivity.this.getApplicationContext(), "New Folder Create Successfully", 0).show();
                    } else {
                        StorageHelper.mkdir(ExternalActivity.this, new File(ExternalActivity.path, nameFolder));
                        ExternalActivity.this.readFile(ExternalActivity.path, ExternalActivity.this.getApplicationContext());
                        Toast.makeText(ExternalActivity.this.getApplicationContext(), "New Folder Create Successfully", 0).show();
                    }
                    ExternalActivity.this.floatingActionMenu.close(false);
                    createDialog.dismiss();
                }
            }

            public void onClickCancel() {
                ExternalActivity.this.floatingActionMenu.close(false);
                createDialog.dismiss();
            }
        });
        createDialog.show();
    }

    private void createFile() {
        final CreateDialog createDialog = new CreateDialog(this);
        createDialog.getTitle().setText("New File");
        createDialog.setOnReceiveDataListener(new OnReceiveDataListener() {
            public void onClickOk(String nameFolder) {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    if (ExternalActivity.pathItem != null) {
                        try {
                            File file = new File(ExternalActivity.pathItem, nameFolder);
                            if (!file.exists()) {
                                file.createNewFile();
                                Toast.makeText(ExternalActivity.this.getApplicationContext(), "New File Create Successfully", 0).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
                    } else {
                        File mediaStorageDir = new File(ExternalActivity.path, nameFolder);
                        if (!mediaStorageDir.exists()) {
                            try {
                                mediaStorageDir.createNewFile();
                                Toast.makeText(ExternalActivity.this.getApplicationContext(), "New File Create Successfully", 0).show();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                            ExternalActivity.this.readFile(ExternalActivity.path, ExternalActivity.this.getApplicationContext());
                        }
                    }
                    ExternalActivity.this.floatingActionMenu.close(false);
                    createDialog.dismiss();
                }
            }

            public void onClickCancel() {
                ExternalActivity.this.floatingActionMenu.close(false);
                createDialog.dismiss();
            }
        });
        createDialog.show();
    }

    private void notifyMediaStoreScanner(File file) throws FileNotFoundException {
        Media.insertImage(getApplicationContext().getContentResolver(), file.getAbsolutePath(), file.getName(), null);
        getApplicationContext().sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_home:
                arrayList.clear();
                readFile(path, getApplicationContext());
                if (actionMode != null) {
                    actionMode.finish();
                    return;
                }
                return;
            case R.id.img_more:
                PopupMenu menu = new PopupMenu(this, menu_item);
                menu.getMenuInflater().inflate(R.menu.more_menu_popupp, menu.getMenu());
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.exitt:
                                ExternalActivity.this.finish();
                                break;
                            case R.id.menu_sortt:
                                ExternalActivity.this.SortBy();
                                break;
                        }
                        return true;
                    }
                });
                menu.show();
                return;
            case R.id.img_paste:
                past.setVisibility(4);
                Constant.flagForVisibleButton = false;
                if (Constant.isInternalPath) {
                    int i;
                    if (!Constant.copy_flag) {
                        Constant.copy_flag = true;
                        i = 0;
                        while (i < InternalActivity.checked.size()) {
                            try {
                                multiCopy = ((FileItem) InternalActivity.selectedFile.get(i)).getPathFile();
                                if (pathItem == null) {
                                    pathItem = sdCardPath;
                                }
                                if (new File(multiCopy).isDirectory()) {
                                    StorageHelper.copyDirectory(getApplicationContext(), new File(multiCopy), new File(pathItem));
                                    Toast.makeText(this, "copy Successfully..", 0).show();
                                } else {
                                    StorageHelper.copyFile(this, new File(multiCopy), new File(pathItem));
                                    Toast.makeText(this, "copy Successfully..", 0).show();
                                }
                                i++;
                            } catch (Exception e) {
                            }
                        }
                        InternalActivity.selectedFile.clear();
                        readFile(pathItem, getApplicationContext());
                    } else if (Constant.move_flag) {
                        try {
                            if (pathItem == null) {
                                pathItem = sdCardPath;
                            }
                            if (new File(InternalActivity.pathCopy).isDirectory()) {
                                StorageHelper.copyDirectory(getApplicationContext(), new File(InternalActivity.pathCopy), new File(pathItem));
                                Toast.makeText(this, "copy Successfully..", 0).show();
                            } else {
                                StorageHelper.copyFile(this, new File(InternalActivity.pathCopy), new File(pathItem));
                                Toast.makeText(this, "copy Successfully..", 0).show();
                            }
                            readFile(pathItem, getApplicationContext());
                        } catch (Exception e2) {
                        }
                    } else {
                        Constant.move_flag = true;
                        i = 0;
                        while (i < InternalActivity.checked.size()) {
                            try {
                                multimove = ((FileItem) InternalActivity.selectedFile.get(i)).getPathFile();
                                if (pathItem == null) {
                                    pathItem = sdCardPath;
                                }
                                if (new File(multimove).isDirectory()) {
                                    StorageHelper.moveDirectory(getApplicationContext(), new File(multimove), new File(pathItem));
                                    Toast.makeText(this, "Move Successfully..", 0).show();
                                } else {
                                    StorageHelper.copyFile(this, new File(multimove), new File(pathItem));
                                    StorageHelper.deleteFile(this, new File(multimove));
                                    Toast.makeText(this, "Move Successfully..", 0).show();
                                }
                                i++;
                            } catch (Exception e3) {
                            }
                        }
                        InternalActivity.selectedFile.clear();
                        readFile(pathItem, getApplicationContext());
                    }
                    Constant.isInternalPath = false;
                    return;
                } else if (!Constant.copy_flag) {
                    Constant.copy_flag = true;
                    new CopyTask().execute(new Void[0]);
                    return;
                } else if (Constant.move_flag) {
                    new SingleCopy().execute(new Void[0]);
                    return;
                } else {
                    Constant.move_flag = true;
                    new MoveTask().execute(new Void[0]);
                    return;
                }
            default:
                return;
        }
    }

    private void SortBy() {
        CharSequence[] values = new CharSequence[]{" Name ", " Date ", " Size "};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Sort By");
        builder.setItems(values, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new SortTask(which).execute(new Void[0]);
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteFolder(final int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Delete Folder");
        builder.setMessage((CharSequence) "Are You Sure Want To Delete ?");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String path = ((FileItem) ExternalActivity.arrayList.get(i)).getPathFile();
                File f = new File(path);
                Toast.makeText(ExternalActivity.this.getApplicationContext(), "Deleted Successfully", 0).show();
                ExternalActivity.pathItem = path.substring(0, path.lastIndexOf("/"));
                StorageHelper.deleteFile(ExternalActivity.this, f);
                ExternalActivity.arrayList.clear();
                ExternalActivity.fileManager.readFile(ExternalActivity.arrayList, path, getApplicationContext());
                ExternalActivity.adapter.notifyDataSetChanged();
                alertDialog.dismiss();
            }
        });
        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void renameFile(final int i) {
        final CreateDialog createDialog = new CreateDialog(this);
        createDialog.getTitle().setText("ReName");
        createDialog.editText_title.setText(((FileItem) arrayList.get(i)).getName());
        createDialog.setOnReceiveDataListener(new OnReceiveDataListener() {
            @RequiresApi(api = 19)
            public void onClickOk(String nameFolder) {
                ArrayList<String> UpdateFile = new ArrayList();
                ArrayList<String> UpdateDir = new ArrayList();
                String path = ((FileItem) ExternalActivity.arrayList.get(i)).getPathFile();
                File f = new File(path);
                UpdateDir.add(f.getAbsolutePath());
                UpdateFile.add(f.getAbsolutePath());
                if (ExternalActivity.pathItem == null) {
                    try {
                        StorageHelper.renameDir(ExternalActivity.this.getApplicationContext(), f, nameFolder);
                        UpdateFile.add(f.getParent() + "/" + nameFolder);
                        createDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (f.isDirectory()) {
                    StorageHelper.renameDir(ExternalActivity.this.getApplicationContext(), f, nameFolder);
                    String s2 = f.getParent() + "/" + nameFolder;
                    ExternalActivity.arrayList.clear();
                    ExternalActivity.fileManager.readFile(ExternalActivity.arrayList, path, getApplicationContext());
                    ExternalActivity.adapter.notifyDataSetChanged();
                    createDialog.dismiss();
                    UpdateDir.add(s2);
                } else {
                    try {
                        StorageHelper.renameDir(ExternalActivity.this.getApplicationContext(), f, nameFolder);
                        String s1 = f.getParent() + "/" + nameFolder;
                        ExternalActivity.arrayList.clear();
                        ExternalActivity.fileManager.readFile(ExternalActivity.arrayList, path, getApplicationContext());
                        ExternalActivity.adapter.notifyDataSetChanged();
                        createDialog.dismiss();
                        UpdateFile.add(s1);
                        Toast.makeText(ExternalActivity.this.getApplicationContext(), "ReName Successfully", Toast.LENGTH_SHORT).show();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                String[] folder = (String[]) UpdateDir.toArray(new String[0]);
                MediaScannerConnection.scanFile(ExternalActivity.this.getApplicationContext(), (String[]) UpdateFile.toArray(new String[0]), null, new OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
                MediaScannerConnection.scanFile(ExternalActivity.this.getApplicationContext(), folder, null, new OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                    }
                });
            }

            public void onClickCancel() {
                ExternalActivity.this.floatingActionMenu.close(false);
                createDialog.dismiss();
            }
        });
        createDialog.show();
    }

    private void refreshFile(File file) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }

    private void copyFolder(int i) {
        pathCopy = ((FileItem) arrayList.get(i)).getPathFile();
    }

    private void folderInfo(int position) {
        FileItem fileItem = arrayList.get(position);
        InfoDialog.getInstance(fileItem).show(getSupportFragmentManager(), null);
    }

    private void shareMultipleFolder(ActionMode mode) {
        ArrayList<FileItem> checked = selectedFile;
        ArrayList<Uri> share = new ArrayList();
        for (int i = 0; i < checked.size(); i++) {
            if (((FileItem) selectedFile.get(i)).isSelected) {
                String d_path = ((FileItem) selectedFile.get(i)).getPathFile();
                File file = new File(d_path);
                share.add(StorageHelper.getUriFromFile(this, d_path));
            }
        }
        Intent i2 = new Intent();
        i2.setAction("android.intent.action.SEND_MULTIPLE");
        i2.setType("*/*");
        i2.putParcelableArrayListExtra("android.intent.extra.STREAM", share);
        i2.addFlags(1);
        startActivity(i2);
        mode.finish();
    }

    private void moveMultipleItem(ActionMode mode) {
        ArrayList<FileItem> checked = selectedFile;
        for (int i = 0; i < checked.size(); i++) {
            if (((FileItem) selectedFile.get(i)).isSelected) {
                multimove = ((FileItem) selectedFile.get(i)).getPathFile();
            }
        }
        mode.finish();
    }

    private void copyMultipleItem(ActionMode mode) {
        ArrayList<FileItem> checked = selectedFile;
        for (int i = 0; i < checked.size(); i++) {
            if (((FileItem) selectedFile.get(i)).isSelected) {
                multiCopy = ((FileItem) selectedFile.get(i)).getPathFile();
            }
        }
        mode.finish();
    }

    private void deleteMultipleItem(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Delete Folder");
        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new DeleteTask().execute(new Void[0]);
                alertDialog.dismiss();
                mode.finish();
            }
        });
        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
                mode.finish();
            }
        });
    }

    private void deSelect() {
        for (int i = 0; i < arrayList.size(); i++) {
            ((FileItem) arrayList.get(i)).isSelected = false;
        }
        selectCount = 0;
        adapter.notifyDataSetChanged();
    }

    private void toggleselection(int position) {
        ((FileItem) arrayList.get(position)).isSelected = !((FileItem) arrayList.get(position)).isSelected;
        if (((FileItem) arrayList.get(position)).isSelected) {
            selectedFile.add(arrayList.get(position));
            selectCount++;
        } else {
            selectedFile.add(arrayList.get(position));
            selectCount--;
        }
        adapter.notifyDataSetChanged();
    }

    public void onBackPressed() {
        if (pathItem != null) {
            String url = pathItem.substring(0, pathItem.lastIndexOf("/"));
            title.setText(url.substring(url.lastIndexOf("/") + 1));
            if (pathItem.equals(path)) {
                super.onBackPressed();
                return;
            }
            arrayList.clear();
            readFile(url, getApplicationContext());
            pathItem = url;
        } else if (pathItem == null) {
            super.onBackPressed();
        }
    }

    public String[] getExternalStorageDirectories() {
        int i;
        List<String> results = new ArrayList();
        if (VERSION.SDK_INT >= 19) {
            try {
                this.externalDirs = getExternalFilesDirs(null);
            } catch (Exception e) {
            }
            for (File file : this.externalDirs) {
                boolean addPath;
                String path = file.getPath().split("/Android")[0];
                if (VERSION.SDK_INT >= 21) {
                    addPath = Environment.isExternalStorageRemovable(file);
                } else {
                    addPath = "mounted".equals(EnvironmentCompat.getStorageState(file));
                }
                if (addPath) {
                    results.add(path);
                }
            }
        }
        if (results.isEmpty()) {
            String output = "";
            try {
                Process process = new ProcessBuilder(new String[0]).command(new String[]{"mount | grep /dev/block/vold"}).redirectErrorStream(true).start();
                process.waitFor();
                InputStream is = process.getInputStream();
                byte[] buffer = new byte[1024];
                while (is.read(buffer) != -1) {
                    output = output + new String(buffer);
                }
                is.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (!output.trim().isEmpty()) {
                for (String voldPoint : output.split("\n")) {
                    results.add(voldPoint.split(" ")[2]);
                }
            }
        }
        int i2;
        if (VERSION.SDK_INT >= 23) {
            i = 0;
            while (i < results.size()) {
                if (!((String) results.get(i)).toLowerCase().matches(".*[0-9a-f]{4}[-][0-9a-f]{4}")) {
                    Log.d("nnnnnnn", ((String) results.get(i)) + " might not be extSDcard");
                    i2 = i - 1;
                    results.remove(i);
                    i = i2;
                }
                i++;
            }
        } else {
            i = 0;
            while (i < results.size()) {
                if (!(((String) results.get(i)).toLowerCase().contains("ext") || ((String) results.get(i)).toLowerCase().contains("sdcard"))) {
                    Log.d("mmmmmmm", ((String) results.get(i)) + " might not be extSDcard");
                    i2 = i - 1;
                    results.remove(i);
                    i = i2;
                }
                i++;
            }
        }
        String[] storageDirectories = new String[results.size()];
        for (i = 0; i < results.size(); i++) {
            storageDirectories[i] = (String) results.get(i);
        }
        return storageDirectories;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(1, (float) dp, getResources().getDisplayMetrics());
    }

    public static class ExternalAdapter extends BaseAdapter {
        List<FileItem> arrayList;
        Context context;
        ViewHolder viewHolder;

        public ExternalAdapter(Context context, List<FileItem> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        public int getCount() {
            return this.arrayList.size();
        }

        public FileItem getItem(int position) {
            return (FileItem) this.arrayList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View view, ViewGroup parent) {
            this.viewHolder = new ViewHolder();
            if (view == null) {
                view = LayoutInflater.from(this.context).inflate(R.layout.activity_external_adapter, parent, false);
                this.viewHolder.imgFolder = (ImageView) view.findViewById(R.id.img_folderr);
                this.viewHolder.txtNameFile = (TextView) view.findViewById(R.id.txt_name_folderr);
                this.viewHolder.txtDate = (TextView) view.findViewById(R.id.txt_datee);
                this.viewHolder.imgCreate = (ImageView) view.findViewById(R.id.img_createe);
                this.viewHolder.frameLayout = (FrameLayout) view.findViewById(R.id.fl_vieww);
                view.setTag(this.viewHolder);
            } else {
                this.viewHolder = (ViewHolder) view.getTag();
            }
            FileItem fileItem = getItem(position);
            if (fileItem.isSelected) {
                view.setBackgroundResource(R.color.multiple_image_select_albumTextBackground);
            } else {
                view.setBackgroundResource(R.color.white);
            }
            try {
                if (fileItem.isDirectory()) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.ic_folder_black_24dp);
                } else if (fileItem.getName().endsWith(".txt")) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.im_txt);
                } else if (fileItem.getName().endsWith(".apk")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_apk).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".pdf")) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.im_pdf);
                } else if (fileItem.getName().endsWith(".doc")) {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.im_doc);
                } else if (fileItem.getName().endsWith(".jpg")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_jpg).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".jpeg")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_png).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".png")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_png).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".mp3")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_mp3).centerCrop().into(this.viewHolder.imgFolder);
                } else if (fileItem.getName().endsWith(".mp4")) {
                    Glide.with(this.context).load(new File(fileItem.getPathFile())).placeholder((int) R.drawable.im_mp4).centerCrop().into(this.viewHolder.imgFolder);
                } else {
                    this.viewHolder.imgFolder.setImageResource(R.drawable.ic_insert_drive_file_black_24dp);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.viewHolder.txtNameFile.setText(fileItem.getName());
            this.viewHolder.txtDate.setText(fileItem.getDate());
            return view;
        }

        public static class ViewHolder {
            FrameLayout frameLayout;
            ImageView imgCreate;
            ImageView imgFolder;
            TextView txtDate;
            TextView txtNameFile;
        }
    }

    public class CopyTask extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            ExternalActivity.progressDialog = new ProgressDialog(ExternalActivity.this);
            ExternalActivity.progressDialog.setProgressStyle(0);
            ExternalActivity.progressDialog.setTitle("Copy");
            ExternalActivity.progressDialog.setCancelable(false);
            ExternalActivity.progressDialog.setMessage("Please Wait.....");
            ExternalActivity.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            ArrayList<FileItem> checked = ExternalActivity.selectedFile;
            for (int i = 0; i < checked.size(); i++) {
                ExternalActivity.multiCopy = ((FileItem) ExternalActivity.selectedFile.get(i)).getPathFile();
                if (new File(ExternalActivity.multiCopy).isDirectory()) {
                    try {
                        if (ExternalActivity.multiCopy == ExternalActivity.pathItem) {
                            Toast.makeText(ExternalActivity.this, "data..", 0).show();
                        } else {
                            try {
                                StorageHelper.copyDirectory(ExternalActivity.this.getApplicationContext(), new File(ExternalActivity.multiCopy), new File(ExternalActivity.pathItem));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e2) {
                    }
                } else {
                    StorageHelper.copyFile(ExternalActivity.this, new File(ExternalActivity.multiCopy), new File(ExternalActivity.pathItem));
                }
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ExternalActivity.past.setVisibility(4);
            try {
                if (ExternalActivity.multiCopy == ExternalActivity.pathItem) {
                    Toast.makeText(ExternalActivity.this, "same directory data are not paste..", 0).show();
                } else {
                    Toast.makeText(ExternalActivity.this.getApplicationContext(), "Copy Successfully", 0).show();
                }
            } catch (Exception e) {
            }
            ExternalActivity.progressDialog.dismiss();
            ExternalActivity.selectedFile.clear();
            ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
        }
    }

    private class DeleteTask extends AsyncTask<Void, Void, Void> {
        private DeleteTask() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            ExternalActivity.progressDialog = new ProgressDialog(ExternalActivity.this);
            ExternalActivity.progressDialog.setProgressStyle(0);
            ExternalActivity.progressDialog.setCancelable(false);
            ExternalActivity.progressDialog.setTitle("Delete");
            ExternalActivity.progressDialog.setMessage("Please Wait.....");
            ExternalActivity.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            ArrayList<FileItem> checkedItems = ExternalActivity.selectedFile;
            for (int i = 0; i < checkedItems.size(); i++) {
                StorageHelper.deleteFile(ExternalActivity.this, new File(((FileItem) ExternalActivity.selectedFile.get(i)).getPathFile()));
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (ExternalActivity.pathItem != null) {
                ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
            } else {
                ExternalActivity.this.readFile(ExternalActivity.path, ExternalActivity.this.getApplicationContext());
            }
            ExternalActivity.selectedFile.clear();
            Toast.makeText(ExternalActivity.this.getApplicationContext(), "Delete Successfully", 0).show();
            ExternalActivity.progressDialog.dismiss();
        }
    }

    public class MoveTask extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            ExternalActivity.progressDialog = new ProgressDialog(ExternalActivity.this);
            ExternalActivity.progressDialog.setProgressStyle(0);
            ExternalActivity.progressDialog.setCancelable(false);
            ExternalActivity.progressDialog.setTitle("Move");
            ExternalActivity.progressDialog.setMessage("Please Wait.....");
            ExternalActivity.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            for (int i = 0; i < ExternalActivity.checked.size(); i++) {
                ExternalActivity.multimove = ((FileItem) ExternalActivity.selectedFile.get(i)).getPathFile();
                ExternalActivity.moveMulti = new File(ExternalActivity.multimove);
                if (new File(ExternalActivity.multimove).isDirectory()) {
                    try {
                        if (ExternalActivity.multimove == ExternalActivity.pathItem) {
                            Toast.makeText(ExternalActivity.this, "data..", 0).show();
                        } else {
                            try {
                                StorageHelper.moveDirectory(ExternalActivity.this.getApplicationContext(), new File(ExternalActivity.multimove), new File(ExternalActivity.pathItem));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e2) {
                    }
                } else {
                    StorageHelper.moveFile(ExternalActivity.this, new File(ExternalActivity.multimove), new File(ExternalActivity.pathItem));
                    StorageHelper.deleteFile(ExternalActivity.this, ExternalActivity.moveMulti);
                }
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (ExternalActivity.multimove == ExternalActivity.pathItem) {
                    Toast.makeText(ExternalActivity.this, "same directory data are not paste..", 0).show();
                } else {
                    Toast.makeText(ExternalActivity.this.getApplicationContext(), "Move Successfully", 0).show();
                }
            } catch (Exception e) {
            }
            ExternalActivity.selectedFile.clear();
            ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
            ExternalActivity.progressDialog.dismiss();
        }
    }

    public class SingleCopy extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            ExternalActivity.progressDialog = new ProgressDialog(ExternalActivity.this);
            ExternalActivity.progressDialog.setProgressStyle(0);
            ExternalActivity.progressDialog.setCancelable(false);
            ExternalActivity.progressDialog.setTitle("Copy");
            ExternalActivity.progressDialog.setMessage("Please Wait.....");
            ExternalActivity.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            if (new File(ExternalActivity.pathCopy).isDirectory()) {
                try {
                    if (ExternalActivity.pathCopy == ExternalActivity.pathItem) {
                        Toast.makeText(ExternalActivity.this, "data..", 0).show();
                    } else {
                        try {
                            StorageHelper.copyDirectory(ExternalActivity.this.getApplicationContext(), new File(ExternalActivity.pathCopy), new File(ExternalActivity.pathItem));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e2) {
                }
            } else {
                StorageHelper.copyFile(ExternalActivity.this, new File(ExternalActivity.pathCopy), new File(ExternalActivity.pathItem));
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
            ExternalActivity.progressDialog.dismiss();
            try {
                if (ExternalActivity.pathCopy == ExternalActivity.pathItem) {
                    Toast.makeText(ExternalActivity.this, "same directory data are not paste..", 0).show();
                } else {
                    Toast.makeText(ExternalActivity.this.getApplicationContext(), "Copy Successfully", 0).show();
                }
            } catch (Exception e) {
            }
        }
    }

    private class SortTask extends AsyncTask<Void, Void, Void> {
        int whi;

        public SortTask(int which) {
            this.whi = which;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            ExternalActivity.progressDialog = new ProgressDialog(ExternalActivity.this);
            ExternalActivity.progressDialog.setProgressStyle(0);
            ExternalActivity.progressDialog.setCancelable(false);
            ExternalActivity.progressDialog.setTitle("Sorting");
            ExternalActivity.progressDialog.setMessage("Please Wait.....");
            ExternalActivity.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            OrderType orderType = OrderType.values()[this.whi];
            Constant.getInstance(ExternalActivity.this.getApplicationContext()).setOrderType(ExternalActivity.this.getApplicationContext(), orderType);
            if (ExternalActivity.pathItem == null) {
                switch (orderType) {
                    case NAME:
                        Collections.sort(ExternalActivity.arrayList, OrderType.NAME.getComparator());
                        break;
                    case DATE:
                        Collections.sort(ExternalActivity.arrayList, OrderType.DATE.getComparator());
                        break;
                    case SIZE:
                        Collections.sort(ExternalActivity.arrayList, OrderType.SIZE.getComparator());
                        break;
                }
            }
            switch (orderType) {
                case NAME:
                    Collections.sort(ExternalActivity.arrayList, OrderType.NAME.getComparator());
                    break;
                case DATE:
                    Collections.sort(ExternalActivity.arrayList, OrderType.DATE.getComparator());
                    break;
                case SIZE:
                    Collections.sort(ExternalActivity.arrayList, OrderType.SIZE.getComparator());
                    break;
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (ExternalActivity.pathItem != null) {
                ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
            } else {
                ExternalActivity.this.readFile(ExternalActivity.path, ExternalActivity.this.getApplicationContext());
            }
            Toast.makeText(ExternalActivity.this.getApplicationContext(), "Sorting Successfully", 0).show();
            ExternalActivity.progressDialog.dismiss();
        }
    }

    private class ZipTask extends AsyncTask<Void, Void, Void> {
        String f_Name;

        public ZipTask(String nameFolder) {
            this.f_Name = nameFolder;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            ExternalActivity.progressDialog = new ProgressDialog(ExternalActivity.this);
            ExternalActivity.progressDialog.setProgressStyle(0);
            ExternalActivity.progressDialog.setCancelable(false);
            ExternalActivity.progressDialog.setTitle("Zip");
            ExternalActivity.progressDialog.setMessage("Please Wait.....");
            ExternalActivity.progressDialog.show();
        }

        protected Void doInBackground(Void... params) {
            ArrayList<FileItem> checkedItems;
            int i;
            String d_path;
            if (ExternalActivity.pathItem != null) {
                checkedItems = ExternalActivity.selectedFile;
                for (i = 0; i < checkedItems.size(); i++) {
                    d_path = ((FileItem) ExternalActivity.selectedFile.get(i)).getPathFile();
                    new Compress(new String[]{d_path}, ExternalActivity.pathItem + this.f_Name).zip();
                }
            } else {
                checkedItems = ExternalActivity.selectedFile;
                for (i = 0; i < checkedItems.size(); i++) {
                    d_path = ((FileItem) ExternalActivity.selectedFile.get(i)).getPathFile();
                    new Compress(new String[]{d_path}, ExternalActivity.path + this.f_Name).zip();
                }
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (ExternalActivity.pathItem != null) {
                ExternalActivity.this.readFile(ExternalActivity.pathItem, ExternalActivity.this.getApplicationContext());
            } else {
                ExternalActivity.this.readFile(ExternalActivity.path, ExternalActivity.this.getApplicationContext());
            }
            Toast.makeText(ExternalActivity.this.getApplicationContext(), "Zip Successfully", 0).show();
            ExternalActivity.selectedFile.clear();
            ExternalActivity.progressDialog.dismiss();
        }
    }


}
