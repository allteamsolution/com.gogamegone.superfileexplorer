package org.vocrama.cleanexpert.ui.presenter;

import android.os.Environment;

import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.ui.FileManager;
import org.vocrama.cleanexpert.ui.presenter.IPresenter.IAsyncFragmentPresenter;
import org.vocrama.cleanexpert.ui.presenter.IPresenter.IView;
import org.vocrama.cleanexpert.ui.view.FileItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ZipPresenter implements IAsyncFragmentPresenter<FileItem> {

    private IView<FileItem> view;
    private List<FileItem> items;

    @Override
    public void onCreate() {
        items = new ArrayList<>();
        view.showProgress();
    }

    @Override
    public void getDataAsync() {
        thread.start();
    }

    @Override
    public void setView(IView<FileItem> view) {
        this.view = view;
    }

    @Override
    public void onDestroy() {
        thread.interrupt();
    }

    private Thread thread = new Thread(() -> {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        getZipDocuments(file, items);
        App.getUIHandler().post(() -> {
            view.onDetData(items);
            view.hideProgress();
        });
    });

    private void getZipDocuments(File dir, List<FileItem> fileItems) {
        File[] listFile = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (File file : listFile) {
                if (file.isDirectory()) {
                    getZipDocuments(file, fileItems);
                } else {
                    if (file.getName().endsWith(".zip")
                            || file.getName().endsWith(".rar")
                            || file.getName().endsWith(".7z")
                            || file.getName().endsWith(".tar")) {
                        fileItems.add(FileManager.getFileItem(file));
                    }
                }
            }
        }
    }
}
