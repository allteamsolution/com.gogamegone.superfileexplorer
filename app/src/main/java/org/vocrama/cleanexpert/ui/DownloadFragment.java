package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.view.FileItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DownloadFragment extends Fragment {

    private boolean isThreadStarted;
    private CompositeDisposable compositeDisposable;
    private FileManager.OnFileLoadingCallback listener;

    public static DownloadFragment getInstance() {
        return new DownloadFragment();
    }

    public DownloadFragment() {

    }

    public static String d_path;
    public static File myFile;
    public static String name;
    public static Long size;
    public TextView txtNameDetail_video;
    public TextView txtPathDetail_video;
    public TextView txtSizeDetail_video;
    int CountSelected;
    ActionMode actionMode;
    RecyclerView.Adapter adapter;
    ArrayList<FileItem> down = new ArrayList<>();
    FileItem down_movie;
    File download;
    RecyclerView grid_down;
    ProgressDialog progressDialog;
    String s;
    ArrayList<FileItem> selectedImage = new ArrayList<>();

    Callback callback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.down_context_menu, menu);
            MenuItem mi = menu.getItem(0);
            mi.setTitle(mi.getTitle().toString());
            DownloadFragment.this.actionMode = mode;
            DownloadFragment.this.CountSelected = 0;
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.d_delete:
                    DownloadFragment.this.multipleDelete(mode);
                    break;
                case R.id.d_share:
                    DownloadFragment.this.multipleShare(mode);
                    break;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            if (DownloadFragment.this.CountSelected > 0) {
                DownloadFragment.this.deselectAll();
            }
            DownloadFragment.this.actionMode = null;
        }
    };

    @RequiresApi(api = 19)
    public static String checkSDCARD(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_downloads, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        compositeDisposable = new CompositeDisposable();
        listener = (FileManager.OnFileLoadingCallback) requireActivity();

        this.grid_down = (RecyclerView) view.findViewById(R.id.grid_down);
        this.download = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
        this.down = new ArrayList<>();
        this.grid_down.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        this.adapter = getAdapter();
        this.grid_down.setAdapter(this.adapter);
        registerForContextMenu(this.grid_down);
        try {
            LayoutParams p = new LayoutParams(-1, -1);
            p.weight = 6.0f;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDownload(download, down);
    }

    private void toggleSelection(int position) {
        ((FileItem) this.down.get(position)).isSelected = !((FileItem) this.down.get(position)).isSelected;
        if (((FileItem) this.down.get(position)).isSelected) {
            this.selectedImage.add(this.down.get(position));
            this.CountSelected++;
        } else {
            this.selectedImage.remove(this.down.get(position));
            this.CountSelected--;
        }
        this.adapter.notifyDataSetChanged();
    }

    private void multipleShare(ActionMode mode) {
        ArrayList<FileItem> checkedItems = this.selectedImage;
        ArrayList<Uri> share = new ArrayList();
        for (int i = 0; i < checkedItems.size(); i++) {
            if (((FileItem) this.selectedImage.get(i)).isSelected) {
                Uri uri;
                d_path = ((FileItem) this.selectedImage.get(i)).getPathFile();
                File file = new File(d_path);
                if (VERSION.SDK_INT >= 24) {
                    uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
                } else {
                    uri = Uri.fromFile(file);
                }
                share.add(uri);
            }
        }
        Intent i2 = new Intent();
        i2.setAction("android.intent.action.SEND_MULTIPLE");
        if (d_path.endsWith(".txt")) {
            i2.setType("text/plain");
        } else if (d_path.endsWith(".doc")) {
            i2.setType("application/msword");
        } else if (d_path.endsWith(".apk")) {
            i2.setType("application/vnd.android.package-archive");
        } else if (d_path.endsWith(".pdf")) {
            i2.setType("application/pdf");
        } else if (d_path.endsWith(".png")) {
            i2.setType("image/*");
        } else if (d_path.endsWith(".jpg")) {
            i2.setType("image/*");
        } else if (d_path.endsWith(".jpeg")) {
            i2.setType("image/*");
            i2.setType("image/*");
        } else if (d_path.endsWith(".mp4")) {
            i2.setType("video/*");
        } else if (d_path.endsWith(".3gp")) {
            i2.setType("video/*");
        } else if (d_path.endsWith(".mp3")) {
            i2.setType("audio/*");
        } else if (d_path.endsWith(".zip")) {
            i2.setType("zip/*");
        } else if (d_path.endsWith(".rar")) {
            i2.setType("rar/*");
        }
        i2.putParcelableArrayListExtra("android.intent.extra.STREAM", share);
        i2.addFlags(1);
        startActivity(Intent.createChooser(i2, ""));
        this.adapter.notifyDataSetChanged();
        mode.finish();
    }

    private void multipleDelete(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
        builder.setTitle((CharSequence) "Delete File");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(v -> {
            new DeleteTask().execute(new Void[0]);
            alertDialog.dismiss();
            mode.finish();
        });
        alertDialog.getButton(-2).setOnClickListener(v -> {
            alertDialog.dismiss();
            mode.finish();
        });
    }

    private void deselectAll() {
        int l = this.down.size();
        for (int i = 0; i < l; i++) {
            ((FileItem) this.down.get(i)).isSelected = false;
        }
        this.CountSelected = 0;
        this.adapter.notifyDataSetChanged();
    }

    private void loadDownload(File download, List<FileItem> items) {

        if (isThreadStarted)
            return;
        if (down.isEmpty()) {
            compositeDisposable.add(
                    Observable
                            .just(download)
                            .flatMap(file -> Observable.fromArray(file.listFiles()))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnSubscribe(disposable -> {
                                listener.onFileLoading();
                                isThreadStarted = true;
                            })
                            .doOnNext(file -> {
                                if (file.isDirectory()) {
                                    loadDownload(file, down);
                                } else {
                                    if (file.getName().endsWith(".jpg")
                                            || file.getName().endsWith(".png")
                                            || file.getName().endsWith(".mp4")
                                            || file.getName().endsWith(".mp3")
                                            || file.getName().endsWith(".mkv")
                                            || file.getName().endsWith(".txt")
                                            || file.getName().endsWith(".apk")
                                            || file.getName().endsWith(".pdf")
                                            || file.getName().endsWith(".doc")
                                    ) {
                                        down.add(FileManager.getFileItem(file));
                                    }
                                }
                            })
                            .doOnComplete(() -> {
                                listener.onFileLoaded();
                                isThreadStarted = false;
                                adapter.notifyDataSetChanged();
                            })
                            .doOnError(throwable -> {
                                listener.onFileLoaded();
                                isThreadStarted = false;
                            })
                            .subscribe());
        }
    }

    private RecyclerView.Adapter getAdapter() {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        adapter = new RecyclerView.Adapter() {
            @RequiresApi(api = 16)
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new MyViewHolder(inflater.inflate(R.layout.adapter_down, parent, false));
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                MyViewHolder myHolder = (MyViewHolder) holder;
                FileItem mediaFile = (FileItem) DownloadFragment.this.down.get(position);
                myHolder.textView_down.setText(((FileItem) DownloadFragment.this.down.get(position)).getName());
                if (((FileItem) DownloadFragment.this.down.get(position)).isSelected) {
                    myHolder.cardview.setBackgroundResource(R.color.actionbar_grey);
                } else {
                    myHolder.cardview.setBackgroundResource(R.color.white);
                }
                if (mediaFile.isSelected) {
                    myHolder.view_down.setAlpha(0.5f);
                    myHolder.frameLayout_down.setForeground(ContextCompat.getDrawable(DownloadFragment.this.getContext(), R.drawable.ic_check_box_black_24dp));
                } else {
                    myHolder.view_down.setAlpha(0.0f);
                    myHolder.frameLayout_down.setForeground(null);
                }
                DownloadFragment.this.down_movie = (FileItem) DownloadFragment.this.down.get(position);
                DownloadFragment.myFile = new File(DownloadFragment.this.down_movie.getPathFile());
                try {
                    if (((FileItem) DownloadFragment.this.down.get(position)).getName().endsWith(".txt")) {
                        myHolder.down_image.setImageResource(R.drawable.im_txt);
                    } else if (((FileItem) DownloadFragment.this.down.get(position)).getName().endsWith(".pdf")) {
                        myHolder.down_image.setImageResource(R.drawable.im_pdf);
                    } else if (((FileItem) DownloadFragment.this.down.get(position)).getName().endsWith(".doc")) {
                        myHolder.down_image.setImageResource(R.drawable.im_doc);
                    } else if (((FileItem) DownloadFragment.this.down.get(position)).getName().endsWith(".mp3")) {
                        Glide.with(DownloadFragment.this.getActivity()).load(DownloadFragment.myFile).placeholder((int) R.drawable.image_placeholder).error((int) R.drawable.music_icon).into(myHolder.down_image);
                    } else if (((FileItem) DownloadFragment.this.down.get(position)).getName().endsWith(".png")) {
                        Glide.with(DownloadFragment.this.getActivity()).load(DownloadFragment.myFile).placeholder((int) R.drawable.image_placeholder).error((int) R.drawable.music_icon).into(myHolder.down_image);
                    } else if (((FileItem) DownloadFragment.this.down.get(position)).getName().endsWith(".jpg")) {
                        Glide.with(DownloadFragment.this.getActivity()).load(DownloadFragment.myFile).placeholder((int) R.drawable.image_placeholder).error((int) R.drawable.music_icon).into(myHolder.down_image);
                    } else if (((FileItem) DownloadFragment.this.down.get(position)).getName().endsWith(".mp4")) {
                        Glide.with(DownloadFragment.this.getActivity()).load(DownloadFragment.myFile).placeholder((int) R.drawable.image_placeholder).error((int) R.drawable.music_icon).into(myHolder.down_image);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                myHolder.cardview.setOnClickListener(new OnClickListener() {
                    @RequiresApi(api = 16)
                    public void onClick(View v) {
                        if (DownloadFragment.this.CountSelected > 0) {
                            if (DownloadFragment.this.actionMode == null) {
                                DownloadFragment.this.actionMode = DownloadFragment.this.getActivity().startActionMode(DownloadFragment.this.callback);
                            }
                            DownloadFragment.this.toggleSelection(position);
                            DownloadFragment.this.actionMode.setTitle(DownloadFragment.this.CountSelected + " " + "Selected");
                            if (DownloadFragment.this.CountSelected == 0) {
                                DownloadFragment.this.actionMode.finish();
                            }
                        } else if (DownloadFragment.this.CountSelected == 0) {
                            DownloadFragment.size = Long.valueOf(((FileItem) DownloadFragment.this.down.get(position)).getLength());
                            DownloadFragment.this.s = ((FileItem) DownloadFragment.this.down.get(position)).getPathFile();
                            DownloadFragment.this.down_movie = (FileItem) DownloadFragment.this.down.get(position);
                            DownloadFragment.myFile = new File(DownloadFragment.this.down_movie.getPathFile());
                            DownloadFragment.name = ((FileItem) DownloadFragment.this.down.get(position)).getName();
                            int pos = DownloadFragment.name.lastIndexOf(".");
                            if (pos > 0) {
                                DownloadFragment.name = DownloadFragment.name.substring(0, pos);
                            }
                            String[] parts = DownloadFragment.this.s.split(".");
                            Intent i = new Intent("android.intent.action.VIEW");
                            String name = ((FileItem) DownloadFragment.this.down.get(position)).getName();
                            String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1));
                            Uri uri = FileProvider.getUriForFile(DownloadFragment.this.getActivity(), DownloadFragment.this.getActivity().getPackageName() + ".provider", DownloadFragment.myFile);
                            i.setDataAndType(uri, type);
                            i.addFlags(1);
                            i.addFlags(268435456);
                            Intent intent = FileManager.createIntent(new File(DownloadFragment.this.s), DownloadFragment.this.getActivity());
                            for (ResolveInfo resolveInfo : DownloadFragment.this.getActivity().getPackageManager().queryIntentActivities(i, 65536)) {
                                DownloadFragment.this.getActivity().grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                            }
                            DownloadFragment.this.startActivity(i);
                        }
                    }
                });
                myHolder.cardview.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        if (DownloadFragment.this.actionMode == null) {
                            DownloadFragment.this.actionMode = DownloadFragment.this.getActivity().startActionMode(DownloadFragment.this.callback);
                        }
                        DownloadFragment.this.toggleSelection(position);
                        DownloadFragment.this.actionMode.setTitle(DownloadFragment.this.CountSelected + " " + "Selected");
                        if (DownloadFragment.this.CountSelected == 0) {
                            DownloadFragment.this.actionMode.finish();
                        }
                        return false;
                    }
                });
                myHolder.more_document.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        try {
                            PopupMenu popup = new PopupMenu(DownloadFragment.this.getActivity(), v);
                            popup.getMenuInflater().inflate(R.menu.first_more, popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.delete:
                                            AlertDialog.Builder builder = new AlertDialog.Builder(DownloadFragment.this.getActivity());
                                            builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
                                            builder.setTitle((CharSequence) "Delete Download File");
                                            builder.setIcon((int) R.drawable.ic_delete_black_24dp);
                                            builder.setPositiveButton((CharSequence) "OK", null);
                                            builder.setNegativeButton((CharSequence) "Cancel", null);
                                            final AlertDialog alertDialog = builder.create();
                                            alertDialog.show();
                                            alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
                                                @RequiresApi(api = 19)
                                                public void onClick(View v) {
                                                    DownloadFragment.this.s = ((FileItem) DownloadFragment.this.down.get(position)).getPathFile();
                                                    File file = new File(DownloadFragment.this.s);
                                                    if (DownloadFragment.this.s.startsWith("/storage/emulated/0/")) {
                                                        DownloadFragment.this.scanFile(new File(DownloadFragment.this.s));
                                                    } else if (DownloadFragment.this.s.startsWith(DownloadFragment.checkSDCARD(DownloadFragment.this.getActivity()))) {
                                                        if (VERSION.SDK_INT < 24) {
                                                            DownloadFragment.this.scanFile(new File(DownloadFragment.this.s));
                                                        } else {
                                                            try {
                                                                File f = new File(DownloadFragment.this.s);
                                                                if (StorageHelper.deleteFile(DownloadFragment.this.getActivity(), f)) {
                                                                    Toast.makeText(DownloadFragment.this.getActivity(), "Done...", 0).show();
                                                                    DownloadFragment.this.scanFile(f);
                                                                    if (f.delete()) {
                                                                        DownloadFragment.this.scanFile(f);
                                                                    }
                                                                } else {
                                                                    Toast.makeText(DownloadFragment.this.getActivity(), "Error...", 0).show();
                                                                }
                                                            } catch (Exception e) {
                                                            }
                                                        }
                                                    }
                                                    file.delete();
                                                    DownloadFragment.this.down.clear();
                                                    DownloadFragment.this.adapter = DownloadFragment.this.getAdapter();
                                                    DownloadFragment.this.grid_down.setAdapter(DownloadFragment.this.adapter);
                                                    alertDialog.dismiss();
                                                    DownloadFragment.this.loadDownload(download, down);
                                                    Toast.makeText(DownloadFragment.this.getActivity(), "Delete Successfully", 0).show();
                                                }
                                            });
                                            alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
                                                public void onClick(View v) {
                                                    alertDialog.dismiss();
                                                }
                                            });
                                            break;
                                        case R.id.details:
                                            Detail_Download();
                                            break;
                                        case R.id.share:
                                            Uri uri;
                                            DownloadFragment.this.s = ((FileItem) DownloadFragment.this.down.get(position)).getPathFile();
                                            DownloadFragment.this.down_movie = (FileItem) DownloadFragment.this.down.get(position);
                                            DownloadFragment.myFile = new File(DownloadFragment.this.down_movie.getPathFile());
                                            File file = new File(DownloadFragment.this.s);
                                            Intent i = new Intent();
                                            if (VERSION.SDK_INT >= 24) {
                                                uri = FileProvider.getUriForFile(DownloadFragment.this.getActivity(), DownloadFragment.this.getActivity().getPackageName() + ".provider", file);
                                            } else {
                                                uri = Uri.fromFile(file);
                                            }
                                            for (ResolveInfo resolveInfo : DownloadFragment.this.getActivity().getPackageManager().queryIntentActivities(i, 65536)) {
                                                DownloadFragment.this.getActivity().grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                                            }
                                            i.setAction("android.intent.action.SEND");
                                            if (DownloadFragment.this.s.endsWith(".txt")) {
                                                i.setType("text/plain");
                                            } else if (DownloadFragment.this.s.endsWith(".doc")) {
                                                i.setType("application/msword");
                                            } else if (DownloadFragment.this.s.endsWith(".doc")) {
                                                i.setType("application/msword");
                                            } else if (DownloadFragment.this.s.endsWith(".mp3")) {
                                                i.setType("application/msword");
                                            } else if (DownloadFragment.this.s.endsWith(".mp4")) {
                                                i.setType("application/msword");
                                            } else if (DownloadFragment.this.s.endsWith(".mkv")) {
                                                i.setType("application/msword");
                                            } else if (DownloadFragment.this.s.endsWith(".doc")) {
                                                i.setType("application/msword");
                                            } else if (DownloadFragment.this.s.endsWith(".png")) {
                                                i.setType("application/vnd.android.package-archive");
                                            } else if (DownloadFragment.this.s.endsWith(".jpg")) {
                                                i.setType("application/vnd.android.package-archive");
                                            } else if (DownloadFragment.this.s.endsWith(".pdf")) {
                                                i.setType("application/pdf");
                                            }
                                            i.putExtra("android.intent.extra.STREAM", uri);
                                            i.addFlags(1);
                                            DownloadFragment.this.startActivity(Intent.createChooser(i, ""));
                                            Toast.makeText(DownloadFragment.this.getActivity(), "share.. " + DownloadFragment.myFile, Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                    return true;
                                }

                                private void Detail_Download() {
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DownloadFragment.this.getActivity());
                                    View dialogView = DownloadFragment.this.getLayoutInflater().inflate(R.layout.detail_video, null);
                                    dialogBuilder.setView(dialogView);
                                    DownloadFragment.this.txtNameDetail_video = (TextView) dialogView.findViewById(R.id.txtNameDetail_video);
                                    DownloadFragment.this.txtPathDetail_video = (TextView) dialogView.findViewById(R.id.txtPathDetail_video);
                                    DownloadFragment.this.txtSizeDetail_video = (TextView) dialogView.findViewById(R.id.txtSizeDetail_video);
                                    DownloadFragment.this.txtPathDetail_video.setText("" + ((FileItem) DownloadFragment.this.down.get(position)).getPathFile());
                                    DownloadFragment.this.txtNameDetail_video.setText("" + ((FileItem) DownloadFragment.this.down.get(position)).getName());
                                    DownloadFragment.this.txtSizeDetail_video.setText("" + Formatter.formatFileSize(DownloadFragment.this.getActivity(), ((FileItem) DownloadFragment.this.down.get(position)).getLength()));
                                    dialogBuilder.setPositiveButton((CharSequence) "Done", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                        }
                                    });
                                    dialogBuilder.create().show();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                FileItem fileItem1 = (FileItem) DownloadFragment.this.down.get(position);
            }

            public int getItemCount() {
                return DownloadFragment.this.down.size();
            }
        };
        return this.adapter;
    }

    public void openFile(File url) {
        File file = url;
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
        for (ResolveInfo resolveInfo : getActivity().getPackageManager().queryIntentActivities(intent, 65536)) {
            getActivity().grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
        }
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            intent.setDataAndType(uri, "application/msword");
        } else if (url.toString().contains(".pdf")) {
            intent.setDataAndType(uri, "application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            intent.setDataAndType(uri, "application/x-wav");
        } else if (url.toString().contains(".rtf")) {
            intent.setDataAndType(uri, "application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            intent.setDataAndType(uri, "image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            intent.setDataAndType(uri, "image/jpeg");
        } else if (url.toString().contains(".txt")) {
            intent.setDataAndType(uri, "text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            intent.setDataAndType(uri, "video/*");
        } else {
            intent.setDataAndType(uri, "*/*");
        }
        intent.addFlags(268435456);
        startActivity(intent);
    }

    private void scanFile(File file) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                DownloadFragment.this.getActivity().getContentResolver().delete(uri, null, null);
            }
        });
    }


    private class DeleteTask extends AsyncTask<Void, Void, Void> {
        private DeleteTask() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            DownloadFragment.this.progressDialog = new ProgressDialog(DownloadFragment.this.getActivity());
            DownloadFragment.this.progressDialog.setProgressStyle(0);
            DownloadFragment.this.progressDialog.setCancelable(false);
            DownloadFragment.this.progressDialog.setTitle("Delete");
            DownloadFragment.this.progressDialog.setMessage("Please Wait.....");
            DownloadFragment.this.progressDialog.show();
        }

        @RequiresApi(api = 19)
        protected Void doInBackground(Void... params) {
            ArrayList<FileItem> checkedItems = DownloadFragment.this.selectedImage;
            for (int i = 0; i < checkedItems.size(); i++) {
                String S1 = ((FileItem) DownloadFragment.this.selectedImage.get(i)).getPathFile();
                File file = new File(S1);
                if (S1.startsWith("/storage/emulated/0/")) {
                    DownloadFragment.this.scanFile(new File(S1));
                } else if (S1.startsWith(DownloadFragment.checkSDCARD(DownloadFragment.this.getActivity()))) {
                    if (VERSION.SDK_INT < 24) {
                        DownloadFragment.this.scanFile(new File(S1));
                    } else {
                        try {
                            File f = new File(S1);
                            if (StorageHelper.deleteFile(DownloadFragment.this.getActivity(), f)) {
                                Toast.makeText(DownloadFragment.this.getActivity(), "Done...", 0).show();
                                DownloadFragment.this.scanFile(f);
                                if (f.delete()) {
                                    DownloadFragment.this.scanFile(f);
                                }
                            } else {
                                Toast.makeText(DownloadFragment.this.getActivity(), "Error...", 0).show();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                file.delete();
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DownloadFragment.this.selectedImage.clear();
            DownloadFragment.this.down.clear();
            Toast.makeText(DownloadFragment.this.getActivity(), "Delete Successfully", 0).show();
            DownloadFragment.this.progressDialog.dismiss();
            DownloadFragment.this.loadDownload(download, down);
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cardview;
        ImageView down_image;
        FrameLayout frameLayout_down;
        LinearLayout linear1;
        ImageView more_document;
        TextView textView_down;
        TextView textView_size;
        View view_down;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textView_down = (TextView) itemView.findViewById(R.id.down_tx);
            this.down_image = (ImageView) itemView.findViewById(R.id.down_image);
            this.textView_size = (TextView) itemView.findViewById(R.id.down_size);
            this.view_down = itemView.findViewById(R.id.view_down);
            this.frameLayout_down = (FrameLayout) itemView.findViewById(R.id.frameLayout_down);
            this.more_document = (ImageView) itemView.findViewById(R.id.more_down);
            this.cardview = (LinearLayout) itemView.findViewById(R.id.cardview_down);
            this.linear1 = (LinearLayout) itemView.findViewById(R.id.linear1);
        }
    }
}
