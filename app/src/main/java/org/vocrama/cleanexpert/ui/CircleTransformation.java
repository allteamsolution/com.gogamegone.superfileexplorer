package org.vocrama.cleanexpert.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Shader.TileMode;
import com.squareup.picasso.Transformation;

class CircleTransformation implements Transformation {
    private static final int STROKE_WIDTH = 6;

    CircleTransformation() {
    }

    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());
        Bitmap squaredBitmap = Bitmap.createBitmap(source, (source.getWidth() - size) / 2, (source.getHeight() - size) / 2, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }
        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());
        Canvas canvas = new Canvas(bitmap);
        Paint avatarPaint = new Paint();
        avatarPaint.setShader(new BitmapShader(squaredBitmap, TileMode.CLAMP, TileMode.CLAMP));
        Paint outlinePaint = new Paint();
        outlinePaint.setColor(-1);
        outlinePaint.setStyle(Style.STROKE);
        outlinePaint.setStrokeWidth(6.0f);
        outlinePaint.setAntiAlias(true);
        float r = ((float) size) / 2.0f;
        canvas.drawCircle(r, r, r, avatarPaint);
        canvas.drawCircle(r, r, r - 3.0f, outlinePaint);
        squaredBitmap.recycle();
        return bitmap;
    }

    public String key() {
        return "circleTransformation()";
    }
}
