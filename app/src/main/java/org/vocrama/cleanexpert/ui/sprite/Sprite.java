package org.vocrama.cleanexpert.ui.sprite;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.util.Property;
import org.vocrama.cleanexpert.ui.animation.AnimationUtils;
import org.vocrama.cleanexpert.ui.animation.FloatProperty;
import org.vocrama.cleanexpert.ui.animation.IntProperty;

public abstract class Sprite extends Drawable implements AnimatorUpdateListener, Animatable, Callback {
    public static final Property<Sprite, Integer> ALPHA = new IntProperty<Sprite>("alpha") {
        public void setValue(Sprite object, int value) {
            object.setAlpha(value);
        }

        public Integer get(Sprite object) {
            return Integer.valueOf(object.getAlpha());
        }
    };
    public static final Property<Sprite, Integer> ROTATE = new IntProperty<Sprite>("rotate") {
        public void setValue(Sprite object, int value) {
            object.setRotate(value);
        }

        public Integer get(Sprite object) {
            return Integer.valueOf(object.getRotate());
        }
    };
    public static final Property<Sprite, Integer> ROTATE_X = new IntProperty<Sprite>("rotateX") {
        public void setValue(Sprite object, int value) {
            object.setRotateX(value);
        }

        public Integer get(Sprite object) {
            return Integer.valueOf(object.getRotateX());
        }
    };
    public static final Property<Sprite, Integer> ROTATE_Y = new IntProperty<Sprite>("rotateY") {
        public void setValue(Sprite object, int value) {
            object.setRotateY(value);
        }

        public Integer get(Sprite object) {
            return Integer.valueOf(object.getRotateY());
        }
    };
    public static final Property<Sprite, Float> SCALE = new FloatProperty<Sprite>("scale") {
        public void setValue(Sprite object, float value) {
            object.setScale(value);
        }

        public Float get(Sprite object) {
            return Float.valueOf(object.getScale());
        }
    };
    public static final Property<Sprite, Float> SCALE_X = new FloatProperty<Sprite>("scaleX") {
        public void setValue(Sprite object, float value) {
            object.setScaleX(value);
        }

        public Float get(Sprite object) {
            return Float.valueOf(object.getScaleX());
        }
    };
    public static final Property<Sprite, Float> SCALE_Y = new FloatProperty<Sprite>("scaleY") {
        public void setValue(Sprite object, float value) {
            object.setScaleY(value);
        }

        public Float get(Sprite object) {
            return Float.valueOf(object.getScaleY());
        }
    };
    public static final Property<Sprite, Integer> TRANSLATE_X = new IntProperty<Sprite>("translateX") {
        public void setValue(Sprite object, int value) {
            object.setTranslateX(value);
        }

        public Integer get(Sprite object) {
            return Integer.valueOf(object.getTranslateX());
        }
    };
    public static final Property<Sprite, Float> TRANSLATE_X_PERCENTAGE = new FloatProperty<Sprite>("translateXPercentage") {
        public void setValue(Sprite object, float value) {
            object.setTranslateXPercentage(value);
        }

        public Float get(Sprite object) {
            return Float.valueOf(object.getTranslateXPercentage());
        }
    };
    public static final Property<Sprite, Integer> TRANSLATE_Y = new IntProperty<Sprite>("translateY") {
        public void setValue(Sprite object, int value) {
            object.setTranslateY(value);
        }

        public Integer get(Sprite object) {
            return Integer.valueOf(object.getTranslateY());
        }
    };
    public static final Property<Sprite, Float> TRANSLATE_Y_PERCENTAGE = new FloatProperty<Sprite>("translateYPercentage") {
        public void setValue(Sprite object, float value) {
            object.setTranslateYPercentage(value);
        }

        public Float get(Sprite object) {
            return Float.valueOf(object.getTranslateYPercentage());
        }
    };
    private static final Rect ZERO_BOUNDS_RECT = new Rect();
    private int alpha = 255;
    private int animationDelay;
    private ValueAnimator animator;
    protected Rect drawBounds = ZERO_BOUNDS_RECT;
    private Camera mCamera = new Camera();
    private Matrix mMatrix = new Matrix();
    private float pivotX;
    private float pivotY;
    private int rotate;
    private int rotateX;
    private int rotateY;
    private float scale = 1.0f;
    private float scaleX = 1.0f;
    private float scaleY = 1.0f;
    private int translateX;
    private float translateXPercentage;
    private int translateY;
    private float translateYPercentage;

    protected abstract void drawSelf(Canvas canvas);

    public abstract int getColor();

    public abstract ValueAnimator onCreateAnimation();

    public abstract void setColor(int i);

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getAlpha() {
        return this.alpha;
    }

    @SuppressLint({"WrongConstant"})
    public int getOpacity() {
        return 1;
    }

    public float getTranslateXPercentage() {
        return this.translateXPercentage;
    }

    public void setTranslateXPercentage(float translateXPercentage) {
        this.translateXPercentage = translateXPercentage;
    }

    public float getTranslateYPercentage() {
        return this.translateYPercentage;
    }

    public void setTranslateYPercentage(float translateYPercentage) {
        this.translateYPercentage = translateYPercentage;
    }

    public int getTranslateX() {
        return this.translateX;
    }

    public void setTranslateX(int translateX) {
        this.translateX = translateX;
    }

    public int getTranslateY() {
        return this.translateY;
    }

    public void setTranslateY(int translateY) {
        this.translateY = translateY;
    }

    public int getRotate() {
        return this.rotate;
    }

    public void setRotate(int rotate) {
        this.rotate = rotate;
    }

    public float getScale() {
        return this.scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
        setScaleX(scale);
        setScaleY(scale);
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

    public int getRotateX() {
        return this.rotateX;
    }

    public void setRotateX(int rotateX) {
        this.rotateX = rotateX;
    }

    public int getRotateY() {
        return this.rotateY;
    }

    public void setRotateY(int rotateY) {
        this.rotateY = rotateY;
    }

    public float getPivotX() {
        return this.pivotX;
    }

    public void setPivotX(float pivotX) {
        this.pivotX = pivotX;
    }

    public float getPivotY() {
        return this.pivotY;
    }

    public void setPivotY(float pivotY) {
        this.pivotY = pivotY;
    }

    public int getAnimationDelay() {
        return this.animationDelay;
    }

    public Sprite setAnimationDelay(int animationDelay) {
        this.animationDelay = animationDelay;
        return this;
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public void start() {
        if (!AnimationUtils.isStarted(this.animator)) {
            this.animator = obtainAnimation();
            if (this.animator != null) {
                AnimationUtils.start(this.animator);
                invalidateSelf();
            }
        }
    }

    public ValueAnimator obtainAnimation() {
        if (this.animator == null) {
            this.animator = onCreateAnimation();
        }
        if (this.animator != null) {
            this.animator.addUpdateListener(this);
            this.animator.setStartDelay((long) this.animationDelay);
        }
        return this.animator;
    }

    public void stop() {
        if (AnimationUtils.isStarted(this.animator)) {
            this.animator.removeAllUpdateListeners();
            this.animator.end();
            reset();
        }
    }

    public void reset() {
        this.scale = 1.0f;
        this.rotateX = 0;
        this.rotateY = 0;
        this.translateX = 0;
        this.translateY = 0;
        this.rotate = 0;
        this.translateXPercentage = 0.0f;
        this.translateYPercentage = 0.0f;
    }

    public boolean isRunning() {
        return AnimationUtils.isRunning(this.animator);
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        setDrawBounds(bounds);
    }

    public void setDrawBounds(Rect drawBounds) {
        setDrawBounds(drawBounds.left, drawBounds.top, drawBounds.right, drawBounds.bottom);
    }

    public void setDrawBounds(int left, int top, int right, int bottom) {
        this.drawBounds = new Rect(left, top, right, bottom);
        setPivotX((float) getDrawBounds().centerX());
        setPivotY((float) getDrawBounds().centerY());
    }

    public void invalidateDrawable(Drawable who) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable who, Runnable what, long when) {
    }

    public void unscheduleDrawable(Drawable who, Runnable what) {
    }

    public void onAnimationUpdate(ValueAnimator animation) {
        Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public Rect getDrawBounds() {
        return this.drawBounds;
    }

    public void draw(Canvas canvas) {
        int tx = getTranslateX();
        if (tx == 0) {
            tx = (int) (((float) getBounds().width()) * getTranslateXPercentage());
        }
        int ty = getTranslateY();
        if (ty == 0) {
            ty = (int) (((float) getBounds().height()) * getTranslateYPercentage());
        }
        canvas.translate((float) tx, (float) ty);
        canvas.scale(getScaleX(), getScaleY(), getPivotX(), getPivotY());
        canvas.rotate((float) getRotate(), getPivotX(), getPivotY());
        if (!(getRotateX() == 0 && getRotateY() == 0)) {
            this.mCamera.save();
            this.mCamera.rotateX((float) getRotateX());
            this.mCamera.rotateY((float) getRotateY());
            this.mCamera.getMatrix(this.mMatrix);
            this.mMatrix.preTranslate(-getPivotX(), -getPivotY());
            this.mMatrix.postTranslate(getPivotX(), getPivotY());
            this.mCamera.restore();
            canvas.concat(this.mMatrix);
        }
        drawSelf(canvas);
    }

    public Rect clipSquare(Rect rect) {
        int min = Math.min(rect.width(), rect.height());
        int cx = rect.centerX();
        int cy = rect.centerY();
        int r = min / 2;
        return new Rect(cx - r, cy - r, cx + r, cy + r);
    }
}
