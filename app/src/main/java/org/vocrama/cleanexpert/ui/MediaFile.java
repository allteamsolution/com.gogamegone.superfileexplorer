package org.vocrama.cleanexpert.ui;

import android.os.Parcel;
import android.os.Parcelable;

class MediaFile implements Parcelable {
    public static final Creator<MediaFile> CREATOR = new Creator<MediaFile>() {
        public MediaFile createFromParcel(Parcel in) {
            return new MediaFile(in);
        }

        public MediaFile[] newArray(int size) {
            return new MediaFile[size];
        }
    };
    public long id;
    public long imageSize;
    public boolean isSelected;
    public String name;
    public String path;
    public String type;
    public int videoDuration;

    public MediaFile(long id, String name, String path, boolean isSelected, String type, long imageSize) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.isSelected = isSelected;
        this.type = type;
        this.imageSize = imageSize;
    }

    public MediaFile(long id, String name, String path, boolean isSelected, String type, int videoDuration, long imageSize) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.isSelected = isSelected;
        this.type = type;
        this.videoDuration = videoDuration;
        this.imageSize = imageSize;
    }

    public MediaFile(long id, String name, String path, String songs, int duration, long length) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.type = songs;
        this.videoDuration = duration;
        this.imageSize = length;
    }

    protected MediaFile(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.path = in.readString();
        this.type = in.readString();
        this.videoDuration = in.readInt();
        this.imageSize = in.readLong();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.path);
        dest.writeString(this.type);
        dest.writeInt(this.videoDuration);
        dest.writeLong(this.imageSize);
    }
}
