package org.vocrama.cleanexpert.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.App;


public class BaseTheme extends AppCompatActivity {
    public static final int THEME_DARK = 2;
    public static final int THEME_LIGHT = 1;

    public void onCreate(Bundle savedInstanceState) {
        updateTheme();
        super.onCreate(savedInstanceState);
    }

    private void updateTheme() {
        int themeMode = App.getCurrentUser().getThemeMode();

        if (themeMode == THEME_LIGHT) {
            setTheme(R.style.Theme_Explorer_Light);
        } else {
            setTheme(R.style.Theme_Explorer_Dark);
        }
    }
}
