package org.vocrama.cleanexpert.ui.presenter.IPresenter;

import java.util.List;

public interface IView<T> {
    void showProgress();
    void onDetData(List<T> data);
    void hideProgress();
}
