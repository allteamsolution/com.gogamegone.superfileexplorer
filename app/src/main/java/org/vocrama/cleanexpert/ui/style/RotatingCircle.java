package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.CircleSprite;

public class RotatingCircle extends CircleSprite {
    public ValueAnimator onCreateAnimation() {
        float[] fractions = new float[]{0.0f, 0.5f, 1.0f};
        return new SpriteAnimatorBuilder(this).rotateX(fractions, Integer.valueOf(0), Integer.valueOf(-180), Integer.valueOf(-180)).rotateY(fractions, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(-180)).duration(1200).easeInOut(fractions).build();
    }
}
