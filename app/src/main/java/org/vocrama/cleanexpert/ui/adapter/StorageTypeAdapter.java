package org.vocrama.cleanexpert.ui.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gogamegone.superfileexplorer.R;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import org.vocrama.cleanexpert.model.Storage;

import java.util.List;

public class StorageTypeAdapter extends RecyclerView.Adapter<StorageTypeAdapter.StorageHolder> {

    private List<Storage> storageList;
    private OnStorageItemClickListener listener;

    public StorageTypeAdapter(List<Storage> storageList, OnStorageItemClickListener listener) {
        this.storageList = storageList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public StorageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_item, viewGroup, false);
        return new StorageHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull StorageHolder storageHolder, int position) {
        Storage storage = storageList.get(position);
        long totalMemory = storage.getTotal();
        long usedMemory = storage.getUsed();

        storageHolder.itemView.setBackground(storage.getImage());
        storageHolder.tvStorageType.setText(storage.getName());
        storageHolder.lpiStorageCapacity.setProgressCompat((int) ((usedMemory * 100) / totalMemory), true);
    }

    @Override
    public int getItemCount() {
        return storageList.size();
    }

    public interface OnStorageItemClickListener {
        void onItemCLick(int position);
    }

    static class StorageHolder extends RecyclerView.ViewHolder {

        private TextView tvStorageType;
        private LinearProgressIndicator lpiStorageCapacity;

        public StorageHolder(@NonNull View itemView, OnStorageItemClickListener listener) {
            super(itemView);

            tvStorageType = itemView.findViewById(R.id.tvStorageType);
            lpiStorageCapacity = itemView.findViewById(R.id.lpiStorageCapacity);

            itemView.setOnClickListener(v -> {
                if (listener != null)
                    listener.onItemCLick(getAdapterPosition());
            });
        }
    }
}
