package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.callback.OnMediaFileClickListener;
import org.vocrama.cleanexpert.ui.presenter.DocumentsPresenter;
import org.vocrama.cleanexpert.ui.presenter.IPresenter.IAsyncFragmentPresenter;
import org.vocrama.cleanexpert.ui.presenter.IPresenter.IView;
import org.vocrama.cleanexpert.ui.presenter.ZipPresenter;
import org.vocrama.cleanexpert.ui.view.FileItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DocumentFragment extends Fragment implements IView<FileItem> {

    public static FileItem movie;
    private FileManager.OnFileLoadingCallback listener;
    private IAsyncFragmentPresenter<FileItem> presenter;
    private ActionMode actionMode;
    private static File root;
    public TextView txtNameDetail_video;
    public TextView txtPathDetail_video;
    public TextView txtSizeDetail_video;
    private ArrayList<FileItem> fileList;
    private int countSelected;

    DocumentAdapter adapter;
    String d_path;
    RecyclerView rvDocs;
    File myFile;
    ProgressDialog progressDialog;
    String s;
    ArrayList<FileItem> selectedImage = new ArrayList<>();

    public static DocumentFragment getInstance() {
        return new DocumentFragment();
    }

    public DocumentFragment() {

    }

    private Callback callback = new Callback() {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.document_context_menu, menu);
            MenuItem mi = menu.getItem(0);
            mi.setTitle(mi.getTitle().toString());
            DocumentFragment.this.actionMode = mode;
            DocumentFragment.this.countSelected = 0;
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.doc_d:
                    DocumentFragment.this.deleteDoc(mode);
                    break;
                case R.id.doc_share:
                    DocumentFragment.this.shareDoc(mode);
                    break;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            if (DocumentFragment.this.countSelected > 0) {
                DocumentFragment.this.deselectAll();
            }
            DocumentFragment.this.actionMode = null;
        }
    };

    @RequiresApi(api = 19)
    public static String checkSDCARD(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_documents, container, false);

        if (presenter == null) {
            presenter = new DocumentsPresenter();
            presenter.setView(DocumentFragment.this);
            presenter.onCreate();
        }

        return inflate;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (FileManager.OnFileLoadingCallback) requireActivity();
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvDocs = view.findViewById(R.id.main_recycler_view);
        root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        presenter.getDataAsync();
    }

    private void toggleSelection(int position) {
        ((FileItem) this.fileList.get(position)).isSelected = !((FileItem) this.fileList.get(position)).isSelected;
        if (((FileItem) this.fileList.get(position)).isSelected) {
            this.selectedImage.add(this.fileList.get(position));
            this.countSelected++;
        } else {
            this.selectedImage.remove(this.fileList.get(position));
            this.countSelected--;
        }
        this.adapter.notifyDataSetChanged();
    }

    private void shareDoc(ActionMode mode) {
        ArrayList<FileItem> checkedItems = this.selectedImage;
        ArrayList<Uri> share = new ArrayList();
        for (int i = 0; i < checkedItems.size(); i++) {
            if (((FileItem) this.selectedImage.get(i)).isSelected) {
                Uri uri;
                this.d_path = ((FileItem) this.selectedImage.get(i)).getPathFile();
                File file = new File(this.d_path);
                if (VERSION.SDK_INT >= 24) {
                    uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
                } else {
                    uri = Uri.fromFile(file);
                }
                share.add(uri);
            }
        }
        Intent i2 = new Intent();
        i2.setAction("android.intent.action.SEND_MULTIPLE");
        if (this.d_path.endsWith(".txt")) {
            i2.setType("text/plain");
        } else if (this.d_path.endsWith(".doc")) {
            i2.setType("application/msword");
        } else if (this.d_path.endsWith(".pdf")) {
            i2.setType("application/pdf");
        }
        i2.putParcelableArrayListExtra("android.intent.extra.STREAM", share);
        i2.addFlags(1);
        startActivity(Intent.createChooser(i2, ""));
        this.adapter.notifyDataSetChanged();
        mode.finish();
    }

    private void deleteDoc(final ActionMode mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage((CharSequence) "Are You Sure Want To Delete ?");
        builder.setTitle((CharSequence) "Delete File");
        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
        builder.setPositiveButton((CharSequence) "OK", null);
        builder.setNegativeButton((CharSequence) "Cancel", null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new DeleteTask().execute(new Void[0]);
                alertDialog.dismiss();
                mode.finish();
            }
        });
        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
                mode.finish();
            }
        });
    }

    private void deselectAll() {
        int l = this.fileList.size();
        for (int i = 0; i < l; i++) {
            ((FileItem) this.fileList.get(i)).isSelected = false;
        }
        this.countSelected = 0;
        this.adapter.notifyDataSetChanged();
    }

    private void scanFile(File file) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
                DocumentFragment.this.getActivity().getContentResolver().delete(uri, null, null);
            }
        });
    }

    @Override
    public void showProgress() {
        listener.onFileLoading();
    }

    @Override
    public void onDetData(List<FileItem> data) {
        fileList = new ArrayList<>();
        fileList.addAll(data);
        adapter = new DocumentAdapter(fileList, onMediaFileClickListener);
        rvDocs.setLayoutManager(new GridLayoutManager(requireActivity(), 4, RecyclerView.VERTICAL, false));
        rvDocs.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.onDestroy();
            presenter = null;
        }
    }

    @Override
    public void hideProgress() {
        listener.onFileLoaded();
    }


    private class DeleteTask extends AsyncTask<Void, Void, Void> {
        private DeleteTask() {
        }


        protected void onPreExecute() {
            super.onPreExecute();
            DocumentFragment.this.progressDialog = new ProgressDialog(DocumentFragment.this.getActivity());
            DocumentFragment.this.progressDialog.setProgressStyle(0);
            DocumentFragment.this.progressDialog.setCancelable(false);
            DocumentFragment.this.progressDialog.setTitle("Delete");
            DocumentFragment.this.progressDialog.setMessage("Please Wait.....");
            DocumentFragment.this.progressDialog.show();
        }

        @RequiresApi(api = 19)
        protected Void doInBackground(Void... params) {
            ArrayList<FileItem> checkedItems = DocumentFragment.this.selectedImage;
            for (int i = 0; i < checkedItems.size(); i++) {
                String S1 = ((FileItem) DocumentFragment.this.selectedImage.get(i)).getPathFile();
                File file = new File(S1);
                if (S1.startsWith("/storage/emulated/0/")) {
                    DocumentFragment.this.scanFile(new File(S1));
                } else if (S1.startsWith(DocumentFragment.checkSDCARD(DocumentFragment.this.getActivity()))) {
                    if (VERSION.SDK_INT < 24) {
                        DocumentFragment.this.scanFile(new File(S1));
                    } else {
                        try {
                            File f = new File(S1);
                            if (StorageHelper.deleteFile(DocumentFragment.this.getActivity(), f)) {
                                Toast.makeText(DocumentFragment.this.getActivity(), "Done...", Toast.LENGTH_SHORT).show();
                                DocumentFragment.this.scanFile(f);
                                if (f.delete()) {
                                    DocumentFragment.this.scanFile(f);
                                }
                            } else {
                                Toast.makeText(DocumentFragment.this.getActivity(), "Error...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                file.delete();
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DocumentFragment.this.selectedImage.clear();
            DocumentFragment.this.fileList.clear();
            presenter.getDataAsync();
            Toast.makeText(DocumentFragment.this.getActivity(), "Delete Successfully", 0).show();
            DocumentFragment.this.progressDialog.dismiss();
        }
    }

    public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.DocumentHolder> {

        private final ArrayList<FileItem> docList;
        private final OnMediaFileClickListener listener;

        public DocumentAdapter(ArrayList<FileItem> videoList, OnMediaFileClickListener listener) {
            this.docList = videoList;
            this.listener = listener;
        }

        @NonNull
        @Override
        public DocumentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doc_adapter, parent, false);
            return new DocumentHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull DocumentHolder holder, int position) {
            FileItem file = docList.get(position);
            holder.bind(file, listener);
        }

        @Override
        public int getItemCount() {
            return docList.size();
        }

        public class DocumentHolder extends RecyclerView.ViewHolder {
            private LinearLayout cardView;
            private ImageView more_documnet;
            private ImageView show_image;
            private TextView textView_doc;
            private TextView textView_size;

            public DocumentHolder(@NonNull View itemView) {
                super(itemView);
                textView_doc = (TextView) itemView.findViewById(R.id.doc_tv);
                show_image = (ImageView) itemView.findViewById(R.id.show_image);
                cardView = (LinearLayout) itemView.findViewById(R.id.cardview);
                more_documnet = (ImageView) itemView.findViewById(R.id.more_document);
            }

            public void bind(FileItem file, OnMediaFileClickListener listener) {

                textView_doc.setText(file.getName());

                cardView.setOnClickListener(v -> {
                    listener.onClick(getAdapterPosition());
                });

                cardView.setOnLongClickListener(v -> {
                    listener.onLongClick(getAdapterPosition());
                    return true;
                });

                more_documnet.setOnClickListener((OnClickListener) v -> {
                    try {
                        PopupMenu popup = new PopupMenu(requireActivity(), v);
                        popup.getMenuInflater().inflate(R.menu.first_more, popup.getMenu());
                        popup.show();
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.delete:
                                        AlertDialog.Builder builder = new AlertDialog.Builder(DocumentFragment.this.getActivity());
                                        builder.setMessage((CharSequence) "          Are You Sure Want To Delete ?");
                                        builder.setTitle((CharSequence) "Delete Document File");
                                        builder.setIcon((int) R.drawable.ic_delete_black_24dp);
                                        builder.setPositiveButton((CharSequence) "OK", null);
                                        builder.setNegativeButton((CharSequence) "Cancel", null);
                                        final AlertDialog alertDialog = builder.create();
                                        alertDialog.show();
                                        alertDialog.getButton(-1).setOnClickListener(new OnClickListener() {
                                            @RequiresApi(api = 19)
                                            public void onClick(View v) {
                                                DocumentFragment.this.s = ((FileItem) DocumentFragment.this.fileList.get(getAdapterPosition())).getPathFile();
                                                File file = new File(DocumentFragment.this.s);
                                                if (DocumentFragment.this.s.startsWith("/storage/emulated/0/")) {
                                                    DocumentFragment.this.scanFile(new File(DocumentFragment.this.s));
                                                } else if (DocumentFragment.this.s.startsWith(DocumentFragment.checkSDCARD(DocumentFragment.this.getActivity()))) {
                                                    if (VERSION.SDK_INT < 24) {
                                                        DocumentFragment.this.scanFile(new File(DocumentFragment.this.s));
                                                    } else {
                                                        try {
                                                            File f = new File(DocumentFragment.this.s);
                                                            if (StorageHelper.deleteFile(DocumentFragment.this.getActivity(), f)) {
                                                                Toast.makeText(DocumentFragment.this.getActivity(), "Done...", 0).show();
                                                                DocumentFragment.this.scanFile(f);
                                                                if (f.delete()) {
                                                                    DocumentFragment.this.scanFile(f);
                                                                }
                                                            } else {
                                                                Toast.makeText(DocumentFragment.this.getActivity(), "Error...", 0).show();
                                                            }
                                                        } catch (Exception e) {
                                                        }
                                                    }
                                                }
                                                file.delete();
                                                DocumentFragment.this.fileList.clear();
                                                DocumentFragment.this.adapter = new DocumentAdapter(DocumentFragment.this.fileList, onMediaFileClickListener);
                                                DocumentFragment.this.rvDocs.setAdapter(DocumentFragment.this.adapter);
                                                alertDialog.dismiss();
                                                presenter.getDataAsync();
                                                Toast.makeText(DocumentFragment.this.getActivity(), "Delete Successfully", 0).show();
                                            }
                                        });
                                        alertDialog.getButton(-2).setOnClickListener(new OnClickListener() {
                                            public void onClick(View v) {
                                                alertDialog.dismiss();
                                            }
                                        });
                                        break;
                                    case R.id.details:
                                        Detail_Documnet();
                                        break;
                                    case R.id.share:
                                        Uri uri;
                                        DocumentFragment.this.s = ((FileItem) DocumentFragment.this.fileList.get(getAdapterPosition())).getPathFile();
                                        DocumentFragment.movie = (FileItem) DocumentFragment.this.fileList.get(getAdapterPosition());
                                        DocumentFragment.this.myFile = new File(DocumentFragment.movie.getPathFile());
                                        File file = new File(DocumentFragment.this.s);
                                        Intent i = new Intent();
                                        if (VERSION.SDK_INT >= 24) {
                                            uri = FileProvider.getUriForFile(requireActivity(), DocumentFragment.this.getActivity().getPackageName() + ".provider", file);
                                        } else {
                                            uri = Uri.fromFile(file);
                                        }
                                        for (ResolveInfo resolveInfo : requireActivity().getPackageManager().queryIntentActivities(i, 65536)) {
                                            DocumentFragment.this.getActivity().grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                                        }
                                        i.setAction("android.intent.action.SEND");
                                        if (DocumentFragment.this.s.endsWith(".txt")) {
                                            i.setType("text/plain");
                                        } else if (DocumentFragment.this.s.endsWith(".doc")) {
                                            i.setType("application/msword");
                                        } else if (DocumentFragment.this.s.endsWith(".pdf")) {
                                            i.setType("application/pdf");
                                        }
                                        i.putExtra("android.intent.extra.STREAM", uri);
                                        i.addFlags(1);
                                        DocumentFragment.this.startActivity(Intent.createChooser(i, ""));
                                        Toast.makeText(DocumentFragment.this.getActivity(), "share.. " + DocumentFragment.this.myFile, 0).show();
                                        break;
                                }
                                return true;
                            }

                            private void Detail_Documnet() {
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DocumentFragment.this.getActivity());
                                View dialogView = DocumentFragment.this.getLayoutInflater().inflate(R.layout.detail_video, null);
                                dialogBuilder.setView(dialogView);
                                DocumentFragment.this.txtNameDetail_video = (TextView) dialogView.findViewById(R.id.txtNameDetail_video);
                                DocumentFragment.this.txtPathDetail_video = (TextView) dialogView.findViewById(R.id.txtPathDetail_video);
                                DocumentFragment.this.txtSizeDetail_video = (TextView) dialogView.findViewById(R.id.txtSizeDetail_video);
                                DocumentFragment.this.txtPathDetail_video.setText("" + ((FileItem) DocumentFragment.this.fileList.get(getAdapterPosition())).getPathFile());
                                DocumentFragment.this.txtNameDetail_video.setText("" + ((FileItem) DocumentFragment.this.fileList.get(getAdapterPosition())).getName());
                                DocumentFragment.this.txtSizeDetail_video.setText("" + Formatter.formatFileSize(DocumentFragment.this.getActivity(), ((FileItem) DocumentFragment.this.fileList.get(getAdapterPosition())).getLength()));
                                dialogBuilder.setPositiveButton((CharSequence) "Done", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                });
                                dialogBuilder.create().show();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

                try {
                    if (file.getName().endsWith(".txt")) {
                        show_image.setImageResource(R.drawable.im_txt);
                    } else if (file.getName().endsWith(".pdf")) {
                        show_image.setImageResource(R.drawable.im_pdf);
                    } else if (file.getName().endsWith(".doc")) {
                        show_image.setImageResource(R.drawable.im_doc);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private OnMediaFileClickListener onMediaFileClickListener = new OnMediaFileClickListener() {
        @Override
        public void onClick(int position) {
            if (DocumentFragment.this.countSelected > 0) {
                if (DocumentFragment.this.actionMode == null) {
                    DocumentFragment.this.actionMode = DocumentFragment.this.getActivity().startActionMode(DocumentFragment.this.callback);
                }
                DocumentFragment.this.toggleSelection(position);
                DocumentFragment.this.actionMode.setTitle(DocumentFragment.this.countSelected + " " + "Selected");
                if (DocumentFragment.this.countSelected == 0) {
                    DocumentFragment.this.actionMode.finish();
                }
            } else if (DocumentFragment.this.countSelected == 0) {
                DocumentFragment.this.s = ((FileItem) DocumentFragment.this.fileList.get(position)).getPathFile();
                DocumentFragment.movie = (FileItem) DocumentFragment.this.fileList.get(position);
                DocumentFragment.this.myFile = new File(DocumentFragment.movie.getPathFile());
                Intent i = new Intent("android.intent.action.VIEW");
                String name = ((FileItem) DocumentFragment.this.fileList.get(position)).getName();
                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1));
                Uri uri = FileProvider.getUriForFile(DocumentFragment.this.getActivity(), DocumentFragment.this.getActivity().getPackageName() + ".provider", DocumentFragment.this.myFile);
                i.setDataAndType(uri, type);
                i.addFlags(1);
                i.addFlags(268435456);
                for (ResolveInfo resolveInfo : DocumentFragment.this.getActivity().getPackageManager().queryIntentActivities(i, 65536)) {
                    DocumentFragment.this.getActivity().grantUriPermission(resolveInfo.activityInfo.packageName, uri, 3);
                }
                DocumentFragment.this.startActivity(i);
            }
        }

        @Override
        public void onLongClick(int position) {
            if (DocumentFragment.this.actionMode == null) {
                DocumentFragment.this.actionMode = DocumentFragment.this.getActivity().startActionMode(DocumentFragment.this.callback);
            }
            DocumentFragment.this.toggleSelection(position);
            DocumentFragment.this.actionMode.setTitle(DocumentFragment.this.countSelected + " " + "Selected");
            if (DocumentFragment.this.countSelected == 0) {
                DocumentFragment.this.actionMode.finish();
            }
        }
    };
}
