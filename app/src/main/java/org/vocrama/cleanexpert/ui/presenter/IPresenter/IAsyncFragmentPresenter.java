package org.vocrama.cleanexpert.ui.presenter.IPresenter;

import org.vocrama.cleanexpert.ui.view.FileItem;

import java.util.List;

public interface IAsyncFragmentPresenter<T> {
    void onCreate();
    void getDataAsync();
    void setView(IView<T> view);
    void onDestroy();
}
