package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.animation.interpolator.KeyFrameInterpolator;
import org.vocrama.cleanexpert.ui.sprite.RingSprite;

public class PulseRing extends RingSprite {
    public PulseRing() {
        setScale(0.0f);
    }

    public ValueAnimator onCreateAnimation() {
        float[] fractions = new float[]{0.0f, 0.7f, 1.0f};
        return new SpriteAnimatorBuilder(this).scale(fractions, Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(1.0f)).alpha(fractions, Integer.valueOf(255), Integer.valueOf(178), Integer.valueOf(0)).duration(1000).interpolator(KeyFrameInterpolator.pathInterpolator(0.21f, 0.53f, 0.56f, 0.8f, fractions)).build();
    }
}
