package org.vocrama.cleanexpert.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public abstract class MyBaseAdapter<T> extends BaseAdapter {
    public Context context;
    public List<T> dataList = new ArrayList();
    public LayoutInflater inflater;

    public abstract View getView(int i, View view, ViewGroup viewGroup);

    public MyBaseAdapter(List<T> dateList, Context context) {
        this.dataList = dateList;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setDataList(List<T> list) {
        this.dataList.clear();
        this.dataList.addAll(list);
    }

    public List<T> getData() {
        return this.dataList;
    }

    public int getCount() {
        return this.dataList.size();
    }

    public T getItem(int i) {
        return this.dataList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }
}
