package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.os.Build.VERSION;
import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.CircleLayoutContainer;
import org.vocrama.cleanexpert.ui.sprite.CircleSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;

public class FadingCircle extends CircleLayoutContainer {

    private class Dot extends CircleSprite {
        Dot() {
            setAlpha(0);
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.39f, 0.4f, 1.0f};
            return new SpriteAnimatorBuilder(this).alpha(fractions, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(255), Integer.valueOf(0)).duration(1200).easeInOut(fractions).build();
        }
    }

    public Sprite[] onCreateChild() {
        Dot[] dots = new Dot[12];
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new Dot();
            if (VERSION.SDK_INT >= 24) {
                dots[i].setAnimationDelay(i * 100);
            } else {
                dots[i].setAnimationDelay((i * 100) - 1200);
            }
        }
        return dots;
    }
}
