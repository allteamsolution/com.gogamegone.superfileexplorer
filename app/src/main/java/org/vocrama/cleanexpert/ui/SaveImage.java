package org.vocrama.cleanexpert.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SaveImage {
    public static String NameOfFile = null;
    private String NameOfFolder = "/FileManager//PhotoEffect/";
    private Context TheThis;

    public void Save(Context context, Bitmap ImageToSave) {
        this.TheThis = context;
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + this.NameOfFolder;
        String CurrentDateAndTime = getCurrentDateAndTime();
        File dir = new File(file_path);
        NameOfFile = "Photo_" + CurrentDateAndTime + ".png";
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir, NameOfFile);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            ImageToSave.compress(CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            MakeSureFileWasCreatedThenMakeAvabile(file);
        } catch (FileNotFoundException e) {
            Log.d("Error : ", e.getMessage());
        } catch (IOException e2) {
            UnableToSave();
        }
    }

    private void MakeSureFileWasCreatedThenMakeAvabile(File file) {
        MediaScannerConnection.scanFile(this.TheThis, new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
            }
        });
    }

    @SuppressLint({"SimpleDateFormat"})
    private String getCurrentDateAndTime() {
        return new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(Calendar.getInstance().getTime());
    }

    private void UnableToSave() {
        Toast.makeText(this.TheThis, "Photo cannot save to FileManager Effect", 0).show();
    }
}
