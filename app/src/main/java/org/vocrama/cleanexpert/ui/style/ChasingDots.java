package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.view.animation.LinearInterpolator;

import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.CircleSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.sprite.SpriteContainer;

public class ChasingDots extends SpriteContainer {

    private class Dot extends CircleSprite {
        Dot() {
            setScale(0.0f);
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.5f, 1.0f};
            return new SpriteAnimatorBuilder(this).scale(fractions, Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.0f)).duration(2000).easeInOut(fractions).build();
        }
    }

    public Sprite[] onCreateChild() {
        return new Sprite[]{new Dot(), new Dot()};
    }

    public void onChildCreated(Sprite... sprites) {
        super.onChildCreated(sprites);
        if (VERSION.SDK_INT >= 24) {
            sprites[1].setAnimationDelay(1000);
        } else {
            sprites[1].setAnimationDelay(-1000);
        }
    }

    public ValueAnimator onCreateAnimation() {
        float[] fractions = new float[]{0.0f, 1.0f};
        return new SpriteAnimatorBuilder(this).rotate(fractions, Integer.valueOf(0), Integer.valueOf(360)).duration(2000).interpolator(new LinearInterpolator()).build();
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        bounds = clipSquare(bounds);
        int drawW = (int) (((float) bounds.width()) * 0.6f);
        getChildAt(0).setDrawBounds(bounds.right - drawW, bounds.top, bounds.right, bounds.top + drawW);
        getChildAt(1).setDrawBounds(bounds.right - drawW, bounds.bottom - drawW, bounds.right, bounds.bottom);
    }
}
