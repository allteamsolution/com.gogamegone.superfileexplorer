package org.vocrama.cleanexpert.ui.style;

import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.os.Build.VERSION;

import org.vocrama.cleanexpert.ui.animation.SpriteAnimatorBuilder;
import org.vocrama.cleanexpert.ui.sprite.RectSprite;
import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.sprite.SpriteContainer;

public class Wave extends SpriteContainer {

    private class WaveItem extends RectSprite {
        WaveItem() {
            setScaleY(0.4f);
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.2f, 0.4f, 1.0f};
            return new SpriteAnimatorBuilder(this).scaleY(fractions, Float.valueOf(0.4f), Float.valueOf(1.0f), Float.valueOf(0.4f), Float.valueOf(0.4f)).duration(1200).easeInOut(fractions).build();
        }
    }

    public Sprite[] onCreateChild() {
        WaveItem[] waveItems = new WaveItem[5];
        for (int i = 0; i < waveItems.length; i++) {
            waveItems[i] = new WaveItem();
            if (VERSION.SDK_INT >= 24) {
                waveItems[i].setAnimationDelay((i * 100) + 600);
            } else {
                waveItems[i].setAnimationDelay((i * 100) - 1200);
            }
        }
        return waveItems;
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        bounds = clipSquare(bounds);
        int rw = bounds.width() / getChildCount();
        int width = ((bounds.width() / 5) * 3) / 5;
        for (int i = 0; i < getChildCount(); i++) {
            int l = (bounds.left + (i * rw)) + (rw / 5);
            getChildAt(i).setDrawBounds(l, bounds.top, l + width, bounds.bottom);
        }
    }
}
