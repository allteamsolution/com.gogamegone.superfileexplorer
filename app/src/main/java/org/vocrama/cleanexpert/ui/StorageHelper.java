package org.vocrama.cleanexpert.ui;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.MediaStore;
import android.provider.MediaStore.Files;
import android.provider.MediaStore.Images.Media;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.documentfile.provider.DocumentFile;

import com.gogamegone.superfileexplorer.R;
import com.orhanobut.hawk.Hawk;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;


public class StorageHelper {
    private static final int OPEN_DOCUMENT_TREE = 2;
    private static final String PRIMARY_VOLUME_NAME = "primary";
    private static final String TAG = "StorageHelper";

    private static boolean isWritable(@NonNull File file) {
        boolean isExisting = file.exists();
        try {
            new FileOutputStream(file, true).close();
        } catch (IOException e) {
        }
        boolean result = file.canWrite();
        if (isExisting) {
            return result;
        }
        file.delete();
        return result;
    }

    public static void scanFile(Context context, String[] paths) {
        MediaScannerConnection.scanFile(context, paths, null, null);
    }

    public static void scanFile1(Context context, String paths) {
        MediaScannerConnection.scanFile(context, new String[]{paths}, null, null);
    }

    public static boolean mkdir(Context context, @NonNull File dir) {
        boolean success = dir.exists();
        if (!success) {
            success = dir.mkdir();
        }
        if (!success && VERSION.SDK_INT >= 21) {
            DocumentFile document = getDocumentFile(context, dir, true, true);
            if (document == null || !document.exists()) {
                success = false;
            } else {
                success = true;
            }
        }
        if (success) {
            scanFile(context, new String[]{dir.getPath()});
        }
        return success;
    }

    private static File getTargetFile(File source, File targetDir) {
        File file = new File(targetDir, source.getName());
        return (source.getParentFile().equals(targetDir) || file.exists()) ? new File(targetDir, StringUtils.incrementFileNameSuffix(source.getName())) : file;
    }

    public static boolean copyDirectory(File sourceLocation, File targetLocation) throws IOException {
        if (!sourceLocation.isDirectory()) {
            File directory = targetLocation.getParentFile();
            if (directory == null || directory.exists() || directory.mkdirs()) {
                InputStream in = new FileInputStream(sourceLocation);
                OutputStream out = new FileOutputStream(targetLocation);
                byte[] buf = new byte[1024];
                while (true) {
                    int len = in.read(buf);
                    if (len <= 0) {
                        break;
                    }
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
            } else {
                throw new IOException("Cannot create dir " + directory.getAbsolutePath());
            }
        } else if (targetLocation.exists() || targetLocation.mkdirs()) {
            String[] children = sourceLocation.list();
            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
            }
        } else {
            throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
        }
        return Boolean.valueOf(true).booleanValue();
    }

    public static boolean copyDirectory(Context context, @NonNull File source, @NonNull File targetDir) throws IOException {
        if (source.isDirectory()) {
            InputStream inStream = null;
            OutputStream outStream = null;
            boolean success = false;
            File target = getTargetFile(source, targetDir);
            if (VERSION.SDK_INT >= 21) {
                if (isFileOnSdCard(context, source)) {
                    DocumentFile sourceDocument = getDocumentFile(context, source, true, true);
                    if (sourceDocument != null) {
                        context.getContentResolver().openInputStream(sourceDocument.getUri());
                    }
                }
                DocumentFile targetDocument = getDocumentFile(context, target, true, true);
                for (String file : source.list()) {
                    copyDirectory(context, new File(new File(source, file).toString()), new File(target.getAbsolutePath()));
                }
                success = true;
            } else if (VERSION.SDK_INT == 19) {
                Uri uri = getUriFromFile(context, target.getAbsolutePath());
                if (uri != null) {
                    outStream = context.getContentResolver().openOutputStream(uri);
                }
            }
            if (outStream != null) {
                byte[] buffer = new byte[4096];
                while (true) {
                    int bytesRead = inStream.read(buffer);
                    if (bytesRead == -1) {
                        break;
                    }
                    outStream.write(buffer, 0, bytesRead);
                }
                success = true;
            }
            if (!success) {
                return success;
            }
            scanFile(context, new String[]{target.getPath()});
            return success;
        }
        Boolean aBoolean = Boolean.valueOf(copyFile(context, source, targetDir));
        if (aBoolean.booleanValue()) {
            scanFile(context, new String[]{targetDir.getPath()});
        }
        return aBoolean.booleanValue();
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A:{Catch:{ Exception -> 0x00a2 }} */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0114 A:{ExcHandler: all (th java.lang.Throwable), Splitter: B:3:0x000e} */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0114 A:{ExcHandler: all (th java.lang.Throwable), Splitter: B:3:0x000e} */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing block: B:69:0x0114, code:
            r4 = th;
     */
    /* JADX WARNING: Missing block: B:70:0x0115, code:
            r11 = r12;
     */
    /* JADX WARNING: Missing block: B:71:0x0117, code:
            r10 = e;
     */
    /* JADX WARNING: Missing block: B:72:0x0118, code:
            r11 = r12;
     */
    public static boolean copyFile(Context r19, @NonNull File r20, @NonNull File r21) {
        /*
        r11 = 0;
        r13 = 0;
        r15 = 0;
        r16 = getTargetFile(r20, r21);
        r12 = new java.io.FileInputStream;	 Catch:{ Exception -> 0x00a2 }
        r0 = r20;
        r12.<init>(r0);	 Catch:{ Exception -> 0x00a2 }
        r4 = isWritable(r16);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        if (r4 == 0) goto L_0x0054;
    L_0x0014:
        r4 = new java.io.FileInputStream;	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r0 = r20;
        r4.<init>(r0);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r3 = r4.getChannel();	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r4 = new java.io.FileOutputStream;	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r0 = r16;
        r4.<init>(r0);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r8 = r4.getChannel();	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r4 = 0;
        r6 = r3.size();	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r3.transferTo(r4, r6, r8);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r15 = 1;
        r3.close();	 Catch:{ Exception -> 0x0102, all -> 0x0114 }
    L_0x0037:
        r8.close();	 Catch:{ Exception -> 0x0105, all -> 0x0114 }
    L_0x003a:
        r11 = r12;
    L_0x003b:
        r11.close();	 Catch:{ Exception -> 0x0108 }
    L_0x003e:
        r13.close();	 Catch:{ Exception -> 0x010b }
    L_0x0041:
        if (r15 == 0) goto L_0x0052;
    L_0x0043:
        r4 = 1;
        r4 = new java.lang.String[r4];
        r5 = 0;
        r6 = r16.getPath();
        r4[r5] = r6;
        r0 = r19;
        scanFile(r0, r4);
    L_0x0052:
        r4 = r15;
    L_0x0053:
        return r4;
    L_0x0054:
        r4 = android.os.Build.VERSION.SDK_INT;	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r5 = 21;
        if (r4 < r5) goto L_0x00d9;
    L_0x005a:
        r4 = isFileOnSdCard(r19, r20);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        if (r4 == 0) goto L_0x011d;
    L_0x0060:
        r4 = 0;
        r5 = 0;
        r0 = r19;
        r1 = r20;
        r14 = getDocumentFile(r0, r1, r4, r5);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        if (r14 == 0) goto L_0x011d;
    L_0x006c:
        r4 = r19.getContentResolver();	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r5 = r14.getUri();	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r11 = r4.openInputStream(r5);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
    L_0x0078:
        r4 = 0;
        r5 = 0;
        r0 = r19;
        r1 = r16;
        r17 = getDocumentFile(r0, r1, r4, r5);	 Catch:{ Exception -> 0x00a2 }
        if (r17 == 0) goto L_0x0090;
    L_0x0084:
        r4 = r19.getContentResolver();	 Catch:{ Exception -> 0x00a2 }
        r5 = r17.getUri();	 Catch:{ Exception -> 0x00a2 }
        r13 = r4.openOutputStream(r5);	 Catch:{ Exception -> 0x00a2 }
    L_0x0090:
        if (r13 == 0) goto L_0x003b;
    L_0x0092:
        r4 = 4096; // 0x1000 float:5.74E-42 double:2.0237E-320;
        r2 = new byte[r4];	 Catch:{ Exception -> 0x00a2 }
    L_0x0096:
        r9 = r11.read(r2);	 Catch:{ Exception -> 0x00a2 }
        r4 = -1;
        if (r9 == r4) goto L_0x00f7;
    L_0x009d:
        r4 = 0;
        r13.write(r2, r4, r9);	 Catch:{ Exception -> 0x00a2 }
        goto L_0x0096;
    L_0x00a2:
        r10 = move-exception;
    L_0x00a3:
        r4 = "StorageHelper";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fa }
        r5.<init>();	 Catch:{ all -> 0x00fa }
        r6 = "Error when copying file from ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x00fa }
        r6 = r20.getAbsolutePath();	 Catch:{ all -> 0x00fa }
        r5 = r5.append(r6);	 Catch:{ all -> 0x00fa }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x00fa }
        r6 = r16.getAbsolutePath();	 Catch:{ all -> 0x00fa }
        r5 = r5.append(r6);	 Catch:{ all -> 0x00fa }
        r5 = r5.toString();	 Catch:{ all -> 0x00fa }
        android.util.Log.e(r4, r5, r10);	 Catch:{ all -> 0x00fa }
        r4 = 0;
        r11.close();	 Catch:{ Exception -> 0x010e }
    L_0x00d1:
        r13.close();	 Catch:{ Exception -> 0x00d6 }
        goto L_0x0053;
    L_0x00d6:
        r5 = move-exception;
        goto L_0x0053;
    L_0x00d9:
        r4 = android.os.Build.VERSION.SDK_INT;	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r5 = 19;
        if (r4 != r5) goto L_0x011a;
    L_0x00df:
        r4 = r16.getAbsolutePath();	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r0 = r19;
        r18 = getUriFromFile(r0, r4);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        if (r18 == 0) goto L_0x011a;
    L_0x00eb:
        r4 = r19.getContentResolver();	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r0 = r18;
        r13 = r4.openOutputStream(r0);	 Catch:{ Exception -> 0x0117, all -> 0x0114 }
        r11 = r12;
        goto L_0x0090;
    L_0x00f7:
        r15 = 1;
        goto L_0x003b;
    L_0x00fa:
        r4 = move-exception;
    L_0x00fb:
        r11.close();	 Catch:{ Exception -> 0x0110 }
    L_0x00fe:
        r13.close();	 Catch:{ Exception -> 0x0112 }
    L_0x0101:
        throw r4;
    L_0x0102:
        r4 = move-exception;
        goto L_0x0037;
    L_0x0105:
        r4 = move-exception;
        goto L_0x003a;
    L_0x0108:
        r4 = move-exception;
        goto L_0x003e;
    L_0x010b:
        r4 = move-exception;
        goto L_0x0041;
    L_0x010e:
        r5 = move-exception;
        goto L_0x00d1;
    L_0x0110:
        r5 = move-exception;
        goto L_0x00fe;
    L_0x0112:
        r5 = move-exception;
        goto L_0x0101;
    L_0x0114:
        r4 = move-exception;
        r11 = r12;
        goto L_0x00fb;
    L_0x0117:
        r10 = move-exception;
        r11 = r12;
        goto L_0x00a3;
    L_0x011a:
        r11 = r12;
        goto L_0x0090;
    L_0x011d:
        r11 = r12;
        goto L_0x0078;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.mazhuang.cleanexpert.ui.StorageHelper.copyFile(android.content.Context, java.io.File, java.io.File):boolean");
    }

    @RequiresApi(api = 19)
    private static boolean isFileOnSdCard(Context context, File file) {
        String sdcardPath = getSdcardPath(context);
        return sdcardPath != null && file.getPath().startsWith(sdcardPath);
    }

    public static boolean moveFile(Context context, @NonNull File source, @NonNull File target) {
        boolean success = source.renameTo(target);
        if (!success) {
            success = copyFile(context, source, target);
            if (success) {
                success = deleteFile(context, source);
            }
        }
        if (success) {
            scanFile(context, new String[]{source.getPath(), target.getPath()});
        }
        return success;
    }

    public static Uri getUriFromFile(Context context, String path) {
        ContentResolver resolver = context.getContentResolver();
        Cursor filecursor = resolver.query(Files.getContentUri("external"), new String[]{"_id"}, "_data = ?", new String[]{path}, "date_added desc");
        if (filecursor == null) {
            return null;
        }
        filecursor.moveToFirst();
        if (filecursor.isAfterLast()) {
            filecursor.close();
            ContentValues values = new ContentValues();
            values.put("_data", path);
            return resolver.insert(Files.getContentUri("external"), values);
        }
        Uri uri = Files.getContentUri("external").buildUpon().appendPath(Integer.toString(filecursor.getInt(filecursor.getColumnIndex("_id")))).build();
        filecursor.close();
        return uri;
    }

    public static boolean deleteFilesInFolder(Context context, @NonNull File folder) {
        boolean totalSuccess = true;
        String[] children = folder.list();
        if (children != null) {
            for (String child : children) {
                File file = new File(folder, child);
                if (!(file.isDirectory() || deleteFile(context, file))) {
                    Log.w(TAG, "Failed to delete file" + child);
                    totalSuccess = false;
                }
            }
        }
        return totalSuccess;
    }

    public static boolean deleteFile(Context context, @NonNull File file) {
        boolean success = file.delete();
        if (!success && VERSION.SDK_INT >= 21) {
            DocumentFile document = getDocumentFile(context, file, false, false);
            success = document != null && document.delete();
        }
        if (!success && VERSION.SDK_INT == 19) {
            ContentResolver resolver = context.getContentResolver();
            if (null != null) {
                try {
                    resolver.delete(null, null, null);
                } catch (Exception e) {
                    Log.e(TAG, "Error when deleting file " + file.getAbsolutePath(), e);
                    return false;
                }
            }
            if (file.exists()) {
                success = false;
            } else {
                success = true;
            }
        }
        if (success) {
            scanFile(context, new String[]{file.getPath()});
        }
        return success;
    }

    public static boolean deleteDirectory(Context context, File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(context, files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        scanFile(context, new String[]{path.getAbsolutePath()});
        return path.delete();
    }

    @RequiresApi(api = 19)
    public static HashSet<File> getStorageRoots(Context context) {
        HashSet<File> paths = new HashSet();
        for (File file : context.getExternalFilesDirs("external")) {
            if (file != null) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index < 0) {
                    Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
                } else {
                    paths.add(new File(file.getAbsolutePath().substring(0, index)));
                }
            }
        }
        return paths;
    }

    @RequiresApi(api = 19)
    public static String getSdcardPath(Context context) {
        for (File file : context.getExternalFilesDirs("external")) {
            if (!(file == null || file.equals(context.getExternalFilesDir("external")))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index >= 0) {
                    return new File(file.getAbsolutePath().substring(0, index)).getPath();
                }
                Log.w("asd", "Unexpected external file dir: " + file.getAbsolutePath());
            }
        }
        return null;
    }

    public static boolean moveDirectory(Context context, @NonNull File source, @NonNull File target) throws IOException {
        boolean success = source.renameTo(target);
        if (!success) {
            success = copyDirectory(context, source, target);
            if (success) {
                success = removeDir(context, source);
                if (!success) {
                    success = deleteDirectory(context, source.getAbsolutePath()).booleanValue();
                }
            }
        }
        if (success) {
            scanFile(context, new String[]{source.getPath(), target.getPath()});
        }
        return success;
    }

    public static Boolean deleteDirectory(Context context, String folder) {
        File file = new File(folder);
        ArrayList<File> arrFiles = new ArrayList();
        if (file.isDirectory()) {
            if (file.exists()) {
                File[] files = file.listFiles();
                if (files == null) {
                    return Boolean.valueOf(false);
                }
                for (int i = 0; i < files.length; i++) {
                    deleteDirectory(context, files[i].getPath());
                    arrFiles.add(files[i]);
                }
                deleteFile(context, file);
            }
            arrFiles.add(file);
            Iterator it = arrFiles.iterator();
            while (it.hasNext()) {
                File file1 = (File) it.next();
                deleteFile(context, file);
                deleteMediaFile(context, file1);
            }
            return Boolean.valueOf(true);
        }
        deleteFile(context, file);
        deleteMediaFile(context, file);
        return Boolean.valueOf(true);
    }

    private static void deleteMediaFile(final Context context, File file) {
        try {
            MediaScannerConnection.scanFile(context, new String[]{file.toString()}, null, new OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Log.d("Delete File Uri -->>", "" + uri);
                    context.getContentResolver().delete(uri, null, null);
                }
            });
        } catch (Exception e) {
        }
    }

    public static boolean removeDir(Context context, @NonNull File file) {
        if (!file.exists() && !file.isDirectory()) {
            Log.d("=====>>FDELETE 353", "False");
            return false;
        } else if (file.delete()) {
            Log.d("=====>>FDELETE 359", "true");
            return true;
        } else if (VERSION.SDK_INT >= 21) {
            Log.d("=====>>FDELETE 365", "Success");
            DocumentFile document = getDocumentFile(context, file, true, true);
            if (document == null || !document.delete()) {
                return false;
            }
            return true;
        } else {
            if (VERSION.SDK_INT == 19) {
                ContentResolver resolver = context.getContentResolver();
                ContentValues values = new ContentValues();
                values.put("_data", file.getAbsolutePath());
                resolver.insert(Media.EXTERNAL_CONTENT_URI, values);
                resolver.delete(Files.getContentUri("external"), "_data=?", new String[]{file.getAbsolutePath()});
            }
            if (file.exists()) {
                return false;
            }
            return true;
        }
    }

    private static void scanMediaFile(Context context, File file) {
        MediaScannerConnection.scanFile(context, new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
            }
        });
    }

    private static void deletescanMediaFile(Context context, File file) {
        MediaScannerConnection.scanFile(context, new String[]{file.toString()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String path, Uri uri) {
            }
        });
    }

    @RequiresApi(api = 19)
    public static boolean renameDirectory(Context context, @NonNull File file, String nameChange) {
        DocumentFile document = getDocumentFile(context, file, true, true);
        Log.d("=====>> 394", "" + document);
        Boolean aBoolean = Boolean.valueOf(document.renameTo(nameChange));
        if (document == null || !aBoolean.booleanValue()) {
            return false;
        }
        return true;
    }

    public static boolean renameDir(Context context, @NonNull File file, String nameChange) {
        boolean z = true;
        if (!file.exists() && !file.isDirectory()) {
            Log.d("=====>>FRENAME 387", "False");
            return false;
        } else if (VERSION.SDK_INT >= 21) {
            Log.d("=====>>FRENAME 392", "Success");
            DocumentFile document = getDocumentFile(context, file, true, true);
            Log.d("=====>> 394", "" + document);
            Boolean aBoolean = Boolean.valueOf(document.renameTo(nameChange));
            if (document == null || !aBoolean.booleanValue()) {
                z = false;
            }
            return z;
        } else {
            scanFile(context, new String[]{new File(nameChange).getPath()});
            return false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static DocumentFile getDocumentOnlyDirectory(Context context, File file) {
        Uri treeUri = getTreeUri(context);
        if (treeUri == null) {
            Log.d("=============>>", "NULLLLLLLLLLLLLLLLLLLLLLLLL");
            return null;
        } else if (!file.getAbsolutePath().contains(getSdcardPath(context))) {
            return null;
        } else {
            DocumentFile document = DocumentFile.fromTreeUri(context, treeUri);
            String[] directoryList = file.getAbsolutePath().substring((new File(getSdcardPath(context)).getAbsolutePath() + "/").length()).split("/");
            for (int k = 0; k < directoryList.length; k++) {
                DocumentFile[] documentFiles = document.listFiles();
                for (int i = 0; i < documentFiles.length; i++) {
                    if (Objects.equals(documentFiles[i].getName(), directoryList[k])) {
                        document = documentFiles[i];
                        break;
                    }
                }
                if (!Objects.equals(document.getName(), directoryList[k])) {
                    return document.createDirectory(directoryList[k]);
                }
            }
            return document;
        }
    }

    @RequiresApi(api = 19)
    private static DocumentFile getDocumentFile(Context context, @NonNull File file, boolean isDirectory, boolean createDirectories) {
        Uri treeUri = getTreeUri(context);
        if (treeUri == null) {
            return null;
        }
        DocumentFile document = DocumentFile.fromTreeUri(context, treeUri);
        String sdcardPath = getSavedSdcardPath(context);
        String suffixPathPart = null;
        if (sdcardPath == null) {
            Iterator it = getStorageRoots(context).iterator();
            while (it.hasNext()) {
                File root = (File) it.next();
                if (!(root == null || file.getPath().indexOf(root.getPath()) == -1)) {
                    suffixPathPart = file.getAbsolutePath().substring(file.getPath().length());
                }
            }
        } else if (file.getPath().indexOf(sdcardPath) != -1) {
            suffixPathPart = file.getAbsolutePath().substring(sdcardPath.length());
        }
        if (suffixPathPart == null) {
            Log.d(TAG, "unable to find the document file, filePath:" + file.getPath() + " root: " + "" + sdcardPath);
            return null;
        }
        if (suffixPathPart.startsWith(File.separator)) {
            suffixPathPart = suffixPathPart.substring(1);
        }
        String[] parts = suffixPathPart.split("/");
        for (int i = 0; i < parts.length; i++) {
            if (document.findFile(parts[i]) != null) {
                document = document.findFile(parts[i]);
                Log.d("=====>> FDELETE 451", "0");
            } else if (i < parts.length - 1) {
                Log.d("=====>>FDELETE 453", "1");
                if (!createDirectories) {
                    return null;
                }
                Log.d("=====>>FDELETE 455", "2");
                document = document.createDirectory(parts[i]);
            } else if (!isDirectory) {
                return document.createFile("image", parts[i]);
            } else {
                Log.d("=====>>FDELETE 461", "3");
                document = document.createDirectory(parts[i]);
            }
        }
        return document;
    }

    public static Uri getTreeUri(Context context) {
        String uriString = (String) Hawk.get(context.getString(R.string.preference_internal_uri_extsdcard_photos), null);
        if (uriString == null) {
            return null;
        }
        return Uri.parse(uriString);
    }

    @RequiresApi(api = 19)
    public static void saveSdCardInfo(Context context, @Nullable Uri uri) {
        Hawk.put(context.getString(R.string.preference_internal_uri_extsdcard_photos), uri == null ? null : uri.toString());
        Hawk.put("sd_card_path", getSdcardPath(context));
    }

    private static String getSavedSdcardPath(Context context) {
        return (String) Hawk.get("sd_card_path", null);
    }

    @RequiresApi(api = 19)
    public static String getMediaPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.KITKAT;
        Log.i("URI", uri + "");
        String result = uri + "";
        // DocumentProvider
        //  if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
        if (isKitKat && (result.contains("media.documents"))) {

            String[] ary = result.split("/");
            int length = ary.length;
            String imgary = ary[length - 1];
            final String[] dat = imgary.split("%3A");

            final String docId = dat[1];
            final String type = dat[0];

            Uri contentUri = null;
            if ("image".equals(type)) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {

            } else if ("audio".equals(type)) {
            }

            final String selection = "_id=?";
            final String[] selectionArgs = new String[]{
                    dat[1]
            };

            return getDataColumn(context, contentUri, selection, selectionArgs);
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        String column = "_data";
        try {
            cursor = context.getContentResolver().query(uri, new String[]{"_data"}, selection, selectionArgs, null);
            if (cursor == null || !cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            }
            String string = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
            return string;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}
