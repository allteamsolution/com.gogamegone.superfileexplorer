package org.vocrama.cleanexpert.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gogamegone.superfileexplorer.R;


public class ThemActivity extends AppCompatActivity {
    TextView txtDark;
    TextView txtDefault;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_them);
        this.txtDefault = (TextView) findViewById(R.id.txtDefault);
        this.txtDark = (TextView) findViewById(R.id.txtDark);
        this.txtDark.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Utility.setTheme(ThemActivity.this.getApplicationContext(), 2);
                ThemActivity.this.recreateActivity();
            }
        });
        this.txtDefault.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Utility.setTheme(ThemActivity.this.getApplicationContext(), 1);
                ThemActivity.this.recreateActivity();
            }
        });
    }

    public void recreateActivity() {
        PackageManager packageManager = getPackageManager();
        startActivity(new Intent(this, FirstActivity.class));
        finish();
        overridePendingTransition(0, 0);
    }
}
