package org.vocrama.cleanexpert.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.FileUtils;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.cleanexpert.ui.view.AppList;
import org.vocrama.cleanexpert.util.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

public class AppBackupFragment extends Fragment {

    public static AppBackupFragment getInstance() {
        return new AppBackupFragment();
    }

    public static String fileName;
    ApkAdapter apkAdapter = null;
    AppList appList;
    GridView grid_apk;
    List<AppList> res = new ArrayList<>();

    private static final int UNINSTALL_REQUEST_CODE = 124;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_applications, container, false);
    }

    private boolean isThreadStarted = false;
    private CompositeDisposable compositeDisposable;

    private FileManager.OnFileLoadingCallback onFileLoadingCallback;

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        compositeDisposable = new CompositeDisposable();
        onFileLoadingCallback = (FileManager.OnFileLoadingCallback) requireActivity();

        this.grid_apk = (GridView) view.findViewById(R.id.grid_apk);

        this.apkAdapter = new ApkAdapter(getActivity(), this.res);
        this.grid_apk.setAdapter(this.apkAdapter);
        registerForContextMenu(this.grid_apk);
        this.grid_apk.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                AppBackupFragment.this.appList = (AppList) AppBackupFragment.this.res.get(position);
                final FileUtil fileUtil = new FileUtil();
                View renameFileView = LayoutInflater.from(AppBackupFragment.this.getActivity()).inflate(R.layout.backup_file, null);
                final EditText fileNameET = (EditText) renameFileView.findViewById(R.id.file_name);
                Button btnCancle = (Button) renameFileView.findViewById(R.id.btnOpen);
                Button btnExtract = (Button) renameFileView.findViewById(R.id.btnExtract);
                fileNameET.setText(AppBackupFragment.this.appList.getName());
                try {
                    final File path = AppBackupFragment.this.getApkFile();
                    if (path.exists()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AppBackupFragment.this.getActivity());
                        builder.setView(renameFileView);
                        final AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                        btnExtract.setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                AppBackupFragment.fileName = fileNameET.getText().toString().trim();
                                FileUtils.copyFile(path, fileUtil.generateFile(AppBackupFragment.this.appList.getName() + AppBackupFragment.fileName));
                                Toast.makeText(AppBackupFragment.this.getActivity(), "FileManager/BackUp/" + AppBackupFragment.fileName + ".apk", 0).show();
                                alertDialog.dismiss();
                            }
                        });
                        btnCancle.setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                AppBackupFragment.this.apk_open();
                                alertDialog.dismiss();
                            }
                        });
                        return;
                    }
                    Toast.makeText(AppBackupFragment.this.getActivity(), "Could not find application file", 0).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(AppBackupFragment.this.getActivity(), "error..   " + e.getMessage(), 0).show();
                }
            }
        });
    }

    private void GetApp() {

        if(isThreadStarted)
            return;

        if(res.isEmpty()) {
            List<PackageInfo> packs = requireActivity().getPackageManager().getInstalledPackages(0);

            Observable
                    .fromIterable(packs)
                    .filter(packageInfo -> !isSystemPackage(packageInfo))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> {
                        isThreadStarted = true;
                        onFileLoadingCallback.onFileLoading();
                    })
                    .doOnNext(p -> {
                        File file = new File(p.applicationInfo.publicSourceDir);
                        res.add(new AppList(p.applicationInfo.loadLabel(requireActivity().getPackageManager()).toString(), p.applicationInfo.loadIcon(requireActivity().getPackageManager()), file.length(), p, false, file.getAbsolutePath()));
                    })
                    .doOnError(throwable -> {
                        isThreadStarted = false;
                        onFileLoadingCallback.onFileLoaded();
                    })
                    .doOnComplete(() -> {
                        isThreadStarted = false;
                        onFileLoadingCallback.onFileLoaded();
                        apkAdapter.notifyDataSetChanged();
                    })
                    .subscribe();
        }
    }

    private boolean isSystemPackage(PackageInfo p) {
        return (p.applicationInfo.flags & 1) != 0;
    }

    public File getApkFile() throws NameNotFoundException {
        if (this.appList.getFilePath() == null) {
            this.appList.setFilePath(getActivity().getPackageManager().getApplicationInfo(this.appList.getPackageInfo().packageName, 0).sourceDir);
        }
        return new File(this.appList.getFilePath());
    }

    private void apk_open() {
        getActivity().startActivity(getActivity().getPackageManager().getLaunchIntentForPackage(this.appList.getPackageInfo().packageName));
    }

    @Override
    public void onResume() {
        super.onResume();
        GetApp();
    }

    public class ApkAdapter extends BaseAdapter {
        Context context;
        List<AppList> images;
        ViewHoder viewHoder;

        public ApkAdapter(Context context, List<AppList> images) {
            this.context = context;
            this.images = images;
        }

        public int getCount() {
            return this.images.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            this.viewHoder = new ViewHoder();
            if (convertView == null) {
                convertView = LayoutInflater.from(this.context).inflate(R.layout.adapter_app, parent, false);
                this.viewHoder.image_apk = (ImageView) convertView.findViewById(R.id.app_icon);
                this.viewHoder.textView = (TextView) convertView.findViewById(R.id.list_app_name);
                this.viewHoder.img_more_main_apkbackup = (ImageView) convertView.findViewById(R.id.img_more_main_apk);
                convertView.setTag(this.viewHoder);
            } else {
                this.viewHoder = (ViewHoder) convertView.getTag();
            }
            this.viewHoder.image_apk.setImageDrawable(((AppList) this.images.get(position)).getIcon());
            this.viewHoder.textView.setText(((AppList) this.images.get(position)).getName());
            this.viewHoder.img_more_main_apkbackup.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    try {
                        PopupMenu popup = new PopupMenu(ApkAdapter.this.context, v);
                        popup.getMenuInflater().inflate(R.menu.backup_menu, popup.getMenu());
                        popup.show();
                        popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.apk_info:
                                        AppBackupFragment.this.appList = (AppList) AppBackupFragment.this.res.get(position);
                                        Intent info = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                                        info.setData(Uri.parse("package:" + AppBackupFragment.this.appList.getPackageInfo().packageName));
                                        AppBackupFragment.this.startActivity(info);
                                        break;
                                    case R.id.apk_unin:
                                        appList = (AppList) res.get(position);
                                        Intent uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
                                        uninstallIntent.setData(Uri.parse("package:" + appList.getPackageInfo().packageName));
                                        uninstallIntent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
                                        startActivityForResult(uninstallIntent, UNINSTALL_REQUEST_CODE);
                                        break;
                                }
                                return true;
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            return convertView;
        }

        private class ViewHoder {
            ImageView image_apk;
            ImageView img_more_main_apkbackup;
            TextView textView;
            TextView textView_size;

            private ViewHoder() {
            }


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == UNINSTALL_REQUEST_CODE) {
                res.remove(appList);
                apkAdapter.notifyDataSetChanged();
                Toast.makeText(requireContext(), "Uninstall success", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
