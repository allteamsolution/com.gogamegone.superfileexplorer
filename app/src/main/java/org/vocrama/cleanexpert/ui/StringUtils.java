package org.vocrama.cleanexpert.ui;

import android.content.Context;
import android.os.Build.VERSION;

import androidx.annotation.Nullable;
import androidx.core.os.EnvironmentCompat;
import android.text.Html;
import android.text.Spanned;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

public class StringUtils {
    public static String getMimeType(String path) {
        if (path != null) {
            int index = path.lastIndexOf(46);
            if (index != -1) {
                String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(path.substring(index + 1).toLowerCase());
                return mime == null ? EnvironmentCompat.MEDIA_UNKNOWN : mime;
            }
        }
        return EnvironmentCompat.MEDIA_UNKNOWN;
    }

    public static String[] asArray(String... a) {
        return a;
    }

    public static String getGenericMIME(String mime) {
        return mime.split("/")[0] + "/*";
    }

    public static String getPhotoNameByPath(String path) {
        String[] b = path.split("/");
        String fi = b[b.length - 1];
        return fi.substring(0, fi.lastIndexOf(46));
    }

    public static String join(String jointChar, Object... collection) {
        String s = "";
        for (Object o : collection) {
            s = s + o.toString() + jointChar;
        }
        int i = s.lastIndexOf(jointChar);
        if (i != -1) {
            return s.substring(0, i);
        }
        return s;
    }

    public static Spanned html(String s) {
        if (VERSION.SDK_INT >= 24) {
            return Html.fromHtml(s, 0);
        }
        return Html.fromHtml(s);
    }

    public static String getName(String path) {
        String[] b = path.split("/");
        return b[b.length - 1];
    }

    public static String getPhotoPathRenamed(String olderPath, String newName) {
        String c = "";
        String[] b = olderPath.split("/");
        for (int x = 0; x < b.length - 1; x++) {
            c = c + b[x] + "/";
        }
        c = c + newName;
        String name = b[b.length - 1];
        return c + name.substring(name.lastIndexOf(46));
    }

    public static String incrementFileNameSuffix(String name) {
        String baseName;
        StringBuilder builder = new StringBuilder();
        int dot = name.lastIndexOf(46);
        if (dot != -1) {
            baseName = name.subSequence(0, dot).toString();
        } else {
            baseName = name;
        }
        String nameWoSuffix = baseName;
        if (Pattern.compile("_\\d").matcher(baseName).find()) {
            int i = baseName.lastIndexOf("_");
            if (i != -1) {
                nameWoSuffix = baseName.subSequence(0, i).toString();
            }
        }
        builder.append(nameWoSuffix).append("_").append(new Date().getTime());
        builder.append(name.substring(dot));
        return builder.toString();
    }

    public static String getPhotoPathRenamedAlbumChange(String olderPath, String albumNewName) {
        String c = "";
        String[] b = olderPath.split("/");
        for (int x = 0; x < b.length - 2; x++) {
            c = c + b[x] + "/";
        }
        return c + albumNewName + "/" + b[b.length - 1];
    }

    public static String getAlbumPathRenamed(String olderPath, String newName) {
        return olderPath.substring(0, olderPath.lastIndexOf(47)) + "/" + newName;
    }

    public static String getPhotoPathMoved(String olderPath, String folderPath) {
        String[] b = olderPath.split("/");
        return (folderPath + "/") + b[b.length - 1];
    }

    public static String getBucketPathByImagePath(String path) {
        String[] b = path.split("/");
        String c = "";
        for (int x = 0; x < b.length - 1; x++) {
            c = c + b[x] + "/";
        }
        return c.substring(0, c.length() - 1);
    }

    public static void showToast(Context x, String s) {
        Toast.makeText(x, s, Toast.LENGTH_SHORT).show();
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < ((long) unit)) {
            return bytes + " B";
        }
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(((int) (Math.log((double) bytes) / Math.log((double) unit))) - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", new Object[]{Double.valueOf(((double) bytes) / Math.pow((double) unit, (double) ((int) (Math.log((double) bytes) / Math.log((double) unit))))), pre});
    }

    public static String b(String content) {
        return String.format(Locale.ENGLISH, "<b>%s</b>", new Object[]{content});
    }

    public static String i(String content) {
        return String.format(Locale.ENGLISH, "<i>%s</i>", new Object[]{content});
    }

    public static Spanned htmlFormat(String content, int color, boolean bold, boolean italic) {
        String res = content;
        if (color != -1) {
            res = color(color, res);
        }
        if (bold) {
            res = b(res);
        }
        if (italic) {
            res = i(res);
        }
        return html(res);
    }

    public static Spanned htmlFormat(String content, @Nullable String hexcolor, boolean bold, boolean italic) {
        String res = content;
        if (hexcolor != null) {
            res = color(hexcolor, res);
        }
        if (bold) {
            res = b(res);
        }
        if (italic) {
            res = i(res);
        }
        return html(res);
    }

    public static String color(int color, String content) {
        return String.format(Locale.ENGLISH, "<font color='%s'>%s</font>", new Object[]{content});
    }

    public static String color(String color, String content) {
        return String.format(Locale.ENGLISH, "<font color='%s'>%s</font>", new Object[]{color, content});
    }
}
