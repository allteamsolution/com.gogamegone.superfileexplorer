package org.vocrama.cleanexpert.ui.style;


import org.vocrama.cleanexpert.ui.sprite.Sprite;
import org.vocrama.cleanexpert.ui.sprite.SpriteContainer;

public class MultiplePulse extends SpriteContainer {
    public Sprite[] onCreateChild() {
        return new Sprite[]{new Pulse(), new Pulse(), new Pulse()};
    }

    public void onChildCreated(Sprite... sprites) {
        for (int i = 0; i < sprites.length; i++) {
            sprites[i].setAnimationDelay((i + 1) * 200);
        }
    }
}
