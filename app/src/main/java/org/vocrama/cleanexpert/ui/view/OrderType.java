package org.vocrama.cleanexpert.ui.view;

import java.util.Comparator;

public enum OrderType {
    NAME,
    DATE,
    SIZE,
    TYPE;

    public Comparator<FileItem> getComparator() {
        switch (ordinal()) {
            case 0:
                return new Comparator<FileItem>() {
                    public int compare(FileItem lhs, FileItem rhs) {
                        return lhs.getName().compareTo(rhs.getName());
                    }
                };
            case 1:
                return new Comparator<FileItem>() {
                    public int compare(FileItem lhs, FileItem rhs) {
                        return rhs.getDate1().compareTo(lhs.getDate1());
                    }
                };
            case 2:
                return new Comparator<FileItem>() {
                    public int compare(FileItem lhs, FileItem rhs) {
                        return (int) (rhs.getLength() - lhs.getLength());
                    }
                };
            case 3 :
                return new Comparator<FileItem>() {
                    @Override
                    public int compare(FileItem o1, FileItem o2) {
                        return(o1.isDirectory()) ? 1 : 0;
                    }
                };
            default:
                return null;
        }
    }
}
