package org.vocrama.cleanexpert.ui;

public class temp {
    int images;
    String name;

    public temp(int images, String name) {
        this.images = images;
        this.name = name;
    }

    public int getImages() {
        return this.images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
