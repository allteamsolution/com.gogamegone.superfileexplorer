package org.vocrama.cleanexpert.ui.view;

import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;

public class AppList {
    private String filePath;
    Drawable icon;
    public boolean isSelected;
    String name;
    PackageInfo packageInfo;
    private String packageName;
    long size;

    public AppList(String name, Drawable icon, long size, PackageInfo p, boolean isSelected, String filePath) {
        this.name = name;
        this.icon = icon;
        this.size = size;
        this.packageInfo = p;
        this.isSelected = isSelected;
        this.filePath = filePath;
    }

    public String getName() {
        return this.name;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public long getSize() {
        return this.size;
    }

    public PackageInfo getPackageInfo() {
        return this.packageInfo;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
