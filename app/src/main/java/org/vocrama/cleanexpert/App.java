package org.vocrama.cleanexpert;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import androidx.multidex.MultiDexApplication;

import com.gogamegone.superfileexplorer.BuildConfig;


import org.vocrama.User;
import org.vocrama.cleanexpert.ui.Constants;
import org.vocrama.cleanexpert.util.PreferenceHelper;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Base64;

public class App extends MultiDexApplication {
    public static String AUDIO_HIDE_LOCATION;
    public static String HIDDEN_FOLDER_LOCATION;
    public static String IMAGE_HIDE_LOCATION;
    public static String OTHER_FILE_HIDE_LOCATION;
    public static String UNHIDDEN_FOLDER_LOCATION;
    public static String VIDEO_HIDE_LOCATION;
    public static File folderImages;
    private static Context context;
    private static Handler handler;
    private static App sInstance;
    private static User currentUser;

    public static Application getInstance() {
        return sInstance;
    }

    public static Handler getUIHandler() {
        return handler;
    }

    public static User getCurrentUser() {
        if (currentUser == null) {
            currentUser = new User();
            currentUser.load();
        }

        return currentUser;
    }

    public static void setCurrentUser(User user) {
        currentUser = user;
    }


    public static void update() {
        currentUser = getCurrentUser();
        currentUser.save();
    }

    public static Context getContext() {
        return context;
    }


    public void onCreate() {
        super.onCreate();
        sInstance = this;
        context = getApplicationContext();
        handler = new Handler();

        if (!PreferenceHelper.contains("password")) {
            PreferenceHelper.putBoolean(Constants.IS_SOUND_ON, true);
            PreferenceHelper.putInt(Constants.SOUND_NO, 0);
        }
    }



}
