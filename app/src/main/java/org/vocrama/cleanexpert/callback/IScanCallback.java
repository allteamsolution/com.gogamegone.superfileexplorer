package org.vocrama.cleanexpert.callback;

import java.util.ArrayList;
import org.vocrama.cleanexpert.model.JunkInfo;

public interface IScanCallback {
    void onBegin();

    void onFinish(ArrayList<JunkInfo> arrayList);

    void onProgress(JunkInfo junkInfo);
}
