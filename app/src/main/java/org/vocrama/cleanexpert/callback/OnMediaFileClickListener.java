package org.vocrama.cleanexpert.callback;

public interface OnMediaFileClickListener {
    void onClick(int position);
    void onLongClick(int position);
}