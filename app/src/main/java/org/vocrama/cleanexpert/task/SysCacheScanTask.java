package org.vocrama.cleanexpert.task;

import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.IPackageStatsObserver.Stub;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.os.AsyncTask;
import android.os.RemoteException;


import com.gogamegone.superfileexplorer.R;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.callback.IScanCallback;
import org.vocrama.cleanexpert.model.JunkInfo;

public class SysCacheScanTask extends AsyncTask<Void, Void, Void> {
    private HashMap<String, String> mAppNames;
    private IScanCallback mCallback;
    private int mScanCount;
    private ArrayList<JunkInfo> mSysCaches;
    private int mTotalCount;
    private long mTotalSize = 0;

    private class PackageStatsObserver extends Stub {
        private PackageStatsObserver() {
        }

        public void onGetStatsCompleted(PackageStats packageStats, boolean succeeded) throws RemoteException {
            JunkInfo info;
            SysCacheScanTask.this.mScanCount = SysCacheScanTask.this.mScanCount + 1;
            if (packageStats != null && succeeded) {
                info = new JunkInfo();
                info.mPackageName = packageStats.packageName;
                info.name = (String) SysCacheScanTask.this.mAppNames.get(info.mPackageName);
                info.mSize = packageStats.cacheSize + packageStats.externalCacheSize;
                if (info.mSize > 0) {
                    SysCacheScanTask.this.mSysCaches.add(info);
                    SysCacheScanTask.this.mTotalSize = SysCacheScanTask.this.mTotalSize + info.mSize;
                }
                SysCacheScanTask.this.mCallback.onProgress(info);
            }
            if (SysCacheScanTask.this.mScanCount == SysCacheScanTask.this.mTotalCount) {
                info = new JunkInfo();
                info.name = App.getInstance().getString(R.string.system_cache);
                info.mSize = SysCacheScanTask.this.mTotalSize;
                Collections.sort(SysCacheScanTask.this.mSysCaches);
                Collections.reverse(SysCacheScanTask.this.mSysCaches);
                info.mChildren = SysCacheScanTask.this.mSysCaches;
                info.mIsVisible = true;
                info.mIsChild = false;
                ArrayList<JunkInfo> list = new ArrayList();
                list.add(info);
                SysCacheScanTask.this.mCallback.onFinish(list);
            }
        }
    }

    public SysCacheScanTask(IScanCallback callback) {
        this.mCallback = callback;
    }

    protected Void doInBackground(Void... params) {
        this.mCallback.onBegin();
        PackageManager pm = App.getInstance().getPackageManager();
        List<ApplicationInfo> installedPackages = pm.getInstalledApplications(256);
        Stub observer = new PackageStatsObserver();
        this.mScanCount = 0;
        this.mTotalCount = installedPackages.size();
        this.mSysCaches = new ArrayList();
        this.mAppNames = new HashMap();
        for (int i = 0; i < this.mTotalCount; i++) {
            ApplicationInfo info = (ApplicationInfo) installedPackages.get(i);
            this.mAppNames.put(info.packageName, pm.getApplicationLabel(info).toString());
            getPackageInfo(info.packageName, observer);
        }
        return null;
    }

    public void getPackageInfo(String packageName, Stub observer) {
        try {
            PackageManager pm = App.getInstance().getPackageManager();
            pm.getClass().getMethod("getPackageSizeInfo", new Class[]{String.class, IPackageStatsObserver.class}).invoke(pm, new Object[]{packageName, observer});
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        }
    }
}
