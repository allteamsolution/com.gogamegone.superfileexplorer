package org.vocrama.cleanexpert.task;

import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import org.vocrama.cleanexpert.callback.IScanCallback;
import org.vocrama.cleanexpert.model.JunkInfo;

public class OverallScanTask extends AsyncTask<Void, Void, Void> {
    private final int SCAN_LEVEL = 4;
    private JunkInfo mApkInfo;
    private IScanCallback mCallback;
    private JunkInfo mLogInfo;
    private JunkInfo mTmpInfo;

    public OverallScanTask(IScanCallback callback) {
        this.mCallback = callback;
        this.mApkInfo = new JunkInfo();
        this.mLogInfo = new JunkInfo();
        this.mTmpInfo = new JunkInfo();
    }

    private void travelPath(File root, int level) {
        if (root != null && root.exists() && level <= 4) {
            for (File file : root.listFiles()) {
                if (file.isFile()) {
                    String name = file.getName();
                    JunkInfo info = null;
                    JunkInfo junkInfo;
                    if (name.endsWith(".apk")) {
                        info = new JunkInfo();
                        info.mSize = file.length();
                        info.name = name;
                        info.mPath = file.getAbsolutePath();
                        info.mIsChild = false;
                        info.mIsVisible = true;
                        this.mApkInfo.mChildren.add(info);
                        junkInfo = this.mApkInfo;
                        junkInfo.mSize += info.mSize;
                    } else if (name.endsWith(".log")) {
                        info = new JunkInfo();
                        info.mSize = file.length();
                        info.name = name;
                        info.mPath = file.getAbsolutePath();
                        info.mIsChild = false;
                        info.mIsVisible = true;
                        this.mLogInfo.mChildren.add(info);
                        junkInfo = this.mLogInfo;
                        junkInfo.mSize += info.mSize;
                    } else if (name.endsWith(".tmp") || name.endsWith(".temp")) {
                        info = new JunkInfo();
                        info.mSize = file.length();
                        info.name = name;
                        info.mPath = file.getAbsolutePath();
                        info.mIsChild = false;
                        info.mIsVisible = true;
                        this.mTmpInfo.mChildren.add(info);
                        junkInfo = this.mTmpInfo;
                        junkInfo.mSize += info.mSize;
                    }
                    if (info != null) {
                        this.mCallback.onProgress(info);
                    }
                } else if (level < 4) {
                    travelPath(file, level + 1);
                }
            }
        }
    }

    protected Void doInBackground(Void... params) {
        this.mCallback.onBegin();
        File externalDir = Environment.getExternalStorageDirectory();
        if (externalDir != null) {
            travelPath(externalDir, 0);
        }
        ArrayList<JunkInfo> list = new ArrayList();
        if (this.mApkInfo.mSize > 0) {
            Collections.sort(this.mApkInfo.mChildren);
            Collections.reverse(this.mApkInfo.mChildren);
            list.add(this.mApkInfo);
        }
        if (this.mLogInfo.mSize > 0) {
            Collections.sort(this.mLogInfo.mChildren);
            Collections.reverse(this.mLogInfo.mChildren);
            list.add(this.mLogInfo);
        }
        if (this.mTmpInfo.mSize > 0) {
            Collections.sort(this.mTmpInfo.mChildren);
            Collections.reverse(this.mTmpInfo.mChildren);
            list.add(this.mTmpInfo);
        }
        this.mCallback.onFinish(list);
        return null;
    }
}
