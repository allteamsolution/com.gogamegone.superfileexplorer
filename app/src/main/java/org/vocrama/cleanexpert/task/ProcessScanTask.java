package org.vocrama.cleanexpert.task;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;

import com.jaredrummler.android.processes.AndroidProcesses;

import com.jaredrummler.android.processes.models.AndroidAppProcess;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.callback.IScanCallback;
import org.vocrama.cleanexpert.model.JunkInfo;

public class ProcessScanTask extends AsyncTask<Void, Void, Void> {
    private IScanCallback mCallback;

    public ProcessScanTask(IScanCallback mCallback) {
        this.mCallback = mCallback;
    }

    protected Void doInBackground(Void... params) {
        this.mCallback.onBegin();
        List<AndroidAppProcess> processes = AndroidProcesses.getRunningAppProcesses();
        ArrayList<JunkInfo> junks = new ArrayList();
        for (AndroidAppProcess process : processes) {
            JunkInfo info = new JunkInfo();
            info.mIsChild = false;
            info.mIsVisible = true;
            info.mPackageName = process.getPackageName();
            try {
                info.mSize = process.statm().getResidentSetSize();
                try {
                    info.name = process.getPackageInfo(App.getInstance(), 0).applicationInfo.loadLabel(App.getInstance().getPackageManager()).toString();
                    this.mCallback.onProgress(info);
                    junks.add(info);
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        Collections.sort(junks);
        Collections.reverse(junks);
        this.mCallback.onFinish(junks);
        return null;
    }
}
