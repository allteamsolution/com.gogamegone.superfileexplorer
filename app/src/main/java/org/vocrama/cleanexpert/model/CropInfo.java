package org.vocrama.cleanexpert.model;

import android.graphics.Bitmap;
import org.vocrama.cleanexpert.ui.Constants.app;
import org.vocrama.cleanexpert.util.BitmapLoadUtils;

public class CropInfo {
    private float cropHeight;
    private float cropLeft;
    private float cropTop;
    private float cropWidth;
    private float scale;
    private float viewBitmapWidth;
    private float viewImageLeft;
    private float viewImageTop;

    public CropInfo(float scale, float viewBitmapWidth, float viewImageTop, float viewImageLeft, float cropTop, float cropLeft, float cropWidth, float cropHeight) {
        this.scale = scale;
        this.viewBitmapWidth = viewBitmapWidth;
        this.viewImageTop = viewImageTop;
        this.viewImageLeft = viewImageLeft;
        this.cropTop = cropTop;
        this.cropLeft = cropLeft;
        this.cropWidth = cropWidth;
        this.cropHeight = cropHeight;
    }

    public Bitmap getCroppedImage(String path) {
        return getCroppedImage(path, app.APP_CLEANER);
    }

    public Bitmap getCroppedImage(String path, int reqSize) {
        return getCroppedImage(BitmapLoadUtils.decode(path, reqSize, reqSize));
    }

    public Bitmap getCroppedImage(Bitmap bitmap) {
        float scale = this.scale * (this.viewBitmapWidth / ((float) bitmap.getWidth()));
        float x = Math.abs(this.viewImageLeft - this.cropLeft) / scale;
        float y = Math.abs(this.viewImageTop - this.cropTop) / scale;
        float actualCropWidth = this.cropWidth / scale;
        float actualCropHeight = this.cropHeight / scale;
        if (x < 0.0f) {
            x = 0.0f;
        }
        if (y < 0.0f) {
            y = 0.0f;
        }
        if (y + actualCropHeight > ((float) bitmap.getHeight())) {
            actualCropHeight = ((float) bitmap.getHeight()) - y;
        }
        if (x + actualCropWidth > ((float) bitmap.getWidth())) {
            actualCropWidth = ((float) bitmap.getWidth()) - x;
        }
        return Bitmap.createBitmap(bitmap, (int) x, (int) y, (int) actualCropWidth, (int) actualCropHeight);
    }

    public float getScale() {
        return this.scale;
    }

    public float getViewBitmapWidth() {
        return this.viewBitmapWidth;
    }

    public float getViewImageTop() {
        return this.viewImageTop;
    }

    public float getViewImageLeft() {
        return this.viewImageLeft;
    }

    public float getCropTop() {
        return this.cropTop;
    }

    public float getCropLeft() {
        return this.cropLeft;
    }

    public float getCropWidth() {
        return this.cropWidth;
    }

    public float getCropHeight() {
        return this.cropHeight;
    }
}
