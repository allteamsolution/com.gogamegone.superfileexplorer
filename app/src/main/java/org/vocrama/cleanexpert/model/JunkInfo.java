package org.vocrama.cleanexpert.model;

import com.gogamegone.superfileexplorer.R;

import java.util.ArrayList;

import org.vocrama.cleanexpert.App;

public class JunkInfo implements Comparable<JunkInfo> {
    public ArrayList<JunkInfo> mChildren = new ArrayList();
    public boolean mIsChild = true;
    public boolean mIsVisible = false;
    public String mPackageName;
    public String mPath;
    public long mSize;
    public String name;

    public int compareTo(JunkInfo another) {
        String top = App.getInstance().getString(R.string.system_cache);
        if (this.name != null && this.name.equals(top)) {
            return 1;
        }
        if (another.name != null && another.name.equals(top)) {
            return -1;
        }
        if (this.mSize > another.mSize) {
            return 1;
        }
        if (this.mSize < another.mSize) {
            return -1;
        }
        return 0;
    }
}
