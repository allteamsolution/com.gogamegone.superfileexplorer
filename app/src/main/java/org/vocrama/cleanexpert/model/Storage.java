package org.vocrama.cleanexpert.model;

import android.graphics.drawable.Drawable;

import java.util.Objects;

public class Storage {

    private int id;
    private String name;
    private Drawable image;
    private long total;
    private long used;

    public Storage(int id, String name, Drawable image, long total, long used) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.total = total;
        this.used = used;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getUsed() {
        return used;
    }

    public void setUsed(long used) {
        this.used = used;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Storage storage = (Storage) o;
        return id == storage.id &&
                total == storage.total &&
                used == storage.used &&
                Objects.equals(name, storage.name) &&
                Objects.equals(image, storage.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, image, total, used);
    }
}
