package org.vocrama;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.billing.BillingHelper;
import org.vocrama.cleanexpert.ui.BaseTheme;


public class SettingsActivity extends BaseTheme {

    private TextView tvPrivacy, tvOurSite, tvSupport, tvRemoveAds, tvTerms;
    private FrameLayout flNative;
    private ImageButton ibBack;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_great);
        initViews();
        checkViewBySubscribe();
        initListeners();

        if (!BillingHelper.isSubscriber()) {
            AdHelper.loadProBanner(flNative);
        }
    }

    private void initListeners() {
        tvPrivacy.setOnClickListener(v -> startActivity(AppInfoHelper.openPrivacyPolicySite()));
        tvOurSite.setOnClickListener(v -> startActivity(AppInfoHelper.openMainSite()));
        tvSupport.setOnClickListener(v -> startActivity(AppInfoHelper.sendMail()));
        tvRemoveAds.setOnClickListener(v -> BillingHelper.openPurchaseActivity(SettingsActivity.this));
        tvTerms.setOnClickListener(v -> startActivity(AppInfoHelper.openTermsOfUse()));
        ibBack.setOnClickListener(v -> finish());
    }

    private void checkViewBySubscribe() {
        tvRemoveAds.setVisibility(BillingHelper.isSubscriber() ? View.GONE : View.VISIBLE);
    }

    private void initViews() {
        tvPrivacy = findViewById(R.id.tvPrivacy);
        tvOurSite = findViewById(R.id.tvOurSite);
        tvSupport = findViewById(R.id.tvSupport);
        tvRemoveAds = findViewById(R.id.tvRemoveAds);
        tvTerms = findViewById(R.id.tvTerms);
        flNative = findViewById(R.id.flNavite);
        ibBack = findViewById(R.id.ibBack);
    }
}