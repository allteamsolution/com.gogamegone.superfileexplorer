package org.vocrama;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.gogamegone.superfileexplorer.BuildConfig;
import com.gogamegone.superfileexplorer.R;

import org.vocrama.billing.BillingHelper;
import org.vocrama.cleanexpert.App;

import java.util.ArrayList;
import java.util.List;

public class AdHelper {
    private static boolean isTestMode = BuildConfig.DEBUG;
    private static String placementFB;



//    public static void addNativeWidgetFacebookBanner(final ViewGroup parent) {
//        placementFB = UMLGs.getServerConfig().getFb_ad_space_uml();
//        if (placementFB == null ||
//                placementFB.length() == 0) {
//            loadProBanner(parent);
//            return;
//        }
//
//        initFb();
//        final NativeAd nativeAd = new NativeAd(App.getContext(), placementFB);
//        if (isTestMode) {
//            AdSettings.addTestDevice("9a7b33c0-26d9-4f58-b04f-21a43b6660ec");
//        }
//        NativeAdListener nativeAdListener = new NativeAdListener() {
//
//            @Override
//            public void onMediaDownloaded(Ad ad) {
//            }
//
//            @Override
//            public void onError(Ad ad, AdError adError) {
//                loadProBanner(parent);
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//                if (ad == null || nativeAd != ad) {
//                    return;
//                }
//                parent.setBackground(null);
//                onFbNativeBannerLoaded(nativeAd, parent);
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
//            }
//        };
//
//        nativeAd.loadAd(
//                nativeAd.buildLoadAdConfig().withAdListener(nativeAdListener).build());
//    }
//
//
//    private static void onFbNativeBannerLoaded(NativeAd nativeAd, ViewGroup parent) {
//        nativeAd.unregisterView();
//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//
//        View adView = inflater.inflate(R.layout.native_banner, parent, false);
//        parent.removeAllViews();
//        parent.addView(adView);
//
//        MediaView nativeAdIcon = adView.findViewById(R.id.native_ad_icon_banner);
//        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action_banner);
//        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
//        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title_banner);
//        nativeAdTitle.setText(nativeAd.getAdvertiserName());
//        LinearLayout adChoicesContainer = adView.findViewById(R.id.ad_choices_container_banner);
//        NativeAdLayout mNativeBannerAdContainer = adView.findViewById(R.id.parent);
//        AdOptionsView adOptionsView =
//                new AdOptionsView(
//                        parent.getContext(),
//                        nativeAd,
//                        mNativeBannerAdContainer);
//        adChoicesContainer.removeAllViews();
//        adChoicesContainer.addView(adOptionsView);
//
//        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body_banner);
//        nativeAdBody.setText(nativeAd.getAdBodyText());
//        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label_banner);
//        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());
//        List<View> clickableViews = new ArrayList<>();
//        clickableViews.add(nativeAdTitle);
//        clickableViews.add(nativeAdCallToAction);
//        MediaView nativeAdMedia = adView.findViewById(R.id.native_add_media);
//        nativeAd.registerViewForInteraction(
//                adView,
//                nativeAdMedia,
//                nativeAdIcon,
//                clickableViews);
//        parent.setVisibility(View.VISIBLE);
//
//    }
//
//    public static void initFb() {
//        if (placementFB != null &&
//                placementFB.length() > 0) {
//            AudienceNetworkAds.initialize(App.getContext());
//        }
//    }

    public static void loadProBanner(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(App.getContext());
        View adView = inflater.inflate(R.layout.pro_banner, parent, false);
        parent.removeAllViews();
        parent.addView(adView);
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BillingHelper.openPurchaseActivity(parent.getContext());
            }
        });
    }
}