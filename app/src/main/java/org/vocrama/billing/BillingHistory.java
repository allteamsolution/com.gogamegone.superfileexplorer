package org.vocrama.billing;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.gen.rxbilling.client.RxBilling;
import com.gen.rxbilling.flow.RxBillingFlow;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BillingHistory {

    public interface BillingHistoryView {

        void onGetHistorySubscribe(List<Purchase> purchases);
        void onGetHistoryPurchase(List<Purchase> purchases);
        void onErrorBilling(Throwable throwable);
    }

    private final BillingHistoryView billingHistoryView;
    private CompositeDisposable compositeDisposable;
    private final RxBilling rxBilling;
    private final RxBillingFlow rxBillingFlow;

    public BillingHistory(BillingHistoryView billingView, RxBilling rxBilling, RxBillingFlow rxBillingFlow) {
        this.billingHistoryView = billingView;
        this.rxBilling = rxBilling;
        this.rxBillingFlow = rxBillingFlow;
    }
    public void onCreate() {
        compositeDisposable = new CompositeDisposable();
    }

    public void getHistorySubscribe() {
        compositeDisposable.add(rxBilling.getPurchases(BillingClient.SkuType.SUBS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(billingHistoryView::onGetHistorySubscribe, billingHistoryView::onErrorBilling)
        );
    }

    public void getHistoryPurchase() {
        compositeDisposable.add(rxBilling.getPurchases(BillingClient.SkuType.INAPP)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(billingHistoryView::onGetHistoryPurchase, billingHistoryView::onErrorBilling)
        );
    }

    public void onDestroy() {
        compositeDisposable.dispose();
    }
}
