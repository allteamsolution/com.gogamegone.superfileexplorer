package org.vocrama.billing;

import android.content.Context;
import android.content.Intent;

import com.android.billingclient.api.Purchase;

import org.vocrama.cleanexpert.App;
import org.vocrama.pro.OfferActivity;
import org.vocrama.pro.ProActivity;
import org.vocrama.pro.TrialActivity;
import org.vocrama.pro.TrialActivityTwo;
import org.vocrama.pro.TrialActivityZero;

import java.util.List;

public class BillingHelper {




    public static final String SUBSCRIBE_WEEK = "subscribe_week";
    public static final String SUBSCRIBE_MONTH = "subscribe_month";
    public static final String SUBSCRIBE_YEAR = "subscribe_year";

    public static final String SUBSCRIBE_OFFER = "subscribe_offer";

    public static final String SUBSCRIBE_YEAR_TRIAL = "subscribe_year_trial";


    public static boolean isActive(String skuId, List<Purchase> purchases) {
        for (Purchase purchase : purchases) {
            if (skuId.equalsIgnoreCase(purchase.getSku())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSubscriber() {
        return (App.getCurrentUser().isBasicBuy() || App.getCurrentUser().isStartBuy() || App.getCurrentUser().isSuperBuy() || App.getCurrentUser().isOfferBuy() || App.getCurrentUser().isSuperTrialBuy());
    }

    public static void openOfferActivity(Context context) {
        Intent intent = new Intent(context, OfferActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public static void openTrialOffer(Context context) {

        Intent intent;
        int rand = new java.util.Random().nextInt(3);
        switch (rand){
            case 0:
                intent = new Intent(context, TrialActivity.class);
                break;
            case 1:
                intent = new Intent(context, TrialActivityTwo.class);
                break;
            case 2:
                intent = new Intent(context, TrialActivityZero.class);
                break;

            default:
                intent = new Intent(context, TrialActivity.class);
                break;
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void openPurchaseActivity(Context context) {
        Intent intent = new Intent(context, ProActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
