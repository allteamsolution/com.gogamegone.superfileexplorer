package org.vocrama;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.TextView;

import com.gogamegone.superfileexplorer.R;

public class GradientTextViewTwo extends TextView  {

    public GradientTextViewTwo(Context context) {
        super(context);
    }

    public GradientTextViewTwo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GradientTextViewTwo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        //Setting the gradient if layout is changed
        if (changed) {
            getPaint().setShader(new LinearGradient(0, 0, getWidth(), getHeight(),
                    ContextCompat.getColor(getContext(), R.color.startGradient2),
                    ContextCompat.getColor(getContext(), R.color.endGradient2),
                    Shader.TileMode.CLAMP));
        }
    }
}