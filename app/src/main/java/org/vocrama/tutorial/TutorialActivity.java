package org.vocrama.tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.gogamegone.superfileexplorer.R;

import org.vocrama.billing.BillingHelper;
import org.vocrama.cleanexpert.App;
import org.vocrama.cleanexpert.ui.StartActivity;


public class TutorialActivity extends AppCompatActivity {

    public static int CURRENT_PAGE;
    private TutorialViewPagerAdapter tutorialViewPagerAdapter;
    private ViewPager viewPager;

    private TextView tvSkipTutorial, tvNext;
    private LinearLayout btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        viewPager = findViewById(R.id.viewPager);
        tvSkipTutorial = findViewById(R.id.tvSkipTutorial);
        btnNext = findViewById(R.id.btnNext);
        tvNext = findViewById(R.id.tvNext);
        tvSkipTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getCurrentUser().setTutorialFinished(true);
                App.update();
                startActivity(new Intent(TutorialActivity.this, StartActivity.class));

                if (!BillingHelper.isSubscriber()) {
                    BillingHelper.openTrialOffer(TutorialActivity.this);
                }
                finish();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CURRENT_PAGE = CURRENT_PAGE + 1;
                setCurrentPage(CURRENT_PAGE);
            }
        });
        CURRENT_PAGE = 0;
        initViewPager();
        getSupportFragmentManager().beginTransaction().replace(R.id.viewPager, new FirstTutorialFragment()).commit();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }


    private void initViewPager() {
        tutorialViewPagerAdapter = new TutorialViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tutorialViewPagerAdapter);
        viewPager.setOffscreenPageLimit(tutorialViewPagerAdapter.NUM_PAGES);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                CURRENT_PAGE = position;
                setCurrentPage(CURRENT_PAGE);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    public void setCurrentPage(int currentPage) {
        if (currentPage >= 1) {
            tvNext.setText(getResources().getString(R.string.start));
        } else {
            tvNext.setText(getResources().getString(R.string.next));
        }
        if (currentPage != 2) {
            CURRENT_PAGE = currentPage;
            viewPager.setCurrentItem(CURRENT_PAGE);
        } else {
            App.getCurrentUser().setTutorialFinished(true);
            App.update();
            startActivity(new Intent(this, StartActivity.class));
            if (!BillingHelper.isSubscriber()) {
                BillingHelper.openTrialOffer(this);
            }
            finish();
        }
    }


}
