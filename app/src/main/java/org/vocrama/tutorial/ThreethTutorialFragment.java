package org.vocrama.tutorial;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;

import com.gogamegone.superfileexplorer.R;

public class ThreethTutorialFragment extends Fragment {

    FrameLayout flGoToGP;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tutorial_fragment_three, container, false);
        flGoToGP = view.findViewById(R.id.flGoToGP);

//        flGoToGP.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String app = "sR1giLgvDKzlJxhR3sY0rd+7oPu6At52KpOF8XE7LPQJoN5kIqnjljV6HB/rh3UWz2ljaVaK5Zfj\n" +
//                        "    LeveiCtYmPD2y6KaoX43A8W8SgNxJ7OwgSXwgUgZZl4izdah645pZZTkshqwFkcEjYhYqdjtwA==";
//                try {
//                    app = UMLDecoder.decMsg(app);
//                } catch (Throwable t) {
//                }
//                final String finalApp = app;
//
//                boolean isAppInstalled = appInstalledOrNot(finalApp);
//                if (isAppInstalled) {
//                    Intent LaunchIntent = getContext().getPackageManager()
//                            .getLaunchIntentForPackage(finalApp);
//                    startActivity(LaunchIntent);
//                } else {
//
//                    String url = "https://play.google.com/store/apps/details?id=" + finalApp;
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    i.setData(Uri.parse(url));
//                    startActivity(i);
//                }
//            }
//        });

        return view;
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getContext().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

}
