package org.vocrama.tutorial;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TutorialViewPagerAdapter extends FragmentPagerAdapter {

    public static final int NUM_PAGES = 2;

    public TutorialViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FirstTutorialFragment();
            case 1:
                return new SecondTutorialFragment();
//            case 2:
//                return new ThreethTutorialFragment();

            default:
                break;
        }
        return null;
    }
}
