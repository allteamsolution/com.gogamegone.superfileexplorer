package org.vocrama;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.vocrama.cleanexpert.App;

public class User {
    private static final String START_BUY = "START_BUY";
    private static final String BASIC_BUY = "BASIC_BUY";
    private static final String SUPER_BUY = "SUPER_BUY";
    private static final String UNLIM_BUY = "UNLIM_BUY";
    private static final String OFFER_BUY = "OFFER_BUY";
    private static final String SUPER_TRIAL_BUY = "SUPER_TRIAL_BUY";
    private static final String FIRST_LAUNCH = "FIRST_LAUNCH";
    private static final String COUNT_OF_OFFER_SHOWS = "COUNT_OF_OFFER_SHOWS";
    private static final String INSTALL_TIME = "INSTALL_TIME";
    private static final String IS_PRO_CHOOSED = "is_pro_choosed";
    private static final String IS_TUTORIAL_FINISHED = "is_tutorial_finished";
    final private static String APP_INSTELLED_FIRST_TIME = "APP_INSTELLED_FIRST_TIME";
    private static final String IS_APP_RATED = "IS_APP_RATED";
    private static final String SELECTED_THEME_MODE = "SELECTED_THEME_MODE";

    private boolean startBuy;
    private boolean basicBuy;
    private boolean superBuy;
    private boolean offerBuy;
    private boolean superTrialBuy;
    private boolean unlimBuy;
    private boolean firstLaunch;
    private boolean isTutorialFinished;
    private int countOfOfferShows;
    private long installTime;
    private boolean isProChoosed;
    private int themeMode;
    private boolean isAppRated;


    public void load() {
        final SharedPreferences storage = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        startBuy = storage.getBoolean(START_BUY, false);
        basicBuy = storage.getBoolean(BASIC_BUY, false);
        superBuy = storage.getBoolean(SUPER_BUY, false);
        offerBuy = storage.getBoolean(OFFER_BUY, false);
        superTrialBuy = storage.getBoolean(SUPER_TRIAL_BUY, false);
        unlimBuy = storage.getBoolean(UNLIM_BUY, false);
        firstLaunch = storage.getBoolean(FIRST_LAUNCH, false);
        countOfOfferShows = storage.getInt(COUNT_OF_OFFER_SHOWS, 0);
        installTime = storage.getLong(INSTALL_TIME, System.currentTimeMillis());
        isProChoosed = storage.getBoolean(IS_PRO_CHOOSED, isProChoosed);
        isTutorialFinished = storage.getBoolean(IS_TUTORIAL_FINISHED, false);
        isAppRated = storage.getBoolean(IS_APP_RATED, false);
        themeMode = storage.getInt(SELECTED_THEME_MODE, 1);
    }

    public void save() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putLong(INSTALL_TIME, installTime);
        storage.putInt(COUNT_OF_OFFER_SHOWS, countOfOfferShows);
        storage.putBoolean(IS_TUTORIAL_FINISHED, isTutorialFinished);
        storage.putInt(SELECTED_THEME_MODE, themeMode);
        storage.putBoolean(FIRST_LAUNCH, firstLaunch);
        storage.putBoolean(IS_APP_RATED, isAppRated);
        storage.putBoolean(START_BUY, startBuy);
        storage.putBoolean(BASIC_BUY, basicBuy);
        storage.putBoolean(SUPER_BUY, superBuy);
        storage.putBoolean(OFFER_BUY, offerBuy);
        storage.putBoolean(SUPER_TRIAL_BUY, superTrialBuy);
        storage.putBoolean(UNLIM_BUY, unlimBuy);
        storage.apply();
    }

    public int getThemeMode() {
        return themeMode;
    }

    public void setThemeMode(int themeMode) {
        this.themeMode = themeMode;
    }

    public void saveTheme() {
        final SharedPreferences.Editor storageEditor = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storageEditor.apply();
    }

    public boolean isAppRated() {
        return isAppRated;
    }

    public void setAppRated(boolean appRated) {
        isAppRated = appRated;
    }

    public boolean registerMoreThenHoursAgo(int hours) {
        return App.getCurrentUser().getInstallTime() < System.currentTimeMillis() - 1000 * 60 * 60 * hours;
    }

    public boolean isTutorialFinished() {
        return isTutorialFinished;
    }

    public void setTutorialFinished(boolean tutorialFinished) {
        isTutorialFinished = tutorialFinished;
    }

    public int getCountOfOfferShows() {
        return countOfOfferShows;
    }

    public void setCountOfOfferShows(int countOfOfferShows) {
        this.countOfOfferShows = countOfOfferShows;
    }

    public long getInstallTime() {
        return installTime;
    }

    public void setFirstLaunch(boolean firstLaunch) {
        this.firstLaunch = firstLaunch;
    }

    public boolean isStartBuy() {
        return startBuy;
    }

    public void setStartBuy(boolean startBuy) {
        this.startBuy = startBuy;
    }

    public boolean isBasicBuy() {
        return basicBuy;
    }

    public void setBasicBuy(boolean basicBuy) {
        this.basicBuy = basicBuy;
    }

    public boolean isSuperBuy() {
        return superBuy;
    }

    public void setSuperBuy(boolean superBuy) {
        this.superBuy = superBuy;
    }

    public boolean isOfferBuy() {
        return offerBuy;
    }

    public void setOfferBuy(boolean offerBuy) {
        this.offerBuy = offerBuy;
    }

    public boolean isSuperTrialBuy() {
        return superTrialBuy;
    }

    public void setSuperTrialBuy(boolean superTrialBuy) {
        this.superTrialBuy = superTrialBuy;
    }
}

