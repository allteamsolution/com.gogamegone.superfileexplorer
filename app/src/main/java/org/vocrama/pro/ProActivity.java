package org.vocrama.pro;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.SkuDetails;
import com.gen.rxbilling.client.RxBillingImpl;
import com.gen.rxbilling.connection.BillingClientFactory;
import com.gen.rxbilling.connection.BillingServiceFactory;
import com.gen.rxbilling.connection.RepeatConnectionTransformer;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.gogamegone.superfileexplorer.BuildConfig;
import com.gogamegone.superfileexplorer.R;
import com.google.android.material.snackbar.Snackbar;

import org.vocrama.AppInfoHelper;
import org.vocrama.analytics.AnalyticsEvent;
import org.vocrama.analytics.FlurryAnalytics;
import org.vocrama.billing.BillingHelper;
import org.vocrama.billing.BillingPresenter;
import org.vocrama.cleanexpert.App;

import java.util.ArrayList;
import java.util.List;


public class ProActivity extends AppCompatActivity implements BillingPresenter.BillingView {

    private TextView tvStart;
    private TextView tvBasic;
    private TextView tvPremium;
    private TextView tvOldPremium;
    private TextView tvPremiumPerWeek;
    private TextView tvBasicPerWeek;
    private TextView tvOldBasic;
    private TextView tvOldStart;
    private FrameLayout flRoot;
    private TextView purchaseDesr;
    private FrameLayout flprogress;
    private LinearLayout llStart, llBasic, llPremium;
    private FrameLayout flBack;


    private BillingPresenter billingPresenter;
    private SkuDetails skuDetailsStart;
    private SkuDetails skuDetailsBasic;
    private SkuDetails skuDetailsSuper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pro);
        FlurryAnalytics.sendEvent(AnalyticsEvent.PURCHASE_SCREEN_OPEN_0);
        findViews();
        if (BuildConfig.DEBUG) {
            flprogress.setVisibility(View.GONE);
        }

        billingPresenter = new BillingPresenter(
                this, new RxBillingImpl(
                new BillingClientFactory(
                        this.getApplicationContext(),
                        new RepeatConnectionTransformer<>()
                )
        ),
                new RxBillingFlow(
                        getApplicationContext(),
                        new BillingServiceFactory(
                                this,
                                new RepeatConnectionTransformer<>()
                        )
                )
        );
        billingPresenter.onCreate();
        init();
        List<String> list = new ArrayList<>();

        list.add("cancel");
        SpannableString ss = AppInfoHelper.makePrivacyUnderline(purchaseDesr.getText(), list);
        purchaseDesr.setText(ss);
        purchaseDesr.setMovementMethod(LinkMovementMethod.getInstance());
        purchaseDesr.setHighlightColor(Color.TRANSPARENT);

    }

    private void findViews() {
        tvStart = findViewById(R.id.tvStart);
        tvBasic = findViewById(R.id.tvBasic);
        tvPremium = findViewById(R.id.tvPremium);
        tvOldPremium = findViewById(R.id.tvOldPremium);
        tvPremiumPerWeek = findViewById(R.id.tvPremiumPerWeek);
        tvBasicPerWeek = findViewById(R.id.tvBasicPerWeek);
        tvOldBasic = findViewById(R.id.tvOldBasic);
        tvOldStart = findViewById(R.id.tvOldStart);
        flRoot = findViewById(R.id.flRoot);
        purchaseDesr = findViewById(R.id.purchaseDesr);
        flprogress = findViewById(R.id.flprogress);
        llStart = findViewById(R.id.llStart);
        llBasic = findViewById(R.id.llBasic);
        llPremium = findViewById(R.id.llPremium);
        flBack = findViewById(R.id.flBack);
        llStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlurryAnalytics.sendEvent(AnalyticsEvent.PURCHASE_SCREEN_CLICKBUY_0);
                if (skuDetailsStart != null) {
                    billingPresenter.buy(skuDetailsStart, ProActivity.this);
                }
            }
        });

        llBasic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlurryAnalytics.sendEvent(AnalyticsEvent.PURCHASE_SCREEN_CLICKBUY_0);
                if (skuDetailsBasic != null) {
                    billingPresenter.buy(skuDetailsBasic, ProActivity.this);
                }
            }
        });
        llPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skuDetailsSuper != null) {
                    billingPresenter.buy(skuDetailsSuper, ProActivity.this);
                }
            }
        });

        flBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

    }


    private void init() {
        List<String> preloadSkuList = new ArrayList<>();
        preloadSkuList.add(BillingHelper.SUBSCRIBE_WEEK);
        preloadSkuList.add(BillingHelper.SUBSCRIBE_MONTH);
        preloadSkuList.add(BillingHelper.SUBSCRIBE_YEAR);
        billingPresenter.loadSubscribeSku(preloadSkuList);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        billingPresenter.handleBillingResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        billingPresenter.onDestroy();
        App.getCurrentUser().save();
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        FlurryAnalytics.sendEvent(AnalyticsEvent.PURCHASE_SCREEN_CLOSE_0);
        super.onBackPressed();
    }


    @Override
    public void onGetSubscribeSku(List<SkuDetails> skuDetails) {
        flprogress.setVisibility(View.GONE);

        for (SkuDetails sku : skuDetails) {
            if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_WEEK)) {
                this.skuDetailsStart = sku;
                float currentPrice = (float) (skuDetailsStart.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));
                tvStart.setText(String.format("%.2f", currentPrice) + " " + skuDetailsStart.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 75;
                String text = String.format("%.2f", oldPrice) + " " + skuDetailsStart.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.weekly);
                tvOldStart.setText(text);
                tvOldStart.setPaintFlags(tvOldStart.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_MONTH)) {
                this.skuDetailsBasic = sku;
                float currentPrice = (float) (skuDetailsBasic.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));
                tvBasic.setText(String.format("%.2f", currentPrice) + " " + skuDetailsBasic.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.monthly));
                tvBasicPerWeek.setText(String.format("%.2f", currentPrice / 4f) + " " + skuDetailsBasic.getPriceCurrencyCode().toLowerCase() + " " + App.getContext().getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 75;
                String text = String.format("%.2f", oldPrice) + " " + skuDetailsBasic.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.monthly);
                tvOldBasic.setText(text);
                tvOldBasic.setPaintFlags(tvOldBasic.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_YEAR)) {
                this.skuDetailsSuper = sku;
                float currentPrice = (float) (skuDetailsSuper.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));
                tvPremium.setText(String.format("%.2f", currentPrice) + " " + skuDetailsSuper.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually));
                tvPremiumPerWeek.setText(String.format("%.2f", currentPrice / 52f) + " " + skuDetailsSuper.getPriceCurrencyCode().toLowerCase() + " " + App.getContext().getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 75;
                String text = String.format("%.2f", oldPrice) + " " + skuDetailsSuper.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually);
                tvOldPremium.setText(text);
                tvOldPremium.setPaintFlags(tvOldPremium.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        }
    }


    @Override
    public void onGetPurchaseSku(List<SkuDetails> skuDetails) {

    }

    @Override
    public void onErrorBilling(Throwable throwable) {
        if (!BuildConfig.DEBUG) {
            Snackbar.make(flRoot, getString(R.string.error_internet), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getResources().getColor(R.color.white))
                    .setAction(R.string.back, v -> finish())
                    .show();
        }
    }

    @Override
    public void onBuyLoadSuccess() {

    }

    @Override
    public void onPaySuccess(String sku) {
        if (BillingHelper.SUBSCRIBE_WEEK.contains(sku)) {
            App.getCurrentUser().setStartBuy(true);
        } else if (BillingHelper.SUBSCRIBE_MONTH.contains(sku)) {
            App.getCurrentUser().setBasicBuy(true);
        } else if (BillingHelper.SUBSCRIBE_YEAR.contains(sku)) {
            App.getCurrentUser().setSuperBuy(true);
        }
        App.update();
        finish();
    }

    @Override
    public void onPayFailed(String sku) {

    }
}