package org.vocrama.pro;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.SkuDetails;
import com.gen.rxbilling.client.RxBillingImpl;
import com.gen.rxbilling.connection.BillingClientFactory;
import com.gen.rxbilling.connection.BillingServiceFactory;
import com.gen.rxbilling.connection.RepeatConnectionTransformer;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.gogamegone.superfileexplorer.BuildConfig;
import com.gogamegone.superfileexplorer.R;
import com.google.android.material.snackbar.Snackbar;

import org.vocrama.AppInfoHelper;
import org.vocrama.analytics.AnalyticsEvent;
import org.vocrama.analytics.FlurryAnalytics;
import org.vocrama.billing.BillingHelper;
import org.vocrama.billing.BillingPresenter;
import org.vocrama.cleanexpert.App;

import java.util.ArrayList;
import java.util.List;


public class TrialActivity extends AppCompatActivity implements BillingPresenter.BillingView {


    private FrameLayout flProgressBar;
    private TextView tvPrice;
    private TextView tvOldPrice;
    private TextView tvPricePerWeek;
    private TextView tvAutomaticPay;
    private FrameLayout flRoot;
    private FrameLayout buttonSubscribe, buttonTrial;
    private LinearLayout llClose;


    private BillingPresenter billingPresenter;
    private SkuDetails skuDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trial_1);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FlurryAnalytics.sendEvent(AnalyticsEvent.TRIAL_OFFER_SCREEN_OPEN_1);
        initViews();

        if (BuildConfig.DEBUG) {
            flProgressBar.setVisibility(View.GONE);
        }
        billingPresenter = new BillingPresenter(
                this, new RxBillingImpl(
                new BillingClientFactory(
                        this.getApplicationContext(),
                        new RepeatConnectionTransformer<>()
                )
        ),
                new RxBillingFlow(
                        getApplicationContext(),
                        new BillingServiceFactory(
                                this,
                                new RepeatConnectionTransformer<>()
                        )
                )
        );
        billingPresenter.onCreate();
        init();

    }

    private void initViews() {
        flProgressBar = findViewById(R.id.flProgressBar);
        tvPrice = findViewById(R.id.tvPrice);
        tvOldPrice = findViewById(R.id.tvOldPrice);
        tvPricePerWeek = findViewById(R.id.tvPricePerWeek);
        tvAutomaticPay = findViewById(R.id.tvAutomaticPay);
        flRoot = findViewById(R.id.flRoot);
        buttonSubscribe = findViewById(R.id.buttonSubscribe);
        buttonTrial = findViewById(R.id.buttonTrial);
        llClose = findViewById(R.id.llClose);

        buttonSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skuDetails != null) {
                    billingPresenter.buy(skuDetails, TrialActivity.this);
                    FlurryAnalytics.sendEvent(AnalyticsEvent.TRIAL_OFFER_SCREEN_CLICKBUY_1);
                }

            }
        });
        buttonTrial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skuDetails != null) {
                    billingPresenter.buy(skuDetails, TrialActivity.this);
                    FlurryAnalytics.sendEvent(AnalyticsEvent.TRIAL_OFFER_SCREEN_CLICKBUY_1);
                }

            }
        });
        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlurryAnalytics.sendEvent(AnalyticsEvent.TRIAL_OFFER_SCREEN_CLOSE_1);
                finish();
            }
        });


    }

    private void init() {
        List<String> preloadSkuList = new ArrayList<>();
        preloadSkuList.add(BillingHelper.SUBSCRIBE_YEAR_TRIAL);
        billingPresenter.loadSubscribeSku(preloadSkuList);
        tvOldPrice.setPaintFlags(tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }


    @Override
    public void onGetSubscribeSku(List<SkuDetails> skuDetailsList) {
        flProgressBar.setVisibility(View.GONE);

        for (SkuDetails sku : skuDetailsList) {
            if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_YEAR_TRIAL)) {

                this.skuDetails = sku;
                float currentPrice = (float) (skuDetails.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));
                tvPrice.setText(String.format("%.2f", currentPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually));
                tvPricePerWeek.setText(String.format("%.2f", currentPrice / 52f) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + App.getContext().getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 75;
                String text = String.format("%.2f", oldPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually);
                tvOldPrice.setText(text);
                tvOldPrice.setPaintFlags(tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                tvAutomaticPay.setText(getResources().getText(R.string.after_trial) + " " + String.format("%.2f", currentPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + getResources().getString(R.string.slash_year) + " " + getResources().getString(R.string.automatically) + " " + getResources().getString(R.string.site_links));
                List<String> list = new ArrayList<>();
                list.add("Privacy Policy");
                list.add("Terms and Conditions");
                list.add("Cancel");
                SpannableString ss = AppInfoHelper.makePrivacyUnderline(tvAutomaticPay.getText(), list);
                tvAutomaticPay.setText(ss);
                tvAutomaticPay.setMovementMethod(LinkMovementMethod.getInstance());
                tvAutomaticPay.setHighlightColor(Color.TRANSPARENT);

            }
        }

    }


    @Override
    public void onBackPressed() {
        return;
    }

    @Override
    protected void onDestroy() {
        billingPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        billingPresenter.handleBillingResult(requestCode, resultCode, data);
    }


    @Override
    public void onGetPurchaseSku(List<SkuDetails> skuDetails) {

    }

    @Override
    public void onErrorBilling(Throwable throwable) {
        if (!BuildConfig.DEBUG) {
            Snackbar.make(flRoot, getString(R.string.error_internet), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getResources().getColor(R.color.white))
                    .setAction(R.string.back, v -> finish())
                    .show();
        }
    }

    @Override
    public void onBuyLoadSuccess() {

    }

    @Override
    public void onPaySuccess(String sku) {
        if (BillingHelper.SUBSCRIBE_YEAR_TRIAL.contains(sku)) {
            App.getCurrentUser().setSuperTrialBuy(true);
            FlurryAnalytics.sendEvent(AnalyticsEvent.TRIAL_OFFER_SCREEN_SUCCESS_BUY_1);
        }
        App.getCurrentUser().save();
        finish();
    }

    @Override
    public void onPayFailed(String sku) {

    }


}