package org.vocrama.pro;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.SkuDetails;
import com.gen.rxbilling.client.RxBillingImpl;
import com.gen.rxbilling.connection.BillingClientFactory;
import com.gen.rxbilling.connection.BillingServiceFactory;
import com.gen.rxbilling.connection.RepeatConnectionTransformer;
import com.gen.rxbilling.flow.RxBillingFlow;
import com.gogamegone.superfileexplorer.BuildConfig;
import com.gogamegone.superfileexplorer.R;
import com.google.android.material.snackbar.Snackbar;

import org.vocrama.AppInfoHelper;
import org.vocrama.analytics.AnalyticsEvent;
import org.vocrama.analytics.FlurryAnalytics;
import org.vocrama.billing.BillingHelper;
import org.vocrama.billing.BillingPresenter;
import org.vocrama.cleanexpert.App;

import java.util.ArrayList;
import java.util.List;


public class OfferActivity extends AppCompatActivity implements BillingPresenter.BillingView {

    private TextView tvOldPrice;
    private TextView tvPrice;
    private TextView tvAutomaticPay;
    private TextView tvWeek;
    private FrameLayout flBack;
    private BillingPresenter billingPresenter;
    private SkuDetails skuDetails;
    private FrameLayout flRoot;
    private FrameLayout flProgressBar;
    private Button buttonBuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_offer);
        findViews();

        if (BuildConfig.DEBUG) {
            flProgressBar.setVisibility(View.GONE);
        }

        billingPresenter = new BillingPresenter(
                this, new RxBillingImpl(
                new BillingClientFactory(
                        this.getApplicationContext(),
                        new RepeatConnectionTransformer<>()
                )
        ),
                new RxBillingFlow(
                        getApplicationContext(),
                        new BillingServiceFactory(
                                this,
                                new RepeatConnectionTransformer<>()
                        )
                )
        );
        billingPresenter.onCreate();
        init();
        FlurryAnalytics.sendEvent(AnalyticsEvent.OFFER_SCREEN_OPEN_0);
    }

    private void findViews() {
        tvOldPrice = findViewById(R.id.tvOldPrice);
        tvPrice = findViewById(R.id.tvPrice);
        tvAutomaticPay = findViewById(R.id.tvAutomaticPay);
        tvWeek = findViewById(R.id.tvWeek);
        flBack = findViewById(R.id.flBack);
        flBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        flRoot = findViewById(R.id.flRoot);
        flProgressBar = findViewById(R.id.flProgressBar);
        buttonBuy = findViewById(R.id.buttonBuy);
        buttonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skuDetails != null) {
                    billingPresenter.buy(skuDetails, OfferActivity.this);
                }
                FlurryAnalytics.sendEvent(AnalyticsEvent.OFFER_SCREEN_CLICKBUY_0);

            }
        });
    }

    private void init() {
        List<String> preloadSkuList = new ArrayList<>();
        preloadSkuList.add(BillingHelper.SUBSCRIBE_OFFER);
        billingPresenter.loadSubscribeSku(preloadSkuList);

    }


    @Override
    public void onGetSubscribeSku(List<SkuDetails> skuDetailsList) {
        flProgressBar.setVisibility(View.GONE);

        for (SkuDetails sku : skuDetailsList) {
            if (sku.getSku().equalsIgnoreCase(BillingHelper.SUBSCRIBE_OFFER)) {
                this.skuDetails = sku;
                float currentPrice = (float) (skuDetails.getPriceAmountMicros() * 1.0f / Math.pow(10, 6));

                tvPrice.setText(String.format("%.2f", currentPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually));
                tvWeek.setText(String.format("%.2f", currentPrice / 52f) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + App.getContext().getResources().getString(R.string.weekly));
                float oldPrice = currentPrice * 100 / 50;
                String text = String.format("%.2f", oldPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + " " + getResources().getString(R.string.annually);
                tvOldPrice.setText(text);
                tvOldPrice.setPaintFlags(tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                tvAutomaticPay.setText(getResources().getString(R.string.after_trial_new) + " " + String.format("%.2f", currentPrice) + " " + skuDetails.getPriceCurrencyCode().toLowerCase() + getResources().getString(R.string.slash_year) + " " + getResources().getString(R.string.automatically) + " " + getResources().getString(R.string.cancel_at_google_play));

                List<String> list = new ArrayList<>();
                list.add("Cancel");
                SpannableString ss = AppInfoHelper.makePrivacyUnderline(tvAutomaticPay.getText(), list);
                tvAutomaticPay.setText(ss);
                tvAutomaticPay.setMovementMethod(LinkMovementMethod.getInstance());
                tvAutomaticPay.setHighlightColor(Color.TRANSPARENT);

            }
        }


    }


    @Override
    public void onGetPurchaseSku(List<SkuDetails> skuDetails) {

    }

    @Override
    public void onErrorBilling(Throwable throwable) {
        if (!BuildConfig.DEBUG) {
            Snackbar.make(flRoot, getString(R.string.error_internet), Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getResources().getColor(R.color.white))
                    .setAction(R.string.back, v -> finish())
                    .show();
        }
    }

    @Override
    public void onBuyLoadSuccess() {

    }

    @Override
    public void onPaySuccess(String sku) {
        if (BillingHelper.SUBSCRIBE_OFFER.contains(sku)) {
            App.getCurrentUser().setOfferBuy(true);
            FlurryAnalytics.sendEvent(AnalyticsEvent.OFFER_SCREEN_SUCCESS_BUY_0);
        }
        App.getCurrentUser().save();
        finish();
    }

    @Override
    public void onPayFailed(String sku) {

    }

    @Override
    protected void onDestroy() {
        billingPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        billingPresenter.handleBillingResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        FlurryAnalytics.sendEvent(AnalyticsEvent.OFFER_SCREEN_CLOSE_0);
        finish();
        return;
    }


}